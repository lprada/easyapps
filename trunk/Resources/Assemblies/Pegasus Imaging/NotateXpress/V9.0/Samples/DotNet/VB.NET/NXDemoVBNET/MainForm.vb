'****************************************************************'
'* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.ImagXpress9
Imports PegasusImaging.WinForms.NotateXpress9
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System
Imports System.Data
Imports System.Drawing
Imports System.Runtime.InteropServices

Public Class MainForm
    Inherits System.Windows.Forms.Form

    <DllImport("user32.dll")> _
    Private Shared Function LoadCursorFromFile(ByVal lpFileName As String) As IntPtr
    End Function
    Private Declare Function GlobalFree Lib "kernel32.dll" (ByVal hMem As IntPtr) As IntPtr

    Private cursor1 As Cursor
    Private cursor2 As Cursor
    Private cursor3 As Cursor

    Private strImageFileName As String

    Private imagX1 As PegasusImaging.WinForms.ImagXpress9.ImageX
    Private ImageXView2 As PegasusImaging.WinForms.ImagXpress9.ImageXView

    Private loIX_loadOptions As PegasusImaging.WinForms.ImagXpress9.LoadOptions
    Private m_saveOptions As PegasusImaging.WinForms.NotateXpress9.SaveOptions
    Private m_loadOptions As PegasusImaging.WinForms.NotateXpress9.LoadOptions

    Private _SavedAnnotationMemoryStream As System.IO.MemoryStream = Nothing
    Private _SavedAnnotationByteArray() As Byte = Nothing

    Private _bAutoSelect As Boolean
    Friend WithEvents mnu_objects_AddNoteAnnotation As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_AddHighlight As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_HighlightToolbar As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_BlockHighlightEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_BlockHighlightVisible As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_NoteToolbar As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_NoteToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_NoteToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_XML As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_saveXML As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_loadXML As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_Custom As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_RectangleCursor As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_RectangleCustom As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_CreatingRectangle As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_MovingRectangle As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_SelectingRectangle As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_EllipseCursor As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_Custom_Ellipse As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_EllipseCreating As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_MovingEllipse As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_EllipseSelecting As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_TextCursor As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_CustomText As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_CreatingText As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_MovingText As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_SelectingText As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_saveXFDF As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_loadXFDF As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_XFDF As System.Windows.Forms.MenuItem
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress9.ImageXView
    Friend WithEvents Processor1 As PegasusImaging.WinForms.ImagXpress9.Processor
    Private _bUpdateMoved As Boolean


#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If (disposing) Then

            ' Don't forget to dispose NXP
            '                
            If Not (NotateXpress1 Is Nothing) Then
                NotateXpress1.ClientDisconnect()
                NotateXpress1.Dispose()
                NotateXpress1 = Nothing
            End If
            ' Don't forget to dispose IX
            '
            If Not (imagX1 Is Nothing) Then

                imagX1.Dispose()
                imagX1 = Nothing
            End If
            If Not (ImageXView1 Is Nothing) Then

                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If
            If Not (ImageXView2 Is Nothing) Then

                ImageXView2.Dispose()
                ImageXView2 = Nothing
            End If

            If Not (Processor1 Is Nothing) Then

                Processor1.Dispose()
                Processor1 = Nothing
            End If
            If Not (ImagXpress1 Is Nothing) Then

                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If
            If Not (components Is Nothing) Then

                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)

    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents NotateXpress1 As PegasusImaging.WinForms.NotateXpress9.NotateXpress
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuLoadSaveAnnType As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ImagingForWindows As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_NotateAnnotations As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ViewDirectorAnnotations As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_file_wangcompatible As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_LoadAnnotations As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_load_tiff As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_SaveAnnotations As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_file_save As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_SaveByteArray As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_SaveMemoryStream As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_UnicodeMode As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_LoadByteArray As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_LoadMemoryStream As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem22 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_fileIResY As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem24 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToggleAllowPaint As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem26 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_file_exit As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_create As System.Windows.Forms.MenuItem
    Friend WithEvents SeparatorLayers1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_setcurrent As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_deletecurrent As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_togglehide As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_DeactivateCurrentLayer As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_setname As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_setdescription As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_reverse As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_SendToBack As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_BringToFront As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_DeleteSelected As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_SaveCurrentLayer As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_toggletoolbar As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_DisableToolbar As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_autoselect As System.Windows.Forms.MenuItem
    Friend WithEvents Separator As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_create10 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_rects As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_buttons As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_createtransrect As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_createImage As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_nudgeup As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_nudgedn As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_inflate As System.Windows.Forms.MenuItem
    Friend WithEvents Separator2 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_redaction As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_AddRulerAnn As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_largetext As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_useMLE As System.Windows.Forms.MenuItem
    Friend WithEvents Separator3 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_brand As System.Windows.Forms.MenuItem
    Friend WithEvents Separator4 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_CreateNewNX8 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_orient As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_orient_ltbr As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_orient_brtl As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_orient_rblt As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_orient_tlbr As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_orientation_rotate180 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_BoundingRectangle As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem75 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_settext As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_backcolor As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_pencolor As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_penwidth As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_bevelshadow As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_bevellight As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_setfont As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_getitemtext As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_hb As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_hf As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_moveable As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_sizeable As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_getitemfont As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_setnewdib As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_mode As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_mode_editmode As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_mode_interactive As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_cm As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_cm_enable As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_Groups As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_allowgrouping As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem97 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_CreateGroup As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_DeleteGroup As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_SetGroupEmpty As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_SelectGroupItems As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_GroupAddSelectedItems As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_GroupRemoveSelectedItems As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_MirrorYoked As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_InvertYoked As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_SetGroupUserString As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToolBar As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToolbarRulerTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_RulerToolEnabeled As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_RulerToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToolbarFreehandTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_FreehandToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_FreehandToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToolbarEllipseTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_EllipseToolEnabeld As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_EllipseToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToolbarTextTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_TextToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_TextToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToolbarRectangleTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_RectangleToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_RectangleToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToolbarStampTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_StampToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_StampToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToolbarPolylineTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_PolylineToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_PolylineToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToolbarLineTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_LineToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_LineToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToolbarImageTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ImageToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ImageToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToolbarButtonTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ButtonToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ButtonToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_ToolbarPolygonTool As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_PolygonToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_PolygonToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents mnu_stuff_toggleuserdraw As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_Help As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_AboutIX As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_AboutNX As System.Windows.Forms.MenuItem
    Friend WithEvents OutputList As System.Windows.Forms.ListBox
    Friend WithEvents lblEvents As System.Windows.Forms.Label
    Public WithEvents ZoomOut As System.Windows.Forms.Button
    Public WithEvents ZoomIn As System.Windows.Forms.Button
    Private WithEvents mnu_YokeGroupItems As System.Windows.Forms.MenuItem
    Friend WithEvents FontDialog1 As System.Windows.Forms.FontDialog
    Friend WithEvents mnu_file_saveann2fileNXP As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_file_saveann2fileANN As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_file_loadannotationNXP As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_file_loadannotationANN As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_fixed As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_SelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents SeparatorLayers2 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.NotateXpress1 = New PegasusImaging.WinForms.NotateXpress9.NotateXpress(Me.components)
        Me.lblLastError = New System.Windows.Forms.Label
        Me.lblerror = New System.Windows.Forms.Label
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.mnuFile = New System.Windows.Forms.MenuItem
        Me.mnuLoadSaveAnnType = New System.Windows.Forms.MenuItem
        Me.mnu_ImagingForWindows = New System.Windows.Forms.MenuItem
        Me.mnu_NotateAnnotations = New System.Windows.Forms.MenuItem
        Me.mnu_ViewDirectorAnnotations = New System.Windows.Forms.MenuItem
        Me.mnu_XML = New System.Windows.Forms.MenuItem
        Me.mnu_XFDF = New System.Windows.Forms.MenuItem
        Me.mnu_file_wangcompatible = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.mnu_LoadAnnotations = New System.Windows.Forms.MenuItem
        Me.mnu_load_tiff = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.mnu_SaveAnnotations = New System.Windows.Forms.MenuItem
        Me.mnu_file_save = New System.Windows.Forms.MenuItem
        Me.MenuItem13 = New System.Windows.Forms.MenuItem
        Me.mnu_file_saveann2fileNXP = New System.Windows.Forms.MenuItem
        Me.mnu_file_saveann2fileANN = New System.Windows.Forms.MenuItem
        Me.mnu_saveXML = New System.Windows.Forms.MenuItem
        Me.mnu_saveXFDF = New System.Windows.Forms.MenuItem
        Me.mnu_SaveByteArray = New System.Windows.Forms.MenuItem
        Me.mnu_SaveMemoryStream = New System.Windows.Forms.MenuItem
        Me.MenuItem17 = New System.Windows.Forms.MenuItem
        Me.mnu_UnicodeMode = New System.Windows.Forms.MenuItem
        Me.mnu_file_loadannotationNXP = New System.Windows.Forms.MenuItem
        Me.mnu_file_loadannotationANN = New System.Windows.Forms.MenuItem
        Me.mnu_loadXML = New System.Windows.Forms.MenuItem
        Me.mnu_loadXFDF = New System.Windows.Forms.MenuItem
        Me.mnu_LoadByteArray = New System.Windows.Forms.MenuItem
        Me.mnu_LoadMemoryStream = New System.Windows.Forms.MenuItem
        Me.MenuItem22 = New System.Windows.Forms.MenuItem
        Me.mnu_fileIResY = New System.Windows.Forms.MenuItem
        Me.MenuItem24 = New System.Windows.Forms.MenuItem
        Me.mnu_ToggleAllowPaint = New System.Windows.Forms.MenuItem
        Me.MenuItem26 = New System.Windows.Forms.MenuItem
        Me.mnu_file_exit = New System.Windows.Forms.MenuItem
        Me.mnu_layers = New System.Windows.Forms.MenuItem
        Me.mnu_layers_create = New System.Windows.Forms.MenuItem
        Me.SeparatorLayers1 = New System.Windows.Forms.MenuItem
        Me.mnu_layers_setcurrent = New System.Windows.Forms.MenuItem
        Me.mnu_layers_deletecurrent = New System.Windows.Forms.MenuItem
        Me.mnu_layers_togglehide = New System.Windows.Forms.MenuItem
        Me.mnu_DeactivateCurrentLayer = New System.Windows.Forms.MenuItem
        Me.mnu_layers_setname = New System.Windows.Forms.MenuItem
        Me.mnu_layers_setdescription = New System.Windows.Forms.MenuItem
        Me.mnu_objects_reverse = New System.Windows.Forms.MenuItem
        Me.mnu_SendToBack = New System.Windows.Forms.MenuItem
        Me.mnu_BringToFront = New System.Windows.Forms.MenuItem
        Me.mnu_DeleteSelected = New System.Windows.Forms.MenuItem
        Me.mnu_SaveCurrentLayer = New System.Windows.Forms.MenuItem
        Me.mnu_SelectAll = New System.Windows.Forms.MenuItem
        Me.SeparatorLayers2 = New System.Windows.Forms.MenuItem
        Me.mnu_layers_toggletoolbar = New System.Windows.Forms.MenuItem
        Me.mnu_DisableToolbar = New System.Windows.Forms.MenuItem
        Me.mnu_objects = New System.Windows.Forms.MenuItem
        Me.mnu_objects_autoselect = New System.Windows.Forms.MenuItem
        Me.Separator = New System.Windows.Forms.MenuItem
        Me.mnu_objects_create10 = New System.Windows.Forms.MenuItem
        Me.mnu_objects_rects = New System.Windows.Forms.MenuItem
        Me.mnu_objects_buttons = New System.Windows.Forms.MenuItem
        Me.mnu_objects_createtransrect = New System.Windows.Forms.MenuItem
        Me.mnu_objects_createImage = New System.Windows.Forms.MenuItem
        Me.mnu_objects_nudgeup = New System.Windows.Forms.MenuItem
        Me.mnu_objects_nudgedn = New System.Windows.Forms.MenuItem
        Me.mnu_objects_inflate = New System.Windows.Forms.MenuItem
        Me.Separator2 = New System.Windows.Forms.MenuItem
        Me.mnu_objects_redaction = New System.Windows.Forms.MenuItem
        Me.mnu_objects_AddRulerAnn = New System.Windows.Forms.MenuItem
        Me.mnu_objects_largetext = New System.Windows.Forms.MenuItem
        Me.mnu_objects_AddNoteAnnotation = New System.Windows.Forms.MenuItem
        Me.mnu_objects_AddHighlight = New System.Windows.Forms.MenuItem
        Me.mnu_objects_useMLE = New System.Windows.Forms.MenuItem
        Me.Separator3 = New System.Windows.Forms.MenuItem
        Me.mnu_objects_brand = New System.Windows.Forms.MenuItem
        Me.Separator4 = New System.Windows.Forms.MenuItem
        Me.mnu_CreateNewNX8 = New System.Windows.Forms.MenuItem
        Me.mnu_orient = New System.Windows.Forms.MenuItem
        Me.mnu_orient_ltbr = New System.Windows.Forms.MenuItem
        Me.mnu_orient_brtl = New System.Windows.Forms.MenuItem
        Me.mnu_orient_rblt = New System.Windows.Forms.MenuItem
        Me.mnu_orient_tlbr = New System.Windows.Forms.MenuItem
        Me.mnu_orientation_rotate180 = New System.Windows.Forms.MenuItem
        Me.mnu_stuff = New System.Windows.Forms.MenuItem
        Me.mnu_BoundingRectangle = New System.Windows.Forms.MenuItem
        Me.MenuItem75 = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_settext = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_backcolor = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_pencolor = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_penwidth = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_bevelshadow = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_bevellight = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_setfont = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_getitemtext = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_hb = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_hf = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_moveable = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_fixed = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_sizeable = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_getitemfont = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_setnewdib = New System.Windows.Forms.MenuItem
        Me.mnu_stuff_toggleuserdraw = New System.Windows.Forms.MenuItem
        Me.mnu_mode = New System.Windows.Forms.MenuItem
        Me.mnu_mode_editmode = New System.Windows.Forms.MenuItem
        Me.mnu_mode_interactive = New System.Windows.Forms.MenuItem
        Me.mnu_cm = New System.Windows.Forms.MenuItem
        Me.mnu_cm_enable = New System.Windows.Forms.MenuItem
        Me.mnu_Custom = New System.Windows.Forms.MenuItem
        Me.mnu_RectangleCursor = New System.Windows.Forms.MenuItem
        Me.mnu_RectangleCustom = New System.Windows.Forms.MenuItem
        Me.mnu_CreatingRectangle = New System.Windows.Forms.MenuItem
        Me.mnu_MovingRectangle = New System.Windows.Forms.MenuItem
        Me.mnu_SelectingRectangle = New System.Windows.Forms.MenuItem
        Me.mnu_EllipseCursor = New System.Windows.Forms.MenuItem
        Me.mnu_Custom_Ellipse = New System.Windows.Forms.MenuItem
        Me.mnu_EllipseCreating = New System.Windows.Forms.MenuItem
        Me.mnu_MovingEllipse = New System.Windows.Forms.MenuItem
        Me.mnu_EllipseSelecting = New System.Windows.Forms.MenuItem
        Me.mnu_TextCursor = New System.Windows.Forms.MenuItem
        Me.mnu_CustomText = New System.Windows.Forms.MenuItem
        Me.mnu_CreatingText = New System.Windows.Forms.MenuItem
        Me.mnu_MovingText = New System.Windows.Forms.MenuItem
        Me.mnu_SelectingText = New System.Windows.Forms.MenuItem
        Me.mnu_Groups = New System.Windows.Forms.MenuItem
        Me.mnu_objects_allowgrouping = New System.Windows.Forms.MenuItem
        Me.MenuItem97 = New System.Windows.Forms.MenuItem
        Me.mnu_CreateGroup = New System.Windows.Forms.MenuItem
        Me.mnu_DeleteGroup = New System.Windows.Forms.MenuItem
        Me.mnu_SetGroupEmpty = New System.Windows.Forms.MenuItem
        Me.mnu_SelectGroupItems = New System.Windows.Forms.MenuItem
        Me.mnu_GroupAddSelectedItems = New System.Windows.Forms.MenuItem
        Me.mnu_GroupRemoveSelectedItems = New System.Windows.Forms.MenuItem
        Me.mnu_YokeGroupItems = New System.Windows.Forms.MenuItem
        Me.mnu_MirrorYoked = New System.Windows.Forms.MenuItem
        Me.mnu_InvertYoked = New System.Windows.Forms.MenuItem
        Me.mnu_SetGroupUserString = New System.Windows.Forms.MenuItem
        Me.mnu_ToolBar = New System.Windows.Forms.MenuItem
        Me.mnu_ToolbarRulerTool = New System.Windows.Forms.MenuItem
        Me.mnu_RulerToolEnabeled = New System.Windows.Forms.MenuItem
        Me.mnu_RulerToolVisible = New System.Windows.Forms.MenuItem
        Me.mnu_ToolbarFreehandTool = New System.Windows.Forms.MenuItem
        Me.mnu_FreehandToolEnabled = New System.Windows.Forms.MenuItem
        Me.mnu_FreehandToolVisible = New System.Windows.Forms.MenuItem
        Me.mnu_ToolbarEllipseTool = New System.Windows.Forms.MenuItem
        Me.mnu_EllipseToolEnabeld = New System.Windows.Forms.MenuItem
        Me.mnu_EllipseToolVisible = New System.Windows.Forms.MenuItem
        Me.mnu_ToolbarTextTool = New System.Windows.Forms.MenuItem
        Me.mnu_TextToolEnabled = New System.Windows.Forms.MenuItem
        Me.mnu_TextToolVisible = New System.Windows.Forms.MenuItem
        Me.mnu_ToolbarRectangleTool = New System.Windows.Forms.MenuItem
        Me.mnu_RectangleToolEnabled = New System.Windows.Forms.MenuItem
        Me.mnu_RectangleToolVisible = New System.Windows.Forms.MenuItem
        Me.mnu_ToolbarStampTool = New System.Windows.Forms.MenuItem
        Me.mnu_StampToolEnabled = New System.Windows.Forms.MenuItem
        Me.mnu_StampToolVisible = New System.Windows.Forms.MenuItem
        Me.mnu_ToolbarPolylineTool = New System.Windows.Forms.MenuItem
        Me.mnu_PolylineToolEnabled = New System.Windows.Forms.MenuItem
        Me.mnu_PolylineToolVisible = New System.Windows.Forms.MenuItem
        Me.mnu_ToolbarLineTool = New System.Windows.Forms.MenuItem
        Me.mnu_LineToolEnabled = New System.Windows.Forms.MenuItem
        Me.mnu_LineToolVisible = New System.Windows.Forms.MenuItem
        Me.mnu_ToolbarImageTool = New System.Windows.Forms.MenuItem
        Me.mnu_ImageToolEnabled = New System.Windows.Forms.MenuItem
        Me.mnu_ImageToolVisible = New System.Windows.Forms.MenuItem
        Me.mnu_ToolbarButtonTool = New System.Windows.Forms.MenuItem
        Me.mnu_ButtonToolEnabled = New System.Windows.Forms.MenuItem
        Me.mnu_ButtonToolVisible = New System.Windows.Forms.MenuItem
        Me.mnu_ToolbarPolygonTool = New System.Windows.Forms.MenuItem
        Me.mnu_PolygonToolEnabled = New System.Windows.Forms.MenuItem
        Me.mnu_PolygonToolVisible = New System.Windows.Forms.MenuItem
        Me.mnu_HighlightToolbar = New System.Windows.Forms.MenuItem
        Me.mnu_BlockHighlightEnabled = New System.Windows.Forms.MenuItem
        Me.mnu_BlockHighlightVisible = New System.Windows.Forms.MenuItem
        Me.mnu_NoteToolbar = New System.Windows.Forms.MenuItem
        Me.mnu_NoteToolEnabled = New System.Windows.Forms.MenuItem
        Me.mnu_NoteToolVisible = New System.Windows.Forms.MenuItem
        Me.mnu_Help = New System.Windows.Forms.MenuItem
        Me.mnu_AboutIX = New System.Windows.Forms.MenuItem
        Me.mnu_AboutNX = New System.Windows.Forms.MenuItem
        Me.lstInfo = New System.Windows.Forms.ListBox
        Me.OutputList = New System.Windows.Forms.ListBox
        Me.lblEvents = New System.Windows.Forms.Label
        Me.ZoomOut = New System.Windows.Forms.Button
        Me.ZoomIn = New System.Windows.Forms.Button
        Me.FontDialog1 = New System.Windows.Forms.FontDialog
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress9.ImagXpress(Me.components)
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(Me.components)
        Me.Processor1 = New PegasusImaging.WinForms.ImagXpress9.Processor(Me.components)
        Me.SuspendLayout()
        '
        'NotateXpress1
        '
        Me.NotateXpress1.AllowPaint = True
        Me.NotateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress9.FontScaling.Normal
        Me.NotateXpress1.ImagXpressLoad = True
        Me.NotateXpress1.ImagXpressSave = True
        Me.NotateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress9.AnnotationMode.Edit
        Me.NotateXpress1.LineScaling = PegasusImaging.WinForms.NotateXpress9.LineScaling.Normal
        Me.NotateXpress1.MultiLineEdit = False
        Me.NotateXpress1.RecalibrateXdpi = -1
        Me.NotateXpress1.RecalibrateYdpi = -1
        Me.NotateXpress1.ToolTipTimeEdit = 0
        Me.NotateXpress1.ToolTipTimeInteractive = 0
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLastError.Location = New System.Drawing.Point(560, 310)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(216, 32)
        Me.lblLastError.TabIndex = 7
        Me.lblLastError.Text = "Last Error:"
        '
        'lblerror
        '
        Me.lblerror.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblerror.Location = New System.Drawing.Point(560, 350)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(224, 128)
        Me.lblerror.TabIndex = 6
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnu_layers, Me.mnu_objects, Me.mnu_orient, Me.mnu_stuff, Me.mnu_mode, Me.mnu_cm, Me.mnu_Custom, Me.mnu_Groups, Me.mnu_ToolBar, Me.mnu_Help})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuLoadSaveAnnType, Me.mnu_file_wangcompatible, Me.MenuItem7, Me.mnu_LoadAnnotations, Me.mnu_load_tiff, Me.MenuItem10, Me.mnu_SaveAnnotations, Me.mnu_file_save, Me.MenuItem13, Me.mnu_file_saveann2fileNXP, Me.mnu_file_saveann2fileANN, Me.mnu_saveXML, Me.mnu_saveXFDF, Me.mnu_SaveByteArray, Me.mnu_SaveMemoryStream, Me.MenuItem17, Me.mnu_UnicodeMode, Me.mnu_file_loadannotationNXP, Me.mnu_file_loadannotationANN, Me.mnu_loadXML, Me.mnu_loadXFDF, Me.mnu_LoadByteArray, Me.mnu_LoadMemoryStream, Me.MenuItem22, Me.mnu_fileIResY, Me.MenuItem24, Me.mnu_ToggleAllowPaint, Me.MenuItem26, Me.mnu_file_exit})
        Me.mnuFile.Text = "File"
        '
        'mnuLoadSaveAnnType
        '
        Me.mnuLoadSaveAnnType.Index = 0
        Me.mnuLoadSaveAnnType.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_ImagingForWindows, Me.mnu_NotateAnnotations, Me.mnu_ViewDirectorAnnotations, Me.mnu_XML, Me.mnu_XFDF})
        Me.mnuLoadSaveAnnType.Text = "Load/Save Annotation Type"
        '
        'mnu_ImagingForWindows
        '
        Me.mnu_ImagingForWindows.Index = 0
        Me.mnu_ImagingForWindows.Text = "Imaging For Windows (Wang)"
        '
        'mnu_NotateAnnotations
        '
        Me.mnu_NotateAnnotations.Index = 1
        Me.mnu_NotateAnnotations.Text = "NotateXpress (NXP)"
        '
        'mnu_ViewDirectorAnnotations
        '
        Me.mnu_ViewDirectorAnnotations.Index = 2
        Me.mnu_ViewDirectorAnnotations.Text = "ViewDirector - TMSSequoia (ANN)"
        '
        'mnu_XML
        '
        Me.mnu_XML.Index = 3
        Me.mnu_XML.Text = "NotateXpressXML (XML)"
        '
        'mnu_XFDF
        '
        Me.mnu_XFDF.Index = 4
        Me.mnu_XFDF.Text = "XFDF"
        '
        'mnu_file_wangcompatible
        '
        Me.mnu_file_wangcompatible.Checked = True
        Me.mnu_file_wangcompatible.Index = 1
        Me.mnu_file_wangcompatible.Text = "Toggle Load/Save Preserve Wang"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "-"
        '
        'mnu_LoadAnnotations
        '
        Me.mnu_LoadAnnotations.Index = 3
        Me.mnu_LoadAnnotations.Text = "Toggle Load Annotations thru IX"
        '
        'mnu_load_tiff
        '
        Me.mnu_load_tiff.Index = 4
        Me.mnu_load_tiff.Text = "Load Image..."
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 5
        Me.MenuItem10.Text = "-"
        '
        'mnu_SaveAnnotations
        '
        Me.mnu_SaveAnnotations.Index = 6
        Me.mnu_SaveAnnotations.Text = "Toggle Save Annotations thru IX"
        '
        'mnu_file_save
        '
        Me.mnu_file_save.Index = 7
        Me.mnu_file_save.Text = "Save Image..."
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 8
        Me.MenuItem13.Text = "-"
        '
        'mnu_file_saveann2fileNXP
        '
        Me.mnu_file_saveann2fileNXP.Enabled = False
        Me.mnu_file_saveann2fileNXP.Index = 9
        Me.mnu_file_saveann2fileNXP.Text = "Save annotation to file(NXP)"
        '
        'mnu_file_saveann2fileANN
        '
        Me.mnu_file_saveann2fileANN.Enabled = False
        Me.mnu_file_saveann2fileANN.Index = 10
        Me.mnu_file_saveann2fileANN.Text = "Save annotation to file(ANN)"
        '
        'mnu_saveXML
        '
        Me.mnu_saveXML.Enabled = False
        Me.mnu_saveXML.Index = 11
        Me.mnu_saveXML.Text = "Save annotation to file (XML)"
        '
        'mnu_saveXFDF
        '
        Me.mnu_saveXFDF.Enabled = False
        Me.mnu_saveXFDF.Index = 12
        Me.mnu_saveXFDF.Text = "Save Annotation to File (XFDF)"
        '
        'mnu_SaveByteArray
        '
        Me.mnu_SaveByteArray.Index = 13
        Me.mnu_SaveByteArray.Text = "Save annotation to ByteArray"
        '
        'mnu_SaveMemoryStream
        '
        Me.mnu_SaveMemoryStream.Index = 14
        Me.mnu_SaveMemoryStream.Text = "Save annotation to MemoryStream"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 15
        Me.MenuItem17.Text = "-"
        '
        'mnu_UnicodeMode
        '
        Me.mnu_UnicodeMode.Checked = True
        Me.mnu_UnicodeMode.Index = 16
        Me.mnu_UnicodeMode.Text = "Toggle Load in Unicode Mode"
        '
        'mnu_file_loadannotationNXP
        '
        Me.mnu_file_loadannotationNXP.Enabled = False
        Me.mnu_file_loadannotationNXP.Index = 17
        Me.mnu_file_loadannotationNXP.Text = "Load Annotation from file(NXP)"
        '
        'mnu_file_loadannotationANN
        '
        Me.mnu_file_loadannotationANN.Enabled = False
        Me.mnu_file_loadannotationANN.Index = 18
        Me.mnu_file_loadannotationANN.Text = "Load Annotation from file(ANN)"
        '
        'mnu_loadXML
        '
        Me.mnu_loadXML.Enabled = False
        Me.mnu_loadXML.Index = 19
        Me.mnu_loadXML.Text = "Load Annotation from file (XML)"
        '
        'mnu_loadXFDF
        '
        Me.mnu_loadXFDF.Enabled = False
        Me.mnu_loadXFDF.Index = 20
        Me.mnu_loadXFDF.Text = "Load Annotation from File (XFDF)"
        '
        'mnu_LoadByteArray
        '
        Me.mnu_LoadByteArray.Index = 21
        Me.mnu_LoadByteArray.Text = "Load Annotation from ByteArray"
        '
        'mnu_LoadMemoryStream
        '
        Me.mnu_LoadMemoryStream.Index = 22
        Me.mnu_LoadMemoryStream.Text = "Load Annotation from MemoryStream"
        '
        'MenuItem22
        '
        Me.MenuItem22.Index = 23
        Me.MenuItem22.Text = "-"
        '
        'mnu_fileIResY
        '
        Me.mnu_fileIResY.Index = 24
        Me.mnu_fileIResY.Text = "IResY FontScaling"
        '
        'MenuItem24
        '
        Me.MenuItem24.Index = 25
        Me.MenuItem24.Text = "-"
        '
        'mnu_ToggleAllowPaint
        '
        Me.mnu_ToggleAllowPaint.Index = 26
        Me.mnu_ToggleAllowPaint.Text = "Toggle Allow Paint"
        '
        'MenuItem26
        '
        Me.MenuItem26.Index = 27
        Me.MenuItem26.Text = "-"
        '
        'mnu_file_exit
        '
        Me.mnu_file_exit.Index = 28
        Me.mnu_file_exit.Text = "E&xit"
        '
        'mnu_layers
        '
        Me.mnu_layers.Index = 1
        Me.mnu_layers.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_layers_create, Me.SeparatorLayers1, Me.mnu_layers_setcurrent, Me.mnu_layers_deletecurrent, Me.mnu_layers_togglehide, Me.mnu_DeactivateCurrentLayer, Me.mnu_layers_setname, Me.mnu_layers_setdescription, Me.mnu_objects_reverse, Me.mnu_SendToBack, Me.mnu_BringToFront, Me.mnu_DeleteSelected, Me.mnu_SaveCurrentLayer, Me.mnu_SelectAll, Me.SeparatorLayers2, Me.mnu_layers_toggletoolbar, Me.mnu_DisableToolbar})
        Me.mnu_layers.Text = "Layers"
        '
        'mnu_layers_create
        '
        Me.mnu_layers_create.Index = 0
        Me.mnu_layers_create.Text = "Create..."
        '
        'SeparatorLayers1
        '
        Me.SeparatorLayers1.Index = 1
        Me.SeparatorLayers1.Text = "-"
        '
        'mnu_layers_setcurrent
        '
        Me.mnu_layers_setcurrent.Index = 2
        Me.mnu_layers_setcurrent.Text = "Set current"
        '
        'mnu_layers_deletecurrent
        '
        Me.mnu_layers_deletecurrent.Index = 3
        Me.mnu_layers_deletecurrent.Text = "Delete current"
        '
        'mnu_layers_togglehide
        '
        Me.mnu_layers_togglehide.Index = 4
        Me.mnu_layers_togglehide.Text = "Hide current"
        '
        'mnu_DeactivateCurrentLayer
        '
        Me.mnu_DeactivateCurrentLayer.Index = 5
        Me.mnu_DeactivateCurrentLayer.Text = "Deactivate current"
        '
        'mnu_layers_setname
        '
        Me.mnu_layers_setname.Index = 6
        Me.mnu_layers_setname.Text = "Current name..."
        '
        'mnu_layers_setdescription
        '
        Me.mnu_layers_setdescription.Index = 7
        Me.mnu_layers_setdescription.Text = "Current description..."
        '
        'mnu_objects_reverse
        '
        Me.mnu_objects_reverse.Index = 8
        Me.mnu_objects_reverse.Text = "Reverse current ZOrder"
        '
        'mnu_SendToBack
        '
        Me.mnu_SendToBack.Index = 9
        Me.mnu_SendToBack.Text = "Current selected To Back of ZOrder"
        '
        'mnu_BringToFront
        '
        Me.mnu_BringToFront.Index = 10
        Me.mnu_BringToFront.Text = "Current selected To Front of ZOrder"
        '
        'mnu_DeleteSelected
        '
        Me.mnu_DeleteSelected.Index = 11
        Me.mnu_DeleteSelected.Text = "Current delete Selected"
        '
        'mnu_SaveCurrentLayer
        '
        Me.mnu_SaveCurrentLayer.Index = 12
        Me.mnu_SaveCurrentLayer.Text = "Save Current..."
        '
        'mnu_SelectAll
        '
        Me.mnu_SelectAll.Index = 13
        Me.mnu_SelectAll.Text = "Toggle Current SelectAll"
        '
        'SeparatorLayers2
        '
        Me.SeparatorLayers2.Index = 14
        Me.SeparatorLayers2.Text = "-"
        '
        'mnu_layers_toggletoolbar
        '
        Me.mnu_layers_toggletoolbar.Index = 15
        Me.mnu_layers_toggletoolbar.Text = "Toggle toolbar visibility"
        '
        'mnu_DisableToolbar
        '
        Me.mnu_DisableToolbar.Index = 16
        Me.mnu_DisableToolbar.Text = "Disable toolbar"
        '
        'mnu_objects
        '
        Me.mnu_objects.Index = 2
        Me.mnu_objects.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_objects_autoselect, Me.Separator, Me.mnu_objects_create10, Me.mnu_objects_rects, Me.mnu_objects_buttons, Me.mnu_objects_createtransrect, Me.mnu_objects_createImage, Me.mnu_objects_nudgeup, Me.mnu_objects_nudgedn, Me.mnu_objects_inflate, Me.Separator2, Me.mnu_objects_redaction, Me.mnu_objects_AddRulerAnn, Me.mnu_objects_largetext, Me.mnu_objects_AddNoteAnnotation, Me.mnu_objects_AddHighlight, Me.mnu_objects_useMLE, Me.Separator3, Me.mnu_objects_brand, Me.Separator4, Me.mnu_CreateNewNX8})
        Me.mnu_objects.Text = "Objects"
        '
        'mnu_objects_autoselect
        '
        Me.mnu_objects_autoselect.Checked = True
        Me.mnu_objects_autoselect.Index = 0
        Me.mnu_objects_autoselect.Text = "Auto-select when created"
        '
        'Separator
        '
        Me.Separator.Index = 1
        Me.Separator.Text = "-"
        '
        'mnu_objects_create10
        '
        Me.mnu_objects_create10.Index = 2
        Me.mnu_objects_create10.Text = "Create 10 objects"
        '
        'mnu_objects_rects
        '
        Me.mnu_objects_rects.Index = 3
        Me.mnu_objects_rects.Text = "Create 10 rects"
        '
        'mnu_objects_buttons
        '
        Me.mnu_objects_buttons.Index = 4
        Me.mnu_objects_buttons.Text = "Create 10 buttons"
        '
        'mnu_objects_createtransrect
        '
        Me.mnu_objects_createtransrect.Index = 5
        Me.mnu_objects_createtransrect.Text = "Create translucent rectangle"
        '
        'mnu_objects_createImage
        '
        Me.mnu_objects_createImage.Index = 6
        Me.mnu_objects_createImage.Text = "Create an Image object"
        '
        'mnu_objects_nudgeup
        '
        Me.mnu_objects_nudgeup.Index = 7
        Me.mnu_objects_nudgeup.Text = "Nudge up"
        '
        'mnu_objects_nudgedn
        '
        Me.mnu_objects_nudgedn.Index = 8
        Me.mnu_objects_nudgedn.Text = "Nudge dn"
        '
        'mnu_objects_inflate
        '
        Me.mnu_objects_inflate.Index = 9
        Me.mnu_objects_inflate.Text = "Inflate me"
        '
        'Separator2
        '
        Me.Separator2.Index = 10
        Me.Separator2.Text = "-"
        '
        'mnu_objects_redaction
        '
        Me.mnu_objects_redaction.Index = 11
        Me.mnu_objects_redaction.Text = "Redaction"
        '
        'mnu_objects_AddRulerAnn
        '
        Me.mnu_objects_AddRulerAnn.Index = 12
        Me.mnu_objects_AddRulerAnn.Text = "Add Ruler Annotation"
        '
        'mnu_objects_largetext
        '
        Me.mnu_objects_largetext.Index = 13
        Me.mnu_objects_largetext.Text = "Add large text object"
        '
        'mnu_objects_AddNoteAnnotation
        '
        Me.mnu_objects_AddNoteAnnotation.Index = 14
        Me.mnu_objects_AddNoteAnnotation.Text = "Add Note Annotation (New in v9)"
        '
        'mnu_objects_AddHighlight
        '
        Me.mnu_objects_AddHighlight.Index = 15
        Me.mnu_objects_AddHighlight.Text = "Add Block Highlight (New in v9)"
        '
        'mnu_objects_useMLE
        '
        Me.mnu_objects_useMLE.Index = 16
        Me.mnu_objects_useMLE.Text = "Use MLE to edit existing text"
        '
        'Separator3
        '
        Me.Separator3.Index = 17
        Me.Separator3.Text = "-"
        '
        'mnu_objects_brand
        '
        Me.mnu_objects_brand.Index = 18
        Me.mnu_objects_brand.Text = "Brand"
        '
        'Separator4
        '
        Me.Separator4.Index = 19
        Me.Separator4.Text = "-"
        '
        'mnu_CreateNewNX8
        '
        Me.mnu_CreateNewNX8.Index = 20
        Me.mnu_CreateNewNX8.Text = "Create new NX8 component"
        '
        'mnu_orient
        '
        Me.mnu_orient.Index = 3
        Me.mnu_orient.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_orient_ltbr, Me.mnu_orient_brtl, Me.mnu_orient_rblt, Me.mnu_orient_tlbr, Me.mnu_orientation_rotate180})
        Me.mnu_orient.Text = "Orientation"
        '
        'mnu_orient_ltbr
        '
        Me.mnu_orient_ltbr.Index = 0
        Me.mnu_orient_ltbr.Text = "Rotate 90 clockwise"
        '
        'mnu_orient_brtl
        '
        Me.mnu_orient_brtl.Index = 1
        Me.mnu_orient_brtl.Text = "Invert"
        '
        'mnu_orient_rblt
        '
        Me.mnu_orient_rblt.Index = 2
        Me.mnu_orient_rblt.Text = "Rotate 90 counterclockwise"
        '
        'mnu_orient_tlbr
        '
        Me.mnu_orient_tlbr.Index = 3
        Me.mnu_orient_tlbr.Text = "Mirror"
        '
        'mnu_orientation_rotate180
        '
        Me.mnu_orientation_rotate180.Index = 4
        Me.mnu_orientation_rotate180.Text = "Rotate 180"
        '
        'mnu_stuff
        '
        Me.mnu_stuff.Index = 4
        Me.mnu_stuff.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_BoundingRectangle, Me.MenuItem75, Me.mnu_stuff_settext, Me.mnu_stuff_backcolor, Me.mnu_stuff_pencolor, Me.mnu_stuff_penwidth, Me.mnu_stuff_bevelshadow, Me.mnu_stuff_bevellight, Me.mnu_stuff_setfont, Me.mnu_stuff_getitemtext, Me.mnu_stuff_hb, Me.mnu_stuff_hf, Me.mnu_stuff_moveable, Me.mnu_stuff_fixed, Me.mnu_stuff_sizeable, Me.mnu_stuff_getitemfont, Me.mnu_stuff_setnewdib, Me.mnu_stuff_toggleuserdraw})
        Me.mnu_stuff.Text = "Stuff"
        '
        'mnu_BoundingRectangle
        '
        Me.mnu_BoundingRectangle.Index = 0
        Me.mnu_BoundingRectangle.Text = "Bounding Rectangle..."
        '
        'MenuItem75
        '
        Me.MenuItem75.Index = 1
        Me.MenuItem75.Text = "-"
        '
        'mnu_stuff_settext
        '
        Me.mnu_stuff_settext.Index = 2
        Me.mnu_stuff_settext.Text = "Set item text"
        '
        'mnu_stuff_backcolor
        '
        Me.mnu_stuff_backcolor.Index = 3
        Me.mnu_stuff_backcolor.Text = "Set item backcolor"
        '
        'mnu_stuff_pencolor
        '
        Me.mnu_stuff_pencolor.Index = 4
        Me.mnu_stuff_pencolor.Text = "Set item pen color"
        '
        'mnu_stuff_penwidth
        '
        Me.mnu_stuff_penwidth.Index = 5
        Me.mnu_stuff_penwidth.Text = "Set item pen width"
        '
        'mnu_stuff_bevelshadow
        '
        Me.mnu_stuff_bevelshadow.Index = 6
        Me.mnu_stuff_bevelshadow.Text = "Set item bevelshadowcolor"
        '
        'mnu_stuff_bevellight
        '
        Me.mnu_stuff_bevellight.Index = 7
        Me.mnu_stuff_bevellight.Text = "Set item bevellightcolor"
        '
        'mnu_stuff_setfont
        '
        Me.mnu_stuff_setfont.Index = 8
        Me.mnu_stuff_setfont.Text = "Set item font..."
        '
        'mnu_stuff_getitemtext
        '
        Me.mnu_stuff_getitemtext.Index = 9
        Me.mnu_stuff_getitemtext.Text = "Get item text"
        '
        'mnu_stuff_hb
        '
        Me.mnu_stuff_hb.Index = 10
        Me.mnu_stuff_hb.Text = "Toggle HighlightBack"
        '
        'mnu_stuff_hf
        '
        Me.mnu_stuff_hf.Index = 11
        Me.mnu_stuff_hf.Text = "Toggle HighlightFill"
        '
        'mnu_stuff_moveable
        '
        Me.mnu_stuff_moveable.Index = 12
        Me.mnu_stuff_moveable.Text = "Toggle item moveable"
        '
        'mnu_stuff_fixed
        '
        Me.mnu_stuff_fixed.Index = 13
        Me.mnu_stuff_fixed.Text = "Toggle item fixed"
        '
        'mnu_stuff_sizeable
        '
        Me.mnu_stuff_sizeable.Index = 14
        Me.mnu_stuff_sizeable.Text = "Toggle item sizeable"
        '
        'mnu_stuff_getitemfont
        '
        Me.mnu_stuff_getitemfont.Index = 15
        Me.mnu_stuff_getitemfont.Text = "Get item font"
        '
        'mnu_stuff_setnewdib
        '
        Me.mnu_stuff_setnewdib.Index = 16
        Me.mnu_stuff_setnewdib.Text = "Set new dib into Image object"
        '
        'mnu_stuff_toggleuserdraw
        '
        Me.mnu_stuff_toggleuserdraw.Index = 17
        Me.mnu_stuff_toggleuserdraw.Text = "Toggle user Draw"
        '
        'mnu_mode
        '
        Me.mnu_mode.Index = 5
        Me.mnu_mode.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_mode_editmode, Me.mnu_mode_interactive})
        Me.mnu_mode.Text = "Mode"
        '
        'mnu_mode_editmode
        '
        Me.mnu_mode_editmode.Index = 0
        Me.mnu_mode_editmode.Text = "Set Edit mode"
        '
        'mnu_mode_interactive
        '
        Me.mnu_mode_interactive.Index = 1
        Me.mnu_mode_interactive.Text = "Set Interactive mode"
        '
        'mnu_cm
        '
        Me.mnu_cm.Index = 6
        Me.mnu_cm.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_cm_enable})
        Me.mnu_cm.Text = "Context Menu"
        '
        'mnu_cm_enable
        '
        Me.mnu_cm_enable.Index = 0
        Me.mnu_cm_enable.Text = "Enable"
        '
        'mnu_Custom
        '
        Me.mnu_Custom.Index = 7
        Me.mnu_Custom.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_RectangleCursor, Me.mnu_EllipseCursor, Me.mnu_TextCursor})
        Me.mnu_Custom.Text = "Custom Cursors"
        '
        'mnu_RectangleCursor
        '
        Me.mnu_RectangleCursor.Index = 0
        Me.mnu_RectangleCursor.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_RectangleCustom, Me.mnu_CreatingRectangle, Me.mnu_MovingRectangle, Me.mnu_SelectingRectangle})
        Me.mnu_RectangleCursor.Text = "For Rectangle Annotations"
        '
        'mnu_RectangleCustom
        '
        Me.mnu_RectangleCustom.Index = 0
        Me.mnu_RectangleCustom.Text = "Select Custom Cursor"
        '
        'mnu_CreatingRectangle
        '
        Me.mnu_CreatingRectangle.Index = 1
        Me.mnu_CreatingRectangle.Text = "Use for Creating"
        '
        'mnu_MovingRectangle
        '
        Me.mnu_MovingRectangle.Index = 2
        Me.mnu_MovingRectangle.Text = "Use for Moving"
        '
        'mnu_SelectingRectangle
        '
        Me.mnu_SelectingRectangle.Index = 3
        Me.mnu_SelectingRectangle.Text = "Use for Selecting"
        '
        'mnu_EllipseCursor
        '
        Me.mnu_EllipseCursor.Index = 1
        Me.mnu_EllipseCursor.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_Custom_Ellipse, Me.mnu_EllipseCreating, Me.mnu_MovingEllipse, Me.mnu_EllipseSelecting})
        Me.mnu_EllipseCursor.Text = "For Ellipse Annotations"
        '
        'mnu_Custom_Ellipse
        '
        Me.mnu_Custom_Ellipse.Index = 0
        Me.mnu_Custom_Ellipse.Text = "Select Custom Cursor"
        '
        'mnu_EllipseCreating
        '
        Me.mnu_EllipseCreating.Index = 1
        Me.mnu_EllipseCreating.Text = "Use for Creating"
        '
        'mnu_MovingEllipse
        '
        Me.mnu_MovingEllipse.Index = 2
        Me.mnu_MovingEllipse.Text = "Use for Moving"
        '
        'mnu_EllipseSelecting
        '
        Me.mnu_EllipseSelecting.Index = 3
        Me.mnu_EllipseSelecting.Text = "Use for Selecting"
        '
        'mnu_TextCursor
        '
        Me.mnu_TextCursor.Index = 2
        Me.mnu_TextCursor.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_CustomText, Me.mnu_CreatingText, Me.mnu_MovingText, Me.mnu_SelectingText})
        Me.mnu_TextCursor.Text = "For Text Annotations"
        '
        'mnu_CustomText
        '
        Me.mnu_CustomText.Index = 0
        Me.mnu_CustomText.Text = "Select Custom Cursor"
        '
        'mnu_CreatingText
        '
        Me.mnu_CreatingText.Index = 1
        Me.mnu_CreatingText.Text = "Use for Creating"
        '
        'mnu_MovingText
        '
        Me.mnu_MovingText.Index = 2
        Me.mnu_MovingText.Text = "Use for Moving"
        '
        'mnu_SelectingText
        '
        Me.mnu_SelectingText.Index = 3
        Me.mnu_SelectingText.Text = "Use for Selecting"
        '
        'mnu_Groups
        '
        Me.mnu_Groups.Index = 8
        Me.mnu_Groups.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_objects_allowgrouping, Me.MenuItem97, Me.mnu_CreateGroup, Me.mnu_DeleteGroup, Me.mnu_SetGroupEmpty, Me.mnu_SelectGroupItems, Me.mnu_GroupAddSelectedItems, Me.mnu_GroupRemoveSelectedItems, Me.mnu_YokeGroupItems, Me.mnu_MirrorYoked, Me.mnu_InvertYoked, Me.mnu_SetGroupUserString})
        Me.mnu_Groups.Text = "Groups"
        '
        'mnu_objects_allowgrouping
        '
        Me.mnu_objects_allowgrouping.Index = 0
        Me.mnu_objects_allowgrouping.Text = "Toggle Allow Grouping"
        '
        'MenuItem97
        '
        Me.MenuItem97.Index = 1
        Me.MenuItem97.Text = "-"
        '
        'mnu_CreateGroup
        '
        Me.mnu_CreateGroup.Index = 2
        Me.mnu_CreateGroup.Text = "Create..."
        '
        'mnu_DeleteGroup
        '
        Me.mnu_DeleteGroup.Index = 3
        Me.mnu_DeleteGroup.Text = "Delete"
        '
        'mnu_SetGroupEmpty
        '
        Me.mnu_SetGroupEmpty.Index = 4
        Me.mnu_SetGroupEmpty.Text = "Set Empty"
        '
        'mnu_SelectGroupItems
        '
        Me.mnu_SelectGroupItems.Index = 5
        Me.mnu_SelectGroupItems.Text = "Show Items Selected"
        '
        'mnu_GroupAddSelectedItems
        '
        Me.mnu_GroupAddSelectedItems.Index = 6
        Me.mnu_GroupAddSelectedItems.Text = "Add Selected Items"
        '
        'mnu_GroupRemoveSelectedItems
        '
        Me.mnu_GroupRemoveSelectedItems.Index = 7
        Me.mnu_GroupRemoveSelectedItems.Text = "Remove Selected Items"
        '
        'mnu_YokeGroupItems
        '
        Me.mnu_YokeGroupItems.Index = 8
        Me.mnu_YokeGroupItems.Text = "Toggle Yoked"
        '
        'mnu_MirrorYoked
        '
        Me.mnu_MirrorYoked.Index = 9
        Me.mnu_MirrorYoked.Text = "Mirror Yoked Items"
        '
        'mnu_InvertYoked
        '
        Me.mnu_InvertYoked.Index = 10
        Me.mnu_InvertYoked.Text = "Invert Yoked Items"
        '
        'mnu_SetGroupUserString
        '
        Me.mnu_SetGroupUserString.Index = 11
        Me.mnu_SetGroupUserString.Text = "User String..."
        '
        'mnu_ToolBar
        '
        Me.mnu_ToolBar.Index = 9
        Me.mnu_ToolBar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_ToolbarRulerTool, Me.mnu_ToolbarFreehandTool, Me.mnu_ToolbarEllipseTool, Me.mnu_ToolbarTextTool, Me.mnu_ToolbarRectangleTool, Me.mnu_ToolbarStampTool, Me.mnu_ToolbarPolylineTool, Me.mnu_ToolbarLineTool, Me.mnu_ToolbarImageTool, Me.mnu_ToolbarButtonTool, Me.mnu_ToolbarPolygonTool, Me.mnu_HighlightToolbar, Me.mnu_NoteToolbar})
        Me.mnu_ToolBar.Text = "ToolBar"
        '
        'mnu_ToolbarRulerTool
        '
        Me.mnu_ToolbarRulerTool.Index = 0
        Me.mnu_ToolbarRulerTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_RulerToolEnabeled, Me.mnu_RulerToolVisible})
        Me.mnu_ToolbarRulerTool.Text = "RulerTool"
        '
        'mnu_RulerToolEnabeled
        '
        Me.mnu_RulerToolEnabeled.Index = 0
        Me.mnu_RulerToolEnabeled.Text = "Enabled"
        '
        'mnu_RulerToolVisible
        '
        Me.mnu_RulerToolVisible.Index = 1
        Me.mnu_RulerToolVisible.Text = "Visible"
        '
        'mnu_ToolbarFreehandTool
        '
        Me.mnu_ToolbarFreehandTool.Index = 1
        Me.mnu_ToolbarFreehandTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_FreehandToolEnabled, Me.mnu_FreehandToolVisible})
        Me.mnu_ToolbarFreehandTool.Text = "Freehand Tool"
        '
        'mnu_FreehandToolEnabled
        '
        Me.mnu_FreehandToolEnabled.Index = 0
        Me.mnu_FreehandToolEnabled.Text = "Enabled"
        '
        'mnu_FreehandToolVisible
        '
        Me.mnu_FreehandToolVisible.Index = 1
        Me.mnu_FreehandToolVisible.Text = "Visible"
        '
        'mnu_ToolbarEllipseTool
        '
        Me.mnu_ToolbarEllipseTool.Index = 2
        Me.mnu_ToolbarEllipseTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_EllipseToolEnabeld, Me.mnu_EllipseToolVisible})
        Me.mnu_ToolbarEllipseTool.Text = "Elliplse Tool"
        '
        'mnu_EllipseToolEnabeld
        '
        Me.mnu_EllipseToolEnabeld.Index = 0
        Me.mnu_EllipseToolEnabeld.Text = "Enabled"
        '
        'mnu_EllipseToolVisible
        '
        Me.mnu_EllipseToolVisible.Index = 1
        Me.mnu_EllipseToolVisible.Text = "Visible"
        '
        'mnu_ToolbarTextTool
        '
        Me.mnu_ToolbarTextTool.Index = 3
        Me.mnu_ToolbarTextTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_TextToolEnabled, Me.mnu_TextToolVisible})
        Me.mnu_ToolbarTextTool.Text = "Text Tool"
        '
        'mnu_TextToolEnabled
        '
        Me.mnu_TextToolEnabled.Index = 0
        Me.mnu_TextToolEnabled.Text = "Enabled"
        '
        'mnu_TextToolVisible
        '
        Me.mnu_TextToolVisible.Index = 1
        Me.mnu_TextToolVisible.Text = "Visible"
        '
        'mnu_ToolbarRectangleTool
        '
        Me.mnu_ToolbarRectangleTool.Index = 4
        Me.mnu_ToolbarRectangleTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_RectangleToolEnabled, Me.mnu_RectangleToolVisible})
        Me.mnu_ToolbarRectangleTool.Text = "Rectangle Tool"
        '
        'mnu_RectangleToolEnabled
        '
        Me.mnu_RectangleToolEnabled.Index = 0
        Me.mnu_RectangleToolEnabled.Text = "Enabled"
        '
        'mnu_RectangleToolVisible
        '
        Me.mnu_RectangleToolVisible.Index = 1
        Me.mnu_RectangleToolVisible.Text = "Visible"
        '
        'mnu_ToolbarStampTool
        '
        Me.mnu_ToolbarStampTool.Index = 5
        Me.mnu_ToolbarStampTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_StampToolEnabled, Me.mnu_StampToolVisible})
        Me.mnu_ToolbarStampTool.Text = "Stamp Tool"
        '
        'mnu_StampToolEnabled
        '
        Me.mnu_StampToolEnabled.Index = 0
        Me.mnu_StampToolEnabled.Text = "Enabled"
        '
        'mnu_StampToolVisible
        '
        Me.mnu_StampToolVisible.Index = 1
        Me.mnu_StampToolVisible.Text = "Visible"
        '
        'mnu_ToolbarPolylineTool
        '
        Me.mnu_ToolbarPolylineTool.Index = 6
        Me.mnu_ToolbarPolylineTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_PolylineToolEnabled, Me.mnu_PolylineToolVisible})
        Me.mnu_ToolbarPolylineTool.Text = "Polyline Tool"
        '
        'mnu_PolylineToolEnabled
        '
        Me.mnu_PolylineToolEnabled.Index = 0
        Me.mnu_PolylineToolEnabled.Text = "Enabled"
        '
        'mnu_PolylineToolVisible
        '
        Me.mnu_PolylineToolVisible.Index = 1
        Me.mnu_PolylineToolVisible.Text = "Visible"
        '
        'mnu_ToolbarLineTool
        '
        Me.mnu_ToolbarLineTool.Index = 7
        Me.mnu_ToolbarLineTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_LineToolEnabled, Me.mnu_LineToolVisible})
        Me.mnu_ToolbarLineTool.Text = "Line Tool"
        '
        'mnu_LineToolEnabled
        '
        Me.mnu_LineToolEnabled.Index = 0
        Me.mnu_LineToolEnabled.Text = "Enabled"
        '
        'mnu_LineToolVisible
        '
        Me.mnu_LineToolVisible.Index = 1
        Me.mnu_LineToolVisible.Text = "Visible"
        '
        'mnu_ToolbarImageTool
        '
        Me.mnu_ToolbarImageTool.Index = 8
        Me.mnu_ToolbarImageTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_ImageToolEnabled, Me.mnu_ImageToolVisible})
        Me.mnu_ToolbarImageTool.Text = "Image Tool"
        '
        'mnu_ImageToolEnabled
        '
        Me.mnu_ImageToolEnabled.Index = 0
        Me.mnu_ImageToolEnabled.Text = "Enabled"
        '
        'mnu_ImageToolVisible
        '
        Me.mnu_ImageToolVisible.Index = 1
        Me.mnu_ImageToolVisible.Text = "Visible"
        '
        'mnu_ToolbarButtonTool
        '
        Me.mnu_ToolbarButtonTool.Index = 9
        Me.mnu_ToolbarButtonTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_ButtonToolEnabled, Me.mnu_ButtonToolVisible})
        Me.mnu_ToolbarButtonTool.Text = "Button Tool"
        '
        'mnu_ButtonToolEnabled
        '
        Me.mnu_ButtonToolEnabled.Index = 0
        Me.mnu_ButtonToolEnabled.Text = "Enabled"
        '
        'mnu_ButtonToolVisible
        '
        Me.mnu_ButtonToolVisible.Index = 1
        Me.mnu_ButtonToolVisible.Text = "Visible"
        '
        'mnu_ToolbarPolygonTool
        '
        Me.mnu_ToolbarPolygonTool.Index = 10
        Me.mnu_ToolbarPolygonTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_PolygonToolEnabled, Me.mnu_PolygonToolVisible})
        Me.mnu_ToolbarPolygonTool.Text = "Polygon Tool"
        '
        'mnu_PolygonToolEnabled
        '
        Me.mnu_PolygonToolEnabled.Index = 0
        Me.mnu_PolygonToolEnabled.Text = "Enabled"
        '
        'mnu_PolygonToolVisible
        '
        Me.mnu_PolygonToolVisible.Index = 1
        Me.mnu_PolygonToolVisible.Text = "Visible"
        '
        'mnu_HighlightToolbar
        '
        Me.mnu_HighlightToolbar.Index = 11
        Me.mnu_HighlightToolbar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_BlockHighlightEnabled, Me.mnu_BlockHighlightVisible})
        Me.mnu_HighlightToolbar.Text = "Block Highlight Tool"
        '
        'mnu_BlockHighlightEnabled
        '
        Me.mnu_BlockHighlightEnabled.Index = 0
        Me.mnu_BlockHighlightEnabled.Text = "Enabled"
        '
        'mnu_BlockHighlightVisible
        '
        Me.mnu_BlockHighlightVisible.Index = 1
        Me.mnu_BlockHighlightVisible.Text = "Visible"
        '
        'mnu_NoteToolbar
        '
        Me.mnu_NoteToolbar.Index = 12
        Me.mnu_NoteToolbar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_NoteToolEnabled, Me.mnu_NoteToolVisible})
        Me.mnu_NoteToolbar.Text = "Note Tool"
        '
        'mnu_NoteToolEnabled
        '
        Me.mnu_NoteToolEnabled.Index = 0
        Me.mnu_NoteToolEnabled.Text = "Enabled"
        '
        'mnu_NoteToolVisible
        '
        Me.mnu_NoteToolVisible.Index = 1
        Me.mnu_NoteToolVisible.Text = "Visible"
        '
        'mnu_Help
        '
        Me.mnu_Help.Index = 10
        Me.mnu_Help.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_AboutIX, Me.mnu_AboutNX})
        Me.mnu_Help.Text = "&About"
        '
        'mnu_AboutIX
        '
        Me.mnu_AboutIX.Index = 0
        Me.mnu_AboutIX.Text = "&ImagXpress..."
        '
        'mnu_AboutNX
        '
        Me.mnu_AboutNX.Index = 1
        Me.mnu_AboutNX.Text = "&NotateXpress..."
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following:", "1)Adding various annotation types including redaction and ruler types.", "2)Creating,deleting and hiding layers.", "3)Saving annotations in the Pegasus NXP format and the Kodak Wang Imaging format." & _
                        ""})
        Me.lstInfo.Location = New System.Drawing.Point(24, 12)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(752, 43)
        Me.lstInfo.TabIndex = 10
        '
        'OutputList
        '
        Me.OutputList.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.OutputList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OutputList.Location = New System.Drawing.Point(24, 582)
        Me.OutputList.Name = "OutputList"
        Me.OutputList.Size = New System.Drawing.Size(736, 121)
        Me.OutputList.TabIndex = 11
        '
        'lblEvents
        '
        Me.lblEvents.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblEvents.Location = New System.Drawing.Point(24, 550)
        Me.lblEvents.Name = "lblEvents"
        Me.lblEvents.Size = New System.Drawing.Size(104, 24)
        Me.lblEvents.TabIndex = 12
        Me.lblEvents.Text = "Events:"
        '
        'ZoomOut
        '
        Me.ZoomOut.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZoomOut.BackColor = System.Drawing.SystemColors.Control
        Me.ZoomOut.Cursor = System.Windows.Forms.Cursors.Default
        Me.ZoomOut.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZoomOut.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ZoomOut.Location = New System.Drawing.Point(569, 542)
        Me.ZoomOut.Name = "ZoomOut"
        Me.ZoomOut.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ZoomOut.Size = New System.Drawing.Size(95, 33)
        Me.ZoomOut.TabIndex = 14
        Me.ZoomOut.Text = "Zoom Out"
        Me.ZoomOut.UseVisualStyleBackColor = False
        '
        'ZoomIn
        '
        Me.ZoomIn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ZoomIn.BackColor = System.Drawing.SystemColors.Control
        Me.ZoomIn.Cursor = System.Windows.Forms.Cursors.Default
        Me.ZoomIn.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZoomIn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ZoomIn.Location = New System.Drawing.Point(569, 502)
        Me.ZoomIn.Name = "ZoomIn"
        Me.ZoomIn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ZoomIn.Size = New System.Drawing.Size(95, 33)
        Me.ZoomIn.TabIndex = 13
        Me.ZoomIn.Text = "Zoom In"
        Me.ZoomIn.UseVisualStyleBackColor = False
        '
        'ImagXpress1
        '
        '
        'ImageXView1
        '
        Me.ImageXView1.AutoResize = PegasusImaging.WinForms.ImagXpress9.AutoResizeType.BestFit
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(24, 61)
        Me.ImageXView1.MouseWheelCapture = False
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(522, 486)
        Me.ImageXView1.TabIndex = 15
        '
        'Processor1
        '
        Me.Processor1.BackgroundColor = System.Drawing.Color.Black
        Me.Processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast
        Me.Processor1.ProgressPercent = 10
        Me.Processor1.Redeyes = Nothing
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(792, 735)
        Me.Controls.Add(Me.ImageXView1)
        Me.Controls.Add(Me.ZoomOut)
        Me.Controls.Add(Me.ZoomIn)
        Me.Controls.Add(Me.lblEvents)
        Me.Controls.Add(Me.OutputList)
        Me.Controls.Add(Me.lstInfo)
        Me.Controls.Add(Me.lblLastError)
        Me.Controls.Add(Me.lblerror)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.Menu = Me.MainMenu1
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Demo"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress9.ImagXpress
    Dim printDocument1 As System.Drawing.Printing.PrintDocument

    Dim strCurrentDir As System.String = "..\..\..\..\..\..\..\Common\Images\"
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
           + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress9.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
           + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub



    Private Overloads Function PegasusOpenFile(ByVal sTitle As String, ByVal sFileFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = sTitle
        dlg.Filter = sFileFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusSaveFile(ByVal sTitle As String, ByVal sFilter As String) As String
        Dim dlg As SaveFileDialog = New SaveFileDialog()
        dlg.Title = sTitle
        dlg.Filter = sFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function


#End Region

    Private Function createNewLayer(ByVal strLayerName As String, ByVal DibHandle As System.IntPtr) As PegasusImaging.WinForms.NotateXpress9.Layer
        ' ---------------
        ' create new layer
        ' ---------------
        Dim layer As PegasusImaging.WinForms.NotateXpress9.Layer = New Layer()

        ' ---------------
        ' Add the new layer to the collection and make it the selected layer
        ' ---------------

        NotateXpress1.Layers.Add(layer)
        layer.Select()

        layer.Name = strLayerName
        layer.Description = "Layer created " + System.DateTime.Now.ToLongTimeString()
        layer.Active = True ' probably default value
        layer.Visible = True ' probably default value
        If NotateXpress1.InteractMode = AnnotationMode.Edit Then
            layer.Toolbar.Visible = True
        Else
            layer.Toolbar.Visible = False
            mnu_ToolBar.Enabled = False
        End If

        ' ---------------
        ' set the button tool text and pen width for this demo
        ' ---------------
        layer.Toolbar.ButtonToolbarDefaults.PenWidth = 4
        layer.Toolbar.ButtonToolbarDefaults.Text = "Click me"

        ' ---------------
        ' set the image tool defaults for this demo
        ' ---------------
        layer.Toolbar.ImageToolbarDefaults.BackStyle = BackStyle.Transparent
        If DibHandle.ToInt32 <> IntPtr.Zero.ToInt32 Then
            layer.Toolbar.ImageToolbarDefaults.DibHandle = DibHandle
        End If

        ' ---------------
        ' set the stamp tool text and font for this demo
        ' ---------------
        layer.Toolbar.StampToolbarDefaults.Text = "NotateXpress 9"
        Dim fontCurrent As System.Drawing.Font = layer.Toolbar.StampToolbarDefaults.TextFont
        Dim fontNew As System.Drawing.Font = New System.Drawing.Font(fontCurrent.Name, 16, System.Drawing.FontStyle.Bold)
        layer.Toolbar.StampToolbarDefaults.TextFont = fontNew

        Return layer
    End Function

    Private Sub LoadFile(ByVal ImageXView As PegasusImaging.WinForms.ImagXpress9.ImageXView)
        Try
            imagX1 = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, strImageFileName)
            ImageXView.Image = imagX1
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = (Screen.PrimaryScreen.WorkingArea.Height - Me.Height) / 2
        Me.Left = (Screen.PrimaryScreen.WorkingArea.Width - Me.Width) / 2
        '--------------
        ' load file into ImagXpress
        '--------------
        Try
            Dim strCurrentDir As System.String = System.IO.Directory.GetCurrentDirectory()

            '**The UnlockRuntime function must be called to distribute the runtime**
            'imagXpress1.License.UnlockRuntime(12345, 12345, 12345, 12345)

            m_saveOptions = New PegasusImaging.WinForms.NotateXpress9.SaveOptions()
            m_loadOptions = New PegasusImaging.WinForms.NotateXpress9.LoadOptions()

            'Create a new load options object so we can recieve events from the images we load
            loIX_loadOptions = New PegasusImaging.WinForms.ImagXpress9.LoadOptions()



            strImageFileName = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\benefits.tif")
            LoadFile(ImageXView1)

            ' pass an image file relative path from the samples
            strImageFileName = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\vermont.jpg")

            ImageXView2 = New PegasusImaging.WinForms.ImagXpress9.ImageXView(ImagXpress1)
            LoadFile(ImageXView2)

            ' Use the SetClientMethod to connect the ImagXpress control and NotateXpress control
            NotateXpress1.ClientWindow = ImageXView1.Handle
            NotateXpress1.ToolbarDefaults.ToolbarActivated = True

            ImageXView1.AutoScroll = True

            ' set the "global" toolbar default values
            Dim toolbarDefaults As ToolbarDefaults = NotateXpress1.ToolbarDefaults
            toolbarDefaults.SetToolTip(AnnotationTool.EllipseTool, "The Ellipse-O-Matic!")
            toolbarDefaults.SetToolTip(AnnotationTool.FreehandTool, "Draw with this!")
            toolbarDefaults.SetToolTip(AnnotationTool.LineTool, "Liney, very, very Liney")
            toolbarDefaults.SetToolTip(AnnotationTool.PointerTool, "Point 'em out!")
            toolbarDefaults.SetToolTip(AnnotationTool.PolygonTool, "Polygons aren't gone")
            toolbarDefaults.SetToolTip(AnnotationTool.RectangleTool, "Rectangle, we got Rectangle")
            toolbarDefaults.SetToolTip(AnnotationTool.StampTool, "Stamp out dumb tool tips!")
            toolbarDefaults.SetToolTip(AnnotationTool.TextTool, "Write me a masterpiece!")
            toolbarDefaults.SetToolTip(AnnotationTool.PolyLineTool, "PolyLine")
            toolbarDefaults.SetToolTip(AnnotationTool.ButtonTool, "Button Tool")
            toolbarDefaults.SetToolTip(AnnotationTool.ImageTool, "Image Tool!")
            toolbarDefaults.SetToolTip(AnnotationTool.RulerTool, "Ruler Tool")

            ' Create a new layer and its toolbar
            Dim layer As Layer = createNewLayer("Layer1", ImageXView2.Image.ToHdib(False))

            m_loadOptions.AnnType = AnnotationType.ImagingForWindows
            m_saveOptions.AnnType = AnnotationType.ImagingForWindows

            mnu_mode_interactive.Checked = False
            mnu_mode_editmode.Checked = True
            _bAutoSelect = True
            _bUpdateMoved = True
        Catch ex As Exception
            MessageBox.Show("FormMain_Load Error msg:   " + ex.Message + _
             "Error type:  " + ex.GetType().ToString())
        End Try


    End Sub

    Private Sub mnuFile_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuFile.Select
        mnu_ImagingForWindows.Checked = (m_saveOptions.AnnType = AnnotationType.ImagingForWindows)
        mnu_NotateAnnotations.Checked = (m_saveOptions.AnnType = AnnotationType.NotateXpress)
        mnu_XML.Checked = (m_saveOptions.AnnType = AnnotationType.NotateXpressXml)

        mnu_XFDF.Checked = (m_saveOptions.AnnType = AnnotationType.Xfdf)


        mnu_ViewDirectorAnnotations.Checked = (m_saveOptions.AnnType = AnnotationType.TMSSequoia)
        mnu_file_wangcompatible.Checked = m_saveOptions.PreserveWangLayers
        mnu_UnicodeMode.Checked = m_loadOptions.UnicodeMode
        mnu_ToggleAllowPaint.Checked = NotateXpress1.AllowPaint

        mnu_LoadMemoryStream.Enabled = Not (_SavedAnnotationMemoryStream) Is Nothing
        mnu_LoadByteArray.Enabled = Not (_SavedAnnotationByteArray) Is Nothing

        mnu_LoadAnnotations.Checked = NotateXpress1.ImagXpressLoad
        mnu_SaveAnnotations.Checked = NotateXpress1.ImagXpressSave


    End Sub
    Private Sub menuItemImagingForWindows_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_ImagingForWindows.Click
        m_saveOptions.AnnType = AnnotationType.ImagingForWindows
        m_loadOptions.AnnType = AnnotationType.ImagingForWindows

    End Sub

    Private Sub menuItemNotateAnnotations_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_NotateAnnotations.Click
        m_saveOptions.AnnType = AnnotationType.NotateXpress
        m_loadOptions.AnnType = AnnotationType.NotateXpress

        mnu_file_saveann2fileANN.Enabled = False
        mnu_file_loadannotationANN.Enabled = False

        mnu_file_loadannotationNXP.Enabled = True
        mnu_file_saveann2fileNXP.Enabled = True

        mnu_saveXML.Enabled = False
        mnu_loadXML.Enabled = False

        mnu_loadXFDF.Enabled = False
        mnu_saveXFDF.Enabled = False
    End Sub

    Private Sub menuItemViewDirectorAnnotations_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_ViewDirectorAnnotations.Click
        m_saveOptions.AnnType = AnnotationType.TMSSequoia
        m_loadOptions.AnnType = AnnotationType.TMSSequoia

        mnu_file_saveann2fileANN.Enabled = True
        mnu_file_loadannotationANN.Enabled = True

        mnu_file_loadannotationNXP.Enabled = False
        mnu_file_saveann2fileNXP.Enabled = False

        mnu_saveXML.Enabled = False
        mnu_loadXML.Enabled = False

        mnu_loadXFDF.Enabled = False
        mnu_saveXFDF.Enabled = False
    End Sub

    Private Sub mnu_file_wangcompatible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_wangcompatible.Click

        'If (m_saveOptions.PreserveWangLayers) Then
        '    m_loadOptions.PreserveWangLayers = m_saveOptions.PreserveWangLayers = False
        'Else
        '    m_loadOptions.PreserveWangLayers = m_saveOptions.PreserveWangLayers = True
        'End If

        m_saveOptions.PreserveWangLayers = Not mnu_file_wangcompatible.Checked
        m_loadOptions.PreserveWangLayers = Not mnu_file_wangcompatible.Checked

        mnu_file_wangcompatible.Checked = Not mnu_file_wangcompatible.Checked

    End Sub

    Private Sub menuItemLoadAnnotations_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_LoadAnnotations.Click
        NotateXpress1.ImagXpressLoad = Not NotateXpress1.ImagXpressLoad
        mnu_LoadAnnotations.Checked = Not mnu_LoadAnnotations.Checked
    End Sub

    Private Sub mnu_load_tiff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_load_tiff.Click
        strImageFileName = PegasusOpenFile("Select an Image File", strDefaultImageFilter)
        If strImageFileName.Length <> 0 Then

            ' ---------------
            ' clear existing annotations and layers
            ' ---------------
            NotateXpress1.Layers.Clear()

            LoadFile(ImageXView1)
        End If
        Try

            createNewLayer("Layer1", System.IntPtr.Zero)
            Me.Text = "Image = " + ImageXView1.Image.ImageXData.Width.ToString() + " x " + ImageXView1.Image.ImageXData.Height.ToString()

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub menuItemSaveAnnotations_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_SaveAnnotations.Click
        NotateXpress1.ImagXpressSave = Not NotateXpress1.ImagXpressSave
        mnu_SaveAnnotations.Checked = Not mnu_SaveAnnotations.Checked

    End Sub

    Private Sub mnu_file_save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_save.Click
        Dim strSaveFilePath As String = PegasusSaveFile("Save TIFF File", "Tiff File (*.tif) | *.tif")

        If strSaveFilePath <> "" Then

            Try
                NotateXpress1.Layers.SetSaveOptions(m_saveOptions)

                Dim saveOptions As PegasusImaging.WinForms.ImagXpress9.SaveOptions = New PegasusImaging.WinForms.ImagXpress9.SaveOptions()
                ImageXView1.Image.Save(strSaveFilePath, saveOptions)

                'clear out the error in case there was an error from a previous operation
                lblerror.Text = ""
            Catch ex As PegasusImaging.WinForms.ImagXpress9.ImagXpressException
                PegasusError(ex, lblerror)

            Catch ex As System.Exception
                PegasusError(ex, lblerror)

            End Try
        End If
    End Sub

    Private Sub mnu_file_saveann2fileNXP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_saveann2fileNXP.Click
        Dim strFileName As String = PegasusSaveFile("Save Annotation File", "NotateXpress File (*.nxp) | *.nxp")
        Try
            NotateXpress1.Layers.Comments = "This file was saved today"
            NotateXpress1.Layers.Subject = "testing 123"
            NotateXpress1.Layers.Title = "One awesome annotation file"
            NotateXpress1.Layers.SaveToFile(strFileName, m_saveOptions)

            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub mnu_file_saveann2fileANN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_saveann2fileANN.Click
        Dim strFileName As String = PegasusSaveFile("Save Annotation File", "TMS Annotation File (*.ann) | *.ann")
        Try
            NotateXpress1.Layers.Comments = "This file was saved today"
            NotateXpress1.Layers.Subject = "testing 123"
            NotateXpress1.Layers.Title = "One awesome annotation file"
            NotateXpress1.Layers.SaveToFile(strFileName, m_saveOptions)

            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub menuItemSaveByteArray_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_SaveByteArray.Click
        If (Not (_SavedAnnotationByteArray) Is Nothing) Then
            _SavedAnnotationByteArray = Nothing
        End If
        _SavedAnnotationByteArray = NotateXpress1.Layers.SaveToByteArray(m_saveOptions)
    End Sub

    Private Sub menuItemSaveMemoryStream_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_SaveMemoryStream.Click
        If (Not (_SavedAnnotationMemoryStream) Is Nothing) Then
            _SavedAnnotationMemoryStream = Nothing
        End If
        _SavedAnnotationMemoryStream = NotateXpress1.Layers.SaveToMemoryStream(m_saveOptions)
    End Sub

    Private Sub menuItemUnicodeMode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_UnicodeMode.Click
        m_loadOptions.UnicodeMode = Not m_loadOptions.UnicodeMode
    End Sub

    Private Sub mnu_file_loadannotationNXP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_loadannotationNXP.Click
        Dim strFileName As String = PegasusOpenFile("Open NotateXpress File", "NotateXpress File (*.nxp) | *.nxp")
        Dim layer As Layer
        Try
            NotateXpress1.Layers.FromFile(strFileName, m_loadOptions)

            layer = NotateXpress1.Layers.Selected()
            If Not layer Is Nothing Then
                layer.Toolbar.Visible = True
            End If
            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub mnu_file_loadannotationANN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_loadannotationANN.Click
        Dim strFileName As String = PegasusOpenFile("Open TMS Annotation File", "TMS Annotation File (*.ann) | *.ann")
        Dim layer As Layer
        Try
            NotateXpress1.Layers.FromFile(strFileName, m_loadOptions)

            layer = NotateXpress1.Layers.Selected()
            If Not layer Is Nothing Then
                layer.Toolbar.Visible = True
            End If

            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub menuItemLoadByteArray_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_LoadByteArray.Click
        If (Not (_SavedAnnotationByteArray) Is Nothing) Then
            NotateXpress1.Layers.FromByteArray(_SavedAnnotationByteArray, m_loadOptions)
        End If
    End Sub

    Private Sub menuItemLoadMemoryStream_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_LoadMemoryStream.Click
        If (Not (_SavedAnnotationMemoryStream) Is Nothing) Then
            NotateXpress1.Layers.FromMemoryStream(_SavedAnnotationMemoryStream, m_loadOptions)
        End If
    End Sub

    Private Sub mnu_fileIResY_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_fileIResY.Click
        If (NotateXpress1.FontScaling = FontScaling.ResolutionY) Then
            NotateXpress1.FontScaling = FontScaling.Normal
        Else
            NotateXpress1.FontScaling = FontScaling.ResolutionY
        End If
        mnu_fileIResY.Checked = (NotateXpress1.FontScaling = FontScaling.ResolutionY)
    End Sub

    Private Sub menuItemToggleAllowPaint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_ToggleAllowPaint.Click
        NotateXpress1.AllowPaint = Not NotateXpress1.AllowPaint
        mnu_ToggleAllowPaint.Checked = NotateXpress1.AllowPaint
    End Sub

    Private Sub menuItemAboutBox_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        NotateXpress1.AboutBox()
    End Sub

    Private Sub mnu_file_exit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_exit.Click
        System.Windows.Forms.Application.Exit()
    End Sub

    Private Sub mnu_layers_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnu_layers.Select

        Dim Layer As PegasusImaging.WinForms.NotateXpress9.Layer

        Layer = NotateXpress1.Layers.Selected
        Dim bEnabled As Boolean = (Not (Layer) Is Nothing)
        mnu_layers_deletecurrent.Enabled = bEnabled
        mnu_layers_togglehide.Enabled = bEnabled
        mnu_layers_setdescription.Enabled = bEnabled
        mnu_layers_setname.Enabled = bEnabled
        mnu_DeactivateCurrentLayer.Enabled = bEnabled
        mnu_SendToBack.Enabled = False
        mnu_BringToFront.Enabled = False
        mnu_DeleteSelected.Enabled = False
        mnu_DisableToolbar.Enabled = bEnabled
        If bEnabled Then
            mnu_layers_toggletoolbar.Checked = Layer.Toolbar.Visible

            If Layer.Visible Then
                mnu_layers_togglehide.Text = "Hide current"
            Else
                mnu_layers_togglehide.Text = "Show current"
            End If
            If Layer.Active Then
                mnu_DeactivateCurrentLayer.Text = "Deactivate current"
            Else
                mnu_DeactivateCurrentLayer.Text = "Activate current"
            End If
            If Layer.Toolbar.Enabled Then
                mnu_DisableToolbar.Text = "Disable toolbar"
            Else
                mnu_DisableToolbar.Text = "Enable toolbar"
            End If
            If (Not (Layer.Elements.FirstSelected) Is Nothing) Then
                mnu_SendToBack.Enabled = True
                mnu_BringToFront.Enabled = True
                mnu_DeleteSelected.Enabled = True
            End If
        Else
            mnu_layers_toggletoolbar.Enabled = False
        End If
        If (NotateXpress1.Layers.Count < 1) Then
            mnu_layers_setcurrent.Enabled = False
        Else
            mnu_layers_setcurrent.Enabled = True
            ' ---------------
            '  clean set current layer sub-menu
            ' ---------------
            mnu_layers_setcurrent.MenuItems.Clear()
            Dim layers_setcurrent_eventhandler As System.EventHandler = AddressOf Me.mnu_layers_setcurrent_Click
            ' ---------------
            '  add the description for each layer under this submenu
            ' ---------------
            Dim i As Integer = 0
            Do While (i < NotateXpress1.Layers.Count)
                Dim str As String
                str = NotateXpress1.Layers(i).Name _
                 + "   [" _
                 + NotateXpress1.Layers(i).Visible.ToString _
                 + IIf(NotateXpress1.Layers(i).Active, "Active ", "NOTActive ") _
                 + NotateXpress1.Layers(i).Elements.Count.ToString _
                 + "-Elements]"

                ' creates new MenuItem
                mnu_layers_setcurrent.MenuItems.Add(str)

                ' check if selected layer
                If (NotateXpress1.Layers.Selected Is NotateXpress1.Layers(i)) Then
                    mnu_layers_setcurrent.MenuItems(i).Checked = True
                Else

                    mnu_layers_setcurrent.MenuItems(i).Checked = False
                End If
                ' set event handler for new MenuItem
                AddHandler mnu_layers_setcurrent.MenuItems(i).Click, layers_setcurrent_eventhandler

                i = (i + 1)
            Loop
        End If
    End Sub

    Private Sub mnu_layers_create_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_create.Click
        Dim dlg As InputDlg = New InputDlg("Specify Name of Layer to Create")
        If System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me) Then
            ' Create a new layer, note that it will also now be the selected layer
            createNewLayer(dlg.InputText, System.IntPtr.Zero)
        End If
    End Sub

    Private Sub mnu_layers_setcurrent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_setcurrent.Click

        If (TypeOf sender Is System.Windows.Forms.MenuItem) Then
            NotateXpress1.Layers(CType(sender, System.Windows.Forms.MenuItem).Index).Select()
        End If


    End Sub

    Private Sub mnu_layers_deletecurrent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_deletecurrent.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            NotateXpress1.Layers.Remove(layer)
        End If
    End Sub

    Private Sub mnu_layers_togglehide_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_togglehide.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            layer.Visible = Not layer.Visible
        End If
    End Sub

    Private Sub menuItemDeactivateCurrentLayer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_DeactivateCurrentLayer.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            layer.Active = Not layer.Active
        End If
    End Sub

    Private Sub mnu_layers_setname_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_setname.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim dlg As InputDlg = New InputDlg("Set Layer Name")
            dlg.InputText = layer.Name
            If System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me) Then
                layer.Name = dlg.InputText
            End If
        End If
    End Sub

    Private Sub mnu_layers_setdescription_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_setdescription.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim dlg As InputDlg = New InputDlg("Set Layer Description")
            dlg.InputText = layer.Description
            If System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me) Then
                layer.Description = dlg.InputText
            End If
        End If
    End Sub

    Private Sub mnu_objects_reverse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_reverse.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            If layer.Elements.Count > 0 Then
                layer.Elements.Reverse()
            End If
        End If
    End Sub

    Private Sub menuItemSendToBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_SendToBack.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            layer.Elements.SendToBack()
            ImageXView1.Refresh()
        End If
    End Sub

    Private Sub menuItemBringToFront_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_BringToFront.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            layer.Elements.BringToFront()
            ImageXView1.Refresh()
        End If
    End Sub

    Private Sub menuItemDeleteSelected_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_DeleteSelected.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            layer.Elements.DeleteSelected()
        End If
    End Sub

    Private Sub menuItemSaveCurrentLayer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_SaveCurrentLayer.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        Dim strFileName As String
        If Not layer Is Nothing Then


            strFileName = PegasusSaveFile("Save Current Annotation Layer to File", "NotateXpress File (*.nxp) | *.nxp")

            Try
                NotateXpress1.Layers.Comments = "This file contains 1 saved layer"
                NotateXpress1.Layers.Subject = "Single layer annotation file"
                NotateXpress1.Layers.Title = "One awesome single layer annotation file"
                NotateXpress1.Layers.SaveToFile(strFileName, m_saveOptions)
                m_saveOptions.AllLayers = True
            Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
                PegasusError(ex, lblerror)

            Catch ex As System.Exception
                PegasusError(ex, lblerror)

            End Try
        End If
    End Sub

    Private Sub mnu_layers_toggletoolbar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_toggletoolbar.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            If layer.Toolbar.Visible Then
                layer.Toolbar.Visible = False
                mnu_ToolBar.Enabled = False
            Else
                layer.Toolbar.Visible = True
                mnu_ToolBar.Enabled = True
            End If
        End If
    End Sub

    Private Sub menuItemDisableToolbar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_DisableToolbar.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            If layer.Toolbar.Enabled Then
                layer.Toolbar.Enabled = False
            Else
                layer.Toolbar.Enabled = True
            End If
        End If
    End Sub

    Private Sub mnu_objects_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnu_objects.Select

        ' ------------
        ' get the selected/current layer
        ' ------------
        Dim layer As Layer = NotateXpress1.Layers.Selected()


        If layer Is Nothing Then
            ' ------------
            '  disable all menu selections on this menu
            ' ------------
            Dim item As MenuItem
            For Each item In mnu_objects.MenuItems
                item.Enabled = False
            Next
        Else
            Dim nCount As Integer = layer.Elements.Count
            mnu_objects_reverse.Enabled = nCount > 1
            mnu_objects_brand.Enabled = nCount > 0
            mnu_objects_nudgeup.Enabled = layer.Elements.IsSelection
            mnu_objects_nudgedn.Enabled = layer.Elements.IsSelection
            mnu_objects_createtransrect.Enabled = True

            ' determine whether to enable inflate
            mnu_objects_inflate.Enabled = False

            Dim element As Element = layer.Elements.FirstSelected()



            While Not element Is Nothing
                ' enable inflate if ALL the seleted elements are either an EllipseTool, RectangleTool, TextTool, or ButtonTool
                If TypeOf element Is EllipseTool Or TypeOf element Is RectangleTool Or TypeOf element Is TextTool Or TypeOf element Is ButtonTool Then
                    mnu_objects_inflate.Enabled = True
                Else
                    mnu_objects_inflate.Enabled = False
                    Exit While
                End If
                element = layer.Elements.NextSelected()
            End While
        End If

    End Sub

    Private Sub mnu_objects_autoselect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_autoselect.Click
        _bAutoSelect = Not _bAutoSelect
        mnu_objects_autoselect.Checked = _bAutoSelect
    End Sub

    Private Sub mnu_objects_create10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_create10.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim top As Integer = 15, left As Integer = 15, right As Integer = 115, bottom As Integer = 115

            Dim i As Integer
            For i = 1 To 10 Step i + 1
                Dim rand As System.Random = New System.Random()
                Dim rect As System.Drawing.Rectangle = New Rectangle(top, left, right, bottom)
                If Decimal.Remainder(i, 2) > 0 Then
                    ' create a rectangle object
                    Dim element As RectangleTool = New RectangleTool()
                    element.PenWidth = Decimal.Remainder(i, 3) + 1
                    element.PenColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                    element.FillColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                    element.BoundingRectangle = rect
                    element.ToolTipText = "Rectangle " + i.ToString()
                    layer.Elements.Add(element)
                Else
                    ' create an ellipse object
                    Dim element As EllipseTool = New EllipseTool()
                    element.PenWidth = Decimal.Remainder(i, 3) + 1
                    element.PenColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                    element.FillColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                    element.BoundingRectangle = rect
                    element.ToolTipText = "Ellipse " + i.ToString()
                    layer.Elements.Add(element)
                End If

                top = top + 2
                left = left + 2
                right = right + 2
                bottom = bottom + 2
            Next
        End If
    End Sub

    Private Sub mnu_objects_rects_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_rects.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim top As Integer = 20, left As Integer = 20, right As Integer = 120, bottom As Integer = 120
            Dim i As Integer
            For i = 1 To 10 Step i + 1
                ' create a rectangle object
                Dim element As RectangleTool = New RectangleTool()
                Dim rect As System.Drawing.Rectangle = New Rectangle(top, left, right, bottom)
                element.BoundingRectangle = rect
                element.PenWidth = 1
                Dim rand As System.Random = New System.Random()
                element.PenColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                element.FillColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                element.ToolTipText = "Rectangle " + i.ToString()
                layer.Elements.Add(element)
                top += 2
                left += 2
                right += 2
                bottom += 2
            Next
        End If
    End Sub

    Private Sub mnu_objects_buttons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_buttons.Click
        '---------------
        ' initialize object location
        '---------------
        Dim top As Integer = 10
        Dim left As Integer = 10
        Dim right As Integer = 100
        Dim bottom As Integer = 35

        '--------------
        ' now using this one programmable element,
        ' create 10 buttons
        '--------------
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim i As Integer
            For i = 1 To 10 Step i + 1
                '---------------
                ' create a programmable element
                '---------------
                Dim element As ButtonTool = New ButtonTool()

                '---------------
                ' set some programmable element properties
                '---------------
                Dim rect As System.Drawing.Rectangle = New Rectangle(top, left, right, bottom)
                element.BoundingRectangle = rect
                element.PenWidth = 4
                element.Text = "Button " + i.ToString()

                element.TextColor = SystemColors.ControlText
                element.BackColor = SystemColors.Control
                element.BevelLightColor = SystemColors.ControlLight
                element.BevelShadowColor = SystemColors.ControlDark

                Dim rand As System.Random = New System.Random()
                Dim x As Integer = New System.Random().NextDouble() * 400
                Dim y As Integer = New System.Random().NextDouble() * 400

                element.ToolTipText = "Button " + i.ToString()

                layer.Elements.Add(element)
                top += 2
                left += 2
                right += 2
                bottom += 2
            Next
        End If
    End Sub

    Private Sub mnu_objects_createtransrect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_createtransrect.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As RectangleTool = New RectangleTool()
            Dim rect As System.Drawing.Rectangle = New Rectangle(100, 100, 200, 200)
            element.BoundingRectangle = rect
            element.FillColor = System.Drawing.Color.Yellow
            element.PenWidth = 1
            element.PenColor = System.Drawing.Color.Blue ' 				element.HighlightFill = True  ****** Outline is "permanent" without Me line */
            element.ToolTipText = "Translucent Rectangle"
            layer.Elements.Add(element)
            layer.Toolbar.Selected = AnnotationTool.PointerTool
        End If
    End Sub

    Private Sub mnu_objects_createImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_createImage.Click
        Try
            Dim layer As Layer = NotateXpress1.Layers.Selected()
            If Not layer Is Nothing Then
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(ImagXpress1, System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "..\..\..\..\..\..\..\Common\Images\vermont.jpg"))

                Dim element As ImageTool = New ImageTool()


                element.DibHandle = ImageXView2.Image.ToHdib(False)
                element.ToolTipText = "Image"
                Dim rect As System.Drawing.Rectangle = New Rectangle(100, 100, 100 + ImageXView2.Image.ImageXData.Width, 100 + ImageXView2.Image.ImageXData.Height)
                element.BoundingRectangle = rect
                layer.Elements.Add(element)
            End If
        Catch ex As Exception
            MessageBox.Show("Error msg:   " + ex.Message + _
             "Error type:  " + ex.GetType().ToString())
        End Try
    End Sub

    Private Sub mnu_objects_nudgeup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_nudgeup.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim i As Integer
            For i = 0 To layer.Elements.Count - 1 Step i + 1
                If layer.Elements(i).Selected Then

                    layer.Elements(i).NudgeUp()

                End If
            Next
        End If
    End Sub

    Private Sub mnu_objects_nudgedn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_nudgedn.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing

                element.NudgeDown()
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_objects_inflate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_inflate.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing

                Dim rect As System.Drawing.Rectangle = element.BoundingRectangle
                Dim i As Integer
                For i = 1 To 100 Step i + 1
                    element.BoundingRectangle = New Rectangle(Math.Max(rect.Left - i, 0), Math.Max(rect.Top - i, 0), rect.Width + i, rect.Height + i)
                    Application.DoEvents()
                Next
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_objects_redaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_redaction.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As TextTool = New TextTool()
            Dim rect As System.Drawing.Rectangle = New Rectangle(100, 100, 300, 200)
            element.BoundingRectangle = rect
            element.TextJustification = TextJustification.Center
            element.TextColor = System.Drawing.Color.Red
            element.BackStyle = BackStyle.Opaque
            element.Text = "Approved by Pegasus Imaging Corp." + System.DateTime.Now
            element.PenWidth = 1
            element.PenColor = System.Drawing.Color.White
            element.BackColor = System.Drawing.Color.White
            element.ToolTipText = "Redaction"
            layer.Elements.Add(element)
            layer.Toolbar.Selected = AnnotationTool.PointerTool
        End If
    End Sub

    Private Sub mnu_objects_AddRulerAnn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_AddRulerAnn.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            'set the annotation type to the Ruler annotation
            Dim element As RulerTool = New RulerTool()
            Dim rect As System.Drawing.Rectangle = New Rectangle(25, 50, 200, 100)
            element.BoundingRectangle = rect
            element.PenWidth = 4
            element.GaugeLength = 20
            element.MeasurementUnit = MeasurementUnit.Inches
            element.Precision = 1
            element.ToolTipText = "Ruler"
            layer.Elements.Add(element)
            element.Selected = True
        End If
    End Sub

    Private Sub mnu_objects_largetext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_largetext.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As TextTool = New TextTool()
            Dim rect As System.Drawing.Rectangle = New Rectangle(30, 30, 280, 300)
            element.BoundingRectangle = rect
            element.TextJustification = TextJustification.Center
            element.BackColor = System.Drawing.Color.FromArgb(255, 255, 0)
            element.TextColor = System.Drawing.Color.FromArgb(0, 0, 0)
            element.BackStyle = BackStyle.Translucent
            element.PenWidth = 2
            element.PenColor = System.Drawing.Color.FromArgb(255, 0, 0)

            Dim strText As String = "NotateXpress Version 9\n\n"
            strText += "NotateXpress version 9's programmatic annotation features let you enter, edit, and query all types of annotation objects programmatically. "
            strText += "Using NotateXpress's programmatic capabilities is fun, fascinating, and has a New Ruler Tool! Try it today."

            element.Text = strText
            Dim fontNew As System.Drawing.Font = New System.Drawing.Font("Times New Roman", 12)
            element.TextFont = fontNew
            element.ToolTipText = "Large Text"
            layer.Elements.Add(element)
        End If
    End Sub

    Private Sub mnu_objects_useMLE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_useMLE.Click
        NotateXpress1.MultiLineEdit = Not NotateXpress1.MultiLineEdit
        mnu_objects_useMLE.Checked = NotateXpress1.MultiLineEdit
    End Sub

    Private Sub mnu_objects_brand_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_brand.Click
        NotateXpress1.Layers.Brand(24)
    End Sub

    Private Sub menuItemCreateNewNX8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_CreateNewNX8.Click
        NotateXpress1.Layers.Clear()
        ' 'detach' existing NX8 from the IX window
        NotateXpress1.ClientWindow = IntPtr.Zero
        ' create new NX8 component
        NotateXpress1 = New PegasusImaging.WinForms.NotateXpress9.NotateXpress()
        ' init default values for new NX8 component
        NotateXpress1.ClientWindow = ImageXView1.Handle
        NotateXpress1.AllowPaint = True
        NotateXpress1.Debug = False
        NotateXpress1.DebugLogFile = "c:\\NotateXpress9.log"
        NotateXpress1.DebugErrorLevel = PegasusImaging.WinForms.NotateXpress9.ErrorLevelInfo.Production
        NotateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress9.FontScaling.Normal
        NotateXpress1.ImagXpressLoad = True
        NotateXpress1.ImagXpressSave = True
        NotateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress9.AnnotationMode.Edit
        NotateXpress1.MultiLineEdit = False
        ' create a layer for the new NX8 component
        createNewLayer("Layer1", ImageXView2.Image.ToHdib(False))
    End Sub

    Private Sub mnu_orient_ltbr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_orient_ltbr.Click
        Processor1.Image = ImageXView1.Image
        Processor1.Rotate(270)
    End Sub

    Private Sub mnu_orient_brtl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_orient_brtl.Click
        Processor1.Image = ImageXView1.Image
        Processor1.Flip()
    End Sub

    Private Sub mnu_orient_rblt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_orient_rblt.Click
        Processor1.Image = ImageXView1.Image
        Processor1.Rotate(90)
    End Sub

    Private Sub mnu_orient_tlbr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_orient_tlbr.Click
        Processor1.Image = ImageXView1.Image
        Processor1.Mirror()
    End Sub

    Private Sub mnu_orientation_rotate180_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_orientation_rotate180.Click
        Processor1.Image = ImageXView1.Image
        Processor1.Rotate(180)
    End Sub

    Private Sub mnu_stuff_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnu_stuff.Select

        '  ------------
        '   disable all menu selections on this menu
        '  ------------
        mnu_BoundingRectangle.Enabled = False
        mnu_stuff_backcolor.Enabled = False
        mnu_stuff_bevellight.Enabled = False
        mnu_stuff_bevelshadow.Enabled = False
        mnu_stuff_getitemfont.Enabled = False
        mnu_stuff_getitemtext.Enabled = False
        mnu_stuff_hb.Enabled = False
        mnu_stuff_hf.Enabled = False
        mnu_stuff_moveable.Enabled = False
        mnu_stuff_fixed.Enabled = False
        mnu_stuff_pencolor.Enabled = False
        mnu_stuff_penwidth.Enabled = False
        mnu_stuff_setfont.Enabled = False
        mnu_stuff_setnewdib.Enabled = False
        mnu_stuff_settext.Enabled = False
        mnu_stuff_sizeable.Enabled = False
        mnu_stuff_toggleuserdraw.Enabled = False
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            '-----------------
            ' Enable menu items as appropriate for this menu
            '-----------------
            Dim element As Element = layer.Elements.FirstSelected
            ' only one element must be selected to enable
            If ((Not (element) Is Nothing) _
               AndAlso (layer.Elements.NextSelected Is Nothing)) Then
                mnu_BoundingRectangle.Enabled = True
                mnu_stuff_settext.Enabled = HasProperty(element, "Text")
                mnu_stuff_backcolor.Enabled = HasProperty(element, "BackColor")
                mnu_stuff_pencolor.Enabled = HasProperty(element, "PenColor")
                mnu_stuff_penwidth.Enabled = HasProperty(element, "PenWidth")
                mnu_stuff_bevelshadow.Enabled = HasProperty(element, "BevelShadowColor")
                mnu_stuff_bevellight.Enabled = HasProperty(element, "BevelLightColor")
                mnu_stuff_setfont.Enabled = HasProperty(element, "TextFont")
                mnu_stuff_getitemtext.Enabled = HasProperty(element, "Text")
                mnu_stuff_hb.Enabled = HasProperty(element, "HighlightBack")
                mnu_stuff_hf.Enabled = HasProperty(element, "HighlightFill")
                mnu_stuff_moveable.Enabled = HasProperty(element, "Moveable")
                mnu_stuff_fixed.Enabled = HasProperty(element, "Fixed")
                mnu_stuff_sizeable.Enabled = HasProperty(element, "Sizeable")
                mnu_stuff_getitemfont.Enabled = HasProperty(element, "TextFont")
                mnu_stuff_setnewdib.Enabled = HasProperty(element, "DibHandle")
                mnu_stuff_toggleuserdraw.Enabled = HasProperty(element, "UserDraw")
                If mnu_stuff_toggleuserdraw.Enabled Then
                    mnu_stuff_toggleuserdraw.Checked = CType(GetPropertyValue(element, "UserDraw"), Boolean)
                End If
            End If
        End If

    End Sub

    Private Sub menuItemBoundingRectangle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_BoundingRectangle.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            '-----------------
            ' Enable menu items as appropriate for this menu
            '-----------------
            Dim element As Element = layer.Elements.FirstSelected()
            ' only one element must be selected to enable
            If Not element Is Nothing And layer.Elements.NextSelected() Is Nothing Then
                Dim dlg As BoundingRectDlg = New BoundingRectDlg(element.BoundingRectangle)
                If System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me) Then
                    element.BoundingRectangle = dlg.Rect
                End If
            End If
        End If
    End Sub

    Private Sub mnu_stuff_settext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_settext.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim NewText As String = "This is a test and only a test. Had this been something other than a test, something significant would have happened!"
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                SetPropertyValue(element, "Text", NewText)
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_backcolor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_backcolor.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                SetPropertyValue(element, "BackColor", System.Drawing.Color.FromArgb(0, 255, 0))
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_pencolor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_pencolor.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                SetPropertyValue(element, "PenColor", System.Drawing.Color.FromArgb(0, 255, 0))
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_penwidth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_penwidth.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim NewWidth As System.Int32 = 15
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                SetPropertyValue(element, "PenWidth", NewWidth)
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_bevelshadow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_bevelshadow.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                SetPropertyValue(element, "BevelShadowColor", System.Drawing.Color.FromArgb(0, 255, 0))
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_bevellight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_bevellight.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                SetPropertyValue(element, "BevelLightColor", System.Drawing.Color.FromArgb(0, 255, 255))
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_setfont_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_setfont.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim font As System.Drawing.Font = Nothing
            ' only perform on first selected items
            Dim element As Element = layer.Elements.FirstSelected()
            If Not element Is Nothing Then
                font = CType(GetPropertyValue(element, "TextFont"), Font)
                If Not font Is Nothing Then
                    FontDialog1.Font = font
                End If

                If FontDialog1.ShowDialog(Me) <> DialogResult.Cancel Then
                    font = FontDialog1.Font
                Else
                    Return ' font dialog canceled
                End If
            End If
            While Not element Is Nothing
                SetPropertyValue(element, "TextFont", font)
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_getitemtext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_getitemtext.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            ' only perform on first selected element
            Dim element As Object = layer.Elements.FirstSelected()
            If Not element Is Nothing Then
                Dim obj As Object = GetPropertyValue(element, "Text")
                If Not obj Is Nothing Then
                    System.Windows.Forms.MessageBox.Show(CType(obj, String))
                End If
            End If
        End If
    End Sub

    Private Sub mnu_stuff_hb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_hb.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                Dim obj As Object = GetPropertyValue(Me, "HighlightBack")
                If Not obj Is Nothing Then
                    Dim b As Boolean = CType(obj, Boolean)
                    b = Not b
                    SetPropertyValue(Me, "HighlightBack", b)
                End If
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_hf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_hf.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                Dim obj As Object = GetPropertyValue(element, "HighlightFill")
                If Not obj Is Nothing Then
                    Dim b As Boolean = CType(obj, Boolean)
                    b = Not b
                    SetPropertyValue(element, "HighlightFill", b)
                End If
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_moveable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_moveable.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                Dim obj As Object = GetPropertyValue(element, "Moveable")
                If Not obj Is Nothing Then
                    Dim b As Boolean = CType(obj, Boolean)
                    b = Not b
                    SetPropertyValue(element, "Moveable", b)
                End If
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_fixed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_fixed.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                Dim obj As Object = GetPropertyValue(element, "Fixed")
                If Not obj Is Nothing Then
                    Dim b As Boolean = CType(obj, Boolean)
                    b = Not b
                    SetPropertyValue(element, "Fixed", b)
                End If
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_sizeable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_sizeable.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                Dim obj As Object = GetPropertyValue(element, "Sizeable")
                If Not obj Is Nothing Then
                    Dim b As Boolean = CType(obj, Boolean)
                    b = Not b
                    SetPropertyValue(element, "Sizeable", b)
                End If
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_getitemfont_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_getitemfont.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            ' only perform on first selected element
            Dim element As Object = layer.Elements.FirstSelected()
            If Not element Is Nothing Then
                Dim obj As Object = GetPropertyValue(element, "TextFont")
                If Not obj Is Nothing Then
                    Dim font As System.Drawing.Font = CType(obj, System.Drawing.Font)
                    MessageBox.Show(font.Name + " " + font.SizeInPoints.ToString())
                End If
            End If
        End If
    End Sub

    Private Sub mnu_stuff_setnewdib_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_setnewdib.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Try
                Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()
                strImageFileName = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\..\Common\Images\Water.jpg")
                LoadFile(ImageXView2)
                If Not ImageXView2.Image Is Nothing Then
                    If layer.Toolbar.ImageToolbarDefaults.DibHandle.ToInt32 = 0 Then


                        GlobalFree(CType(layer.Toolbar.ImageToolbarDefaults.DibHandle, System.IntPtr))
                        layer.Toolbar.ImageToolbarDefaults.DibHandle = System.IntPtr.Zero
                    End If

                    layer.Toolbar.ImageToolbarDefaults.DibHandle = ImageXView2.Image.ToHdib(False)
                End If
            Catch ex As Exception
                PegasusError(ex, lblerror)
            End Try
        End If
    End Sub

    Private Sub mnu_stuff_toggleuserdraw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_toggleuserdraw.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            Dim element As Element = layer.Elements.FirstSelected
            ' only one element must be selected
            If (((Not (element) Is Nothing) And TypeOf element Is RectangleTool _
               And (layer.Elements.NextSelected Is Nothing))) Then
                Dim rectTool As RectangleTool = CType(element, RectangleTool)
                rectTool.UserDraw = Not rectTool.UserDraw
            End If
        End If


    End Sub

    Private Function HasProperty(ByVal obj As Object, ByVal strPropName As String) As Boolean
        Dim piProperty As System.Reflection.PropertyInfo
        For Each piProperty In obj.GetType().GetProperties()
            If piProperty.Name = strPropName Then
                Return True
            End If
        Next

        Return False
    End Function


    Private Function GetPropertyValue(ByVal obj As Object, ByVal strPropName As String) As Object
        Dim piProperty As System.Reflection.PropertyInfo
        For Each piProperty In obj.GetType().GetProperties()
            If piProperty.Name = strPropName Then
                Return piProperty.GetValue(obj, Nothing)
            End If
        Next

        Return Nothing
    End Function


    Private Function SetPropertyValue(ByVal obj As Object, ByVal strPropName As String, ByVal objValue As Object) As Boolean
        Dim piProperty As System.Reflection.PropertyInfo


        For Each piProperty In obj.GetType().GetProperties()
            If (piProperty.Name = strPropName) Then
                piProperty.SetValue(obj, objValue, Nothing)
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub mnu_stuff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff.Click
        '  ------------
        '   disable all menu selections on this menu
        '  ------------
        mnu_BoundingRectangle.Enabled = False
        mnu_stuff_backcolor.Enabled = False
        mnu_stuff_bevellight.Enabled = False
        mnu_stuff_bevelshadow.Enabled = False
        mnu_stuff_getitemfont.Enabled = False
        mnu_stuff_getitemtext.Enabled = False
        mnu_stuff_hb.Enabled = False
        mnu_stuff_hf.Enabled = False
        mnu_stuff_moveable.Enabled = False
        mnu_stuff_fixed.Enabled = False
        mnu_stuff_pencolor.Enabled = False
        mnu_stuff_penwidth.Enabled = False
        mnu_stuff_setfont.Enabled = False
        mnu_stuff_setnewdib.Enabled = False
        mnu_stuff_settext.Enabled = False
        mnu_stuff_sizeable.Enabled = False

        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            '-----------------
            ' Enable menu items as appropriate for this menu
            '-----------------
            Dim element As Element = layer.Elements.FirstSelected()
            ' only one element must be selected to enable
            If Not element Is Nothing And layer.Elements.NextSelected() Is Nothing Then
                mnu_BoundingRectangle.Enabled = True
                mnu_stuff_settext.Enabled = HasProperty(element, "Text")
                mnu_stuff_backcolor.Enabled = HasProperty(element, "BackColor")
                mnu_stuff_pencolor.Enabled = HasProperty(element, "PenColor")
                mnu_stuff_penwidth.Enabled = HasProperty(element, "PenWidth")
                mnu_stuff_bevelshadow.Enabled = HasProperty(element, "BevelShadowColor")
                mnu_stuff_bevellight.Enabled = HasProperty(element, "BevelLightColor")
                mnu_stuff_setfont.Enabled = HasProperty(element, "TextFont")
                mnu_stuff_getitemtext.Enabled = HasProperty(element, "Text")
                mnu_stuff_hb.Enabled = HasProperty(element, "HighlightBack")
                mnu_stuff_hf.Enabled = HasProperty(element, "HighlightFill")
                mnu_stuff_moveable.Enabled = HasProperty(element, "Moveable")
                mnu_stuff_fixed.Enabled = HasProperty(element, "Fixed")
                mnu_stuff_sizeable.Enabled = HasProperty(element, "Sizeable")
                mnu_stuff_getitemfont.Enabled = HasProperty(element, "TextFont")
                mnu_stuff_setnewdib.Enabled = HasProperty(element, "DibHandle")
            End If
        End If

    End Sub


    Private Sub mnu_mode_editmode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_mode_editmode.Click
        NotateXpress1.InteractMode = AnnotationMode.Edit
        mnu_mode_interactive.Checked = False
        mnu_mode_editmode.Checked = True

    End Sub

    Private Sub mnu_mode_interactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_mode_interactive.Click
        NotateXpress1.InteractMode = AnnotationMode.Interactive
        mnu_mode_interactive.Checked = True
        mnu_mode_editmode.Checked = False

    End Sub

    Private Sub mnu_cm_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnu_cm.Select
        If NotateXpress1.MenuGetEnabled(MenuType.Context, AnnotationTool.NoTool) Then
            mnu_cm_enable.Text = "Disable"
        Else
            mnu_cm_enable.Text = "Enable"
        End If


    End Sub

    Private Sub menuItemGroups_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnu_Groups.Select
        mnu_objects_allowgrouping.Enabled = False
        mnu_CreateGroup.Enabled = False
        mnu_DeleteGroup.Enabled = False
        mnu_SetGroupEmpty.Enabled = False
        mnu_SelectGroupItems.Enabled = False
        mnu_GroupAddSelectedItems.Enabled = False
        mnu_GroupRemoveSelectedItems.Enabled = False
        mnu_YokeGroupItems.Enabled = False
        mnu_SetGroupUserString.Enabled = False
        mnu_MirrorYoked.Enabled = False
        mnu_InvertYoked.Enabled = False
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            mnu_objects_allowgrouping.Enabled = True
            mnu_objects_allowgrouping.Checked = layer.Groups.AllowUserGrouping
            If Not mnu_objects_allowgrouping.Checked Then
                Return
            End If
            mnu_CreateGroup.Enabled = True
            If (layer.Groups.Count > 0) Then
                ' ---------------
                '  Initialize delete group sub-menu
                ' ---------------
                mnu_DeleteGroup.Enabled = True
                '  clean groups sub-menu      
                mnu_DeleteGroup.MenuItems.Clear()

                Dim menuItemDeleteGroup_eventhandler As System.EventHandler = AddressOf Me.menuItemDeleteGroup_Click
                Dim i As Integer = 0
                Do While (i < layer.Groups.Count)
                    ' creates new MenuItem
                    mnu_DeleteGroup.MenuItems.Add(layer.Groups(i).Name)

                    ' set event handler for new MenuItem                
                    AddHandler mnu_DeleteGroup.MenuItems(i).Click, menuItemDeleteGroup_eventhandler ' (menuItemDeleteGroup.MenuItems(i).Click + menuItemDeleteGroup_eventhandler)

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize set group empty sub-menu
                ' ---------------
                mnu_SetGroupEmpty.Enabled = True
                '  clean groups sub-menu      
                mnu_SetGroupEmpty.MenuItems.Clear()

                Dim menuItemSetGroupEmpty_eventhandler As System.EventHandler = AddressOf Me.menuItemSetGroupEmpty_Click
                i = 0
                Do While (i < layer.Groups.Count)
                    Dim str As String = (layer.Groups(i).Name + ("  #Items: " + layer.Groups(i).GroupElements.Count.ToString))
                    mnu_SetGroupEmpty.MenuItems.Add(str)
                    ' creates new MenuItem
                    mnu_SetGroupEmpty.MenuItems(i).Enabled = (layer.Groups(i).GroupElements.Count > 0)

                    ' set event handler for new MenuItem
                    AddHandler mnu_SetGroupEmpty.MenuItems(i).Click, menuItemSetGroupEmpty_eventhandler
                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize select group items sub-menu
                ' ---------------
                mnu_SelectGroupItems.Enabled = True
                '  clean groups sub-menu   
                mnu_SelectGroupItems.MenuItems.Clear()

                Dim menuItemSelectGroupItems_eventhandler As System.EventHandler = AddressOf Me.menuItemSelectGroupItems_Click
                i = 0
                Do While (i < layer.Groups.Count)

                    ' creates new MenuItem
                    mnu_SelectGroupItems.MenuItems.Add(layer.Groups(i).Name)

                    mnu_SelectGroupItems.MenuItems(i).Enabled = (layer.Groups(i).GroupElements.Count > 0)

                    ' set event handler for new MenuItem     
                    AddHandler mnu_SelectGroupItems.MenuItems(i).Click, menuItemSelectGroupItems_eventhandler

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize add selected items to group
                ' ---------------
                mnu_GroupAddSelectedItems.Enabled = (Not (layer.Elements.FirstSelected) Is Nothing)
                '  clean groups sub-menu      
                mnu_GroupAddSelectedItems.MenuItems.Clear()

                Dim menuItemGroupAddSelectedItems_eventhandler As System.EventHandler = AddressOf Me.menuItemGroupAddSelectedItems_Click
                i = 0
                Do While (i < layer.Groups.Count)

                    ' creates new MenuItem
                    mnu_GroupAddSelectedItems.MenuItems.Add(layer.Groups(i).Name)

                    ' set event handler for new MenuItem                
                    AddHandler mnu_GroupAddSelectedItems.MenuItems(i).Click, menuItemGroupAddSelectedItems_eventhandler

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize remove selected items from group
                ' ---------------
                mnu_GroupRemoveSelectedItems.Enabled = (Not (layer.Elements.FirstSelected) Is Nothing)
                '  clean groups sub-menu      
                mnu_GroupRemoveSelectedItems.MenuItems.Clear()

                Dim menuItemGroupRemoveSelectedItems_eventhandler As System.EventHandler = AddressOf Me.menuItemGroupRemoveSelectedItems_Click
                i = 0
                Do While (i < layer.Groups.Count)
                    mnu_GroupRemoveSelectedItems.MenuItems.Add(layer.Groups(i).Name)
                    ' creates new MenuItem
                    mnu_GroupRemoveSelectedItems.MenuItems(i).Enabled = (layer.Groups(i).GroupElements.Count > 0)

                    ' set event handler for new MenuItem                
                    AddHandler mnu_GroupRemoveSelectedItems.MenuItems(i).Click, menuItemGroupRemoveSelectedItems_eventhandler

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize yoke group items sub-menu
                ' ---------------
                mnu_YokeGroupItems.Enabled = True
                mnu_YokeGroupItems.MenuItems.Clear()
                '  clean groups sub-menu      
                Dim menuItemYokeGroupItems_eventhandler As System.EventHandler = AddressOf Me.menuItemYokeGroupItems_Click
                i = 0
                Do While (i < layer.Groups.Count)

                    ' creates new MenuItem
                    mnu_YokeGroupItems.MenuItems.Add(layer.Groups(i).Name)

                    mnu_YokeGroupItems.MenuItems(i).Checked = layer.Groups(i).Yoked

                    ' set event handler for new MenuItem                
                    AddHandler mnu_YokeGroupItems.MenuItems(i).Click, menuItemYokeGroupItems_eventhandler

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize invert group items Yoked sub-menu
                ' ---------------
                mnu_InvertYoked.Enabled = True
                '  clean groups sub-menu   
                mnu_InvertYoked.MenuItems.Clear()

                Dim menuItemInvertYoked_eventhandler As System.EventHandler = AddressOf Me.menuItemInvertYoked_Click
                i = 0
                Do While (i < layer.Groups.Count)

                    ' creates new MenuItem
                    mnu_InvertYoked.MenuItems.Add(layer.Groups(i).Name)

                    mnu_InvertYoked.MenuItems(i).Enabled = layer.Groups(i).Yoked

                    ' set event handler for new MenuItem                
                    AddHandler mnu_InvertYoked.MenuItems(i).Click, menuItemInvertYoked_eventhandler

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize Mirror group items Yoked sub-menu
                ' ---------------
                mnu_MirrorYoked.Enabled = True

                '  clean groups sub-menu  
                mnu_MirrorYoked.MenuItems.Clear()

                Dim menuItemMirrorYoked_eventhandler As System.EventHandler = AddressOf Me.menuItemMirrorYoked_Click
                i = 0
                Do While (i < layer.Groups.Count)
                    mnu_MirrorYoked.MenuItems.Add(layer.Groups(i).Name)
                    ' creates new MenuItem
                    mnu_MirrorYoked.MenuItems(i).Enabled = layer.Groups(i).Yoked

                    ' set event handler for new MenuItem                
                    AddHandler mnu_MirrorYoked.MenuItems(i).Click, menuItemMirrorYoked_eventhandler

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize group User String sub-menu
                ' ---------------
                mnu_SetGroupUserString.Enabled = True

                ' clean groups sub-menu
                mnu_SetGroupUserString.MenuItems.Clear()

                Dim SetGroupUserString_eventhandler As System.EventHandler = AddressOf Me.menuItemSetGroupUserString_select
                i = 0
                Do While (i < layer.Groups.Count)

                    ' creates new MenuItem
                    mnu_SetGroupUserString.MenuItems.Add(layer.Groups(i).Name)

                    ' set event handler for new MenuItem                
                    AddHandler mnu_SetGroupUserString.MenuItems(i).Click, SetGroupUserString_eventhandler

                    i = (i + 1)
                Loop
            End If
        End If
    End Sub

    Private Sub mnu_objects_allowgrouping_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_allowgrouping.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (layer Is Nothing) Then
            MessageBox.Show("A Layer must be selected before a Group operation can be performed")
            Return
        End If
        Dim bUserGrouping As Boolean = Not layer.Groups.AllowUserGrouping
        layer.Groups.AllowUserGrouping = bUserGrouping
        mnu_objects_allowgrouping.Checked = bUserGrouping
    End Sub

    Private Sub menuItemGroupAddSelectedItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_GroupAddSelectedItems.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (((Not (layer) Is Nothing) _
           AndAlso TypeOf sender _
           Is System.Windows.Forms.MenuItem)) Then
            Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
            Dim element As Element = layer.Elements.FirstSelected

            While (Not (element) Is Nothing)
                If element.Selected Then
                    group.GroupElements.Add(element)
                End If
                element = layer.Elements.NextSelected

            End While
        End If

    End Sub


    Private Sub menuItemCreateGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_CreateGroup.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (layer Is Nothing) Then
            MessageBox.Show("A Layer must be selected before a Group operation can be performed")
            Return
        End If
        Dim dlg As InputDlg = New InputDlg("Specify Name of Group to create")
        If (System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me)) Then
            Dim group As PegasusImaging.WinForms.NotateXpress9.Group = New PegasusImaging.WinForms.NotateXpress9.Group()
            group.Name = dlg.InputText
            layer.Groups.Add(group)
        End If
    End Sub

    Private Sub menuItemDeleteGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_DeleteGroup.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            If (TypeOf sender Is System.Windows.Forms.MenuItem) Then
                layer.Groups.RemoveAt(CType(sender, System.Windows.Forms.MenuItem).Index)
            End If
        End If

    End Sub


    Private Sub menuItemSetGroupEmpty_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_SetGroupEmpty.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            If (TypeOf sender Is System.Windows.Forms.MenuItem) Then
                Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
                group.GroupElements.Clear()
            End If
        End If

    End Sub

    Private Sub menuItemSelectGroupItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_SelectGroupItems.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            If (TypeOf sender Is System.Windows.Forms.MenuItem) Then
                Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
                ' first clear any currently selected items in the layer
                Dim element As Element = layer.Elements.FirstSelected

                While (Not (element) Is Nothing)
                    element.Selected = False
                    element = layer.Elements.NextSelected

                End While
                ' now select all items that are in the group
                Dim i As Integer = 0
                Do While (i < group.GroupElements.Count)
                    group.GroupElements(i).Selected = True
                    i = (i + 1)
                Loop
            End If
        End If

    End Sub


    Private Sub menuItemGroupRemoveSelectedItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_GroupRemoveSelectedItems.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (((Not (layer) Is Nothing) _
           AndAlso TypeOf sender _
           Is System.Windows.Forms.MenuItem)) Then
            Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
            Dim element As Element = layer.Elements.FirstSelected

            While (Not (element) Is Nothing)
                If element.Selected Then
                    group.GroupElements.Remove(element)
                End If
                element = layer.Elements.NextSelected

            End While
        End If

    End Sub

    Private Sub menuItemYokeGroupItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_YokeGroupItems.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (((Not (layer) Is Nothing) _
           AndAlso TypeOf sender _
           Is System.Windows.Forms.MenuItem)) Then
            Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
            group.Yoked = Not group.Yoked
        End If

    End Sub

    Private Sub menuItemMirrorYoked_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_MirrorYoked.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected

        If (((Not (layer) Is Nothing) _
           AndAlso TypeOf sender _
           Is System.Windows.Forms.MenuItem)) Then
            Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
            group.GroupElements.GroupMirror()
        End If

    End Sub

    Private Sub menuItemInvertYoked_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_InvertYoked.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (((Not (layer) Is Nothing) _
         AndAlso TypeOf sender _
           Is System.Windows.Forms.MenuItem)) Then
            Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
            group.GroupElements.GroupInvert()


        End If

    End Sub

    Private Sub menuItemSetGroupUserString_select(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_SetGroupUserString.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (((Not (layer) Is Nothing) _
           AndAlso TypeOf sender _
           Is System.Windows.Forms.MenuItem)) Then
            Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
            Dim dlg As InputDlg = New InputDlg(("User String for Group: " + group.Name))
            dlg.InputText = group.UserString
            If (System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me)) Then
                group.UserString = dlg.InputText
            End If
        End If

    End Sub

    Private Sub mnu_cm_enable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_cm_enable.Click
        NotateXpress1.MenuSetEnabled(MenuType.Context, AnnotationTool.NoTool, Not NotateXpress1.MenuGetEnabled(MenuType.Context, AnnotationTool.NoTool))
    End Sub

    Private Sub menuItemToolBar_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnu_ToolBar.Select
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then

            Dim bEnabled As Boolean = layer.Toolbar.Visible
            mnu_ToolbarRulerTool.Enabled = bEnabled
            mnu_ToolbarFreehandTool.Enabled = bEnabled
            mnu_ToolbarEllipseTool.Enabled = bEnabled
            mnu_ToolbarTextTool.Enabled = bEnabled
            mnu_ToolbarRectangleTool.Enabled = bEnabled
            mnu_ToolbarStampTool.Enabled = bEnabled
            mnu_ToolbarPolylineTool.Enabled = bEnabled
            mnu_ToolbarLineTool.Enabled = bEnabled
            mnu_ToolbarImageTool.Enabled = bEnabled
            mnu_ToolbarButtonTool.Enabled = bEnabled
            mnu_ToolbarPolygonTool.Enabled = bEnabled
            mnu_RulerToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.RulerTool)
            mnu_RulerToolEnabeled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.RulerTool)
            mnu_FreehandToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.FreehandTool)
            mnu_FreehandToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.FreehandTool)
            mnu_EllipseToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.EllipseTool)
            mnu_EllipseToolEnabeld.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.EllipseTool)
            mnu_TextToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.TextTool)
            mnu_TextToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.TextTool)
            mnu_RectangleToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.RectangleTool)
            mnu_RectangleToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.RectangleTool)
            mnu_StampToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.StampTool)
            mnu_StampToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.StampTool)
            mnu_PolylineToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.PolyLineTool)
            mnu_PolylineToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.PolyLineTool)
            mnu_LineToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.LineTool)
            mnu_LineToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.LineTool)
            mnu_ImageToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.ImageTool)
            mnu_ImageToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.ImageTool)
            mnu_ButtonToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.ButtonTool)
            mnu_ButtonToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.ButtonTool)
            mnu_PolygonToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.PolygonTool)
            mnu_PolygonToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.PolygonTool)
            mnu_BlockHighlightEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.BlockHighlightTool)
            mnu_BlockHighlightVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.BlockHighlightTool)
            mnu_NoteToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.NoteTool)
            mnu_NoteToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.NoteTool)
        End If
    End Sub

    Private Sub menuItemRulerToolEnabeled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_RulerToolEnabeled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.RulerTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.RulerTool))
        End If
    End Sub

    Private Sub menuItemRulerToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_RulerToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.RulerTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.RulerTool))
        End If
    End Sub


    Private Sub menuItemFreehandToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_FreehandToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.FreehandTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.FreehandTool))
        End If
    End Sub

    Private Sub menuItemFreehandToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_FreehandToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.FreehandTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.FreehandTool))
        End If
    End Sub

    Private Sub menuItemEllipseToolEnabeld_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_EllipseToolEnabeld.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.EllipseTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.EllipseTool))
        End If
    End Sub

    Private Sub menuItemEllipseToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_EllipseToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.EllipseTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.EllipseTool))
        End If
    End Sub

    Private Sub menuItemTextToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_TextToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.TextTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.TextTool))
        End If
    End Sub

    Private Sub menuItemTextToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_TextToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.TextTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.TextTool))
        End If
    End Sub

    Private Sub menuItemRectangleToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_RectangleToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.RectangleTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.RectangleTool))
        End If
    End Sub

    Private Sub menuItemRectangleToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_RectangleToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.RectangleTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.RectangleTool))
        End If
    End Sub

    Private Sub menuItemStampToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_StampToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.StampTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.StampTool))
        End If
    End Sub

    Private Sub menuItemStampToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_StampToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.StampTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.StampTool))
        End If
    End Sub

    Private Sub menuItemPolylineToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_PolylineToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.PolyLineTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.PolyLineTool))
        End If
    End Sub

    Private Sub menuItemPolylineToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_PolylineToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.PolyLineTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.PolyLineTool))
        End If
    End Sub

    Private Sub menuItemLineToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_LineToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.LineTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.LineTool))
        End If
    End Sub

    Private Sub menuItemLineToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_LineToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.LineTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.LineTool))
        End If
    End Sub

    Private Sub menuItemImageToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_ImageToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.ImageTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.ImageTool))
        End If
    End Sub

    Private Sub menuItemImageToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_ImageToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.ImageTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.ImageTool))
        End If
    End Sub

    Private Sub menuItemButtonToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_ButtonToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.ButtonTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.ButtonTool))
        End If
    End Sub

    Private Sub menuItemButtonToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_ButtonToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.ButtonTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.ButtonTool))
        End If
    End Sub

    Private Sub menuItemPolygonToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_PolygonToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.PolygonTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.PolygonTool))
        End If
    End Sub

    Private Sub menuItemPolygonToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_PolygonToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.PolygonTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.PolygonTool))
        End If
    End Sub


    Private Sub menuItemAboutNX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_AboutNX.Click
        NotateXpress1.AboutBox()
    End Sub

    Private Sub menuItemAboutIX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_AboutIX.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub NotateXpress1_AnnotationAdded(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.AnnotationAddedEventArgs) Handles NotateXpress1.AnnotationAdded
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
        End If
        If Not (TypeOf sender Is PegasusImaging.WinForms.NotateXpress9.NotateXpress) Then
            MessageBox.Show(("sender is " + sender.GetType.ToString))
        End If
        ' entry has been added
        OutputList.Items.Insert(0, ("Add event for" _
         + (strAnnotationType + " annotation")))
        If (_bAutoSelect AndAlso (Not (element) Is Nothing)) Then
            element.Selected = True
        End If
    End Sub

    Private Sub NotateXpress1_AnnotationDeleted(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.AnnotationDeletedEventArgs) Handles NotateXpress1.AnnotationDeleted
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        Dim layer As Layer = NotateXpress1.Layers.Selected

        Try
            If (Not (layer) Is Nothing) Then
                Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
                If (i = -1) Then
                    strAnnotationType = (strAnnotationType + element.GetType.ToString)
                Else
                    strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
                End If

                If (_bAutoSelect _
                    AndAlso (Not (element) Is Nothing)) Then
                    element.Selected = True
                End If
            End If
            If Not (TypeOf sender Is PegasusImaging.WinForms.NotateXpress9.NotateXpress) Then
                MessageBox.Show(("sender is " + sender.GetType.ToString))
            End If
            ' entry has been deleted
            OutputList.Items.Insert(0, ("Delete event for" _
             + (strAnnotationType + " annotation")))

        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try

    End Sub

    Private Sub NotateXpress1_AnnotationMoved(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.AnnotationMovedEventArgs) Handles NotateXpress1.AnnotationMoved
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
        End If
        If Not (TypeOf sender Is PegasusImaging.WinForms.NotateXpress9.NotateXpress) Then
            MessageBox.Show(("sender is " + sender.GetType.ToString))
        End If
        ' entry has been moved
        OutputList.Items.Insert(0, ("Move event for" _
         + (strAnnotationType + " annotation: " + e.Element.BoundingRectangle.ToString())))
        If (_bAutoSelect _
           AndAlso (Not (element) Is Nothing)) Then
            element.Selected = True
        End If
    End Sub

    Private Sub NotateXpress1_AnnotationSelected(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.AnnotationSelectedEventArgs) Handles NotateXpress1.AnnotationSelected
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
        End If

        If Not (TypeOf sender Is PegasusImaging.WinForms.NotateXpress9.NotateXpress) Then
            MessageBox.Show(("sender is " + sender.GetType.ToString))
        End If
        ' entry has been selected
        OutputList.Items.Insert(0, ("Selected event for" _
         + (strAnnotationType + " annotation")))
        '   if (_bAutoSelect && element != null)
        'select newly selected layer element
        'element.Selected = true;
    End Sub


    Private Sub NotateXpress1_Click(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.ClickEventArgs) Handles NotateXpress1.Click
        Dim element As Element = e.Element
        Dim layer As Layer = e.Layer
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
           AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
        End If
        OutputList.Items.Insert(0, (strAnnotationType + (" annotation clicked on, layer name = " _
         + (layer.Name + (", layer description = " + layer.Description)))))
    End Sub

    Private Sub NotateXpress1_MouseDown(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.MouseDownEventArgs) Handles NotateXpress1.MouseDown
        Dim layer As Layer = e.Layer
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
           AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
            OutputList.Items.Insert(0, ("       X coorinate = " _
             + (e.X.ToString + (", Y coordinate = " + e.Y.ToString))))
            OutputList.Items.Insert(0, ("Mouse down event on " _
             + (strAnnotationType + (", current layer name = " _
             + (layer.Name + (", description = " + layer.Description))))))
        End If
    End Sub

    Private Sub NotateXpress1_MouseMove(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.MouseMoveEventArgs) Handles NotateXpress1.MouseMove
        Dim layer As Layer = e.Layer
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
           AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
            OutputList.Items.Insert(0, ("       X coorinate = " _
             + (e.X.ToString + (", Y coordinate = " + e.Y.ToString))))
            OutputList.Items.Insert(0, ("Mouse move event on " _
             + (strAnnotationType + (", current layer name = " _
             + (layer.Name + (", description = " + layer.Description))))))
        End If


    End Sub

    Private Sub NotateXpress1_MouseUp(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.MouseUpEventArgs) Handles NotateXpress1.MouseUp
        Dim layer As Layer = e.Layer
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
           AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
            OutputList.Items.Insert(0, ("       X coorinate = " _
             + (e.X.ToString + (", Y coordinate = " + e.Y.ToString))))
            OutputList.Items.Insert(0, ("Mouse up event on " _
             + (strAnnotationType + (", current layer name = " _
             + (layer.Name + (", description = " + layer.Description))))))
        End If


    End Sub

    Private Sub NotateXpress1_ToolbarSelect(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.ToolbarEventArgs) Handles NotateXpress1.ToolbarSelect
        Dim layer As Layer = e.Layer
        If (Not (layer) Is Nothing) Then
            OutputList.Items.Insert(0, ("Toolbar " _
             + (GetAnnotationToolString(e.Tool) + (" selected, layer name = " _
             + (layer.Name + (", description = " + layer.Description))))))
        End If
    End Sub

    Private Sub NotateXpress1_UserDraw(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.UserDrawEventArgs) Handles NotateXpress1.UserDraw
        Dim layer As Layer = e.Layer
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
           AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
            OutputList.Items.Insert(0, ("         Device context = " _
             + (e.HandleDeviceContext.ToString + (", X cooordinate = " _
             + (e.X.ToString + (", Y coordinate = " _
             + (e.Y.ToString + (", Width = " _
             + (e.Width.ToString + (", Height = " + e.Height.ToString))))))))))
            OutputList.Items.Insert(0, ("User draw event on " _
             + (strAnnotationType + (", current layer name = " _
             + (layer.Name + (", description = " + layer.Description))))))
        End If


    End Sub


    Private Sub NotateXpress1_CurrentLayerChange(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.CurrentLayerChangeEventArgs) Handles NotateXpress1.CurrentLayerChange



        Dim layer As Layer = e.Layer
        If (Not (layer) Is Nothing) Then
            OutputList.Items.Insert(0, ("Layer change event, current layer name = " _
             + (layer.Name + (", description = " + layer.Description))))
        End If

    End Sub

    Private Sub NotateXpress1_DoubleClick(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.DoubleClickEventArgs) Handles NotateXpress1.DoubleClick
        Dim layer As Layer = e.Layer
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
           AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
            OutputList.Items.Insert(0, ("Mouse double clidk event on " _
             + (strAnnotationType + (", current layer name = " _
             + (layer.Name + (", description = " + layer.Description))))))
        End If

    End Sub

    Private Sub NotateXpress1_ItemChanged(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.ItemChangedEventArgs) Handles NotateXpress1.ItemChanged



        Dim layer As Layer = e.Layer
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
           AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
            OutputList.Items.Insert(0, ("Item changed event on " _
             + (strAnnotationType + (", current layer name = " _
             + (layer.Name + (", description = " + layer.Description))))))
        End If


    End Sub

    Private Sub NotateXpress1_LayerRestored(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.LayerRestoredEventArgs) Handles NotateXpress1.LayerRestored

        Dim layer As Layer = e.Layer
        If (Not (layer) Is Nothing) Then
            OutputList.Items.Insert(0, ("Layer restored event on layer name = " _
             + (layer.Name + (", description = " + layer.Description))))
        End If
    End Sub

    Private Sub NotateXpress1_RequestLayerPassword(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.GetLayerPasswordEventArgs) Handles NotateXpress1.RequestLayerPassword
        OutputList.Items.Insert(0, ("Request Layer Password event for layer name = " _
          + (e.LayerName + (", layer password = " + e.LayerPassword))))
        Dim dlg As InputDlg = New InputDlg(("Provide Layer '" _
         + (e.LayerName + "' Password")))
        If (System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me)) Then
            e.LayerPassword = dlg.InputText
        End If


    End Sub

    Private Sub NotateXpress1_UserGroupCreated(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.UserGroupCreatedEventArgs) Handles NotateXpress1.UserGroupCreated

        Dim layer As Layer = e.Layer
        Dim group As Group = e.Group
        OutputList.Items.Insert(0, ("User group '" _
         + (group.Name + ("' created event, layer name = " _
         + (layer.Name + (", description = " + layer.Description))))))
    End Sub

    Private Sub NotateXpress1_UserGroupDestroyed(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress9.UserGroupDestroyedEventArgs) Handles NotateXpress1.UserGroupDestroyed

        Dim layer As Layer = e.Layer
        Dim group As Group = e.Group
        OutputList.Items.Insert(0, ("User group '" _
         + (group.Name + ("' created event, layer name = " _
         + (layer.Name + (", description = " + layer.Description))))))
    End Sub

    Private Function GetAnnotationToolString(ByVal annotationTool As AnnotationTool) As String
        Select Case (annotationTool)
            Case annotationTool.NoTool
                ' = 0x0,
                Return "NoTool"
            Case annotationTool.PointerTool
                ' = 0x1000,
                Return "PointerTool"
            Case annotationTool.TextTool
                ' = 0x1001,
                Return "TextTool"
            Case annotationTool.RectangleTool
                ' = 0x1002,
                Return "RectangleTool"
            Case annotationTool.EllipseTool
                ' = 0x1003,
                Return "EllipseTool"
            Case annotationTool.PolygonTool
                ' = 0x1004,
                Return "PolygonTool"
            Case annotationTool.PolyLineTool
                ' = 0x1005,
                Return "PolyLineTool"
            Case annotationTool.LineTool
                ' = 0x1006,
                Return "LineTool"
            Case annotationTool.FreehandTool
                ' = 0x1007,
                Return "FreehandTool"
            Case annotationTool.StampTool
                ' = 0x1008,
                Return "StampTool"
            Case annotationTool.ImageTool
                ' = 0x1009,
                Return "ImageTool"
            Case annotationTool.ButtonTool
                ' = 0x100a,
                Return "ButtonTool"
            Case annotationTool.RulerTool
                ' = 0x100b
                Return "RulerTool"
            Case Else
                Return "Unknown"
        End Select
    End Function
    Private Sub ZoomIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZoomIn.Click
        ImageXView1.ZoomFactor = ImageXView1.ZoomFactor / 0.9
    End Sub

    Private Sub ZoomOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZoomOut.Click
        ImageXView1.ZoomFactor = ImageXView1.ZoomFactor * 0.9
    End Sub

    Private Sub mnu_SelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_SelectAll.Click

        Dim layer As PegasusImaging.WinForms.NotateXpress9.Layer

        Try
            layer = NotateXpress1.Layers.Selected()

            If (Not (layer) Is Nothing) And layer.Elements.Count > 0 Then
                mnu_SelectAll.Checked = Not mnu_SelectAll.Checked
                layer.Elements.SelectAll(mnu_SelectAll.Checked)

            End If
        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub

    Private Sub mnu_objects_AddNoteAnnotation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_AddNoteAnnotation.Click

        Dim note As NoteTool = New NoteTool()
        note.Text = "This is a note"
        note.BoundingRectangle = New Rectangle(80, 70, 50, 50)
        note.ToolTipText = "Note Tool"
        NotateXpress1.Layers.Selected().Elements.Add(note)

    End Sub

    Private Sub mnu_objects_AddHighlight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_AddHighlight.Click

        Dim highlight As BlockHighlightTool = New BlockHighlightTool()
        highlight.BoundingRectangle = New Rectangle(122, 314, 313, 28)
        highlight.ToolTipText = "BlockHighlight Tool"
        NotateXpress1.Layers.Selected.Elements.Add(highlight)

    End Sub

    Private Sub MenuItemBlockHighlightEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_BlockHighlightEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.BlockHighlightTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.BlockHighlightTool))
        End If
    End Sub

    Private Sub MenuItemBlockHighlightVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_BlockHighlightVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.BlockHighlightTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.BlockHighlightTool))
        End If
    End Sub

    Private Sub MenuItemNoteToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_NoteToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.NoteTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.NoteTool))
        End If
    End Sub

    Private Sub MenuItemNoteToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_NoteToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.NoteTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.NoteTool))
        End If
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_XML.Click
        m_saveOptions.AnnType = AnnotationType.NotateXpressXml
        m_loadOptions.AnnType = AnnotationType.NotateXpressXml

        mnu_file_saveann2fileANN.Enabled = False
        mnu_file_loadannotationANN.Enabled = False

        mnu_file_loadannotationNXP.Enabled = False
        mnu_file_saveann2fileNXP.Enabled = False

        mnu_saveXML.Enabled = True
        mnu_loadXML.Enabled = True

        mnu_loadXFDF.Enabled = False
        mnu_saveXFDF.Enabled = False
    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_loadXML.Click
        Dim strFileName As String = PegasusOpenFile("Open NotateXpress File", "XML File (*.xml) | *.xml")
        Dim layer As Layer
        Try
            NotateXpress1.Layers.FromFile(strFileName, m_loadOptions)

            layer = NotateXpress1.Layers.Selected()
            If Not layer Is Nothing Then
                layer.Toolbar.Visible = True
            End If
            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_saveXML.Click
        Dim strFileName As String = PegasusSaveFile("Save Annotation File", "XML Annotation File (*.xml) | *.xml")
        Try
            NotateXpress1.Layers.Comments = "This file was saved today"
            NotateXpress1.Layers.Subject = "testing 123"
            NotateXpress1.Layers.Title = "One awesome annotation file"
            NotateXpress1.Layers.SaveToFile(strFileName, m_saveOptions)

            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub MenuItem14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_RectangleCustom.Click
        Try
            Dim dlg As OpenFileDialog = New OpenFileDialog()
            dlg.Filter = "CURSOR (*.CUR)|*.CUR"
            dlg.Title = "Choose an Image Cursor"

            dlg.ShowDialog()
            If Not (dlg.FileName = "") Then
                cursor1 = New Cursor(LoadCursorFromFile(dlg.FileName))
            End If
        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub MenuItem19_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_Custom_Ellipse.Click
        Try
            Dim dlg As OpenFileDialog = New OpenFileDialog()
            dlg.Filter = "CURSOR (*.CUR)|*.CUR"
            dlg.Title = "Choose an Image Cursor"

            dlg.ShowDialog()
            If Not (dlg.FileName = "") Then
                cursor2 = New Cursor(LoadCursorFromFile(dlg.FileName))
            End If
        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub MenuItem25_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_CustomText.Click
        Try
            Dim dlg As OpenFileDialog = New OpenFileDialog()
            dlg.Filter = "CURSOR (*.CUR)|*.CUR"
            dlg.Title = "Choose an Image Cursor"

            dlg.ShowDialog()
            If Not (dlg.FileName = "") Then
                cursor3 = New Cursor(LoadCursorFromFile(dlg.FileName))
            End If
        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub CustomCursor(ByVal tool As AnnotationTool, ByVal type As CursorSelection, ByVal cursor As Cursor)
        NotateXpress1.SetUseCustomCursorType(type, tool, cursor.Handle)
    End Sub

    Private Sub MenuItem15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_CreatingRectangle.Click
        CustomCursor(AnnotationTool.RectangleTool, CursorSelection.Creating, cursor1)
    End Sub

    Private Sub MenuItem16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_MovingRectangle.Click
        CustomCursor(AnnotationTool.RectangleTool, CursorSelection.Moving, cursor1)
    End Sub

    Private Sub MenuItem18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_SelectingRectangle.Click
        CustomCursor(AnnotationTool.RectangleTool, CursorSelection.Selecting, cursor1)
    End Sub

    Private Sub MenuItem20_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_EllipseCreating.Click
        CustomCursor(AnnotationTool.EllipseTool, CursorSelection.Creating, cursor2)
    End Sub

    Private Sub MenuItem21_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_MovingEllipse.Click
        CustomCursor(AnnotationTool.EllipseTool, CursorSelection.Moving, cursor2)
    End Sub

    Private Sub MenuItem23_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_EllipseSelecting.Click
        CustomCursor(AnnotationTool.EllipseTool, CursorSelection.Selecting, cursor2)
    End Sub

    Private Sub MenuItem27_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_CreatingText.Click
        CustomCursor(AnnotationTool.TextTool, CursorSelection.Creating, cursor3)
    End Sub

    Private Sub MenuItem28_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_MovingText.Click
        CustomCursor(AnnotationTool.TextTool, CursorSelection.Moving, cursor3)
    End Sub

    Private Sub MenuItem29_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_SelectingText.Click
        CustomCursor(AnnotationTool.TextTool, CursorSelection.Selecting, cursor3)
    End Sub

    Private Sub MenuItem5_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_XFDF.Click
        m_saveOptions.AnnType = AnnotationType.Xfdf
        m_loadOptions.AnnType = AnnotationType.Xfdf

        mnu_file_saveann2fileANN.Enabled = False
        mnu_file_loadannotationANN.Enabled = False

        mnu_file_loadannotationNXP.Enabled = False
        mnu_file_saveann2fileNXP.Enabled = False

        mnu_saveXML.Enabled = False
        mnu_loadXML.Enabled = False

        mnu_loadXFDF.Enabled = True
        mnu_saveXFDF.Enabled = True
    End Sub

    Private Sub saveXFDFmenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_saveXFDF.Click
        Dim strFileName As String = PegasusSaveFile("Save Annotation File", "XFDF Annotation File (*.xfdf) | *.xfdf")
        Try
            NotateXpress1.Layers.Comments = "This file was saved today"
            NotateXpress1.Layers.Subject = "testing 123"
            NotateXpress1.Layers.Title = "One awesome annotation file"
            NotateXpress1.Layers.SaveToFile(strFileName, m_saveOptions)

            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub loadXFDFmenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_loadXFDF.Click
        Dim strFileName As String = PegasusOpenFile("Open NotateXpress File", "XFDF File (*.xfdf) | *.xfdf")
        Dim layer As Layer
        Try
            NotateXpress1.Layers.FromFile(strFileName, m_loadOptions)

            layer = NotateXpress1.Layers.Selected()
            If Not layer Is Nothing Then
                layer.Toolbar.Visible = True
            End If
            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.NotateXpress9.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)
        End Try
    End Sub


End Class

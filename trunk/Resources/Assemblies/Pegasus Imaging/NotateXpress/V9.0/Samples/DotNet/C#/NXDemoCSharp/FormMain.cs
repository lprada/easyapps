/*****************************************************************'
* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;
using PegasusImaging.WinForms.ImagXpress9;
using PegasusImaging.WinForms.NotateXpress9;

namespace NXDemoCSharp
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class FormMain : System.Windows.Forms.Form
    {
        [DllImport("user32.dll")]
        private static extern IntPtr LoadCursorFromFile(string lpFileName);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalFree(IntPtr hMem);

        public System.Windows.Forms.MainMenu mainMenu;
        public System.Windows.Forms.MenuItem mnu_file;
        public System.Windows.Forms.MenuItem mnu_load_tiff;
        public System.Windows.Forms.MenuItem mnu_file_sep1;
        public System.Windows.Forms.MenuItem mnu_file_save;
        public System.Windows.Forms.MenuItem mnu_file_sep2;
        public System.Windows.Forms.MenuItem mnu_file_wangcompatible;
        public System.Windows.Forms.MenuItem mnu_file_sep3;
        public System.Windows.Forms.MenuItem mnu_fileIResY;
        public System.Windows.Forms.MenuItem mfsep;
        public System.Windows.Forms.MenuItem mnu_file_exit;
        public System.Windows.Forms.MenuItem mnu_layers;
        public System.Windows.Forms.MenuItem mnu_layers_create;
        public System.Windows.Forms.MenuItem mnu_layers_setcurrent;
        public System.Windows.Forms.MenuItem mnu_layers_deletecurrent;
        public System.Windows.Forms.MenuItem mnu_layers_togglehide;
        public System.Windows.Forms.MenuItem mnu_layers_toggletoolbar;
        public System.Windows.Forms.MenuItem mnu_layers_setdescription;
        public System.Windows.Forms.MenuItem mnu_layers_setname;
        public System.Windows.Forms.MenuItem mnu_objects;
        public System.Windows.Forms.MenuItem mnu_objects_autoselect;
        public System.Windows.Forms.MenuItem mnu_objects_sep3;
        public System.Windows.Forms.MenuItem mnu_objects_allowgrouping;
        public System.Windows.Forms.MenuItem mnu_objects_set2;
        public System.Windows.Forms.MenuItem mnu_objects_reverse;
        public System.Windows.Forms.MenuItem mnu_objects_create10;
        public System.Windows.Forms.MenuItem mnu_objects_rects;
        public System.Windows.Forms.MenuItem mnu_objects_buttons;
        public System.Windows.Forms.MenuItem mnu_objects_createtransrect;
        public System.Windows.Forms.MenuItem mnu_objects_createImage;
        public System.Windows.Forms.MenuItem mnu_objects_nudgeup;
        public System.Windows.Forms.MenuItem mnu_objects_nudgedn;
        public System.Windows.Forms.MenuItem mnu_objects_inflate;
        public System.Windows.Forms.MenuItem mnu_objects_sep1;
        public System.Windows.Forms.MenuItem mnu_objects_largetext;
        public System.Windows.Forms.MenuItem mnu_objects_useMLE;
        public System.Windows.Forms.MenuItem mnu_objects_sep2;
        public System.Windows.Forms.MenuItem mnu_objects_brand;
        public System.Windows.Forms.MenuItem mnu_orient;
        public System.Windows.Forms.MenuItem mnu_orient_ltbr;
        public System.Windows.Forms.MenuItem mnu_orient_brtl;
        public System.Windows.Forms.MenuItem mnu_orient_rblt;
        public System.Windows.Forms.MenuItem mnu_orient_tlbr;
        public System.Windows.Forms.MenuItem mnu_orientation_rotate180;
        public System.Windows.Forms.MenuItem mnu_stuff;
        public System.Windows.Forms.MenuItem mnu_stuff_settext;
        public System.Windows.Forms.MenuItem mnu_stuff_backcolor;
        public System.Windows.Forms.MenuItem mnu_stuff_pencolor;
        public System.Windows.Forms.MenuItem mnu_stuff_penwidth;
        public System.Windows.Forms.MenuItem mnu_stuff_bevelshadow;
        public System.Windows.Forms.MenuItem mnu_stuff_bevellight;
        public System.Windows.Forms.MenuItem mnu_stuff_setfont;
        public System.Windows.Forms.MenuItem mnu_stuff_getitemtext;
        public System.Windows.Forms.MenuItem mnu_stuff_hb;
        public System.Windows.Forms.MenuItem mnu_stuff_hf;
        public System.Windows.Forms.MenuItem mnu_stuff_moveable;
        public System.Windows.Forms.MenuItem mnu_stuff_sizeable;
        public System.Windows.Forms.MenuItem mnu_stuff_getitemfont;
        public System.Windows.Forms.MenuItem mnu_stuff_setnewdib;
        public System.Windows.Forms.MenuItem mnu_mode;
        public System.Windows.Forms.MenuItem mnu_mode_editmode;
        public System.Windows.Forms.MenuItem mnu_mode_interactive;
        public System.Windows.Forms.MenuItem mnu_cm;
        public System.Windows.Forms.MenuItem mnu_cm_enable;
        public System.Windows.Forms.Button ZoomOut;
        public System.Windows.Forms.Button ZoomIn;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.FontDialog fontDialog1;
        private PegasusImaging.WinForms.NotateXpress9.SaveOptions m_saveOptions = new PegasusImaging.WinForms.NotateXpress9.SaveOptions();
        private PegasusImaging.WinForms.NotateXpress9.LoadOptions m_loadOptions = new PegasusImaging.WinForms.NotateXpress9.LoadOptions();
        private IContainer components;

        /// <summary>
        /// Data variables
        /// </summary>
        /// 

        private bool _bAutoSelect;
        //private bool _bUpdateMoved;
        private System.IO.MemoryStream _SavedAnnotationMemoryStream = null;
        private byte[] _SavedAnnotationByteArray = null;
        private System.Windows.Forms.MenuItem mnu_ruler;
        private System.Windows.Forms.MenuItem mnu_redaction;
        private PegasusImaging.WinForms.ImagXpress9.ImageXView imagXView1;
        private PegasusImaging.WinForms.ImagXpress9.ImageX imageObject;
        private System.Windows.Forms.MenuItem mnu_Groups;
        private System.Windows.Forms.MenuItem mnu_CreateGroup;
        private System.Windows.Forms.MenuItem mnu_DeleteGroup;
        private System.Windows.Forms.MenuItem mnu_SetGroupEmpty;
        private System.Windows.Forms.MenuItem mnu_SelectGroupItems;
        private System.Windows.Forms.MenuItem mnu_GroupAddSelectedItems;
        private System.Windows.Forms.MenuItem mnu_GroupRemoveSelectedItems;
        private System.Windows.Forms.MenuItem mnu_YokeGroupItems;
        private System.Windows.Forms.MenuItem mnu_SetGroupUserString;
        private System.Windows.Forms.MenuItem mnu_ToggleAllowPaint;
        private System.Windows.Forms.MenuItem mnu_SendToBack;
        private System.Windows.Forms.MenuItem mnu_BringToFront;
        private System.Windows.Forms.MenuItem mnu_DeactivateCurrentLayer;
        private System.Windows.Forms.MenuItem mnu_DeleteSelected;
        private System.Windows.Forms.MenuItem mnu_SaveByteArray;
        private System.Windows.Forms.MenuItem mnu_SaveMemoryStream;
        private System.Windows.Forms.MenuItem mnu_LoadByteArray;
        private System.Windows.Forms.MenuItem mnu_LoadMemoryStream;
        private System.Windows.Forms.MenuItem mnu_DisableToolbar;
        private System.Windows.Forms.MenuItem menuItem6;
        private System.Windows.Forms.MenuItem menuItem8;
        private System.Windows.Forms.MenuItem mnu_MirrorYoked;
        private System.Windows.Forms.MenuItem mnu_InvertYoked;
        private System.Windows.Forms.MenuItem mnuItemLoadAnnotations;
        private System.Windows.Forms.MenuItem mnuItemSaveAnnotations;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem mnu_CreateNewNX9;
        private System.Windows.Forms.MenuItem menuItem7;
        private System.Windows.Forms.MenuItem mnuLoadSaveAnnotationType;
        private System.Windows.Forms.MenuItem mnuImagingForWindows;
        private System.Windows.Forms.MenuItem mnuNotateAnnotations;
        private System.Windows.Forms.MenuItem mnuViewDirectorAnnotations;
        private System.Windows.Forms.MenuItem mnu_UnicodeMode;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem mnu_SaveCurrentLayer;
        private System.Windows.Forms.MenuItem mnu_ToolBar;
        private System.Windows.Forms.MenuItem mnu_ToolbarRulerTool;
        private System.Windows.Forms.MenuItem mnu_RulerToolVisible;
        private System.Windows.Forms.MenuItem mnu_RulerToolEnabeled;
        private System.Windows.Forms.MenuItem mnu_ToolbarFreehandTool;
        private System.Windows.Forms.MenuItem mnu_ToolbarEllipseTool;
        private System.Windows.Forms.MenuItem mnu_ToolbarTextTool;
        private System.Windows.Forms.MenuItem mnu_ToolbarRectangleTool;
        private System.Windows.Forms.MenuItem mnu_ToolbarStampTool;
        private System.Windows.Forms.MenuItem mnu_ToolbarPolylineTool;
        private System.Windows.Forms.MenuItem mnu_ToolbarLineTool;
        private System.Windows.Forms.MenuItem mnu_ToolbarImageTool;
        private System.Windows.Forms.MenuItem mnu_FreehandToolEnabled;
        private System.Windows.Forms.MenuItem mnu_FreehandToolVisible;
        private System.Windows.Forms.MenuItem mnu_EllipseToolEnabeld;
        private System.Windows.Forms.MenuItem mnu_EllipseToolVisible;
        private System.Windows.Forms.MenuItem mnu_TextToolEnabled;
        private System.Windows.Forms.MenuItem mnu_TextToolVisible;
        private System.Windows.Forms.MenuItem mnu_RectangleToolEnabled;
        private System.Windows.Forms.MenuItem mnu_RectangleToolVisible;
        private System.Windows.Forms.MenuItem mnu_StampToolEnabled;
        private System.Windows.Forms.MenuItem mnu_StampToolVisible;
        private System.Windows.Forms.MenuItem mnu_PolylineToolVisible;
        private System.Windows.Forms.MenuItem mnu_PolylineToolEnabled;
        private System.Windows.Forms.MenuItem mnu_LineToolEnabled;
        private System.Windows.Forms.MenuItem mnu_LineToolVisible;
        private System.Windows.Forms.MenuItem mnu_ImageToolEnabled;
        private System.Windows.Forms.MenuItem mnu_ImageToolVisible;
        private System.Windows.Forms.MenuItem mnu_ToolbarButtonTool;
        private System.Windows.Forms.MenuItem mnu_ToolbarPolygonTool;
        private System.Windows.Forms.MenuItem mnu_ButtonToolEnabled;
        private System.Windows.Forms.MenuItem mnu_ButtonToolVisible;
        private System.Windows.Forms.MenuItem mnu_PolygonToolEnabled;
        private System.Windows.Forms.MenuItem mnu_PolygonToolVisible;
        private System.Windows.Forms.MenuItem menuItem11;
        private System.Windows.Forms.MenuItem menuItem10;
        private System.Windows.Forms.MenuItem mnu_BoundingRectangle;
        private System.Windows.Forms.ListBox OutputList;
        private System.Windows.Forms.MenuItem mnu_layers_setpassword;
        private System.Windows.Forms.MenuItem mnu_stuff_toggleuserdraw;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuItem mnu_About;
        private System.Windows.Forms.MenuItem mnu_AboutNX;
        private System.Windows.Forms.MenuItem mnu_AboutIX;
        private PegasusImaging.WinForms.NotateXpress9.NotateXpress notateXpress1;
        private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
        internal System.Windows.Forms.ListBox lstInfo;
        internal System.Windows.Forms.Label lblLastError;
        internal System.Windows.Forms.Label lblerror;
        public System.Windows.Forms.MenuItem mnu_file_saveann2fileNXP;
        private System.Windows.Forms.MenuItem mnu_file_saveann2fileANN;
        private System.Windows.Forms.MenuItem mnu_file_loadannotationANN;
        public System.Windows.Forms.MenuItem mnu_file_loadannotationNXP;
        private System.Windows.Forms.MenuItem mnu_stuff_fixed;
        private MenuItem mnu_objects_addNoteAnno;
        private MenuItem mnu_objects_addHighlight;
        private MenuItem mnu_BlockHighlight;
        private MenuItem mnu_BlockHighlightEnabled;
        private MenuItem mnu_BlockHighlightVisible;
        private MenuItem mnu_NoteTool;
        private MenuItem mnu_NoteToolEnabled;
        private MenuItem mnu_NoteToolVisible;
        private MenuItem mnuCustom;
        private MenuItem mnu_Creating;
        private MenuItem mnu_RectangleCustom;
        private MenuItem menuItem13;
        private MenuItem menuItem16;
        private MenuItem menuItem15;
        private MenuItem mnu_Ellipse;
        private MenuItem mnu_CustomEllipse;
        private MenuItem mnu_CreatingEllipse;
        private MenuItem mnu_SelectingEllipse;
        private MenuItem mnu_MovingEllipse;
        private MenuItem mnu_TextAnnotations;
        private MenuItem mnu_CustomText;
        private MenuItem mnu_CreatingText;
        private MenuItem mnu_SelectingText;
        private MenuItem mnu_MovingText;
        private MenuItem mnuXML;
        private MenuItem mnu_saveXML;
        private MenuItem mnu_loadXML;
        private MenuItem mnuXFDF;
        private MenuItem saveXFDFMenuItem;
        private MenuItem mnu_loadXFDF;
        private PegasusImaging.WinForms.ImagXpress9.Processor imagXProcessor;

        public FormMain()
        {
            
            // Required for Windows Form Designer support
            //
            InitializeComponent();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (imageObject != null)
                {
                    imageObject.Dispose();
                    imageObject = null;
                }

                // Don't forget to dispose IX
                //

                if (imagXProcessor != null)
                {
                    if (imagXProcessor.Image != null)
                    {
                        imagXProcessor.Image.Dispose();
                        imagXProcessor.Image = null;
                    }
                    imagXProcessor.Dispose();
                    imagXProcessor = null;
                }

                if (notateXpress1 != null)
                {
                    notateXpress1.Dispose();
                    notateXpress1 = null;
                }

                if (imagXView1.Image != null)
                {
                    imagXView1.Image.Dispose();
                    imagXView1.Image = null;
                }


                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
                }

                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.mnu_file = new System.Windows.Forms.MenuItem();
            this.mnuLoadSaveAnnotationType = new System.Windows.Forms.MenuItem();
            this.mnuImagingForWindows = new System.Windows.Forms.MenuItem();
            this.mnuNotateAnnotations = new System.Windows.Forms.MenuItem();
            this.mnuViewDirectorAnnotations = new System.Windows.Forms.MenuItem();
            this.mnuXML = new System.Windows.Forms.MenuItem();
            this.mnuXFDF = new System.Windows.Forms.MenuItem();
            this.mnu_file_wangcompatible = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.mnuItemLoadAnnotations = new System.Windows.Forms.MenuItem();
            this.mnu_load_tiff = new System.Windows.Forms.MenuItem();
            this.mnu_file_sep1 = new System.Windows.Forms.MenuItem();
            this.mnuItemSaveAnnotations = new System.Windows.Forms.MenuItem();
            this.mnu_file_save = new System.Windows.Forms.MenuItem();
            this.mnu_file_saveann2fileNXP = new System.Windows.Forms.MenuItem();
            this.mnu_file_sep2 = new System.Windows.Forms.MenuItem();
            this.mnu_file_saveann2fileANN = new System.Windows.Forms.MenuItem();
            this.saveXFDFMenuItem = new System.Windows.Forms.MenuItem();
            this.mnu_saveXML = new System.Windows.Forms.MenuItem();
            this.mnu_SaveByteArray = new System.Windows.Forms.MenuItem();
            this.mnu_SaveMemoryStream = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mnu_UnicodeMode = new System.Windows.Forms.MenuItem();
            this.mnu_file_loadannotationNXP = new System.Windows.Forms.MenuItem();
            this.mnu_file_loadannotationANN = new System.Windows.Forms.MenuItem();
            this.mnu_loadXFDF = new System.Windows.Forms.MenuItem();
            this.mnu_loadXML = new System.Windows.Forms.MenuItem();
            this.mnu_LoadByteArray = new System.Windows.Forms.MenuItem();
            this.mnu_LoadMemoryStream = new System.Windows.Forms.MenuItem();
            this.mnu_file_sep3 = new System.Windows.Forms.MenuItem();
            this.mnu_fileIResY = new System.Windows.Forms.MenuItem();
            this.mfsep = new System.Windows.Forms.MenuItem();
            this.mnu_ToggleAllowPaint = new System.Windows.Forms.MenuItem();
            this.menuItem11 = new System.Windows.Forms.MenuItem();
            this.mnu_file_exit = new System.Windows.Forms.MenuItem();
            this.mnu_layers = new System.Windows.Forms.MenuItem();
            this.mnu_layers_create = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.mnu_layers_setcurrent = new System.Windows.Forms.MenuItem();
            this.mnu_layers_deletecurrent = new System.Windows.Forms.MenuItem();
            this.mnu_layers_togglehide = new System.Windows.Forms.MenuItem();
            this.mnu_DeactivateCurrentLayer = new System.Windows.Forms.MenuItem();
            this.mnu_layers_setname = new System.Windows.Forms.MenuItem();
            this.mnu_layers_setdescription = new System.Windows.Forms.MenuItem();
            this.mnu_layers_setpassword = new System.Windows.Forms.MenuItem();
            this.mnu_objects_reverse = new System.Windows.Forms.MenuItem();
            this.mnu_SendToBack = new System.Windows.Forms.MenuItem();
            this.mnu_BringToFront = new System.Windows.Forms.MenuItem();
            this.mnu_DeleteSelected = new System.Windows.Forms.MenuItem();
            this.mnu_SaveCurrentLayer = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.mnu_layers_toggletoolbar = new System.Windows.Forms.MenuItem();
            this.mnu_DisableToolbar = new System.Windows.Forms.MenuItem();
            this.mnu_objects = new System.Windows.Forms.MenuItem();
            this.mnu_objects_autoselect = new System.Windows.Forms.MenuItem();
            this.mnu_objects_sep3 = new System.Windows.Forms.MenuItem();
            this.mnu_objects_create10 = new System.Windows.Forms.MenuItem();
            this.mnu_objects_rects = new System.Windows.Forms.MenuItem();
            this.mnu_objects_buttons = new System.Windows.Forms.MenuItem();
            this.mnu_objects_createtransrect = new System.Windows.Forms.MenuItem();
            this.mnu_objects_createImage = new System.Windows.Forms.MenuItem();
            this.mnu_objects_nudgeup = new System.Windows.Forms.MenuItem();
            this.mnu_objects_nudgedn = new System.Windows.Forms.MenuItem();
            this.mnu_objects_inflate = new System.Windows.Forms.MenuItem();
            this.mnu_objects_sep1 = new System.Windows.Forms.MenuItem();
            this.mnu_redaction = new System.Windows.Forms.MenuItem();
            this.mnu_ruler = new System.Windows.Forms.MenuItem();
            this.mnu_objects_largetext = new System.Windows.Forms.MenuItem();
            this.mnu_objects_addNoteAnno = new System.Windows.Forms.MenuItem();
            this.mnu_objects_addHighlight = new System.Windows.Forms.MenuItem();
            this.mnu_objects_useMLE = new System.Windows.Forms.MenuItem();
            this.mnu_objects_sep2 = new System.Windows.Forms.MenuItem();
            this.mnu_objects_brand = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.mnu_CreateNewNX9 = new System.Windows.Forms.MenuItem();
            this.mnu_orient = new System.Windows.Forms.MenuItem();
            this.mnu_orient_ltbr = new System.Windows.Forms.MenuItem();
            this.mnu_orient_brtl = new System.Windows.Forms.MenuItem();
            this.mnu_orient_rblt = new System.Windows.Forms.MenuItem();
            this.mnu_orient_tlbr = new System.Windows.Forms.MenuItem();
            this.mnu_orientation_rotate180 = new System.Windows.Forms.MenuItem();
            this.mnu_stuff = new System.Windows.Forms.MenuItem();
            this.mnu_BoundingRectangle = new System.Windows.Forms.MenuItem();
            this.menuItem10 = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_settext = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_backcolor = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_pencolor = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_penwidth = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_bevelshadow = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_bevellight = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_setfont = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_getitemtext = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_hb = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_hf = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_moveable = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_fixed = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_sizeable = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_getitemfont = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_setnewdib = new System.Windows.Forms.MenuItem();
            this.mnu_stuff_toggleuserdraw = new System.Windows.Forms.MenuItem();
            this.mnu_mode = new System.Windows.Forms.MenuItem();
            this.mnu_mode_editmode = new System.Windows.Forms.MenuItem();
            this.mnu_mode_interactive = new System.Windows.Forms.MenuItem();
            this.mnu_cm = new System.Windows.Forms.MenuItem();
            this.mnu_cm_enable = new System.Windows.Forms.MenuItem();
            this.mnuCustom = new System.Windows.Forms.MenuItem();
            this.mnu_Creating = new System.Windows.Forms.MenuItem();
            this.mnu_RectangleCustom = new System.Windows.Forms.MenuItem();
            this.menuItem13 = new System.Windows.Forms.MenuItem();
            this.menuItem15 = new System.Windows.Forms.MenuItem();
            this.menuItem16 = new System.Windows.Forms.MenuItem();
            this.mnu_Ellipse = new System.Windows.Forms.MenuItem();
            this.mnu_CustomEllipse = new System.Windows.Forms.MenuItem();
            this.mnu_CreatingEllipse = new System.Windows.Forms.MenuItem();
            this.mnu_MovingEllipse = new System.Windows.Forms.MenuItem();
            this.mnu_SelectingEllipse = new System.Windows.Forms.MenuItem();
            this.mnu_TextAnnotations = new System.Windows.Forms.MenuItem();
            this.mnu_CustomText = new System.Windows.Forms.MenuItem();
            this.mnu_CreatingText = new System.Windows.Forms.MenuItem();
            this.mnu_MovingText = new System.Windows.Forms.MenuItem();
            this.mnu_SelectingText = new System.Windows.Forms.MenuItem();
            this.mnu_Groups = new System.Windows.Forms.MenuItem();
            this.mnu_objects_allowgrouping = new System.Windows.Forms.MenuItem();
            this.mnu_objects_set2 = new System.Windows.Forms.MenuItem();
            this.mnu_CreateGroup = new System.Windows.Forms.MenuItem();
            this.mnu_DeleteGroup = new System.Windows.Forms.MenuItem();
            this.mnu_SetGroupEmpty = new System.Windows.Forms.MenuItem();
            this.mnu_SelectGroupItems = new System.Windows.Forms.MenuItem();
            this.mnu_GroupAddSelectedItems = new System.Windows.Forms.MenuItem();
            this.mnu_GroupRemoveSelectedItems = new System.Windows.Forms.MenuItem();
            this.mnu_YokeGroupItems = new System.Windows.Forms.MenuItem();
            this.mnu_MirrorYoked = new System.Windows.Forms.MenuItem();
            this.mnu_InvertYoked = new System.Windows.Forms.MenuItem();
            this.mnu_SetGroupUserString = new System.Windows.Forms.MenuItem();
            this.mnu_ToolBar = new System.Windows.Forms.MenuItem();
            this.mnu_ToolbarRulerTool = new System.Windows.Forms.MenuItem();
            this.mnu_RulerToolEnabeled = new System.Windows.Forms.MenuItem();
            this.mnu_RulerToolVisible = new System.Windows.Forms.MenuItem();
            this.mnu_ToolbarFreehandTool = new System.Windows.Forms.MenuItem();
            this.mnu_FreehandToolEnabled = new System.Windows.Forms.MenuItem();
            this.mnu_FreehandToolVisible = new System.Windows.Forms.MenuItem();
            this.mnu_ToolbarEllipseTool = new System.Windows.Forms.MenuItem();
            this.mnu_EllipseToolEnabeld = new System.Windows.Forms.MenuItem();
            this.mnu_EllipseToolVisible = new System.Windows.Forms.MenuItem();
            this.mnu_ToolbarTextTool = new System.Windows.Forms.MenuItem();
            this.mnu_TextToolEnabled = new System.Windows.Forms.MenuItem();
            this.mnu_TextToolVisible = new System.Windows.Forms.MenuItem();
            this.mnu_ToolbarRectangleTool = new System.Windows.Forms.MenuItem();
            this.mnu_RectangleToolEnabled = new System.Windows.Forms.MenuItem();
            this.mnu_RectangleToolVisible = new System.Windows.Forms.MenuItem();
            this.mnu_ToolbarStampTool = new System.Windows.Forms.MenuItem();
            this.mnu_StampToolEnabled = new System.Windows.Forms.MenuItem();
            this.mnu_StampToolVisible = new System.Windows.Forms.MenuItem();
            this.mnu_ToolbarPolylineTool = new System.Windows.Forms.MenuItem();
            this.mnu_PolylineToolEnabled = new System.Windows.Forms.MenuItem();
            this.mnu_PolylineToolVisible = new System.Windows.Forms.MenuItem();
            this.mnu_ToolbarLineTool = new System.Windows.Forms.MenuItem();
            this.mnu_LineToolEnabled = new System.Windows.Forms.MenuItem();
            this.mnu_LineToolVisible = new System.Windows.Forms.MenuItem();
            this.mnu_ToolbarImageTool = new System.Windows.Forms.MenuItem();
            this.mnu_ImageToolEnabled = new System.Windows.Forms.MenuItem();
            this.mnu_ImageToolVisible = new System.Windows.Forms.MenuItem();
            this.mnu_ToolbarButtonTool = new System.Windows.Forms.MenuItem();
            this.mnu_ButtonToolEnabled = new System.Windows.Forms.MenuItem();
            this.mnu_ButtonToolVisible = new System.Windows.Forms.MenuItem();
            this.mnu_ToolbarPolygonTool = new System.Windows.Forms.MenuItem();
            this.mnu_PolygonToolEnabled = new System.Windows.Forms.MenuItem();
            this.mnu_PolygonToolVisible = new System.Windows.Forms.MenuItem();
            this.mnu_BlockHighlight = new System.Windows.Forms.MenuItem();
            this.mnu_BlockHighlightEnabled = new System.Windows.Forms.MenuItem();
            this.mnu_BlockHighlightVisible = new System.Windows.Forms.MenuItem();
            this.mnu_NoteTool = new System.Windows.Forms.MenuItem();
            this.mnu_NoteToolEnabled = new System.Windows.Forms.MenuItem();
            this.mnu_NoteToolVisible = new System.Windows.Forms.MenuItem();
            this.mnu_About = new System.Windows.Forms.MenuItem();
            this.mnu_AboutIX = new System.Windows.Forms.MenuItem();
            this.mnu_AboutNX = new System.Windows.Forms.MenuItem();
            this.ZoomOut = new System.Windows.Forms.Button();
            this.ZoomIn = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imagXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.imagXProcessor = new PegasusImaging.WinForms.ImagXpress9.Processor(this.components);
            this.OutputList = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.notateXpress1 = new PegasusImaging.WinForms.NotateXpress9.NotateXpress(this.components);
            this.lstInfo = new System.Windows.Forms.ListBox();
            this.lblLastError = new System.Windows.Forms.Label();
            this.lblerror = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_file,
            this.mnu_layers,
            this.mnu_objects,
            this.mnu_orient,
            this.mnu_stuff,
            this.mnu_mode,
            this.mnu_cm,
            this.mnuCustom,
            this.mnu_Groups,
            this.mnu_ToolBar,
            this.mnu_About});
            // 
            // mnu_file
            // 
            this.mnu_file.Index = 0;
            this.mnu_file.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuLoadSaveAnnotationType,
            this.mnu_file_wangcompatible,
            this.menuItem7,
            this.mnuItemLoadAnnotations,
            this.mnu_load_tiff,
            this.mnu_file_sep1,
            this.mnuItemSaveAnnotations,
            this.mnu_file_save,
            this.mnu_file_saveann2fileNXP,
            this.mnu_file_sep2,
            this.mnu_file_saveann2fileANN,
            this.saveXFDFMenuItem,
            this.mnu_saveXML,
            this.mnu_SaveByteArray,
            this.mnu_SaveMemoryStream,
            this.menuItem1,
            this.mnu_UnicodeMode,
            this.mnu_file_loadannotationNXP,
            this.mnu_file_loadannotationANN,
            this.mnu_loadXFDF,
            this.mnu_loadXML,
            this.mnu_LoadByteArray,
            this.mnu_LoadMemoryStream,
            this.mnu_file_sep3,
            this.mnu_fileIResY,
            this.mfsep,
            this.mnu_ToggleAllowPaint,
            this.menuItem11,
            this.mnu_file_exit});
            this.mnu_file.Text = "File";
            this.mnu_file.Select += new System.EventHandler(this.mnu_file_select);
            // 
            // mnuLoadSaveAnnotationType
            // 
            this.mnuLoadSaveAnnotationType.Index = 0;
            this.mnuLoadSaveAnnotationType.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuImagingForWindows,
            this.mnuNotateAnnotations,
            this.mnuViewDirectorAnnotations,
            this.mnuXML,
            this.mnuXFDF});
            this.mnuLoadSaveAnnotationType.Text = "Load/Save Annotation Type";
            // 
            // mnuImagingForWindows
            // 
            this.mnuImagingForWindows.Index = 0;
            this.mnuImagingForWindows.Text = "Imaging For Windows (Wang)";
            this.mnuImagingForWindows.Click += new System.EventHandler(this.menuItemImagingForWindows_click);
            // 
            // mnuNotateAnnotations
            // 
            this.mnuNotateAnnotations.Index = 1;
            this.mnuNotateAnnotations.Text = "NotateXpress (NXP)";
            this.mnuNotateAnnotations.Click += new System.EventHandler(this.menuItemNotateAnnotations_click);
            // 
            // mnuViewDirectorAnnotations
            // 
            this.mnuViewDirectorAnnotations.Index = 2;
            this.mnuViewDirectorAnnotations.Text = "ViewDirector - TMSSequoia (ANN)";
            this.mnuViewDirectorAnnotations.Click += new System.EventHandler(this.menuItemViewDirectorAnnotations_click);
            // 
            // mnuXML
            // 
            this.mnuXML.Index = 3;
            this.mnuXML.Text = "NotateXpressXML (XML)";
            this.mnuXML.Click += new System.EventHandler(this.menuItem27_Click);
            // 
            // mnuXFDF
            // 
            this.mnuXFDF.Index = 4;
            this.mnuXFDF.Text = "(XFDF)";
            this.mnuXFDF.Click += new System.EventHandler(this.menuItem30_Click);
            // 
            // mnu_file_wangcompatible
            // 
            this.mnu_file_wangcompatible.Checked = true;
            this.mnu_file_wangcompatible.Index = 1;
            this.mnu_file_wangcompatible.Text = "Toggle Load/Save Preserve Wang";
            this.mnu_file_wangcompatible.Click += new System.EventHandler(this.mnu_file_wangcompatible_Click);
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 2;
            this.menuItem7.Text = "-";
            // 
            // mnuItemLoadAnnotations
            // 
            this.mnuItemLoadAnnotations.Index = 3;
            this.mnuItemLoadAnnotations.Text = "Toggle Load Annotations thru IX";
            this.mnuItemLoadAnnotations.Click += new System.EventHandler(this.menuItemLoadAnnotations_Click);
            // 
            // mnu_load_tiff
            // 
            this.mnu_load_tiff.Index = 4;
            this.mnu_load_tiff.Text = "Load Image...";
            this.mnu_load_tiff.Click += new System.EventHandler(this.mnu_load_tiff_Click);
            // 
            // mnu_file_sep1
            // 
            this.mnu_file_sep1.Index = 5;
            this.mnu_file_sep1.Text = "-";
            // 
            // mnuItemSaveAnnotations
            // 
            this.mnuItemSaveAnnotations.Index = 6;
            this.mnuItemSaveAnnotations.Text = "Toggle Save Annotations thru IX";
            this.mnuItemSaveAnnotations.Click += new System.EventHandler(this.menuItemSaveAnnotations_Click);
            // 
            // mnu_file_save
            // 
            this.mnu_file_save.Index = 7;
            this.mnu_file_save.Text = "Save Image...";
            this.mnu_file_save.Click += new System.EventHandler(this.mnu_file_save_Click);
            // 
            // mnu_file_saveann2fileNXP
            // 
            this.mnu_file_saveann2fileNXP.Enabled = false;
            this.mnu_file_saveann2fileNXP.Index = 8;
            this.mnu_file_saveann2fileNXP.Text = "Save annotation to file(NXP)";
            this.mnu_file_saveann2fileNXP.Click += new System.EventHandler(this.mnu_file_saveann2fileNXP_Click);
            // 
            // mnu_file_sep2
            // 
            this.mnu_file_sep2.Index = 9;
            this.mnu_file_sep2.Text = "-";
            // 
            // mnu_file_saveann2fileANN
            // 
            this.mnu_file_saveann2fileANN.Enabled = false;
            this.mnu_file_saveann2fileANN.Index = 10;
            this.mnu_file_saveann2fileANN.Text = "Save annotation to file(ANN)";
            this.mnu_file_saveann2fileANN.Click += new System.EventHandler(this.mnu_file_saveann2fileANN_Click);
            // 
            // saveXFDFMenuItem
            // 
            this.saveXFDFMenuItem.Enabled = false;
            this.saveXFDFMenuItem.Index = 11;
            this.saveXFDFMenuItem.Text = "Save annotation to file (XFDF)";
            this.saveXFDFMenuItem.Click += new System.EventHandler(this.saveXFDFMenuItem_Click);
            // 
            // mnu_saveXML
            // 
            this.mnu_saveXML.Enabled = false;
            this.mnu_saveXML.Index = 12;
            this.mnu_saveXML.Text = "Save Annotation to file (XML)";
            this.mnu_saveXML.Click += new System.EventHandler(this.menuItem28_Click);
            // 
            // mnu_SaveByteArray
            // 
            this.mnu_SaveByteArray.Index = 13;
            this.mnu_SaveByteArray.Text = "Save annotation to ByteArray";
            this.mnu_SaveByteArray.Click += new System.EventHandler(this.menuItemSaveByteArray_Click);
            // 
            // mnu_SaveMemoryStream
            // 
            this.mnu_SaveMemoryStream.Index = 14;
            this.mnu_SaveMemoryStream.Text = "Save annotation to MemoryStream";
            this.mnu_SaveMemoryStream.Click += new System.EventHandler(this.menuItemSaveMemoryStream_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 15;
            this.menuItem1.Text = "-";
            // 
            // mnu_UnicodeMode
            // 
            this.mnu_UnicodeMode.Checked = true;
            this.mnu_UnicodeMode.Index = 16;
            this.mnu_UnicodeMode.Text = "Toggle Load in Unicode Mode";
            this.mnu_UnicodeMode.Click += new System.EventHandler(this.menuItemUnicodeMode_Click);
            // 
            // mnu_file_loadannotationNXP
            // 
            this.mnu_file_loadannotationNXP.Enabled = false;
            this.mnu_file_loadannotationNXP.Index = 17;
            this.mnu_file_loadannotationNXP.Text = "Load annotation from file(NXP)";
            this.mnu_file_loadannotationNXP.Click += new System.EventHandler(this.mnu_file_loadannotationNXP_Click);
            // 
            // mnu_file_loadannotationANN
            // 
            this.mnu_file_loadannotationANN.Enabled = false;
            this.mnu_file_loadannotationANN.Index = 18;
            this.mnu_file_loadannotationANN.Text = "Load Annotation from file(ANN)";
            this.mnu_file_loadannotationANN.Click += new System.EventHandler(this.mnu_file_loadannotationANN_Click);
            // 
            // mnu_loadXFDF
            // 
            this.mnu_loadXFDF.Enabled = false;
            this.mnu_loadXFDF.Index = 19;
            this.mnu_loadXFDF.Text = "Load Annotation from file (XFDF)";
            this.mnu_loadXFDF.Click += new System.EventHandler(this.loadXFDFMenuItem_Click);
            // 
            // mnu_loadXML
            // 
            this.mnu_loadXML.Enabled = false;
            this.mnu_loadXML.Index = 20;
            this.mnu_loadXML.Text = "Load Annotation from file (XML)";
            this.mnu_loadXML.Click += new System.EventHandler(this.menuItem29_Click);
            // 
            // mnu_LoadByteArray
            // 
            this.mnu_LoadByteArray.Index = 21;
            this.mnu_LoadByteArray.Text = "Load annotation from ByteArray";
            this.mnu_LoadByteArray.Click += new System.EventHandler(this.menuItemLoadByteArray_Click);
            // 
            // mnu_LoadMemoryStream
            // 
            this.mnu_LoadMemoryStream.Index = 22;
            this.mnu_LoadMemoryStream.Text = "Load annotation from MemoryStream";
            this.mnu_LoadMemoryStream.Click += new System.EventHandler(this.menuItemLoadMemoryStream_Click);
            // 
            // mnu_file_sep3
            // 
            this.mnu_file_sep3.Index = 23;
            this.mnu_file_sep3.Text = "-";
            // 
            // mnu_fileIResY
            // 
            this.mnu_fileIResY.Index = 24;
            this.mnu_fileIResY.Text = "IResY FontScaling";
            this.mnu_fileIResY.Click += new System.EventHandler(this.mnu_fileIResY_Click);
            // 
            // mfsep
            // 
            this.mfsep.Index = 25;
            this.mfsep.Text = "-";
            // 
            // mnu_ToggleAllowPaint
            // 
            this.mnu_ToggleAllowPaint.Index = 26;
            this.mnu_ToggleAllowPaint.Text = "Toggle Allow Paint";
            this.mnu_ToggleAllowPaint.Click += new System.EventHandler(this.menuItemToggleAllowPain_Click);
            // 
            // menuItem11
            // 
            this.menuItem11.Index = 27;
            this.menuItem11.Text = "-";
            // 
            // mnu_file_exit
            // 
            this.mnu_file_exit.Index = 28;
            this.mnu_file_exit.Text = "E&xit";
            this.mnu_file_exit.Click += new System.EventHandler(this.mnu_file_exit_Click);
            // 
            // mnu_layers
            // 
            this.mnu_layers.Index = 1;
            this.mnu_layers.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_layers_create,
            this.menuItem8,
            this.mnu_layers_setcurrent,
            this.mnu_layers_deletecurrent,
            this.mnu_layers_togglehide,
            this.mnu_DeactivateCurrentLayer,
            this.mnu_layers_setname,
            this.mnu_layers_setdescription,
            this.mnu_layers_setpassword,
            this.mnu_objects_reverse,
            this.mnu_SendToBack,
            this.mnu_BringToFront,
            this.mnu_DeleteSelected,
            this.mnu_SaveCurrentLayer,
            this.menuItem6,
            this.mnu_layers_toggletoolbar,
            this.mnu_DisableToolbar});
            this.mnu_layers.Text = "Layers";
            this.mnu_layers.Select += new System.EventHandler(this.mnu_layers_Select);
            // 
            // mnu_layers_create
            // 
            this.mnu_layers_create.Index = 0;
            this.mnu_layers_create.Text = "Create...";
            this.mnu_layers_create.Click += new System.EventHandler(this.mnu_layers_create_Click);
            // 
            // menuItem8
            // 
            this.menuItem8.Index = 1;
            this.menuItem8.Text = "-";
            // 
            // mnu_layers_setcurrent
            // 
            this.mnu_layers_setcurrent.Index = 2;
            this.mnu_layers_setcurrent.Text = "Set current";
            // 
            // mnu_layers_deletecurrent
            // 
            this.mnu_layers_deletecurrent.Index = 3;
            this.mnu_layers_deletecurrent.Text = "Delete current";
            this.mnu_layers_deletecurrent.Click += new System.EventHandler(this.mnu_layers_deletecurrent_Click);
            // 
            // mnu_layers_togglehide
            // 
            this.mnu_layers_togglehide.Index = 4;
            this.mnu_layers_togglehide.Text = "Hide current";
            this.mnu_layers_togglehide.Click += new System.EventHandler(this.mnu_layers_togglehide_Click);
            // 
            // mnu_DeactivateCurrentLayer
            // 
            this.mnu_DeactivateCurrentLayer.Index = 5;
            this.mnu_DeactivateCurrentLayer.Text = "Deactivate current";
            this.mnu_DeactivateCurrentLayer.Click += new System.EventHandler(this.menuItemDeactivateCurrentLayer_Click);
            // 
            // mnu_layers_setname
            // 
            this.mnu_layers_setname.Index = 6;
            this.mnu_layers_setname.Text = "Current name...";
            this.mnu_layers_setname.Click += new System.EventHandler(this.mnu_layers_setname_Click);
            // 
            // mnu_layers_setdescription
            // 
            this.mnu_layers_setdescription.Index = 7;
            this.mnu_layers_setdescription.Text = "Current description...";
            this.mnu_layers_setdescription.Click += new System.EventHandler(this.mnu_layers_setdescription_Click);
            // 
            // mnu_layers_setpassword
            // 
            this.mnu_layers_setpassword.Index = 8;
            this.mnu_layers_setpassword.Text = "Current password...";
            this.mnu_layers_setpassword.Click += new System.EventHandler(this.mnu_layers_setpassword_Click);
            // 
            // mnu_objects_reverse
            // 
            this.mnu_objects_reverse.Index = 9;
            this.mnu_objects_reverse.Text = "Reverse current ZOrder";
            this.mnu_objects_reverse.Click += new System.EventHandler(this.mnu_objects_reverse_Click);
            // 
            // mnu_SendToBack
            // 
            this.mnu_SendToBack.Index = 10;
            this.mnu_SendToBack.Text = "Current selected To Back of ZOrder";
            this.mnu_SendToBack.Click += new System.EventHandler(this.menuItemSendToBack_Click);
            // 
            // mnu_BringToFront
            // 
            this.mnu_BringToFront.Index = 11;
            this.mnu_BringToFront.Text = "Current selected To Front of ZOrder";
            this.mnu_BringToFront.Click += new System.EventHandler(this.menuItemBringToFront_Click);
            // 
            // mnu_DeleteSelected
            // 
            this.mnu_DeleteSelected.Index = 12;
            this.mnu_DeleteSelected.Text = "Current delete Selected";
            this.mnu_DeleteSelected.Click += new System.EventHandler(this.menuItemDeleteSelected_Click);
            // 
            // mnu_SaveCurrentLayer
            // 
            this.mnu_SaveCurrentLayer.Index = 13;
            this.mnu_SaveCurrentLayer.Text = "Save Current...";
            this.mnu_SaveCurrentLayer.Click += new System.EventHandler(this.menuItemSaveCurrentLayer_click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 14;
            this.menuItem6.Text = "-";
            // 
            // mnu_layers_toggletoolbar
            // 
            this.mnu_layers_toggletoolbar.Index = 15;
            this.mnu_layers_toggletoolbar.Text = "Toggle toolbar visibility";
            this.mnu_layers_toggletoolbar.Click += new System.EventHandler(this.mnu_layers_toggletoolbar_Click);
            // 
            // mnu_DisableToolbar
            // 
            this.mnu_DisableToolbar.Index = 16;
            this.mnu_DisableToolbar.Text = "Disable toolbar";
            this.mnu_DisableToolbar.Click += new System.EventHandler(this.menuItemDisableToolbar_Click);
            // 
            // mnu_objects
            // 
            this.mnu_objects.Index = 2;
            this.mnu_objects.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_objects_autoselect,
            this.mnu_objects_sep3,
            this.mnu_objects_create10,
            this.mnu_objects_rects,
            this.mnu_objects_buttons,
            this.mnu_objects_createtransrect,
            this.mnu_objects_createImage,
            this.mnu_objects_nudgeup,
            this.mnu_objects_nudgedn,
            this.mnu_objects_inflate,
            this.mnu_objects_sep1,
            this.mnu_redaction,
            this.mnu_ruler,
            this.mnu_objects_largetext,
            this.mnu_objects_addNoteAnno,
            this.mnu_objects_addHighlight,
            this.mnu_objects_useMLE,
            this.mnu_objects_sep2,
            this.mnu_objects_brand,
            this.menuItem4,
            this.mnu_CreateNewNX9});
            this.mnu_objects.Text = "Objects";
            this.mnu_objects.Select += new System.EventHandler(this.mnu_objects_Select);
            // 
            // mnu_objects_autoselect
            // 
            this.mnu_objects_autoselect.Checked = true;
            this.mnu_objects_autoselect.Index = 0;
            this.mnu_objects_autoselect.Text = "Auto-select when created";
            this.mnu_objects_autoselect.Click += new System.EventHandler(this.mnu_objects_autoselect_Click);
            // 
            // mnu_objects_sep3
            // 
            this.mnu_objects_sep3.Index = 1;
            this.mnu_objects_sep3.Text = "-";
            // 
            // mnu_objects_create10
            // 
            this.mnu_objects_create10.Index = 2;
            this.mnu_objects_create10.Text = "Create 10 objects";
            this.mnu_objects_create10.Click += new System.EventHandler(this.mnu_objects_create10_Click);
            // 
            // mnu_objects_rects
            // 
            this.mnu_objects_rects.Index = 3;
            this.mnu_objects_rects.Text = "Create 10 rects";
            this.mnu_objects_rects.Click += new System.EventHandler(this.mnu_objects_rects_Click);
            // 
            // mnu_objects_buttons
            // 
            this.mnu_objects_buttons.Index = 4;
            this.mnu_objects_buttons.Text = "Create 10 buttons";
            this.mnu_objects_buttons.Click += new System.EventHandler(this.mnu_objects_buttons_Click);
            // 
            // mnu_objects_createtransrect
            // 
            this.mnu_objects_createtransrect.Index = 5;
            this.mnu_objects_createtransrect.Text = "Create translucent rectangle";
            this.mnu_objects_createtransrect.Click += new System.EventHandler(this.mnu_objects_createtransrect_Click);
            // 
            // mnu_objects_createImage
            // 
            this.mnu_objects_createImage.Index = 6;
            this.mnu_objects_createImage.Text = "Create an Image object";
            this.mnu_objects_createImage.Click += new System.EventHandler(this.mnu_objects_createImage_Click);
            // 
            // mnu_objects_nudgeup
            // 
            this.mnu_objects_nudgeup.Index = 7;
            this.mnu_objects_nudgeup.Text = "Nudge up";
            this.mnu_objects_nudgeup.Click += new System.EventHandler(this.mnu_objects_nudgeup_Click);
            // 
            // mnu_objects_nudgedn
            // 
            this.mnu_objects_nudgedn.Index = 8;
            this.mnu_objects_nudgedn.Text = "Nudge dn";
            this.mnu_objects_nudgedn.Click += new System.EventHandler(this.mnu_objects_nudgedn_Click);
            // 
            // mnu_objects_inflate
            // 
            this.mnu_objects_inflate.Index = 9;
            this.mnu_objects_inflate.Text = "Inflate me";
            this.mnu_objects_inflate.Click += new System.EventHandler(this.mnu_objects_inflate_Click);
            // 
            // mnu_objects_sep1
            // 
            this.mnu_objects_sep1.Index = 10;
            this.mnu_objects_sep1.Text = "-";
            // 
            // mnu_redaction
            // 
            this.mnu_redaction.Index = 11;
            this.mnu_redaction.Text = "Add Redaction";
            this.mnu_redaction.Click += new System.EventHandler(this.menuItem3_Click);
            // 
            // mnu_ruler
            // 
            this.mnu_ruler.Index = 12;
            this.mnu_ruler.Text = "Add Ruler Annotation";
            this.mnu_ruler.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // mnu_objects_largetext
            // 
            this.mnu_objects_largetext.Index = 13;
            this.mnu_objects_largetext.Text = "Add large text object";
            this.mnu_objects_largetext.Click += new System.EventHandler(this.mnu_objects_largetext_Click);
            // 
            // mnu_objects_addNoteAnno
            // 
            this.mnu_objects_addNoteAnno.Index = 14;
            this.mnu_objects_addNoteAnno.Text = "Add Note Annotation (new in v9)";
            this.mnu_objects_addNoteAnno.Click += new System.EventHandler(this.mnu_objects_addNoteAnno_Click_1);
            // 
            // mnu_objects_addHighlight
            // 
            this.mnu_objects_addHighlight.Index = 15;
            this.mnu_objects_addHighlight.Text = "Add Block Highlight (new in v9)";
            this.mnu_objects_addHighlight.Click += new System.EventHandler(this.mnu_objects_addHighlight_Click_1);
            // 
            // mnu_objects_useMLE
            // 
            this.mnu_objects_useMLE.Index = 16;
            this.mnu_objects_useMLE.Text = "Use MLE to edit existing text";
            this.mnu_objects_useMLE.Click += new System.EventHandler(this.mnu_objects_useMLE_Click);
            // 
            // mnu_objects_sep2
            // 
            this.mnu_objects_sep2.Index = 17;
            this.mnu_objects_sep2.Text = "-";
            // 
            // mnu_objects_brand
            // 
            this.mnu_objects_brand.Index = 18;
            this.mnu_objects_brand.Text = "Brand";
            this.mnu_objects_brand.Click += new System.EventHandler(this.mnu_objects_brand_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 19;
            this.menuItem4.Text = "-";
            // 
            // mnu_CreateNewNX9
            // 
            this.mnu_CreateNewNX9.Index = 20;
            this.mnu_CreateNewNX9.Text = "Create new NX9 component";
            this.mnu_CreateNewNX9.Click += new System.EventHandler(this.menuItemCreateNewNX9_click);
            // 
            // mnu_orient
            // 
            this.mnu_orient.Index = 3;
            this.mnu_orient.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_orient_ltbr,
            this.mnu_orient_brtl,
            this.mnu_orient_rblt,
            this.mnu_orient_tlbr,
            this.mnu_orientation_rotate180});
            this.mnu_orient.Text = "Orientation";
            // 
            // mnu_orient_ltbr
            // 
            this.mnu_orient_ltbr.Index = 0;
            this.mnu_orient_ltbr.Text = "Rotate 90 clockwise";
            this.mnu_orient_ltbr.Click += new System.EventHandler(this.mnu_orient_ltbr_Click);
            // 
            // mnu_orient_brtl
            // 
            this.mnu_orient_brtl.Index = 1;
            this.mnu_orient_brtl.Text = "Invert";
            this.mnu_orient_brtl.Click += new System.EventHandler(this.mnu_orient_brtl_Click);
            // 
            // mnu_orient_rblt
            // 
            this.mnu_orient_rblt.Index = 2;
            this.mnu_orient_rblt.Text = "Rotate 90 counterclockwise";
            this.mnu_orient_rblt.Click += new System.EventHandler(this.mnu_orient_rblt_Click);
            // 
            // mnu_orient_tlbr
            // 
            this.mnu_orient_tlbr.Index = 3;
            this.mnu_orient_tlbr.Text = "Mirror";
            this.mnu_orient_tlbr.Click += new System.EventHandler(this.mnu_orient_tlbr_Click);
            // 
            // mnu_orientation_rotate180
            // 
            this.mnu_orientation_rotate180.Index = 4;
            this.mnu_orientation_rotate180.Text = "Rotate 180";
            this.mnu_orientation_rotate180.Click += new System.EventHandler(this.mnu_orientation_rotate180_Click);
            // 
            // mnu_stuff
            // 
            this.mnu_stuff.Index = 4;
            this.mnu_stuff.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_BoundingRectangle,
            this.menuItem10,
            this.mnu_stuff_settext,
            this.mnu_stuff_backcolor,
            this.mnu_stuff_pencolor,
            this.mnu_stuff_penwidth,
            this.mnu_stuff_bevelshadow,
            this.mnu_stuff_bevellight,
            this.mnu_stuff_setfont,
            this.mnu_stuff_getitemtext,
            this.mnu_stuff_hb,
            this.mnu_stuff_hf,
            this.mnu_stuff_moveable,
            this.mnu_stuff_fixed,
            this.mnu_stuff_sizeable,
            this.mnu_stuff_getitemfont,
            this.mnu_stuff_setnewdib,
            this.mnu_stuff_toggleuserdraw});
            this.mnu_stuff.Text = "Stuff";
            this.mnu_stuff.Select += new System.EventHandler(this.mnu_stuff_Select);
            // 
            // mnu_BoundingRectangle
            // 
            this.mnu_BoundingRectangle.Index = 0;
            this.mnu_BoundingRectangle.Text = "Bounding Rectangle...";
            this.mnu_BoundingRectangle.Click += new System.EventHandler(this.menuItemBoundingRectangle_Click);
            // 
            // menuItem10
            // 
            this.menuItem10.Index = 1;
            this.menuItem10.Text = "-";
            // 
            // mnu_stuff_settext
            // 
            this.mnu_stuff_settext.Index = 2;
            this.mnu_stuff_settext.Text = "Set item text";
            this.mnu_stuff_settext.Click += new System.EventHandler(this.mnu_stuff_settext_Click);
            // 
            // mnu_stuff_backcolor
            // 
            this.mnu_stuff_backcolor.Index = 3;
            this.mnu_stuff_backcolor.Text = "Set item backcolor";
            this.mnu_stuff_backcolor.Click += new System.EventHandler(this.mnu_stuff_backcolor_Click);
            // 
            // mnu_stuff_pencolor
            // 
            this.mnu_stuff_pencolor.Index = 4;
            this.mnu_stuff_pencolor.Text = "Set item pen color";
            this.mnu_stuff_pencolor.Click += new System.EventHandler(this.mnu_stuff_pencolor_Click);
            // 
            // mnu_stuff_penwidth
            // 
            this.mnu_stuff_penwidth.Index = 5;
            this.mnu_stuff_penwidth.Text = "Set item pen width";
            this.mnu_stuff_penwidth.Click += new System.EventHandler(this.mnu_stuff_penwidth_Click);
            // 
            // mnu_stuff_bevelshadow
            // 
            this.mnu_stuff_bevelshadow.Index = 6;
            this.mnu_stuff_bevelshadow.Text = "Set item bevelshadowcolor";
            this.mnu_stuff_bevelshadow.Click += new System.EventHandler(this.mnu_stuff_bevelshadow_Click);
            // 
            // mnu_stuff_bevellight
            // 
            this.mnu_stuff_bevellight.Index = 7;
            this.mnu_stuff_bevellight.Text = "Set item bevellightcolor";
            this.mnu_stuff_bevellight.Click += new System.EventHandler(this.mnu_stuff_bevellight_Click);
            // 
            // mnu_stuff_setfont
            // 
            this.mnu_stuff_setfont.Index = 8;
            this.mnu_stuff_setfont.Text = "Set item font...";
            this.mnu_stuff_setfont.Click += new System.EventHandler(this.mnu_stuff_setfont_Click);
            // 
            // mnu_stuff_getitemtext
            // 
            this.mnu_stuff_getitemtext.Index = 9;
            this.mnu_stuff_getitemtext.Text = "Get item text";
            this.mnu_stuff_getitemtext.Click += new System.EventHandler(this.mnu_stuff_getitemtext_Click);
            // 
            // mnu_stuff_hb
            // 
            this.mnu_stuff_hb.Index = 10;
            this.mnu_stuff_hb.Text = "Toggle HighlightBack";
            this.mnu_stuff_hb.Click += new System.EventHandler(this.mnu_stuff_hb_Click);
            // 
            // mnu_stuff_hf
            // 
            this.mnu_stuff_hf.Index = 11;
            this.mnu_stuff_hf.Text = "Toggle HighlightFill";
            this.mnu_stuff_hf.Click += new System.EventHandler(this.mnu_stuff_hf_Click);
            // 
            // mnu_stuff_moveable
            // 
            this.mnu_stuff_moveable.Index = 12;
            this.mnu_stuff_moveable.Text = "Toggle item moveable";
            this.mnu_stuff_moveable.Click += new System.EventHandler(this.mnu_stuff_moveable_Click);
            // 
            // mnu_stuff_fixed
            // 
            this.mnu_stuff_fixed.Index = 13;
            this.mnu_stuff_fixed.Text = "Toggle item fixed";
            this.mnu_stuff_fixed.Click += new System.EventHandler(this.mnu_stuff_fixed_Click);
            // 
            // mnu_stuff_sizeable
            // 
            this.mnu_stuff_sizeable.Index = 14;
            this.mnu_stuff_sizeable.Text = "Toggle item sizeable";
            this.mnu_stuff_sizeable.Click += new System.EventHandler(this.mnu_stuff_sizeable_Click);
            // 
            // mnu_stuff_getitemfont
            // 
            this.mnu_stuff_getitemfont.Index = 15;
            this.mnu_stuff_getitemfont.Text = "Get item font";
            this.mnu_stuff_getitemfont.Click += new System.EventHandler(this.mnu_stuff_getitemfont_Click);
            // 
            // mnu_stuff_setnewdib
            // 
            this.mnu_stuff_setnewdib.Index = 16;
            this.mnu_stuff_setnewdib.Text = "Set new dib into Image object";
            this.mnu_stuff_setnewdib.Click += new System.EventHandler(this.mnu_stuff_setnewdib_Click);
            // 
            // mnu_stuff_toggleuserdraw
            // 
            this.mnu_stuff_toggleuserdraw.Index = 17;
            this.mnu_stuff_toggleuserdraw.Text = "Toggle user draw";
            this.mnu_stuff_toggleuserdraw.Click += new System.EventHandler(this.mnu_stuff_toggleuserdraw_Click);
            // 
            // mnu_mode
            // 
            this.mnu_mode.Index = 5;
            this.mnu_mode.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_mode_editmode,
            this.mnu_mode_interactive});
            this.mnu_mode.Text = "Mode";
            // 
            // mnu_mode_editmode
            // 
            this.mnu_mode_editmode.Index = 0;
            this.mnu_mode_editmode.Text = "Set Edit mode";
            this.mnu_mode_editmode.Click += new System.EventHandler(this.mnu_mode_editmode_Click);
            // 
            // mnu_mode_interactive
            // 
            this.mnu_mode_interactive.Index = 1;
            this.mnu_mode_interactive.Text = "Set Interactive mode";
            this.mnu_mode_interactive.Click += new System.EventHandler(this.mnu_mode_interactive_Click);
            // 
            // mnu_cm
            // 
            this.mnu_cm.Index = 6;
            this.mnu_cm.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_cm_enable});
            this.mnu_cm.Text = "Context Menu";
            this.mnu_cm.Select += new System.EventHandler(this.mnu_cm_Select);
            // 
            // mnu_cm_enable
            // 
            this.mnu_cm_enable.Index = 0;
            this.mnu_cm_enable.Text = "Enable";
            this.mnu_cm_enable.Click += new System.EventHandler(this.mnu_cm_enable_Click);
            // 
            // mnuCustom
            // 
            this.mnuCustom.Index = 7;
            this.mnuCustom.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_Creating,
            this.mnu_Ellipse,
            this.mnu_TextAnnotations});
            this.mnuCustom.Text = "Custom Cursors";
            // 
            // mnu_Creating
            // 
            this.mnu_Creating.Index = 0;
            this.mnu_Creating.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_RectangleCustom,
            this.menuItem13,
            this.menuItem15,
            this.menuItem16});
            this.mnu_Creating.Text = "For Rectangle Annotations";
            // 
            // mnu_RectangleCustom
            // 
            this.mnu_RectangleCustom.Index = 0;
            this.mnu_RectangleCustom.Text = "Select Custom Cursor";
            this.mnu_RectangleCustom.Click += new System.EventHandler(this.menuItem14_Click);
            // 
            // menuItem13
            // 
            this.menuItem13.Index = 1;
            this.menuItem13.Text = "Use for Creating";
            this.menuItem13.Click += new System.EventHandler(this.menuItem13_Click);
            // 
            // menuItem15
            // 
            this.menuItem15.Index = 2;
            this.menuItem15.Text = "Use for Moving";
            this.menuItem15.Click += new System.EventHandler(this.menuItem15_Click);
            // 
            // menuItem16
            // 
            this.menuItem16.Index = 3;
            this.menuItem16.Text = "Use for Selecting";
            this.menuItem16.Click += new System.EventHandler(this.menuItem16_Click);
            // 
            // mnu_Ellipse
            // 
            this.mnu_Ellipse.Index = 1;
            this.mnu_Ellipse.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_CustomEllipse,
            this.mnu_CreatingEllipse,
            this.mnu_MovingEllipse,
            this.mnu_SelectingEllipse});
            this.mnu_Ellipse.Text = "For Ellipse Annotations";
            // 
            // mnu_CustomEllipse
            // 
            this.mnu_CustomEllipse.Index = 0;
            this.mnu_CustomEllipse.Text = "Select Custom Cursor";
            this.mnu_CustomEllipse.Click += new System.EventHandler(this.menuItem18_Click);
            // 
            // mnu_CreatingEllipse
            // 
            this.mnu_CreatingEllipse.Index = 1;
            this.mnu_CreatingEllipse.Text = "Use for Creating";
            this.mnu_CreatingEllipse.Click += new System.EventHandler(this.menuItem19_Click);
            // 
            // mnu_MovingEllipse
            // 
            this.mnu_MovingEllipse.Index = 2;
            this.mnu_MovingEllipse.Text = "Use for Moving";
            this.mnu_MovingEllipse.Click += new System.EventHandler(this.menuItem21_Click);
            // 
            // mnu_SelectingEllipse
            // 
            this.mnu_SelectingEllipse.Index = 3;
            this.mnu_SelectingEllipse.Text = "Use for Selecting";
            this.mnu_SelectingEllipse.Click += new System.EventHandler(this.menuItem20_Click);
            // 
            // mnu_TextAnnotations
            // 
            this.mnu_TextAnnotations.Index = 2;
            this.mnu_TextAnnotations.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_CustomText,
            this.mnu_CreatingText,
            this.mnu_MovingText,
            this.mnu_SelectingText});
            this.mnu_TextAnnotations.Text = "For Text Annotations";
            // 
            // mnu_CustomText
            // 
            this.mnu_CustomText.Index = 0;
            this.mnu_CustomText.Text = "Select Custom Cursor";
            this.mnu_CustomText.Click += new System.EventHandler(this.menuItem23_Click);
            // 
            // mnu_CreatingText
            // 
            this.mnu_CreatingText.Index = 1;
            this.mnu_CreatingText.Text = "Use for Creating";
            this.mnu_CreatingText.Click += new System.EventHandler(this.menuItem24_Click);
            // 
            // mnu_MovingText
            // 
            this.mnu_MovingText.Index = 2;
            this.mnu_MovingText.Text = "Use for Moving";
            this.mnu_MovingText.Click += new System.EventHandler(this.menuItem26_Click);
            // 
            // mnu_SelectingText
            // 
            this.mnu_SelectingText.Index = 3;
            this.mnu_SelectingText.Text = "Use for Selecting";
            this.mnu_SelectingText.Click += new System.EventHandler(this.menuItem25_Click);
            // 
            // mnu_Groups
            // 
            this.mnu_Groups.Index = 8;
            this.mnu_Groups.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_objects_allowgrouping,
            this.mnu_objects_set2,
            this.mnu_CreateGroup,
            this.mnu_DeleteGroup,
            this.mnu_SetGroupEmpty,
            this.mnu_SelectGroupItems,
            this.mnu_GroupAddSelectedItems,
            this.mnu_GroupRemoveSelectedItems,
            this.mnu_YokeGroupItems,
            this.mnu_MirrorYoked,
            this.mnu_InvertYoked,
            this.mnu_SetGroupUserString});
            this.mnu_Groups.Text = "Groups";
            this.mnu_Groups.Select += new System.EventHandler(this.menuItemGroups_Click);
            this.mnu_Groups.Click += new System.EventHandler(this.menuItemGroups_Click);
            // 
            // mnu_objects_allowgrouping
            // 
            this.mnu_objects_allowgrouping.Index = 0;
            this.mnu_objects_allowgrouping.Text = "Toggle Allow Grouping";
            this.mnu_objects_allowgrouping.Click += new System.EventHandler(this.mnu_objects_allowgrouping_Click);
            // 
            // mnu_objects_set2
            // 
            this.mnu_objects_set2.Index = 1;
            this.mnu_objects_set2.Text = "-";
            // 
            // mnu_CreateGroup
            // 
            this.mnu_CreateGroup.Index = 2;
            this.mnu_CreateGroup.Text = "Create...";
            this.mnu_CreateGroup.Click += new System.EventHandler(this.menuItemCreateGroup_Click);
            // 
            // mnu_DeleteGroup
            // 
            this.mnu_DeleteGroup.Checked = true;
            this.mnu_DeleteGroup.Index = 3;
            this.mnu_DeleteGroup.Text = "Delete";
            this.mnu_DeleteGroup.Click += new System.EventHandler(this.menuItemDeleteGroup_Click);
            // 
            // mnu_SetGroupEmpty
            // 
            this.mnu_SetGroupEmpty.Index = 4;
            this.mnu_SetGroupEmpty.Text = "Set Empty";
            this.mnu_SetGroupEmpty.Click += new System.EventHandler(this.menuItemSetGroupEmpty_Click);
            // 
            // mnu_SelectGroupItems
            // 
            this.mnu_SelectGroupItems.Index = 5;
            this.mnu_SelectGroupItems.Text = "Show Items Selected";
            this.mnu_SelectGroupItems.Click += new System.EventHandler(this.menuItemSelectGroupItems_Click);
            // 
            // mnu_GroupAddSelectedItems
            // 
            this.mnu_GroupAddSelectedItems.Index = 6;
            this.mnu_GroupAddSelectedItems.Text = "Add Selected Items";
            this.mnu_GroupAddSelectedItems.Click += new System.EventHandler(this.menuItemGroupAddSelectedItems_Click);
            // 
            // mnu_GroupRemoveSelectedItems
            // 
            this.mnu_GroupRemoveSelectedItems.Index = 7;
            this.mnu_GroupRemoveSelectedItems.Text = "Remove Selected Items";
            this.mnu_GroupRemoveSelectedItems.Click += new System.EventHandler(this.menuItemGroupRemoveSelectedItems_Click);
            // 
            // mnu_YokeGroupItems
            // 
            this.mnu_YokeGroupItems.Index = 8;
            this.mnu_YokeGroupItems.Text = "Toggle Yoked";
            this.mnu_YokeGroupItems.Click += new System.EventHandler(this.menuItemYokeGroupItems_Click);
            // 
            // mnu_MirrorYoked
            // 
            this.mnu_MirrorYoked.Index = 9;
            this.mnu_MirrorYoked.Text = "Mirror Yoked Items";
            this.mnu_MirrorYoked.Click += new System.EventHandler(this.menuItemMirrorYoked_click);
            // 
            // mnu_InvertYoked
            // 
            this.mnu_InvertYoked.Index = 10;
            this.mnu_InvertYoked.Text = "Invert Yoked Items";
            this.mnu_InvertYoked.Click += new System.EventHandler(this.menuItemInvertYoked_click);
            // 
            // mnu_SetGroupUserString
            // 
            this.mnu_SetGroupUserString.Index = 11;
            this.mnu_SetGroupUserString.Text = " User String...";
            this.mnu_SetGroupUserString.Click += new System.EventHandler(this.SetGroupUserString_Select);
            // 
            // mnu_ToolBar
            // 
            this.mnu_ToolBar.Index = 9;
            this.mnu_ToolBar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_ToolbarRulerTool,
            this.mnu_ToolbarFreehandTool,
            this.mnu_ToolbarEllipseTool,
            this.mnu_ToolbarTextTool,
            this.mnu_ToolbarRectangleTool,
            this.mnu_ToolbarStampTool,
            this.mnu_ToolbarPolylineTool,
            this.mnu_ToolbarLineTool,
            this.mnu_ToolbarImageTool,
            this.mnu_ToolbarButtonTool,
            this.mnu_ToolbarPolygonTool,
            this.mnu_BlockHighlight,
            this.mnu_NoteTool});
            this.mnu_ToolBar.Text = "ToolBar";
            this.mnu_ToolBar.Select += new System.EventHandler(this.menuItemToolBar_Select);
            // 
            // mnu_ToolbarRulerTool
            // 
            this.mnu_ToolbarRulerTool.Index = 0;
            this.mnu_ToolbarRulerTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_RulerToolEnabeled,
            this.mnu_RulerToolVisible});
            this.mnu_ToolbarRulerTool.Text = "RulerTool";
            // 
            // mnu_RulerToolEnabeled
            // 
            this.mnu_RulerToolEnabeled.Index = 0;
            this.mnu_RulerToolEnabeled.Text = "Enabled";
            this.mnu_RulerToolEnabeled.Click += new System.EventHandler(this.menuItemRulerToolEnabeled_Click);
            // 
            // mnu_RulerToolVisible
            // 
            this.mnu_RulerToolVisible.Index = 1;
            this.mnu_RulerToolVisible.Text = "Visible";
            this.mnu_RulerToolVisible.Click += new System.EventHandler(this.menuItemRulerToolVisible_Click);
            // 
            // mnu_ToolbarFreehandTool
            // 
            this.mnu_ToolbarFreehandTool.Index = 1;
            this.mnu_ToolbarFreehandTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_FreehandToolEnabled,
            this.mnu_FreehandToolVisible});
            this.mnu_ToolbarFreehandTool.Text = "Freehand Tool";
            // 
            // mnu_FreehandToolEnabled
            // 
            this.mnu_FreehandToolEnabled.Index = 0;
            this.mnu_FreehandToolEnabled.Text = "Enabled";
            this.mnu_FreehandToolEnabled.Click += new System.EventHandler(this.menuItemFreehandToolEnabled_Click);
            // 
            // mnu_FreehandToolVisible
            // 
            this.mnu_FreehandToolVisible.Index = 1;
            this.mnu_FreehandToolVisible.Text = "Visible";
            this.mnu_FreehandToolVisible.Click += new System.EventHandler(this.menuItemFreehandToolVisible_Click);
            // 
            // mnu_ToolbarEllipseTool
            // 
            this.mnu_ToolbarEllipseTool.Index = 2;
            this.mnu_ToolbarEllipseTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_EllipseToolEnabeld,
            this.mnu_EllipseToolVisible});
            this.mnu_ToolbarEllipseTool.Text = "Elliplse Tool";
            this.mnu_ToolbarEllipseTool.Click += new System.EventHandler(this.mnu_ToolbarEllipseTool_Click);
            // 
            // mnu_EllipseToolEnabeld
            // 
            this.mnu_EllipseToolEnabeld.Index = 0;
            this.mnu_EllipseToolEnabeld.Text = "Enabled";
            this.mnu_EllipseToolEnabeld.Click += new System.EventHandler(this.menuItemEllipseToolEnabeld_Click);
            // 
            // mnu_EllipseToolVisible
            // 
            this.mnu_EllipseToolVisible.Index = 1;
            this.mnu_EllipseToolVisible.Text = "Visible";
            this.mnu_EllipseToolVisible.Click += new System.EventHandler(this.menuItemEllipseToolVisible_Click);
            // 
            // mnu_ToolbarTextTool
            // 
            this.mnu_ToolbarTextTool.Index = 3;
            this.mnu_ToolbarTextTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_TextToolEnabled,
            this.mnu_TextToolVisible});
            this.mnu_ToolbarTextTool.Text = "Text Tool";
            // 
            // mnu_TextToolEnabled
            // 
            this.mnu_TextToolEnabled.Index = 0;
            this.mnu_TextToolEnabled.Text = "Enabled";
            this.mnu_TextToolEnabled.Click += new System.EventHandler(this.menuItemTextToolEnabled_Click);
            // 
            // mnu_TextToolVisible
            // 
            this.mnu_TextToolVisible.Index = 1;
            this.mnu_TextToolVisible.Text = "Visible";
            this.mnu_TextToolVisible.Click += new System.EventHandler(this.menuItemTextToolVisible_Click);
            // 
            // mnu_ToolbarRectangleTool
            // 
            this.mnu_ToolbarRectangleTool.Index = 4;
            this.mnu_ToolbarRectangleTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_RectangleToolEnabled,
            this.mnu_RectangleToolVisible});
            this.mnu_ToolbarRectangleTool.Text = "Rectangle Tool";
            this.mnu_ToolbarRectangleTool.Click += new System.EventHandler(this.mnu_ToolbarRectangleTool_Click);
            // 
            // mnu_RectangleToolEnabled
            // 
            this.mnu_RectangleToolEnabled.Index = 0;
            this.mnu_RectangleToolEnabled.Text = "Enabled";
            this.mnu_RectangleToolEnabled.Click += new System.EventHandler(this.menuItemRectangleToolEnabled_Click);
            // 
            // mnu_RectangleToolVisible
            // 
            this.mnu_RectangleToolVisible.Index = 1;
            this.mnu_RectangleToolVisible.Text = "Visible";
            this.mnu_RectangleToolVisible.Click += new System.EventHandler(this.menuItemRectangleToolVisible_Click);
            // 
            // mnu_ToolbarStampTool
            // 
            this.mnu_ToolbarStampTool.Index = 5;
            this.mnu_ToolbarStampTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_StampToolEnabled,
            this.mnu_StampToolVisible});
            this.mnu_ToolbarStampTool.Text = "Stamp Tool";
            // 
            // mnu_StampToolEnabled
            // 
            this.mnu_StampToolEnabled.Index = 0;
            this.mnu_StampToolEnabled.Text = "Enabled";
            this.mnu_StampToolEnabled.Click += new System.EventHandler(this.menuItemStampToolEnabled_Click);
            // 
            // mnu_StampToolVisible
            // 
            this.mnu_StampToolVisible.Index = 1;
            this.mnu_StampToolVisible.Text = "Visible";
            this.mnu_StampToolVisible.Click += new System.EventHandler(this.menuItemStampToolVisible_Click);
            // 
            // mnu_ToolbarPolylineTool
            // 
            this.mnu_ToolbarPolylineTool.Index = 6;
            this.mnu_ToolbarPolylineTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_PolylineToolEnabled,
            this.mnu_PolylineToolVisible});
            this.mnu_ToolbarPolylineTool.Text = "Polyline Tool";
            // 
            // mnu_PolylineToolEnabled
            // 
            this.mnu_PolylineToolEnabled.Index = 0;
            this.mnu_PolylineToolEnabled.Text = "Enabled";
            this.mnu_PolylineToolEnabled.Click += new System.EventHandler(this.menuItemPolylineToolEnabled_Click);
            // 
            // mnu_PolylineToolVisible
            // 
            this.mnu_PolylineToolVisible.Index = 1;
            this.mnu_PolylineToolVisible.Text = "Visible";
            this.mnu_PolylineToolVisible.Click += new System.EventHandler(this.menuItemPolylineToolVisible_Click);
            // 
            // mnu_ToolbarLineTool
            // 
            this.mnu_ToolbarLineTool.Index = 7;
            this.mnu_ToolbarLineTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_LineToolEnabled,
            this.mnu_LineToolVisible});
            this.mnu_ToolbarLineTool.Text = "Line Tool";
            // 
            // mnu_LineToolEnabled
            // 
            this.mnu_LineToolEnabled.Index = 0;
            this.mnu_LineToolEnabled.Text = "Enabled";
            this.mnu_LineToolEnabled.Click += new System.EventHandler(this.menuItemLineToolEnabled_Click);
            // 
            // mnu_LineToolVisible
            // 
            this.mnu_LineToolVisible.Index = 1;
            this.mnu_LineToolVisible.Text = "Visible";
            this.mnu_LineToolVisible.Click += new System.EventHandler(this.menuItemLineToolVisible_Click);
            // 
            // mnu_ToolbarImageTool
            // 
            this.mnu_ToolbarImageTool.Index = 8;
            this.mnu_ToolbarImageTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_ImageToolEnabled,
            this.mnu_ImageToolVisible});
            this.mnu_ToolbarImageTool.Text = "Image Tool";
            // 
            // mnu_ImageToolEnabled
            // 
            this.mnu_ImageToolEnabled.Index = 0;
            this.mnu_ImageToolEnabled.Text = "Enabled";
            this.mnu_ImageToolEnabled.Click += new System.EventHandler(this.menuItemImageToolEnabled_Click);
            // 
            // mnu_ImageToolVisible
            // 
            this.mnu_ImageToolVisible.Index = 1;
            this.mnu_ImageToolVisible.Text = "Visible";
            this.mnu_ImageToolVisible.Click += new System.EventHandler(this.menuItemImageToolVisible_Click);
            // 
            // mnu_ToolbarButtonTool
            // 
            this.mnu_ToolbarButtonTool.Index = 9;
            this.mnu_ToolbarButtonTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_ButtonToolEnabled,
            this.mnu_ButtonToolVisible});
            this.mnu_ToolbarButtonTool.Text = "Button Tool";
            // 
            // mnu_ButtonToolEnabled
            // 
            this.mnu_ButtonToolEnabled.Index = 0;
            this.mnu_ButtonToolEnabled.Text = "Enabled";
            this.mnu_ButtonToolEnabled.Click += new System.EventHandler(this.menuItemButtonToolEnabled_Click);
            // 
            // mnu_ButtonToolVisible
            // 
            this.mnu_ButtonToolVisible.Index = 1;
            this.mnu_ButtonToolVisible.Text = "Visible";
            this.mnu_ButtonToolVisible.Click += new System.EventHandler(this.menuItemButtonToolVisible_Click);
            // 
            // mnu_ToolbarPolygonTool
            // 
            this.mnu_ToolbarPolygonTool.Index = 10;
            this.mnu_ToolbarPolygonTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_PolygonToolEnabled,
            this.mnu_PolygonToolVisible});
            this.mnu_ToolbarPolygonTool.Text = "Polygon Tool";
            // 
            // mnu_PolygonToolEnabled
            // 
            this.mnu_PolygonToolEnabled.Index = 0;
            this.mnu_PolygonToolEnabled.Text = "Enabled";
            this.mnu_PolygonToolEnabled.Click += new System.EventHandler(this.menuItemPolygonToolEnabled_Click);
            // 
            // mnu_PolygonToolVisible
            // 
            this.mnu_PolygonToolVisible.Index = 1;
            this.mnu_PolygonToolVisible.Text = "Visible";
            this.mnu_PolygonToolVisible.Click += new System.EventHandler(this.menuItemPolygonToolVisible_Click);
            // 
            // mnu_BlockHighlight
            // 
            this.mnu_BlockHighlight.Index = 11;
            this.mnu_BlockHighlight.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_BlockHighlightEnabled,
            this.mnu_BlockHighlightVisible});
            this.mnu_BlockHighlight.Text = "Block Highlight Tool";
            // 
            // mnu_BlockHighlightEnabled
            // 
            this.mnu_BlockHighlightEnabled.Index = 0;
            this.mnu_BlockHighlightEnabled.Text = "Enabled";
            this.mnu_BlockHighlightEnabled.Click += new System.EventHandler(this.menuItemBlockHighlightEnabled_Click);
            // 
            // mnu_BlockHighlightVisible
            // 
            this.mnu_BlockHighlightVisible.Index = 1;
            this.mnu_BlockHighlightVisible.Text = "Visible";
            this.mnu_BlockHighlightVisible.Click += new System.EventHandler(this.menuItemBlockHighlightVisible_Click);
            // 
            // mnu_NoteTool
            // 
            this.mnu_NoteTool.Index = 12;
            this.mnu_NoteTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_NoteToolEnabled,
            this.mnu_NoteToolVisible});
            this.mnu_NoteTool.Text = "Note Tool";
            // 
            // mnu_NoteToolEnabled
            // 
            this.mnu_NoteToolEnabled.Index = 0;
            this.mnu_NoteToolEnabled.Text = "Enabled";
            this.mnu_NoteToolEnabled.Click += new System.EventHandler(this.menuItemNoteToolEnabled_Click);
            // 
            // mnu_NoteToolVisible
            // 
            this.mnu_NoteToolVisible.Index = 1;
            this.mnu_NoteToolVisible.Text = "Visible";
            this.mnu_NoteToolVisible.Click += new System.EventHandler(this.menuItemNoteToolVisible_Click);
            // 
            // mnu_About
            // 
            this.mnu_About.Index = 10;
            this.mnu_About.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnu_AboutIX,
            this.mnu_AboutNX});
            this.mnu_About.Text = "&About";
            // 
            // mnu_AboutIX
            // 
            this.mnu_AboutIX.Index = 0;
            this.mnu_AboutIX.Text = "&ImagXpress...";
            this.mnu_AboutIX.Click += new System.EventHandler(this.menuItemAboutIX_Click);
            // 
            // mnu_AboutNX
            // 
            this.mnu_AboutNX.Index = 1;
            this.mnu_AboutNX.Text = "&NotateXpress..";
            this.mnu_AboutNX.Click += new System.EventHandler(this.menuItemAboutNX_Click);
            // 
            // ZoomOut
            // 
            this.ZoomOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ZoomOut.BackColor = System.Drawing.SystemColors.Control;
            this.ZoomOut.Cursor = System.Windows.Forms.Cursors.Default;
            this.ZoomOut.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZoomOut.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ZoomOut.Location = new System.Drawing.Point(780, 598);
            this.ZoomOut.Name = "ZoomOut";
            this.ZoomOut.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ZoomOut.Size = new System.Drawing.Size(65, 33);
            this.ZoomOut.TabIndex = 8;
            this.ZoomOut.Text = "Zoom Out";
            this.ZoomOut.UseVisualStyleBackColor = false;
            this.ZoomOut.Click += new System.EventHandler(this.ZoomOut_Click);
            // 
            // ZoomIn
            // 
            this.ZoomIn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ZoomIn.BackColor = System.Drawing.SystemColors.Control;
            this.ZoomIn.Cursor = System.Windows.Forms.Cursors.Default;
            this.ZoomIn.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ZoomIn.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ZoomIn.Location = new System.Drawing.Point(688, 598);
            this.ZoomIn.Name = "ZoomIn";
            this.ZoomIn.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ZoomIn.Size = new System.Drawing.Size(65, 33);
            this.ZoomIn.TabIndex = 14;
            this.ZoomIn.Text = "Zoom In";
            this.ZoomIn.UseVisualStyleBackColor = false;
            this.ZoomIn.Click += new System.EventHandler(this.ZoomIn_Click);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.FileName = "doc1";
            // 
            // imagXView1
            // 
            this.imagXView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imagXView1.Location = new System.Drawing.Point(16, 88);
            this.imagXView1.MouseWheelCapture = false;
            this.imagXView1.Name = "imagXView1";
            this.imagXView1.Size = new System.Drawing.Size(836, 494);
            this.imagXView1.TabIndex = 7;
            // 
            // imagXProcessor
            // 
            this.imagXProcessor.BackgroundColor = System.Drawing.Color.Black;
            this.imagXProcessor.ContrastMethod = PegasusImaging.WinForms.ImagXpress9.ContrastMethod.DefaultContrast;
            this.imagXProcessor.ProgressPercent = 10;
            this.imagXProcessor.Redeyes = null;
            // 
            // OutputList
            // 
            this.OutputList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.OutputList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutputList.HorizontalScrollbar = true;
            this.OutputList.Location = new System.Drawing.Point(16, 647);
            this.OutputList.Name = "OutputList";
            this.OutputList.Size = new System.Drawing.Size(1059, 82);
            this.OutputList.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.Location = new System.Drawing.Point(23, 615);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(64, 16);
            this.label2.TabIndex = 16;
            this.label2.Text = "Events:";
            // 
            // notateXpress1
            // 
            this.notateXpress1.AllowPaint = true;
            this.notateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress9.FontScaling.Normal;
            this.notateXpress1.ImagXpressLoad = true;
            this.notateXpress1.ImagXpressSave = true;
            this.notateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress9.AnnotationMode.Edit;
            this.notateXpress1.LineScaling = PegasusImaging.WinForms.NotateXpress9.LineScaling.Normal;
            this.notateXpress1.MultiLineEdit = false;
            this.notateXpress1.RecalibrateXdpi = -1;
            this.notateXpress1.RecalibrateYdpi = -1;
            this.notateXpress1.ToolTipTimeEdit = 0;
            this.notateXpress1.ToolTipTimeInteractive = 0;
            this.notateXpress1.CurrentLayerChange += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.CurrentLayerChangeEventHandler(this.notateXpress1_CurrentLayerChange);
            this.notateXpress1.MouseUp += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.MouseUpEventHandler(this.notateXpress1_MouseUp);
            this.notateXpress1.ItemChanged += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.ItemChangedEventHandler(this.notateXpress1_ItemChanged);
            this.notateXpress1.UserDraw += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.UserDrawEventHandler(this.notateXpress1_UserDraw);
            this.notateXpress1.UserGroupDestroyed += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.UserGroupDestroyedEventHandler(this.notateXpress1_UserGroupDestroyed);
            this.notateXpress1.MouseDown += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.MouseDownEventHandler(this.notateXpress1_MouseDown);
            this.notateXpress1.LayerRestored += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.LayerRestoredEventHandler(this.notateXpress1_LayerRestored);
            this.notateXpress1.MenuSelect += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.MenuEventHandler(this.notateXpress1_MenuSelect);
            this.notateXpress1.MouseMove += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.MouseMoveEventHandler(this.notateXpress1_MouseMove);
            this.notateXpress1.RequestLayerPassword += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.GetLayerPasswordEventHandler(this.notateXpress1_RequestLayerPassword);
            this.notateXpress1.AnnotationSelected += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationSelectedEventHandler(this.notateXpress1_AnnotationSelected);
            this.notateXpress1.ToolbarSelect += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.ToolbarEventHandler(this.notateXpress1_ToolbarSelect);
            this.notateXpress1.AnnotationAdded += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationAddedEventHandler(this.notateXpress1_AnnotationAdded);
            this.notateXpress1.DoubleClick += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.DoubleClickEventHandler(this.notateXpress1_DoubleClick);
            this.notateXpress1.AnnotationDeleted += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationDeletedEventHandler(this.notateXpress1_AnnotationDeleted);
            this.notateXpress1.UserGroupCreated += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.UserGroupCreatedEventHandler(this.notateXpress1_UserGroupCreated);
            this.notateXpress1.AnnotationMoved += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.AnnotationMovedEventHandler(this.notateXpress1_AnnotationMoved);
            this.notateXpress1.Click += new PegasusImaging.WinForms.NotateXpress9.NotateXpress.ClickEventHandler(this.notateXpress1_Click);
            // 
            // lstInfo
            // 
            this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstInfo.Items.AddRange(new object[] {
            "This sample demonstrates the following:",
            "1)Adding various annotation types including redaction and ruler types.",
            "2)Creating,deleting and hiding layers.",
            "3)Saving annotations in the Pegasus NXP format and the Kodak Wang Imaging format." +
                ""});
            this.lstInfo.Location = new System.Drawing.Point(16, 8);
            this.lstInfo.Name = "lstInfo";
            this.lstInfo.Size = new System.Drawing.Size(1068, 69);
            this.lstInfo.TabIndex = 21;
            // 
            // lblLastError
            // 
            this.lblLastError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastError.Location = new System.Drawing.Point(860, 248);
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(215, 32);
            this.lblLastError.TabIndex = 18;
            this.lblLastError.Text = "Last Error:";
            // 
            // lblerror
            // 
            this.lblerror.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblerror.Location = new System.Drawing.Point(868, 264);
            this.lblerror.Name = "lblerror";
            this.lblerror.Size = new System.Drawing.Size(224, 91);
            this.lblerror.TabIndex = 17;
            // 
            // FormMain
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1100, 775);
            this.Controls.Add(this.lstInfo);
            this.Controls.Add(this.lblLastError);
            this.Controls.Add(this.lblerror);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.OutputList);
            this.Controls.Add(this.imagXView1);
            this.Controls.Add(this.ZoomOut);
            this.Controls.Add(this.ZoomIn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.mainMenu;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NotateXpress 9 C# Demo";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);

        }
        #endregion

        private Cursor cursor1, cursor2, cursor3;

        #region Pegasus Imaging Sample Application Standard Functions

        System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
        System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
        System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
        private System.String strCurrentDir = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
        string strDefaultImageFilter = ("All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" +
            "" + (";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" + ("2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" + ("t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" + ("e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" + (")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" + ("cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" + ("s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" + "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"))))))));


        private string GetFileName(string FullName)
        {
            return FullName.Substring((FullName.LastIndexOf("\\") + 1), (FullName.Length
                - (FullName.LastIndexOf("\\") + 1)));
        }

        private static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel)
        {
            ErrorLabel.Text = (ErrorException.Message + ("" + ("\n"
                + ((ErrorException.Source + "")
                + "\n"))));
        }

        private static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel)
        {
            ErrorLabel.Text = (ErrorException.Message + ("" + ("\n"
                + (ErrorException.Source + ("" + ("\n" + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))));
        }

        private string PegasusOpenFile(string sTitle, string sFileFilter)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Title = sTitle;
            dlg.Filter = sFileFilter;
            dlg.InitialDirectory = strCurrentDir;
            if ((dlg.ShowDialog() == DialogResult.OK))
            {
                strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
                return dlg.FileName;
            }
            else
            {
                return "";
            }
        }

        private string PegasusSaveFile(string sTitle, string sFilter)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Title = sTitle;
            dlg.Filter = sFilter;
            dlg.InitialDirectory = strCurrentDir;
            if ((dlg.ShowDialog() == DialogResult.OK))
            {
                strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
                return dlg.FileName;
            }
            else
            {
                return "";
            }
        }

        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {            
            Application.Run(new FormMain());
        }

        private void mnu_load_tiff_Click(object sender, System.EventArgs e)
        {
            try
            {
               
                openFileDialog.Title = "Select an Image File";
                openFileDialog.Filter = strDefaultImageFilter;
                openFileDialog.DefaultExt = ".tif";
                openFileDialog.Multiselect = false;
                openFileDialog.InitialDirectory = strCurrentDir;
                openFileDialog.ShowDialog(this);

                string strFilePath = openFileDialog.FileName;
                if (strFilePath != "" && strFilePath != null)
                {

                    //clear out the error in case there was an error from a previous operation
                    lblerror.Text = "";

                    // ---------------
                    // clear existing annotations and layers
                    // ---------------
                    notateXpress1.Layers.Clear();
                    
                    if (imagXView1.Image != null)
                    {
                        imagXView1.Image.Dispose();
                        imagXView1.Image = null;
                    }
                    try
                    {

                        imagXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strFilePath);

                        PegasusImaging.WinForms.NotateXpress9.Layer theNewLayer = createNewLayer("Layer1", System.IntPtr.Zero);

                        this.Text = "Image = " + imagXView1.Image.ImageXData.Width.ToString() + " x " + imagXView1.Image.ImageXData.Height.ToString();
                        theNewLayer.Toolbar.Visible = mnu_ToolBar.Enabled;
                        theNewLayer.Toolbar.Enabled = mnu_ToolBar.Enabled;
                    }
                    catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex)
                    {
                        PegasusError(ex, lblerror);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private PegasusImaging.WinForms.NotateXpress9.Layer createNewLayer(string strLayerName, System.IntPtr DibHandle)
        {
            try
            {
                // ---------------
                // create new layer
                // ---------------
                PegasusImaging.WinForms.NotateXpress9.Layer layer = new Layer();

                // ---------------
                // Add the new layer to the collection and make it the selected layer
                // ---------------
                notateXpress1.Layers.Add(layer);
                layer.Select();

                layer.Name = strLayerName;
                layer.Description = "Layer created " + System.DateTime.Now.ToLongTimeString();
                //layer.Active = true; // probably default value
                layer.Visible = true; // probably default value
                if (notateXpress1.InteractMode == AnnotationMode.Edit)
                    layer.Toolbar.Visible = true;
                else
                {
                    layer.Toolbar.Visible = false;
                    mnu_ToolBar.Enabled = false;
                }

                // ---------------
                // set the button tool text and pen width for this demo
                // ---------------
                layer.Toolbar.ButtonToolbarDefaults.PenWidth = 4;
                layer.Toolbar.ButtonToolbarDefaults.Text = "Click me";

                // ---------------
                // set the image tool defaults for this demo
                // ---------------
                layer.Toolbar.ImageToolbarDefaults.BackStyle = BackStyle.Transparent;
                if (DibHandle != IntPtr.Zero)
                    layer.Toolbar.ImageToolbarDefaults.DibHandle = DibHandle;

                // ---------------
                // set the stamp tool text and font for this demo
                // ---------------
                layer.Toolbar.StampToolbarDefaults.Text = "NotateXpress 9";
                System.Drawing.Font fontCurrent = layer.Toolbar.StampToolbarDefaults.TextFont;
                System.Drawing.Font fontNew = new System.Drawing.Font(fontCurrent.Name, 18, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic);
                layer.Toolbar.StampToolbarDefaults.TextFont = fontNew;

                return layer;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return new Layer();
            }
        }

        private void FormMain_Load(object sender, System.EventArgs e)
        {

            this.Top = (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2;
            this.Left = (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2;
            //--------------
            // load file into ImagXpress
            //--------------
            try
            {

                //**The UnlockRuntime function must be called to distribute the runtime**
                //imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);

                System.String strCurrentDir = System.IO.Directory.GetCurrentDirectory();

                System.String strImagePath = System.IO.Path.Combine(strCurrentDir, "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\benefits.tif");
                imagXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImagePath);


                strImagePath = System.IO.Path.Combine(strCurrentDir, "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\vermont.jpg");
                imageObject = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImagePath);


                // Use the SetClientMethod to connect the ImagXpress control and NotateXpress control
                notateXpress1.ClientWindow = imagXView1.Handle;

                // pass an image file relative path from the samples
                strImagePath = System.IO.Path.Combine(strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\vermont.jpg");
                imageObject = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImagePath);
                imagXView1.AutoScroll = true;

                // set the "global" toolbar default values
                ToolbarDefaults toolbarDefaults = notateXpress1.ToolbarDefaults;
                toolbarDefaults.SetToolTip(AnnotationTool.EllipseTool, "The Ellipse-O-Matic!");
                toolbarDefaults.SetToolTip(AnnotationTool.FreehandTool, "Draw with this!");
                toolbarDefaults.SetToolTip(AnnotationTool.LineTool, "Liney, very, very Liney");
                toolbarDefaults.SetToolTip(AnnotationTool.PointerTool, "Point 'em out!");
                toolbarDefaults.SetToolTip(AnnotationTool.PolygonTool, "Polygons aren't gone");
                toolbarDefaults.SetToolTip(AnnotationTool.RectangleTool, "Rectangle, we got Rectangle");
                toolbarDefaults.SetToolTip(AnnotationTool.StampTool, "Stamp out dumb tool tips!");
                toolbarDefaults.SetToolTip(AnnotationTool.TextTool, "Write me a masterpiece!");
                toolbarDefaults.SetToolTip(AnnotationTool.PolyLineTool, "PolyLine");
                toolbarDefaults.SetToolTip(AnnotationTool.ButtonTool, "Button Tool");
                toolbarDefaults.SetToolTip(AnnotationTool.ImageTool, "Image Tool!");
                toolbarDefaults.SetToolTip(AnnotationTool.RulerTool, "Ruler Tool");
                notateXpress1.ToolbarDefaults.ToolbarActivated = true;

                // Create a new layer and its toolbar
                Layer layer = createNewLayer("Layer1", imageObject.ToHdib(false));

                m_loadOptions.AnnType = m_saveOptions.AnnType = AnnotationType.ImagingForWindows;


                mnu_mode_interactive.Checked = false;
                mnu_mode_editmode.Checked = true;
                _bAutoSelect = true;
                //_bUpdateMoved = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("FormMain_Load Error msg:   " + ex.Message + "\n" +
                    "Error type:  " + ex.GetType().ToString());
            }
        }

        private void mnu_file_select(object sender, System.EventArgs e)
        {
            try
            {
                mnuImagingForWindows.Checked = m_saveOptions.AnnType == AnnotationType.ImagingForWindows ? true : false;
                mnuNotateAnnotations.Checked = m_saveOptions.AnnType == AnnotationType.NotateXpress ? true : false;
                mnuXML.Checked = m_saveOptions.AnnType == AnnotationType.NotateXpressXml ? true : false;
                mnuXFDF.Checked = m_saveOptions.AnnType == AnnotationType.Xfdf ? true : false; 
                mnuViewDirectorAnnotations.Checked = m_saveOptions.AnnType == AnnotationType.TMSSequoia ? true : false;

                mnu_file_wangcompatible.Checked = m_saveOptions.PreserveWangLayers;
                mnu_UnicodeMode.Checked = m_loadOptions.UnicodeMode;
                mnu_ToggleAllowPaint.Checked = notateXpress1.AllowPaint;

                mnu_LoadMemoryStream.Enabled = _SavedAnnotationMemoryStream == null ? false : true;
                mnu_LoadByteArray.Enabled = _SavedAnnotationByteArray == null ? false : true;
                mnuItemLoadAnnotations.Checked = notateXpress1.ImagXpressLoad;
                mnuItemSaveAnnotations.Checked = notateXpress1.ImagXpressSave;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemImagingForWindows_click(object sender, System.EventArgs e)
        {
            try
            {
                m_loadOptions.AnnType = m_saveOptions.AnnType = AnnotationType.ImagingForWindows;

                mnu_file_saveann2fileANN.Enabled = false;
                mnu_file_loadannotationANN.Enabled = false;

                mnu_file_loadannotationNXP.Enabled = false;
                mnu_file_saveann2fileNXP.Enabled = false;

                mnu_saveXML.Enabled = false;
                mnu_loadXML.Enabled = false;

                saveXFDFMenuItem.Enabled = false;
                mnu_loadXFDF.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemNotateAnnotations_click(object sender, System.EventArgs e)
        {
            try
            {
                m_loadOptions.AnnType = m_saveOptions.AnnType = AnnotationType.NotateXpress;

                mnu_file_saveann2fileANN.Enabled = false;
                mnu_file_loadannotationANN.Enabled = false;

                mnu_file_loadannotationNXP.Enabled = true;
                mnu_file_saveann2fileNXP.Enabled = true;

                mnu_saveXML.Enabled = false;
                mnu_loadXML.Enabled = false;

                saveXFDFMenuItem.Enabled = false;
                mnu_loadXFDF.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemViewDirectorAnnotations_click(object sender, System.EventArgs e)
        {
            try
            {
                m_loadOptions.AnnType = m_saveOptions.AnnType = AnnotationType.TMSSequoia;

                mnu_file_saveann2fileANN.Enabled = true;
                mnu_file_loadannotationANN.Enabled = true;

                mnu_file_loadannotationNXP.Enabled = false;
                mnu_file_saveann2fileNXP.Enabled = false;

                mnu_saveXML.Enabled = false;
                mnu_loadXML.Enabled = false;

                saveXFDFMenuItem.Enabled = false;
                mnu_loadXFDF.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemLoadAnnotations_Click(object sender, System.EventArgs e)
        {
            try
            {
                notateXpress1.ImagXpressLoad = !notateXpress1.ImagXpressLoad;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemSaveAnnotations_Click(object sender, System.EventArgs e)
        {
            try
            {
                notateXpress1.ImagXpressSave = !notateXpress1.ImagXpressSave;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_file_save_Click(object sender, System.EventArgs e)
        {
            try
            {
                saveFileDialog.Title = "Save Tiff File";
                saveFileDialog.Filter = "Tiff File (*.tif) | *.tif";
                saveFileDialog.DefaultExt = ".tif";
                saveFileDialog.InitialDirectory = strCurrentDir;
                saveFileDialog.ShowDialog(this);

                string strSaveFilePath = saveFileDialog.FileName;
                if (strSaveFilePath != "" && strSaveFilePath != null)
                {
                    notateXpress1.Layers.SetSaveOptions(m_saveOptions);

                    PegasusImaging.WinForms.ImagXpress9.SaveOptions saveOptions = new PegasusImaging.WinForms.ImagXpress9.SaveOptions();
                    imagXView1.Image.Save(strSaveFilePath, saveOptions);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_file_wangcompatible_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (m_saveOptions.PreserveWangLayers)
                {
                    m_loadOptions.PreserveWangLayers = m_saveOptions.PreserveWangLayers = false;
                }
                else
                {
                    m_loadOptions.PreserveWangLayers = m_saveOptions.PreserveWangLayers = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        private void menuItemSaveByteArray_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (_SavedAnnotationByteArray != null)
                    _SavedAnnotationByteArray = null;

                _SavedAnnotationByteArray = notateXpress1.Layers.SaveToByteArray(m_saveOptions);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemUnicodeMode_Click(object sender, System.EventArgs e)
        {
            try
            {
                m_loadOptions.UnicodeMode = !m_loadOptions.UnicodeMode;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemLoadByteArray_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (_SavedAnnotationByteArray != null)
                    notateXpress1.Layers.FromByteArray(_SavedAnnotationByteArray, m_loadOptions);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemSaveMemoryStream_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (_SavedAnnotationMemoryStream != null)
                    _SavedAnnotationMemoryStream = null;

                _SavedAnnotationMemoryStream = notateXpress1.Layers.SaveToMemoryStream(m_saveOptions);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemLoadMemoryStream_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (_SavedAnnotationMemoryStream != null)
                    notateXpress1.Layers.FromMemoryStream(_SavedAnnotationMemoryStream, m_loadOptions);
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_fileIResY_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (notateXpress1.FontScaling == FontScaling.ResolutionY)
                {
                    notateXpress1.FontScaling = FontScaling.Normal;
                }
                else
                {
                    notateXpress1.FontScaling = FontScaling.ResolutionY;
                }
                mnu_fileIResY.Checked = (notateXpress1.FontScaling == FontScaling.ResolutionY);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemToggleAllowPain_Click(object sender, System.EventArgs e)
        {
            try
            {
                notateXpress1.AllowPaint = !notateXpress1.AllowPaint;
                mnu_ToggleAllowPaint.Checked = notateXpress1.AllowPaint;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_file_exit_Click(object sender, System.EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void mnu_objects_autoselect_Click(object sender, System.EventArgs e)
        {
            try
            {
                _bAutoSelect = !_bAutoSelect;
                mnu_objects_autoselect.Checked = _bAutoSelect;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_reverse_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                    if (layer.Elements.Count > 0)
                        layer.Elements.Reverse();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_create10_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    int top = 15, left = 15, right = 115, bottom = 115;

                    for (int i = 1; i <= 10; i++)
                    {
                        System.Random rand = new System.Random();
                        System.Drawing.Rectangle rect = new Rectangle(top, left, right, bottom);
                        if (i % 2 > 0)
                        { // create a rectangle object
                            RectangleTool element = new RectangleTool();
                            element.PenWidth = (i % 3 + 1);
                            element.PenColor = System.Drawing.Color.FromArgb((int)(rand.NextDouble() * 255), (int)(rand.NextDouble() * 255), (int)(rand.NextDouble() * 255));
                            element.FillColor = System.Drawing.Color.FromArgb((int)(rand.NextDouble() * 255), (int)(rand.NextDouble() * 255), (int)(rand.NextDouble() * 255));
                            element.BoundingRectangle = rect;
                            element.ToolTipText = "Rectangle " + i.ToString();
                            layer.Elements.Add(element);
                        }
                        else
                        { // create an ellipse object
                            EllipseTool element = new EllipseTool();
                            element.PenWidth = (i % 3 + 1);
                            element.PenColor = System.Drawing.Color.FromArgb((int)(rand.NextDouble() * 255), (int)(rand.NextDouble() * 255), (int)(rand.NextDouble() * 255));
                            element.FillColor = System.Drawing.Color.FromArgb((int)(rand.NextDouble() * 255), (int)(rand.NextDouble() * 255), (int)(rand.NextDouble() * 255));
                            element.BoundingRectangle = rect;
                            element.ToolTipText = "Ellipse " + i.ToString();
                            layer.Elements.Add(element);
                        }

                        top = top + 2;
                        left = left + 2;
                        right = right + 2;
                        bottom = bottom + 2;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_rects_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    int top = 20, left = 20, right = 120, bottom = 120;
                    for (int i = 1; i <= 10; i++)
                    {
                        // create a rectangle object
                        RectangleTool element = new RectangleTool();
                        System.Drawing.Rectangle rect = new Rectangle(top, left, right, bottom);
                        element.BoundingRectangle = rect;
                        element.PenWidth = 1;
                        System.Random rand = new System.Random();
                        element.PenColor = (System.Drawing.Color.FromArgb((int)(rand.NextDouble() * 255), (int)(rand.NextDouble() * 255), (int)(rand.NextDouble() * 255)));
                        element.FillColor = (System.Drawing.Color.FromArgb((int)(rand.NextDouble() * 255), (int)(rand.NextDouble() * 255), (int)(rand.NextDouble() * 255)));

                        int x = new System.Random().Next() * 400;
                        int y = new System.Random().Next() * 400;

                        element.ToolTipText = "Rectangle " + i.ToString();

                        layer.Elements.Add(element);
                        top += 2;
                        left += 2;
                        right += 2;
                        bottom += 2;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_buttons_Click(object sender, System.EventArgs e)
        {
            try
            {
                //---------------
                // initialize object location
                //---------------
                int top = 10;
                int left = 10;
                int right = 100;
                int bottom = 35;

                //--------------
                // now using this one programmable element,
                // create 10 buttons
                //--------------
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    for (int i = 1; i <= 10; i++)
                    {
                        //---------------
                        // create a programmable element
                        //---------------
                        ButtonTool element = new ButtonTool();

                        //---------------
                        // set some programmable element properties
                        //---------------
                        System.Drawing.Rectangle rect = new Rectangle(top, left, right, bottom);
                        element.BoundingRectangle = rect;
                        element.PenWidth = 4;
                        element.Text = "Button " + i.ToString();

                        System.Random rand = new System.Random();
                        int x = new System.Random().Next() * 400;
                        int y = new System.Random().Next() * 400;

                        element.ToolTipText = "Button " + i.ToString();

                        layer.Elements.Add(element);
                        top += 2;
                        left += 2;
                        right += 2;
                        bottom += 2;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_createtransrect_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    RectangleTool element = new RectangleTool();
                    System.Drawing.Rectangle rect = new Rectangle(100, 100, 200, 200);
                    element.BoundingRectangle = rect;
                    element.FillColor = System.Drawing.Color.Yellow;
                    element.PenWidth = 1;
                    element.PenColor = System.Drawing.Color.Blue;
                    element.HighlightFill = true; /* ****** Outline is "permanent" without this line */
                    element.ToolTipText = "Translucent Rectangle";
                    layer.Elements.Add(element);
                    layer.Toolbar.Selected = AnnotationTool.PointerTool;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_createImage_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    imageObject = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\vermont.jpg"));

                    ImageTool element = new ImageTool();
                    element.DibHandle = imageObject.ToHdib(false);
                    System.Drawing.Rectangle rect = new Rectangle(100, 100, 100 + imageObject.ImageXData.Width, 100 + imageObject.ImageXData.Height);
                    element.BoundingRectangle = rect;
                    element.ToolTipText = "Image";
                    layer.Elements.Add(element);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error msg:   " + ex.Message + "\n" +
                    "Error type:  " + ex.GetType().ToString());
            }
        }

        private void mnu_objects_nudgeup_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    for (int i = 0; i < layer.Elements.Count; i++)
                    {
                        if (layer.Elements[i].Selected)
                            layer.Elements[i].NudgeUp(); //TODO - doesnt seem to do anything, DevTrack #161
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_nudgedn_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        element.NudgeDown();  //TODO - doesnt seem to do anything, DevTrack #161
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_inflate_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        //TODO - DevTrack 157
                        System.Drawing.Rectangle rect = element.BoundingRectangle;
                        for (int i = 1; i <= 100; i++)
                        {
                            element.BoundingRectangle = new Rectangle(Math.Max(rect.Left - i, 0), Math.Max(rect.Top - i, 0), rect.Width + i, rect.Height + i);
                            Application.DoEvents();
                        }
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_largetext_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    TextTool element = new TextTool();
                    System.Drawing.Rectangle rect = new Rectangle(30, 30, 200, 300);
                    element.BoundingRectangle = rect;

                    MessageBox.Show(element.BoundingRectangle.Height.ToString());
                    MessageBox.Show(element.BoundingRectangle.Width.ToString());

                    element.TextJustification = TextJustification.Center;
                    element.BackColor = System.Drawing.Color.FromArgb(255, 255, 0);
                    element.TextColor = System.Drawing.Color.FromArgb(0, 0, 0);
                    element.BackStyle = BackStyle.Translucent;
                    element.PenWidth = 2;
                    element.PenColor = System.Drawing.Color.FromArgb(255, 0, 0);

                    string strText = "NotateXpress Version 9\n\n";
                    strText += "NotateXpress version 9's programmatic annotation features let you enter, edit, and query all types of annotation objects programmatically. ";
                    strText += "Using NotateXpress's programmatic capabilities is fun and fascinating, and it has a New Ruler Tool! Try it today.";

                    element.Text = strText;
                    System.Drawing.Font fontNew = new System.Drawing.Font("Times New Roman", 12);
                    element.TextFont = fontNew;
                    element.ToolTipText = "Large Text";
                    layer.Elements.Add(element);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_useMLE_Click(object sender, System.EventArgs e)
        {
            try
            {
                notateXpress1.MultiLineEdit = !notateXpress1.MultiLineEdit;
                mnu_objects_useMLE.Checked = notateXpress1.MultiLineEdit;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_brand_Click(object sender, System.EventArgs e)
        {
            try
            {
                notateXpress1.Layers.Brand(24);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemCreateNewNX9_click(object sender, System.EventArgs e)
        {
            try
            {
                notateXpress1.Layers.Clear();
                // 'detach' existing NX9 from the IX window
                notateXpress1.ClientWindow = IntPtr.Zero;
                notateXpress1.Dispose();
                notateXpress1 = null;
                // create new NX9 component
                notateXpress1 = new PegasusImaging.WinForms.NotateXpress9.NotateXpress();
                // init default values for new NX9 component
                notateXpress1.ClientWindow = imagXView1.Handle;
                notateXpress1.AllowPaint = true;
                notateXpress1.Debug = false;
                notateXpress1.DebugLogFile = "c:\\NotateXpress9.log";
                notateXpress1.DebugErrorLevel = PegasusImaging.WinForms.NotateXpress9.DebugErrorLevelInfo.Detailed;
                notateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress9.FontScaling.Normal;
                notateXpress1.ImagXpressLoad = true;
                notateXpress1.ImagXpressSave = true;
                notateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress9.AnnotationMode.Edit;
                notateXpress1.MultiLineEdit = false;
                // create a layer for the new NX9 component
                createNewLayer("Layer1", imageObject.ToHdib(false));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_orient_ltbr_Click(object sender, System.EventArgs e)
        {
            try
            {
                imagXProcessor.Image = imagXView1.Image;
                imagXProcessor.Rotate(270);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void mnu_orient_brtl_Click(object sender, System.EventArgs e)
        {
            try
            {
                imagXProcessor.Image = imagXView1.Image;
                imagXProcessor.Flip();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_orient_rblt_Click(object sender, System.EventArgs e)
        {
            try
            {
                imagXProcessor.Image = imagXView1.Image;
                imagXProcessor.Rotate(90);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void mnu_orient_tlbr_Click(object sender, System.EventArgs e)
        {
            try
            {
                imagXProcessor.Image = imagXView1.Image;
                imagXProcessor.Mirror();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_orientation_rotate180_Click(object sender, System.EventArgs e)
        {
            try
            {
                imagXProcessor.Image = imagXView1.Image;
                imagXProcessor.Rotate(180);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_settext_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    string newText = "This is a test and only a test. Had this been something other than a test, something significant would have happened!";
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        SetPropertyValue(element, "Text", newText);
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_backcolor_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        SetPropertyValue(element, "BackColor", System.Drawing.Color.FromArgb(0, 255, 0));
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_pencolor_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        SetPropertyValue(element, "PenColor", System.Drawing.Color.FromArgb(0, 255, 0));
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_penwidth_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    System.Int32 newWidth = 15;
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        SetPropertyValue(element, "PenWidth", newWidth);
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_bevelshadow_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        SetPropertyValue(element, "BevelShadowColor", System.Drawing.Color.FromArgb(0, 255, 0));
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_bevellight_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        SetPropertyValue(element, "BevelLightColor", System.Drawing.Color.FromArgb(0, 255, 255));
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_setfont_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    System.Drawing.Font font = null;
                    // only perform on first selected items
                    Element element = layer.Elements.FirstSelected();
                    if (element != null)
                    {
                        font = (Font)GetPropertyValue(element, "TextFont");
                        if (font != null)
                            fontDialog1.Font = font;

                        if (fontDialog1.ShowDialog(this) != DialogResult.Cancel)
                            font = fontDialog1.Font;
                        else
                            return; // font dialog canceled
                    }
                    while (element != null)
                    {
                        SetPropertyValue(element, "TextFont", font);
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_getitemtext_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    // only perform on first selected element
                    object element = layer.Elements.FirstSelected();
                    if (element != null)
                    {
                        object obj = GetPropertyValue(element, "Text");
                        if (obj != null)
                            System.Windows.Forms.MessageBox.Show((String)obj);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_hb_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        object obj = GetPropertyValue(this, "HighlightBack");
                        if (obj != null)
                        {
                            Boolean b = (Boolean)obj;
                            b = !b;
                            SetPropertyValue(this, "HighlightBack", b);
                        }
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_hf_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        object obj = GetPropertyValue(element, "HighlightFill");
                        if (obj != null)
                        {
                            Boolean b = (Boolean)obj;
                            b = !b;
                            SetPropertyValue(element, "HighlightFill", b);
                        }
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_moveable_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        object obj = GetPropertyValue(element, "Moveable");
                        if (obj != null)
                        {
                            Boolean b = (Boolean)obj;
                            b = !b;
                            SetPropertyValue(element, "Moveable", b);
                        }
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_fixed_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        object obj = GetPropertyValue(element, "Fixed");
                        if (obj != null)
                        {
                            Boolean b = (Boolean)obj;
                            b = !b;
                            SetPropertyValue(element, "Fixed", b);
                        }
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void mnu_stuff_sizeable_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        object obj = GetPropertyValue(element, "Sizeable");
                        if (obj != null)
                        {
                            Boolean b = (Boolean)obj;
                            b = !b;
                            SetPropertyValue(element, "Sizeable", b);
                        }
                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_getitemfont_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    // only perform on first selected element
                    object element = layer.Elements.FirstSelected();
                    if (element != null)
                    {
                        object obj = GetPropertyValue(element, "TextFont");
                        if (obj != null)
                        {
                            System.Drawing.Font font = (System.Drawing.Font)obj;
                            MessageBox.Show(font.Name + " " + font.SizeInPoints.ToString());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_setnewdib_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    try
                    {
                        string strCurrentDir = System.IO.Directory.GetCurrentDirectory();
                        string strPath = System.IO.Path.Combine(strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\Water.jpg");
                        imageObject = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strPath);
                        if (imageObject != null)
                        {
                            if (layer.Toolbar.ImageToolbarDefaults.DibHandle != System.IntPtr.Zero)
                            {
                                GlobalFree((System.IntPtr)layer.Toolbar.ImageToolbarDefaults.DibHandle);
                                layer.Toolbar.ImageToolbarDefaults.DibHandle = System.IntPtr.Zero;
                            }
                            layer.Toolbar.ImageToolbarDefaults.DibHandle = imageObject.ToHdib(false);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error msg:   " + ex.Message + "\n" +
                            "Error type:  " + ex.GetType().ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_mode_editmode_Click(object sender, System.EventArgs e)
        {
            try
            {
                notateXpress1.InteractMode = AnnotationMode.Edit;
                mnu_mode_interactive.Checked = false;
                mnu_mode_editmode.Checked = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_mode_interactive_Click(object sender, System.EventArgs e)
        {
            try
            {
                notateXpress1.InteractMode = AnnotationMode.Interactive;
                mnu_mode_interactive.Checked = true;
                mnu_mode_editmode.Checked = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_cm_enable_Click(object sender, System.EventArgs e)
        {
            try
            {
                notateXpress1.MenuSetEnabled(MenuType.Context, AnnotationTool.NoTool, !notateXpress1.MenuGetEnabled(MenuType.Context, AnnotationTool.NoTool));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ZoomIn_Click(object sender, System.EventArgs e)
        {
            try
            {
                imagXView1.ZoomFactor = imagXView1.ZoomFactor / .9;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ZoomOut_Click(object sender, System.EventArgs e)
        {
            try
            {
                imagXView1.ZoomFactor = imagXView1.ZoomFactor * .9;
                //this.Text = "Image = " + imagXpressdotNet.IWidth + " x " + imagXpressdotNet.IHeight + " " + (imagXpressdotNet.IPZoomF * 100) + "%";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private object GetPropertyValue(object Object, string strPropName)
        {
            try
            {
                foreach (System.Reflection.PropertyInfo property in Object.GetType().GetProperties())
                    if (property.Name == strPropName)
                        return property.GetValue(Object, null);

                return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        private bool SetPropertyValue(object Object, string strPropName, object objValue)
        {
            try
            {
                foreach (System.Reflection.PropertyInfo property in Object.GetType().GetProperties())
                    if (property.Name == strPropName)
                    {
                        property.SetValue(Object, objValue, null);
                        return true;
                    }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private bool HasProperty(object Object, string strPropName)
        {
            try
            {
                foreach (System.Reflection.PropertyInfo property in Object.GetType().GetProperties())
                    if (property.Name == strPropName)
                        return true;

                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void mnu_stuff_Select(object sender, System.EventArgs e)
        {
            try
            {
                //  ------------
                //   disable all menu selections on this menu
                //  ------------
                mnu_BoundingRectangle.Enabled = false;
                mnu_stuff_backcolor.Enabled = false;
                mnu_stuff_bevellight.Enabled = false;
                mnu_stuff_bevelshadow.Enabled = false;
                mnu_stuff_getitemfont.Enabled = false;
                mnu_stuff_getitemtext.Enabled = false;
                mnu_stuff_hb.Enabled = false;
                mnu_stuff_hf.Enabled = false;
                mnu_stuff_moveable.Enabled = false;
                mnu_stuff_fixed.Enabled = false;
                mnu_stuff_pencolor.Enabled = false;
                mnu_stuff_penwidth.Enabled = false;
                mnu_stuff_setfont.Enabled = false;
                mnu_stuff_setnewdib.Enabled = false;
                mnu_stuff_settext.Enabled = false;
                mnu_stuff_sizeable.Enabled = false;
                mnu_stuff_toggleuserdraw.Enabled = false;

                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    //-----------------
                    // Enable menu items as appropriate for this menu
                    //-----------------
                    Element element = layer.Elements.FirstSelected();
                    // only one element must be selected to enable
                    if (element != null && layer.Elements.NextSelected() == null)
                    {
                        mnu_BoundingRectangle.Enabled = true;
                        mnu_stuff_settext.Enabled = HasProperty(element, "Text");
                        mnu_stuff_backcolor.Enabled = HasProperty(element, "BackColor");
                        mnu_stuff_pencolor.Enabled = HasProperty(element, "PenColor");
                        mnu_stuff_penwidth.Enabled = HasProperty(element, "PenWidth");
                        mnu_stuff_bevelshadow.Enabled = HasProperty(element, "BevelShadowColor");
                        mnu_stuff_bevellight.Enabled = HasProperty(element, "BevelLightColor");
                        mnu_stuff_setfont.Enabled = HasProperty(element, "TextFont");
                        mnu_stuff_getitemtext.Enabled = HasProperty(element, "Text");
                        mnu_stuff_hb.Enabled = HasProperty(element, "HighlightBack");
                        mnu_stuff_hf.Enabled = HasProperty(element, "HighlightFill");
                        mnu_stuff_moveable.Enabled = HasProperty(element, "Moveable");
                        mnu_stuff_fixed.Enabled = HasProperty(element, "Fixed");
                        mnu_stuff_sizeable.Enabled = HasProperty(element, "Sizeable");
                        mnu_stuff_getitemfont.Enabled = HasProperty(element, "TextFont");
                        mnu_stuff_setnewdib.Enabled = HasProperty(element, "DibHandle");
                        mnu_stuff_toggleuserdraw.Enabled = HasProperty(element, "UserDraw");
                        if (mnu_stuff_toggleuserdraw.Enabled)
                            mnu_stuff_toggleuserdraw.Checked = (bool)GetPropertyValue(element, "UserDraw");
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemBoundingRectangle_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    //ImageTool it = new ImageTool();
                    //
                    //				ImageToolbarDefaults tb = new PegasusImaging.WinForms.NotateXpress9.ImageToolbarDefaults();



                    // only one element must be selected
                    if (element != null && layer.Elements.NextSelected() == null)
                    {
                        BoundingRectDlg dlg = new BoundingRectDlg(element.BoundingRectangle, new System.Drawing.Rectangle(0, 0, imagXView1.Image.ImageXData.Width, imagXView1.Image.ImageXData.Height));
                        if (System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
                        {
                            element.BoundingRectangle = dlg.Rect;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_stuff_toggleuserdraw_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    Element element = layer.Elements.FirstSelected();
                    // only one element must be selected
                    if (element != null && element is RectangleTool && layer.Elements.NextSelected() == null)
                    {
                        RectangleTool rectTool = (RectangleTool)element;



                        rectTool.UserDraw = !rectTool.UserDraw;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_layers_Select(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                bool bEnabled = layer != null ? true : false;

                mnu_layers_deletecurrent.Enabled = bEnabled;
                mnu_layers_togglehide.Enabled = bEnabled;
                mnu_layers_setdescription.Enabled = bEnabled;
                mnu_layers_setname.Enabled = bEnabled;
                mnu_layers_setpassword.Enabled = bEnabled;
                mnu_DeactivateCurrentLayer.Enabled = bEnabled;
                mnu_SendToBack.Enabled = false;
                mnu_BringToFront.Enabled = false;
                mnu_DeleteSelected.Enabled = false;
                mnu_DisableToolbar.Enabled = bEnabled;

                if (bEnabled)
                {
                    mnu_layers_toggletoolbar.Checked = layer.Toolbar.Visible;

                    if (layer.Visible)
                    {
                        mnu_layers_togglehide.Text = "Hide current";
                    }
                    else
                    {
                        mnu_layers_togglehide.Text = "Show current";
                    }

                    if (layer.Active)
                    {
                        mnu_DeactivateCurrentLayer.Text = "Deactivate current";
                    }
                    else
                    {
                        mnu_DeactivateCurrentLayer.Text = "Activate current";
                    }

                    if (layer.Toolbar.Enabled)
                        mnu_DisableToolbar.Text = "Disable toolbar";
                    else
                        mnu_DisableToolbar.Text = "Enable toolbar";

                    if (layer.Elements.FirstSelected() != null)
                    {
                        mnu_SendToBack.Enabled = true;
                        mnu_BringToFront.Enabled = true;
                        mnu_DeleteSelected.Enabled = true;
                    }
                }
                else
                    mnu_layers_toggletoolbar.Enabled = false;

                if (notateXpress1.Layers.Count < 1)
                {
                    mnu_layers_setcurrent.Enabled = false;
                }
                else
                {
                    mnu_layers_setcurrent.Enabled = true;

                    // ---------------
                    //  clean set current layer sub-menu
                    // ---------------
                    mnu_layers_setcurrent.MenuItems.Clear();

                    System.EventHandler layers_setcurrent_eventhandler = new System.EventHandler(this.mnu_layers_setcurrent_Click);

                    // ---------------
                    //  add the description for each layer under this submenu
                    // ---------------
                    for (int i = 0; i < notateXpress1.Layers.Count; i++)
                    {
                        string str = notateXpress1.Layers[i].Name + "   [" + (notateXpress1.Layers[i].Visible ? "Visible " : "NOTVisible ")
                            + (notateXpress1.Layers[i].Active ? "Active " : "NOTActive ")
                            + notateXpress1.Layers[i].Elements.Count.ToString() + "-Elements]";
                        mnu_layers_setcurrent.MenuItems.Add(str); // creates new MenuItem
                        if (notateXpress1.Layers.Selected() == notateXpress1.Layers[i])
                            mnu_layers_setcurrent.MenuItems[i].Checked = true; // check if selected layer
                        else
                            mnu_layers_setcurrent.MenuItems[i].Checked = false;
                        mnu_layers_setcurrent.MenuItems[i].Click += layers_setcurrent_eventhandler; // set event handler for new MenuItem
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_layers_create_Click(object sender, System.EventArgs e)
        {
            try
            {
                InputDlg dlg = new InputDlg("Specify Name of Layer to Create");
                if (System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
                {
                    // Create a new layer, note that it will also now be the selected layer
                    createNewLayer(dlg.InputText, System.IntPtr.Zero);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_layers_setcurrent_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (sender is System.Windows.Forms.MenuItem)
                {
                    notateXpress1.Layers[((System.Windows.Forms.MenuItem)sender).Index].Select();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_layers_deletecurrent_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                    notateXpress1.Layers.Remove(layer);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_layers_togglehide_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                    layer.Visible = !layer.Visible;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemDeactivateCurrentLayer_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                    layer.Active = !layer.Active;

                MessageBox.Show(layer.Active.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemSaveCurrentLayer_click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    saveFileDialog.Title = "Save Current Annotation Layer to File";
                    saveFileDialog.Filter = "NotateXpress File (*.nxp) | *.nxp";
                    saveFileDialog.DefaultExt = ".nxp";
                    saveFileDialog.ShowDialog(this);

                    string strFileName = saveFileDialog.FileName;
                    if (strFileName != "" && strFileName != null)
                    {
                        m_saveOptions.AllLayers = false;
                        m_saveOptions.SaveLayer = layer;
                        notateXpress1.Layers.Comments = "This file contains 1 saved layer";
                        notateXpress1.Layers.Subject = "Single layer annotation file";
                        notateXpress1.Layers.Title = "One awesome single layer annotation file";
                        notateXpress1.Layers.SaveToFile(strFileName, m_saveOptions);
                        m_saveOptions.AllLayers = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_layers_toggletoolbar_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    if (layer.Toolbar.Visible)
                    {
                        layer.Toolbar.Visible = false;
                        mnu_ToolBar.Enabled = false;
                    }
                    else
                    {
                        layer.Toolbar.Visible = true;
                        mnu_ToolBar.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemDisableToolbar_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    if (layer.Toolbar.Enabled)
                    {
                        layer.Toolbar.Enabled = false;
                    }
                    else
                    {
                        layer.Toolbar.Enabled = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_layers_setdescription_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    InputDlg dlg = new InputDlg("Set Layer Description");
                    dlg.InputText = layer.Description;
                    if (System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
                    {
                        layer.Description = dlg.InputText;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_layers_setname_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    InputDlg dlg = new InputDlg("Set Layer Name");
                    dlg.InputText = layer.Name;
                    if (System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
                    {
                        layer.Name = dlg.InputText;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_layers_setpassword_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    InputDlg dlg = new InputDlg("Set Layer Password");
                    dlg.InputText = layer.Password;
                    if (System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
                    {
                        layer.Password = dlg.InputText;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemSendToBack_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                    layer.Elements.SendToBack();
                imagXView1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemBringToFront_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                    layer.Elements.BringToFront();
                imagXView1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemDeleteSelected_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                    layer.Elements.DeleteSelected();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_cm_Select(object sender, System.EventArgs e)
        {
            try
            {
                if (notateXpress1.MenuGetEnabled(MenuType.Context, AnnotationTool.NoTool))
                {
                    mnu_cm_enable.Text = "Disable";
                }
                else
                {
                    mnu_cm_enable.Text = "Enable";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_Select(object sender, System.EventArgs e)
        {
            try
            {
                // ------------
                // get the selected/current layer
                // ------------
                Layer layer = notateXpress1.Layers.Selected();
                if (layer == null)
                {
                    // ------------
                    //  disable all menu selections on this menu
                    // ------------
                    foreach (MenuItem item in mnu_objects.MenuItems)
                    {
                        item.Enabled = false;
                    }
                }
                else
                {
                    int nCount = layer.Elements.Count;
                    mnu_objects_reverse.Enabled = (nCount > 1) ? true : false;
                    mnu_objects_brand.Enabled = (nCount > 0) ? true : false;
                    mnu_objects_nudgeup.Enabled = layer.Elements.IsSelection ? true : false;
                    mnu_objects_nudgedn.Enabled = layer.Elements.IsSelection ? true : false;
                    mnu_objects_createtransrect.Enabled = true;

                    // determine whether to enable inflate
                    mnu_objects_inflate.Enabled = false;
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    { // enable inflate if ALL the seleted elements are either an EllipseTool, RectangleTool, TextTool, or ButtonTool
                        if (element is EllipseTool || element is RectangleTool || element is TextTool || element is ButtonTool)
                            mnu_objects_inflate.Enabled = true;
                        else
                        {
                            mnu_objects_inflate.Enabled = false;
                            break;
                        }
                        element = layer.Elements.NextSelected();


                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItem2_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    //set the annotation type to the Ruler annotation
                    RulerTool element = new RulerTool();
                    System.Drawing.Rectangle rect = new Rectangle(25, 50, 200, 100);
                    element.BoundingRectangle = rect;
                    element.PenWidth = 4;
                    element.GaugeLength = 20;
                    element.MeasurementUnit = MeasurementUnit.Pixels;
                    element.Precision = 1;
                    element.ToolTipText = "Ruler";
                    layer.Elements.Add(element);
                    element.Selected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItem3_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    TextTool element = new TextTool();
                    System.Drawing.Rectangle rect = new Rectangle(100, 100, 300, 200);
                    element.BoundingRectangle = rect;
                    element.TextJustification = TextJustification.Center;
                    element.TextColor = System.Drawing.Color.Red;
                    element.BackStyle = BackStyle.Opaque;
                    element.Text = "Approved by Pegasus Imaging Corp." + System.DateTime.Now;
                    element.PenWidth = 1;
                    element.PenColor = System.Drawing.Color.White;
                    element.BackColor = System.Drawing.Color.White;
                    element.ToolTipText = "Redaction";
                    layer.Elements.Add(element);
                    layer.Toolbar.Selected = AnnotationTool.PointerTool;
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemGroups_Click(object sender, System.EventArgs e)
        {
            try
            {
                mnu_objects_allowgrouping.Enabled = false;
                mnu_CreateGroup.Enabled = false;
                mnu_DeleteGroup.Enabled = false;
                mnu_SetGroupEmpty.Enabled = false;
                mnu_SelectGroupItems.Enabled = false;
                mnu_GroupAddSelectedItems.Enabled = false;
                mnu_GroupRemoveSelectedItems.Enabled = false;
                mnu_YokeGroupItems.Enabled = false;
                mnu_SetGroupUserString.Enabled = false;
                mnu_MirrorYoked.Enabled = false;
                mnu_InvertYoked.Enabled = false;

                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    mnu_objects_allowgrouping.Enabled = true;
                    mnu_objects_allowgrouping.Checked = layer.Groups.AllowUserGrouping;
                    if (!mnu_objects_allowgrouping.Checked)
                        return;

                    mnu_CreateGroup.Enabled = true;

                    if (layer.Groups.Count > 0)
                    {
                        // ---------------
                        //  Initialize delete group sub-menu
                        // ---------------
                        mnu_DeleteGroup.Enabled = true;
                        mnu_DeleteGroup.MenuItems.Clear(); //  clean groups sub-menu
                        System.EventHandler menuItemDeleteGroup_eventhandler = new System.EventHandler(this.menuItemDeleteGroup_Click);
                        for (int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
                        {
                            mnu_DeleteGroup.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
                            mnu_DeleteGroup.MenuItems[i].Click += menuItemDeleteGroup_eventhandler; // set event handler for new MenuItem
                        }
                        // ---------------
                        //  Initialize set group empty sub-menu
                        // ---------------
                        mnu_SetGroupEmpty.Enabled = true;
                        mnu_SetGroupEmpty.MenuItems.Clear(); //  clean groups sub-menu
                        System.EventHandler menuItemSetGroupEmpty_eventhandler = new System.EventHandler(this.menuItemSetGroupEmpty_Click);
                        for (int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
                        {
                            string str = layer.Groups[i].Name + "  #Items: " + layer.Groups[i].GroupElements.Count.ToString();
                            mnu_SetGroupEmpty.MenuItems.Add(str); // creates new MenuItem
                            mnu_SetGroupEmpty.MenuItems[i].Enabled = layer.Groups[i].GroupElements.Count > 0 ? true : false;
                            mnu_SetGroupEmpty.MenuItems[i].Click += menuItemSetGroupEmpty_eventhandler; // set event handler for new MenuItem
                        }
                        // ---------------
                        //  Initialize select group items sub-menu
                        // ---------------
                        mnu_SelectGroupItems.Enabled = true;
                        mnu_SelectGroupItems.MenuItems.Clear(); //  clean groups sub-menu
                        System.EventHandler menuItemSelectGroupItems_eventhandler = new System.EventHandler(this.menuItemSelectGroupItems_Click);
                        for (int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
                        {
                            mnu_SelectGroupItems.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
                            mnu_SelectGroupItems.MenuItems[i].Enabled = layer.Groups[i].GroupElements.Count > 0 ? true : false;
                            mnu_SelectGroupItems.MenuItems[i].Click += menuItemSelectGroupItems_eventhandler; // set event handler for new MenuItem
                        }
                        // ---------------
                        //  Initialize add selected items to group
                        // ---------------
                        mnu_GroupAddSelectedItems.Enabled = layer.Elements.FirstSelected() != null ? true : false;
                        mnu_GroupAddSelectedItems.MenuItems.Clear(); //  clean groups sub-menu
                        System.EventHandler menuItemGroupAddSelectedItems_eventhandler = new System.EventHandler(this.menuItemGroupAddSelectedItems_Click);
                        for (int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
                        {
                            mnu_GroupAddSelectedItems.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
                            mnu_GroupAddSelectedItems.MenuItems[i].Click += menuItemGroupAddSelectedItems_eventhandler; // set event handler for new MenuItem
                        }
                        // ---------------
                        //  Initialize remove selected items from group
                        // ---------------
                        mnu_GroupRemoveSelectedItems.Enabled = layer.Elements.FirstSelected() != null ? true : false;
                        mnu_GroupRemoveSelectedItems.MenuItems.Clear(); //  clean groups sub-menu
                        System.EventHandler menuItemGroupRemoveSelectedItems_eventhandler = new System.EventHandler(this.menuItemGroupRemoveSelectedItems_Click);
                        for (int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
                        {
                            mnu_GroupRemoveSelectedItems.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
                            mnu_GroupRemoveSelectedItems.MenuItems[i].Enabled = layer.Groups[i].GroupElements.Count > 0 ? true : false;
                            mnu_GroupRemoveSelectedItems.MenuItems[i].Click += menuItemGroupRemoveSelectedItems_eventhandler; // set event handler for new MenuItem
                        }
                        // ---------------
                        //  Initialize yoke group items sub-menu
                        // ---------------
                        mnu_YokeGroupItems.Enabled = true;
                        mnu_YokeGroupItems.MenuItems.Clear(); //  clean groups sub-menu
                        System.EventHandler menuItemYokeGroupItems_eventhandler = new System.EventHandler(this.menuItemYokeGroupItems_Click);
                        for (int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
                        {
                            mnu_YokeGroupItems.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
                            mnu_YokeGroupItems.MenuItems[i].Checked = layer.Groups[i].Yoked;
                            mnu_YokeGroupItems.MenuItems[i].Click += menuItemYokeGroupItems_eventhandler; // set event handler for new MenuItem
                        }
                        // ---------------
                        //  Initialize invert group items Yoked sub-menu
                        // ---------------
                        mnu_InvertYoked.Enabled = true;
                        mnu_InvertYoked.MenuItems.Clear(); //  clean groups sub-menu
                        System.EventHandler menuItemInvertYoked_eventhandler = new System.EventHandler(this.menuItemInvertYoked_click);
                        for (int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
                        {
                            mnu_InvertYoked.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
                            mnu_InvertYoked.MenuItems[i].Enabled = layer.Groups[i].Yoked;
                            mnu_InvertYoked.MenuItems[i].Click += menuItemInvertYoked_eventhandler; // set event handler for new MenuItem
                        }
                        // ---------------
                        //  Initialize Mirror group items Yoked sub-menu
                        // ---------------
                        mnu_MirrorYoked.Enabled = true;
                        mnu_MirrorYoked.MenuItems.Clear(); //  clean groups sub-menu
                        System.EventHandler menuItemMirrorYoked_eventhandler = new System.EventHandler(this.menuItemMirrorYoked_click);
                        for (int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
                        {
                            mnu_MirrorYoked.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
                            mnu_MirrorYoked.MenuItems[i].Enabled = layer.Groups[i].Yoked;
                            mnu_MirrorYoked.MenuItems[i].Click += menuItemMirrorYoked_eventhandler; // set event handler for new MenuItem
                        }
                        // ---------------
                        //  Initialize group User String sub-menu
                        // ---------------
                        mnu_SetGroupUserString.Enabled = true;
                        mnu_SetGroupUserString.MenuItems.Clear(); //  clean groups sub-menu
                        System.EventHandler SetGroupUserString_eventhandler = new System.EventHandler(this.SetGroupUserString_Select);
                        for (int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
                        {
                            mnu_SetGroupUserString.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
                            mnu_SetGroupUserString.MenuItems[i].Click += SetGroupUserString_eventhandler; // set event handler for new MenuItem
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_allowgrouping_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer == null)
                {
                    MessageBox.Show("A Layer must be selected before a Group operation can be performed");
                    return;
                }
                bool bUserGrouping = !layer.Groups.AllowUserGrouping;
                layer.Groups.AllowUserGrouping = bUserGrouping;
                mnu_objects_allowgrouping.Checked = bUserGrouping;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemCreateGroup_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer == null)
                {
                    MessageBox.Show("A Layer must be selected before a Group operation can be performed");
                    return;
                }
                InputDlg dlg = new InputDlg("Specify Name of Group to create");
                if (System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
                {
                    PegasusImaging.WinForms.NotateXpress9.Group group = new PegasusImaging.WinForms.NotateXpress9.Group();
                    group.Name = dlg.InputText;
                    layer.Groups.Add(group);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemDeleteGroup_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    if (sender is System.Windows.Forms.MenuItem)
                    {
                        layer.Groups.RemoveAt(((System.Windows.Forms.MenuItem)sender).Index);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemSetGroupEmpty_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    if (sender is System.Windows.Forms.MenuItem)
                    {
                        Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
                        group.GroupElements.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemSelectGroupItems_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    if (sender is System.Windows.Forms.MenuItem)
                    {
                        Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
                        // first clear any currently selected items in the layer
                        Element element = layer.Elements.FirstSelected();
                        while (element != null)
                        {
                            element.Selected = false;
                            element = layer.Elements.NextSelected();
                        }
                        // now select all items that are in the group
                        for (int i = 0; i < group.GroupElements.Count; i++)
                            group.GroupElements[i].Selected = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemGroupAddSelectedItems_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null && sender is System.Windows.Forms.MenuItem)
                {
                    Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        if (element.Selected)
                            group.GroupElements.Add(element);

                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void menuItemGroupRemoveSelectedItems_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null && sender is System.Windows.Forms.MenuItem)
                {
                    Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
                    Element element = layer.Elements.FirstSelected();
                    while (element != null)
                    {
                        if (element.Selected)
                            group.GroupElements.Remove(element);

                        element = layer.Elements.NextSelected();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemYokeGroupItems_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null && sender is System.Windows.Forms.MenuItem)
                {
                    Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
                    group.Yoked = !group.Yoked;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SetGroupUserString_Select(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null && sender is System.Windows.Forms.MenuItem)
                {
                    Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
                    InputDlg dlg = new InputDlg("User String for Group: " + group.Name);
                    dlg.InputText = group.UserString;
                    if (System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
                    {
                        group.UserString = dlg.InputText;
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemMirrorYoked_click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null && sender is System.Windows.Forms.MenuItem)
                {
                    Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
                    group.GroupElements.GroupMirror();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemInvertYoked_click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null && sender is System.Windows.Forms.MenuItem)
                {
                    Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
                    group.GroupElements.GroupInvert();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemToolBar_Select(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    bool bEnabled = layer.Toolbar.Visible;
                    mnu_ToolbarRulerTool.Enabled = bEnabled;
                    mnu_ToolbarFreehandTool.Enabled = bEnabled;
                    mnu_ToolbarEllipseTool.Enabled = bEnabled;
                    mnu_ToolbarTextTool.Enabled = bEnabled;
                    mnu_ToolbarRectangleTool.Enabled = bEnabled;
                    mnu_ToolbarStampTool.Enabled = bEnabled;
                    mnu_ToolbarPolylineTool.Enabled = bEnabled;
                    mnu_ToolbarLineTool.Enabled = bEnabled;
                    mnu_ToolbarImageTool.Enabled = bEnabled;
                    mnu_ToolbarButtonTool.Enabled = bEnabled;
                    mnu_ToolbarPolygonTool.Enabled = bEnabled;

                    mnu_RulerToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.RulerTool);
                    mnu_RulerToolEnabeled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.RulerTool);
                    mnu_FreehandToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.FreehandTool);
                    mnu_FreehandToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.FreehandTool);
                    mnu_EllipseToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.EllipseTool);
                    mnu_EllipseToolEnabeld.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.EllipseTool);
                    mnu_TextToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.TextTool);
                    mnu_TextToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.TextTool);
                    mnu_RectangleToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.RectangleTool);
                    mnu_RectangleToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.RectangleTool);
                    mnu_StampToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.StampTool);
                    mnu_StampToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.StampTool);
                    mnu_PolylineToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.PolyLineTool);
                    mnu_PolylineToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.PolyLineTool);
                    mnu_LineToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.LineTool);
                    mnu_LineToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.LineTool);
                    mnu_ImageToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.ImageTool);
                    mnu_ImageToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.ImageTool);
                    mnu_ButtonToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.ButtonTool);
                    mnu_ButtonToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.ButtonTool);
                    mnu_PolygonToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.PolygonTool);
                    mnu_PolygonToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.PolygonTool);
                    mnu_BlockHighlightEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.BlockHighlightTool);
                    mnu_BlockHighlightVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.BlockHighlightTool);
                    mnu_NoteToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.NoteTool);
                    mnu_NoteToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.NoteTool);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemRulerToolVisible_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.RulerTool, !layer.Toolbar.GetToolVisible(AnnotationTool.RulerTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemRulerToolEnabeled_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolEnabled(AnnotationTool.RulerTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.RulerTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemFreehandToolEnabled_Click(object sender, System.EventArgs e)
        {
            Layer layer = notateXpress1.Layers.Selected();
            if (layer != null)
            {
                layer.Toolbar.SetToolEnabled(AnnotationTool.FreehandTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.FreehandTool));
            }
        }

        private void menuItemFreehandToolVisible_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.FreehandTool, !layer.Toolbar.GetToolVisible(AnnotationTool.FreehandTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemEllipseToolEnabeld_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolEnabled(AnnotationTool.EllipseTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.EllipseTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemEllipseToolVisible_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.EllipseTool, !layer.Toolbar.GetToolVisible(AnnotationTool.EllipseTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemTextToolEnabled_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolEnabled(AnnotationTool.TextTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.TextTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemTextToolVisible_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.TextTool, !layer.Toolbar.GetToolVisible(AnnotationTool.TextTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemRectangleToolEnabled_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolEnabled(AnnotationTool.RectangleTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.RectangleTool));
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemRectangleToolVisible_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.RectangleTool, !layer.Toolbar.GetToolVisible(AnnotationTool.RectangleTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemStampToolEnabled_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolEnabled(AnnotationTool.StampTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.StampTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemStampToolVisible_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.StampTool, !layer.Toolbar.GetToolVisible(AnnotationTool.StampTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemPolylineToolVisible_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.PolyLineTool, !layer.Toolbar.GetToolVisible(AnnotationTool.PolyLineTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemPolylineToolEnabled_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolEnabled(AnnotationTool.PolyLineTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.PolyLineTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemLineToolEnabled_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolEnabled(AnnotationTool.LineTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.LineTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemLineToolVisible_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.LineTool, !layer.Toolbar.GetToolVisible(AnnotationTool.LineTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemImageToolEnabled_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolEnabled(AnnotationTool.ImageTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.ImageTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemImageToolVisible_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.ImageTool, !layer.Toolbar.GetToolVisible(AnnotationTool.ImageTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemButtonToolEnabled_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolEnabled(AnnotationTool.ButtonTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.ButtonTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemButtonToolVisible_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.ButtonTool, !layer.Toolbar.GetToolVisible(AnnotationTool.ButtonTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemPolygonToolVisible_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.PolygonTool, !layer.Toolbar.GetToolVisible(AnnotationTool.PolygonTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemPolygonToolEnabled_Click(object sender, System.EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolEnabled(AnnotationTool.PolygonTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.PolygonTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_Click(object sender, PegasusImaging.WinForms.NotateXpress9.ClickEventArgs e)
        {
            try
            {
                // interactive mode

                Element element = e.Element;
                Layer layer = e.Layer;
                string strAnnotationType = " ";
                if (layer != null && element != null)
                {
                    int i = element.GetType().ToString().LastIndexOf('.');
                    if (i == -1)
                        strAnnotationType += element.GetType().ToString();
                    else
                        strAnnotationType += element.GetType().ToString().Substring(i + 1);
                }
                OutputList.Items.Insert(0, strAnnotationType + " annotation clicked on, layer name = " + layer.Name + ", layer description = " + layer.Description);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_DoubleClick(object sender, PegasusImaging.WinForms.NotateXpress9.DoubleClickEventArgs e)
        {
            try
            {
                // interactive mode
                Layer layer = e.Layer;
                Element element = e.Element;
                string strAnnotationType = " ";
                if (layer != null && element != null)
                {
                    int i = element.GetType().ToString().LastIndexOf('.');
                    if (i == -1)
                        strAnnotationType += element.GetType().ToString();
                    else
                        strAnnotationType += element.GetType().ToString().Substring(i + 1);
                    OutputList.Items.Insert(0, "Mouse double clidk event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_MouseDown(object sender, PegasusImaging.WinForms.NotateXpress9.MouseDownEventArgs e)
        {
            try
            {
                // interactive mode
                Layer layer = e.Layer;
                Element element = e.Element;
                string strAnnotationType = " ";
                if (layer != null && element != null)
                {
                    int i = element.GetType().ToString().LastIndexOf('.');
                    if (i == -1)
                        strAnnotationType += element.GetType().ToString();
                    else
                        strAnnotationType += element.GetType().ToString().Substring(i + 1);
                    OutputList.Items.Insert(0, "       X coorinate = " + e.X.ToString() + ", Y coordinate = " + e.Y.ToString());
                    OutputList.Items.Insert(0, "Mouse down event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_MouseMove(object sender, PegasusImaging.WinForms.NotateXpress9.MouseMoveEventArgs e)
        {
            try
            {
                // interactive mode
                Layer layer = e.Layer;
                Element element = e.Element;
                string strAnnotationType = " ";
                if (layer != null && element != null)
                {
                    int i = element.GetType().ToString().LastIndexOf('.');
                    if (i == -1)
                        strAnnotationType += element.GetType().ToString();
                    else
                        strAnnotationType += element.GetType().ToString().Substring(i + 1);
                    OutputList.Items.Insert(0, "       X coorinate = " + e.X.ToString() + ", Y coordinate = " + e.Y.ToString());
                    OutputList.Items.Insert(0, "Mouse move event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_MouseUp(object sender, PegasusImaging.WinForms.NotateXpress9.MouseUpEventArgs e)
        {
            try
            {
                // interactive mode
                Layer layer = e.Layer;
                Element element = e.Element;
                string strAnnotationType = " ";
                if (layer != null && element != null)
                {
                    int i = element.GetType().ToString().LastIndexOf('.');
                    if (i == -1)
                        strAnnotationType += element.GetType().ToString();
                    else
                        strAnnotationType += element.GetType().ToString().Substring(i + 1);
                    OutputList.Items.Insert(0, "       X coorinate = " + e.X.ToString() + ", Y coordinate = " + e.Y.ToString());
                    OutputList.Items.Insert(0, "Mouse up event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_ItemChanged(object sender, PegasusImaging.WinForms.NotateXpress9.ItemChangedEventArgs e)
        {
            try
            {
                Layer layer = e.Layer;
                Element element = e.Element;
                string strAnnotationType = " ";
                if (layer != null && element != null)
                {
                    int i = element.GetType().ToString().LastIndexOf('.');
                    if (i == -1)
                        strAnnotationType += element.GetType().ToString();
                    else
                        strAnnotationType += element.GetType().ToString().Substring(i + 1);
                    OutputList.Items.Insert(0, "Item changed event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_CurrentLayerChange(object sender, PegasusImaging.WinForms.NotateXpress9.CurrentLayerChangeEventArgs e)
        {
            try
            {
                Layer layer = e.Layer;

                if (layer != null)
                {
                    OutputList.Items.Insert(0, "Layer change event, current layer name = " + e.Layer.Name + ", description = " + e.Layer.Description);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_LayerRestored(object sender, PegasusImaging.WinForms.NotateXpress9.LayerRestoredEventArgs e)
        {
            try
            {
                Layer layer = e.Layer;
                if (layer != null)
                {
                    OutputList.Items.Insert(0, "Layer restored event on layer name = " + e.Layer.Name + ", description = " + layer.Description);

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_MenuSelect(object sender, PegasusImaging.WinForms.NotateXpress9.MenuEventArgs e)
        {
            try
            {
                MenuType menuType = e.Menu;
                string strMenuType = "Unknown";
                if (menuType == MenuType.Context)
                    strMenuType = "Context";
                else if (menuType == MenuType.Toolbar)
                    strMenuType = "Toolbar";
                AnnotationTool annotationTool = e.Tool;
                Layer layer = e.Layer;
                Element element = e.Element;
                string strAnnotationType = " ";
                if (layer != null && element != null)
                {
                    int i = element.GetType().ToString().LastIndexOf('.');
                    if (i == -1)
                        strAnnotationType += element.GetType().ToString();
                    else
                        strAnnotationType += element.GetType().ToString().Substring(i + 1);
                    OutputList.Items.Insert(0, "         Top Menu ID = " + e.TopMenuId.ToString() + ", Sub Menu ID = " + e.SubMenuId.ToString() + ", User 1 = " + e.User1.ToString() + ", User 2 = " + e.User2.ToString());
                    OutputList.Items.Insert(0, strMenuType + " menu select event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_RequestLayerPassword(object sender, PegasusImaging.WinForms.NotateXpress9.GetLayerPasswordEventArgs e)
        {
            try
            {
                OutputList.Items.Insert(0, "Request Layer Password event for layer name = " + e.LayerName + ", layer password = " + e.LayerPassword);
                InputDlg dlg = new InputDlg("Provide Layer '" + e.LayerName + "' Password");
                if (System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
                {
                    e.LayerPassword = dlg.InputText;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_ToolbarSelect(object sender, PegasusImaging.WinForms.NotateXpress9.ToolbarEventArgs e)
        {
            try
            {
                Layer layer = e.Layer;
                if (layer != null)
                {
                    OutputList.Items.Insert(0, "Toolbar " + GetAnnotationToolString(e.Tool) + " selected, layer name = " + layer.Name + ", description = " + layer.Description);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private string GetAnnotationToolString(AnnotationTool annotationTool)
        {
            try
            {
                switch (annotationTool)
                {
                    case AnnotationTool.NoTool: // = 0x0,
                        return "NoTool";
                    case AnnotationTool.PointerTool: // = 0x1000,
                        return "PointerTool";
                    case AnnotationTool.TextTool: // = 0x1001,
                        return "TextTool";
                    case AnnotationTool.RectangleTool: // = 0x1002,
                        return "RectangleTool";
                    case AnnotationTool.EllipseTool: // = 0x1003,
                        return "EllipseTool";
                    case AnnotationTool.PolygonTool: // = 0x1004,
                        return "PolygonTool";
                    case AnnotationTool.PolyLineTool: // = 0x1005,
                        return "PolyLineTool";
                    case AnnotationTool.LineTool: // = 0x1006,
                        return "LineTool";
                    case AnnotationTool.FreehandTool: // = 0x1007,
                        return "FreehandTool";
                    case AnnotationTool.StampTool: // = 0x1008,
                        return "StampTool";
                    case AnnotationTool.ImageTool: // = 0x1009,
                        return "ImageTool";
                    case AnnotationTool.ButtonTool: // = 0x100a,
                        return "ButtonTool";
                    case AnnotationTool.RulerTool: // = 0x100b
                        return "RulerTool";
                    default:
                        return "Unknown";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return "Unknown";
            }
        }

        private void notateXpress1_UserDraw(object sender, PegasusImaging.WinForms.NotateXpress9.UserDrawEventArgs e)
        {
            try
            {
                Layer layer = e.Layer;
                Element element = e.Element;
                string strAnnotationType = " ";
                if (layer != null && element != null)
                {
                    int i = element.GetType().ToString().LastIndexOf('.');
                    if (i == -1)
                        strAnnotationType += element.GetType().ToString();
                    else
                        strAnnotationType += element.GetType().ToString().Substring(i + 1);
                    OutputList.Items.Insert(0, "         Device context = " + e.HandleDeviceContext.ToString() + ", X cooordinate = " + e.X.ToString() + ", Y coordinate = " + e.Y.ToString() + ", Width = " + e.Width.ToString() + ", Height = " + e.Height.ToString());
                    OutputList.Items.Insert(0, "User draw event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_UserGroupCreated(object sender, PegasusImaging.WinForms.NotateXpress9.UserGroupCreatedEventArgs e)
        {
            try
            {
                Layer layer = e.Layer;
                Group group = e.Group;
                OutputList.Items.Insert(0, "User group '" + group.Name + "' created event, layer name = " + layer.Name + ", description = " + layer.Description);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_UserGroupDestroyed(object sender, PegasusImaging.WinForms.NotateXpress9.UserGroupDestroyedEventArgs e)
        {
            try
            {
                Layer layer = e.Layer;
                Group group = e.Group;
                OutputList.Items.Insert(0, "User group '" + group.Name + "' deleted event, layer name = " + layer.Name + ", description = " + layer.Description);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemAboutNX_Click(object sender, System.EventArgs e)
        {
            notateXpress1.AboutBox();
        }

        private void menuItemAboutIX_Click(object sender, System.EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void notateXpress1_AnnotationAdded(object sender, PegasusImaging.WinForms.NotateXpress9.AnnotationAddedEventArgs e)
        {
            try
            {
                Element element = e.Element;
                string strAnnotationType = " ";
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    int i = element.GetType().ToString().LastIndexOf('.');
                    if (i == -1)
                        strAnnotationType += element.GetType().ToString();
                    else
                        strAnnotationType += element.GetType().ToString().Substring(i + 1);

                    if (_bAutoSelect && element != null)
                    {
                        // select newly moved layer element
                        element.Selected = true;
                    }

                }

                if (!(sender is PegasusImaging.WinForms.NotateXpress9.NotateXpress))
                    MessageBox.Show("sender is " + sender.GetType().ToString());

                // entry has been Added
                OutputList.Items.Insert(0, "Add event for" + strAnnotationType + " annotation");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void notateXpress1_AnnotationDeleted(object sender, PegasusImaging.WinForms.NotateXpress9.AnnotationDeletedEventArgs e)
        {
            try
            {
                Element element = e.Element;
                string strAnnotationType = " ";
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    int i = element.GetType().ToString().LastIndexOf('.');
                    if (i == -1)
                        strAnnotationType += element.GetType().ToString();
                    else
                        strAnnotationType += element.GetType().ToString().Substring(i + 1);
                }

                if (!(sender is PegasusImaging.WinForms.NotateXpress9.NotateXpress))
                    MessageBox.Show("sender is " + sender.GetType().ToString());

                // entry has been Deleted
                OutputList.Items.Insert(0, "Delete event for" + strAnnotationType + " annotation");
            }
            catch (PegasusImaging.WinForms.NotateXpress9.NotateXpressException ex)
            {
                PegasusError(ex, lblerror);
            }
            catch (System.Exception ex)
            {
                PegasusError(ex, lblerror);
            }
        }


        private void notateXpress1_AnnotationMoved(object sender, PegasusImaging.WinForms.NotateXpress9.AnnotationMovedEventArgs e)
        {
            try
            {
                Element element = e.Element;
                string strAnnotationType = " ";
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    int i = element.GetType().ToString().LastIndexOf('.');
                    if (i == -1)
                        strAnnotationType += element.GetType().ToString();
                    else
                        strAnnotationType += element.GetType().ToString().Substring(i + 1);
                }

                if (!(sender is PegasusImaging.WinForms.NotateXpress9.NotateXpress))
                    MessageBox.Show("sender is " + sender.GetType().ToString());

                // entry has been moved
                OutputList.Items.Insert(0, "Move event for" + strAnnotationType + " annotation");
                if (_bAutoSelect && element != null)
                    // select newly moved layer element
                    element.Selected = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void notateXpress1_AnnotationSelected(object sender, PegasusImaging.WinForms.NotateXpress9.AnnotationSelectedEventArgs e)
        {
            try
            {
                Element element = e.Element;
                string strAnnotationType = " ";
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    int i = element.GetType().ToString().LastIndexOf('.');
                    if (i == -1)
                        strAnnotationType += element.GetType().ToString();
                    else
                        strAnnotationType += element.GetType().ToString().Substring(i + 1);
                }

                if (!(sender is PegasusImaging.WinForms.NotateXpress9.NotateXpress))
                    MessageBox.Show("sender is " + sender.GetType().ToString());

                // entry has been selected
                OutputList.Items.Insert(0, "Selected event for" + strAnnotationType + " annotation");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }


        private void mnu_file_saveann2fileANN_Click(object sender, System.EventArgs e)
        {
            try
            {
                saveFileDialog.Title = "Save Annotation File";
                saveFileDialog.Filter = "TMS Annotation File (*.ann) | *.ann";
                saveFileDialog.DefaultExt = ".ann";
                saveFileDialog.InitialDirectory = strCurrentDir;
                saveFileDialog.ShowDialog(this);

                string strFileName = saveFileDialog.FileName;
                if (strFileName != "" && strFileName != null)
                {

                    //clear out the error in case there was an error from a previous operation
                    lblerror.Text = "";

                    notateXpress1.Layers.Comments = "This file was saved today";
                    notateXpress1.Layers.Subject = "testing 123";
                    notateXpress1.Layers.Title = "One awesome annotation file";
                    notateXpress1.Layers.SaveToFile(strFileName, m_saveOptions);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }




        private void mnu_file_saveann2fileNXP_Click(object sender, System.EventArgs e)
        {
            try
            {
                saveFileDialog.Title = "Save Annotation File";
                saveFileDialog.Filter = "NotateXpress File (*.nxp) | *.nxp";
                saveFileDialog.DefaultExt = ".nxp";
                saveFileDialog.InitialDirectory = strCurrentDir;
                saveFileDialog.ShowDialog(this);

                string strFileName = saveFileDialog.FileName;
                if (strFileName != "" && strFileName != null)
                {

                    notateXpress1.Layers.Comments = "This file was saved today";
                    notateXpress1.Layers.Subject = "testing 123";
                    notateXpress1.Layers.Title = "One awesome annotation file";
                    notateXpress1.Layers.SaveToFile(strFileName, m_saveOptions);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_file_loadannotationNXP_Click(object sender, System.EventArgs e)
        {
            try
            {
                PegasusImaging.WinForms.NotateXpress9.Layer layer;

                openFileDialog.Title = "Open NotateXpress File";
                openFileDialog.Filter = "NotateXpress File (*.nxp) | *.nxp";
                openFileDialog.DefaultExt = ".nxp";
                openFileDialog.Multiselect = false;
                openFileDialog.InitialDirectory = strCurrentDir;
                openFileDialog.ShowDialog(this);

                string strFileName = openFileDialog.FileName;
                if (System.IO.File.Exists(strFileName))
                {

                    notateXpress1.Layers.FromFile(strFileName, m_loadOptions);
                    layer = notateXpress1.Layers.Selected();
                    if (layer != null)
                    {
                        layer.Toolbar.Visible = true;
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_file_loadannotationANN_Click(object sender, System.EventArgs e)
        {
            PegasusImaging.WinForms.NotateXpress9.Layer layer;

            openFileDialog.Title = "Open NotateXpress File";
            openFileDialog.Filter = "TMS Annotation File (*.ann) | *.ann";
            openFileDialog.DefaultExt = ".ann";
            openFileDialog.Multiselect = false;
            openFileDialog.InitialDirectory = strCurrentDir;
            openFileDialog.ShowDialog(this);

            string strFileName = openFileDialog.FileName;
            if (System.IO.File.Exists(strFileName))
            {
                try
                {
                    notateXpress1.Layers.FromFile(strFileName, m_loadOptions);
                    layer = notateXpress1.Layers.Selected();
                    if (layer != null)
                    {
                        layer.Toolbar.Visible = true;
                    }
                }
                catch (PegasusImaging.WinForms.NotateXpress9.NotateXpressException ex)
                {
                    PegasusError(ex, lblerror);
                }
            }
        }

        private void mnu_objects_addNoteAnno_Click_1(object sender, EventArgs e)
        {
            try
            {
                NoteTool note = new NoteTool();
                note.Text = "This is a note";
                note.BoundingRectangle = new Rectangle(80, 70, 50, 50);
                note.ToolTipText = "Note Tool";
                notateXpress1.Layers.Selected().Elements.Add(note);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnu_objects_addHighlight_Click_1(object sender, EventArgs e)
        {
            try
            {
                BlockHighlightTool highlight = new BlockHighlightTool();
                highlight.BoundingRectangle = new Rectangle(122, 314, 313, 28);
                highlight.ToolTipText = "Block Highlight Tool";
                notateXpress1.Layers.Selected().Elements.Add(highlight);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemBlockHighlightEnabled_Click(object sender, EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolEnabled(AnnotationTool.BlockHighlightTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.BlockHighlightTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemBlockHighlightVisible_Click(object sender, EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.BlockHighlightTool, !layer.Toolbar.GetToolVisible(AnnotationTool.BlockHighlightTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemNoteToolEnabled_Click(object sender, EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolEnabled(AnnotationTool.NoteTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.NoteTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItemNoteToolVisible_Click(object sender, EventArgs e)
        {
            try
            {
                Layer layer = notateXpress1.Layers.Selected();
                if (layer != null)
                {
                    layer.Toolbar.SetToolVisible(AnnotationTool.NoteTool, !layer.Toolbar.GetToolVisible(AnnotationTool.NoteTool));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CustomCursor(AnnotationTool tool, CursorSelection type, Cursor cursor)
        {
            try
            {
                notateXpress1.SetUseCustomCursorType(type, tool, cursor.Handle);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItem14_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "CURSOR (*.CUR)|*.CUR";
            dlg.Title = "Choose an Image Cursor";

            dlg.ShowDialog();
            if (dlg.FileName != "")
            {
                cursor1 = new Cursor(LoadCursorFromFile(dlg.FileName));
            }
        }

        private void menuItem18_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "CURSOR (*.CUR)|*.CUR";
            dlg.Title = "Choose an Image Cursor";

            dlg.ShowDialog();
            if (dlg.FileName != "")
            {
                cursor2 = new Cursor(LoadCursorFromFile(dlg.FileName));
            }
        }

        private void menuItem23_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "CURSOR (*.CUR)|*.CUR";
            dlg.Title = "Choose an Image Cursor";

            dlg.ShowDialog();
            if (dlg.FileName != "")
            {
                cursor3 = new Cursor(LoadCursorFromFile(dlg.FileName));
            }
        }

        private void menuItem13_Click(object sender, EventArgs e)
        {
            CustomCursor(AnnotationTool.RectangleTool, CursorSelection.Creating, cursor1);
        }

        private void menuItem16_Click(object sender, EventArgs e)
        {
            CustomCursor(AnnotationTool.RectangleTool, CursorSelection.Moving, cursor1);       
        }

        private void menuItem15_Click(object sender, EventArgs e)
        {
            CustomCursor(AnnotationTool.RectangleTool, CursorSelection.Selecting, cursor1);       
        }

        private void menuItem19_Click(object sender, EventArgs e)
        {
            CustomCursor(AnnotationTool.EllipseTool, CursorSelection.Creating, cursor2);               
        }

        private void menuItem21_Click(object sender, EventArgs e)
        {
            CustomCursor(AnnotationTool.EllipseTool, CursorSelection.Moving, cursor2);  
        }

        private void menuItem20_Click(object sender, EventArgs e)
        {
            CustomCursor(AnnotationTool.EllipseTool, CursorSelection.Selecting, cursor2);  
        }

        private void menuItem24_Click(object sender, EventArgs e)
        {
            CustomCursor(AnnotationTool.TextTool, CursorSelection.Creating, cursor3); 
        }

        private void menuItem26_Click(object sender, EventArgs e)
        {
            CustomCursor(AnnotationTool.TextTool, CursorSelection.Moving, cursor3); 
        }

        private void menuItem25_Click(object sender, EventArgs e)
        {
            CustomCursor(AnnotationTool.TextTool, CursorSelection.Selecting, cursor3); 
        }

        private void menuItem27_Click(object sender, EventArgs e)
        {
            try
            {
                m_loadOptions.AnnType = m_saveOptions.AnnType = AnnotationType.NotateXpressXml;

                mnu_file_saveann2fileANN.Enabled = false;
                mnu_file_loadannotationANN.Enabled = false;

                mnu_file_loadannotationNXP.Enabled = false;
                mnu_file_saveann2fileNXP.Enabled = false;

                mnu_saveXML.Enabled = true;
                mnu_loadXML.Enabled = true;

                saveXFDFMenuItem.Enabled = false;
                mnu_loadXFDF.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItem28_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog.Title = "Save Annotation File";
                saveFileDialog.Filter = "XML Annotation File (*.xml) | *.xml";
                saveFileDialog.DefaultExt = ".xml";
                saveFileDialog.InitialDirectory = strCurrentDir;
                saveFileDialog.ShowDialog(this);

                string strFileName = saveFileDialog.FileName;
                if (strFileName != "" && strFileName != null)
                {

                    //clear out the error in case there was an error from a previous operation
                    lblerror.Text = "";

                    notateXpress1.Layers.Comments = "This file was saved today";
                    notateXpress1.Layers.Subject = "testing 123";
                    notateXpress1.Layers.Title = "One awesome annotation file";
                    notateXpress1.Layers.SaveToFile(strFileName, m_saveOptions);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItem29_Click(object sender, EventArgs e)
        {
            PegasusImaging.WinForms.NotateXpress9.Layer layer;

            openFileDialog.Title = "Open NotateXpress File";
            openFileDialog.Filter = "XML Annotation File (*.xml) | *.xml";
            openFileDialog.DefaultExt = ".xml";
            openFileDialog.Multiselect = false;
            openFileDialog.InitialDirectory = strCurrentDir;
            openFileDialog.ShowDialog(this);

            string strFileName = openFileDialog.FileName;
            if (System.IO.File.Exists(strFileName))
            {
                try
                {
                    notateXpress1.Layers.FromFile(strFileName, m_loadOptions);
                    layer = notateXpress1.Layers.Selected();
                    if (layer != null)
                    {
                        layer.Toolbar.Visible = true;
                    }
                }
                catch (PegasusImaging.WinForms.NotateXpress9.NotateXpressException ex)
                {
                    PegasusError(ex, lblerror);
                }
            }
        }

        private void menuItem30_Click(object sender, EventArgs e)
        {
            try
            {
                m_loadOptions.AnnType = m_saveOptions.AnnType = AnnotationType.Xfdf;

                mnu_file_saveann2fileANN.Enabled = false;
                mnu_file_loadannotationANN.Enabled = false;

                mnu_file_loadannotationNXP.Enabled = false;
                mnu_file_saveann2fileNXP.Enabled = false;

                mnu_saveXML.Enabled = false;
                mnu_loadXML.Enabled = false;

                saveXFDFMenuItem.Enabled = true;
                mnu_loadXFDF.Enabled = true;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void saveXFDFMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                saveFileDialog.Title = "Save Annotation File";
                saveFileDialog.Filter = "XFDF Annotation File (*.xfdf) | *.xfdf";
                saveFileDialog.DefaultExt = ".xfdf";
                saveFileDialog.InitialDirectory = strCurrentDir;
                saveFileDialog.ShowDialog(this);

                string strFileName = saveFileDialog.FileName;
                if (strFileName != "" && strFileName != null)
                {

                    //clear out the error in case there was an error from a previous operation
                    lblerror.Text = "";

                    notateXpress1.Layers.Comments = "This file was saved today";
                    notateXpress1.Layers.Subject = "testing 123";
                    notateXpress1.Layers.Title = "One awesome annotation file";

                    notateXpress1.Layers.SaveToFile(strFileName, m_saveOptions);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void loadXFDFMenuItem_Click(object sender, EventArgs e)
        {
            PegasusImaging.WinForms.NotateXpress9.Layer layer;

            openFileDialog.Title = "Open NotateXpress File";
            openFileDialog.Filter = "XFDF Annotation File (*.xfdf) | *.xfdf";
            openFileDialog.DefaultExt = ".xfdf";
            openFileDialog.Multiselect = false;
            openFileDialog.InitialDirectory = strCurrentDir;
            openFileDialog.ShowDialog(this);

            string strFileName = openFileDialog.FileName;
            if (System.IO.File.Exists(strFileName))
            {
                try
                {
                    notateXpress1.Layers.FromFile(strFileName, m_loadOptions);
                    layer = notateXpress1.Layers.Selected();
                    if (layer != null)
                    {
                        layer.Toolbar.Visible = true;
                    }
                }
                catch (PegasusImaging.WinForms.NotateXpress9.NotateXpressException ex)
                {
                    PegasusError(ex, lblerror);
                }
            }
        }

        private void mnu_ToolbarRectangleTool_Click(object sender, EventArgs e)
        {

        }

        private void mnu_ToolbarEllipseTool_Click(object sender, EventArgs e)
        {

        }      
    }
}
	
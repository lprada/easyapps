/*****************************************************************'
* Copyright 2008 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using PegasusImaging.WinForms.ImagXpress9;
using PegasusImaging.WinForms.NotateXpress9;
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace SimpleAnnotations
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class MainForm : System.Windows.Forms.Form
	{
		string strImageFileName;
		string strNXPFileName;
		private System.String strCurrentDir;
		private System.String strCurrentImage;
		private System.String strSaveFileName;
		PegasusImaging.WinForms.ImagXpress9.ImageX imagX1;
		PegasusImaging.WinForms.NotateXpress9.Layer layer;
		PegasusImaging.WinForms.ImagXpress9.LoadOptions loIXLoadOptions;
		PegasusImaging.WinForms.ImagXpress9.SaveOptions soIXSaveOptions;
		PegasusImaging.WinForms.NotateXpress9.SaveOptions soNXSaveOptions;
        PegasusImaging.WinForms.NotateXpress9.LoadOptions soNXLoadOptions;

		private PegasusImaging.WinForms.ImagXpress9.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress9.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.NotateXpress9.NotateXpress notateXpress1;
        private System.Windows.Forms.MainMenu mainMenu1;
		internal System.Windows.Forms.Label lblLastError;
		internal System.Windows.Forms.ListBox lstInfo;
		internal System.Windows.Forms.Label lblerror;
		private System.Windows.Forms.MenuItem mnuFile;
		private System.Windows.Forms.MenuItem mnuOpen;
		private System.Windows.Forms.MenuItem mnuSave;
		private System.Windows.Forms.MenuItem mnuWang;
		private System.Windows.Forms.MenuItem mnuSaveA;
		private System.Windows.Forms.MenuItem mnuLoadAnno;
		private System.Windows.Forms.MenuItem mnuExit;
		private System.Windows.Forms.MenuItem mnuCreate;
		private System.Windows.Forms.MenuItem mnuTxt;
		private System.Windows.Forms.MenuItem mnuRedac;
		private System.Windows.Forms.MenuItem mnuRuler;
		private System.Windows.Forms.MenuItem mnuRects;
		private System.Windows.Forms.MenuItem mnuNotate;
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.MenuItem mnuToolbar;
        private System.Windows.Forms.MenuItem mnuNotate1;
        private MenuItem NoteMenuItem;
        private MenuItem HighlightMenuItem;
        private MenuItem menuItem1;
        private MenuItem menuItem2;
        private IContainer components;

		public MainForm()
		{
			//
			// Required for Windows Form Designer support

			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

                if (notateXpress1 != null)
                {
                    notateXpress1.ClientDisconnect();

                    notateXpress1.Dispose();
                    notateXpress1 = null;
                }

                if (imagX1 != null)
                {
                    imagX1.Dispose();
                    imagX1 = null;
                }

                if (imageXView1.Image != null)
                {
                    imageXView1.Image.Dispose();
                    imageXView1.Image = null;
                }

				//Don't forget to dispose IX and NX
                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
				}

				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress9.ImagXpress(this.components);
            this.imageXView1 = new PegasusImaging.WinForms.ImagXpress9.ImageXView(this.components);
            this.notateXpress1 = new PegasusImaging.WinForms.NotateXpress9.NotateXpress(this.components);
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            this.mnuFile = new System.Windows.Forms.MenuItem();
            this.mnuOpen = new System.Windows.Forms.MenuItem();
            this.mnuSave = new System.Windows.Forms.MenuItem();
            this.mnuWang = new System.Windows.Forms.MenuItem();
            this.mnuSaveA = new System.Windows.Forms.MenuItem();
            this.mnuLoadAnno = new System.Windows.Forms.MenuItem();
            this.mnuExit = new System.Windows.Forms.MenuItem();
            this.mnuCreate = new System.Windows.Forms.MenuItem();
            this.mnuTxt = new System.Windows.Forms.MenuItem();
            this.mnuRedac = new System.Windows.Forms.MenuItem();
            this.mnuRuler = new System.Windows.Forms.MenuItem();
            this.mnuRects = new System.Windows.Forms.MenuItem();
            this.NoteMenuItem = new System.Windows.Forms.MenuItem();
            this.HighlightMenuItem = new System.Windows.Forms.MenuItem();
            this.mnuToolbar = new System.Windows.Forms.MenuItem();
            this.mnuNotate1 = new System.Windows.Forms.MenuItem();
            this.mnuAbout = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.mnuNotate = new System.Windows.Forms.MenuItem();
            this.lblLastError = new System.Windows.Forms.Label();
            this.lstInfo = new System.Windows.Forms.ListBox();
            this.lblerror = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
       
            // 
            // imageXView1
            // 
            this.imageXView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.imageXView1.Location = new System.Drawing.Point(16, 88);
            this.imageXView1.MouseWheelCapture = false;
            this.imageXView1.Name = "imageXView1";
            this.imageXView1.Size = new System.Drawing.Size(560, 387);
            this.imageXView1.TabIndex = 0;
            // 
            // notateXpress1
            // 
            this.notateXpress1.AllowPaint = true;
            this.notateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress9.FontScaling.Normal;
            this.notateXpress1.ImagXpressLoad = true;
            this.notateXpress1.ImagXpressSave = true;
            this.notateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress9.AnnotationMode.Edit;
            this.notateXpress1.LineScaling = PegasusImaging.WinForms.NotateXpress9.LineScaling.Normal;
            this.notateXpress1.MultiLineEdit = false;
            this.notateXpress1.RecalibrateXdpi = -1;
            this.notateXpress1.RecalibrateYdpi = -1;
            this.notateXpress1.ToolTipTimeEdit = 0;
            this.notateXpress1.ToolTipTimeInteractive = 0;
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuFile,
            this.mnuCreate,
            this.mnuToolbar,
            this.mnuAbout});
            // 
            // mnuFile
            // 
            this.mnuFile.Index = 0;
            this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuOpen,
            this.mnuSave,
            this.mnuWang,
            this.mnuSaveA,
            this.mnuLoadAnno,
            this.mnuExit});
            this.mnuFile.Text = "&File";
            // 
            // mnuOpen
            // 
            this.mnuOpen.Index = 0;
            this.mnuOpen.Text = "&Open Image";
            this.mnuOpen.Click += new System.EventHandler(this.mnuOpen_Click);
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 1;
            this.mnuSave.Text = "&Save Image";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuWang
            // 
            this.mnuWang.Checked = true;
            this.mnuWang.Index = 2;
            this.mnuWang.Text = "&Wang Compatible";
            this.mnuWang.Click += new System.EventHandler(this.mnuWang_Click);
            // 
            // mnuSaveA
            // 
            this.mnuSaveA.Index = 3;
            this.mnuSaveA.Text = "&Save Annotation";
            this.mnuSaveA.Click += new System.EventHandler(this.mnuSaveA_Click);
            // 
            // mnuLoadAnno
            // 
            this.mnuLoadAnno.Index = 4;
            this.mnuLoadAnno.Text = "&Load Annotation";
            this.mnuLoadAnno.Click += new System.EventHandler(this.mnuLoadAnno_Click);
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 5;
            this.mnuExit.Text = "&Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuCreate
            // 
            this.mnuCreate.Index = 1;
            this.mnuCreate.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuTxt,
            this.mnuRedac,
            this.mnuRuler,
            this.mnuRects,
            this.NoteMenuItem,
            this.HighlightMenuItem});
            this.mnuCreate.Text = "&Create Annotations";
            // 
            // mnuTxt
            // 
            this.mnuTxt.Index = 0;
            this.mnuTxt.Text = "Add Text";
            this.mnuTxt.Click += new System.EventHandler(this.mnuTxt_Click);
            // 
            // mnuRedac
            // 
            this.mnuRedac.Index = 1;
            this.mnuRedac.Text = "Add Redaction";
            this.mnuRedac.Click += new System.EventHandler(this.mnuRedac_Click);
            // 
            // mnuRuler
            // 
            this.mnuRuler.Index = 2;
            this.mnuRuler.Text = "Add Ruler";
            this.mnuRuler.Click += new System.EventHandler(this.mnuRuler_Click);
            // 
            // mnuRects
            // 
            this.mnuRects.Index = 3;
            this.mnuRects.Text = "Add Rectangles";
            this.mnuRects.Click += new System.EventHandler(this.mnuRects_Click);
            // 
            // NoteMenuItem
            // 
            this.NoteMenuItem.Index = 4;
            this.NoteMenuItem.Text = "Add Note";
            this.NoteMenuItem.Click += new System.EventHandler(this.NoteMenuItem_Click);
            // 
            // HighlightMenuItem
            // 
            this.HighlightMenuItem.Index = 5;
            this.HighlightMenuItem.Text = "Add HighlightBlock";
            this.HighlightMenuItem.Click += new System.EventHandler(this.HighlightMenuItem_Click);
            // 
            // mnuToolbar
            // 
            this.mnuToolbar.Index = 2;
            this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.mnuNotate1});
            this.mnuToolbar.Text = "&Toolbar";
            // 
            // mnuNotate1
            // 
            this.mnuNotate1.Index = 0;
            this.mnuNotate1.Text = "&Hide";
            this.mnuNotate1.Click += new System.EventHandler(this.mnuNotate1_Click);
            // 
            // mnuAbout
            // 
            this.mnuAbout.Index = 3;
            this.mnuAbout.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1,
            this.menuItem2});
            this.mnuAbout.Text = "&About";
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.Text = "Imag&Xpress";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 1;
            this.menuItem2.Text = "&NoateXpress";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // mnuNotate
            // 
            this.mnuNotate.Index = -1;
            this.mnuNotate.Text = "";
            // 
            // lblLastError
            // 
            this.lblLastError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLastError.Location = new System.Drawing.Point(592, 275);
            this.lblLastError.Name = "lblLastError";
            this.lblLastError.Size = new System.Drawing.Size(184, 32);
            this.lblLastError.TabIndex = 8;
            this.lblLastError.Text = "Last Error:";
            // 
            // lstInfo
            // 
            this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstInfo.Items.AddRange(new object[] {
            "This sample demonstrates the following functionality:",
            "1)Creating various annotations programatically."});
            this.lstInfo.Location = new System.Drawing.Point(16, 8);
            this.lstInfo.Name = "lstInfo";
            this.lstInfo.Size = new System.Drawing.Size(776, 69);
            this.lstInfo.TabIndex = 7;
            // 
            // lblerror
            // 
            this.lblerror.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblerror.Location = new System.Drawing.Point(592, 331);
            this.lblerror.Name = "lblerror";
            this.lblerror.Size = new System.Drawing.Size(200, 128);
            this.lblerror.TabIndex = 6;
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(808, 500);
            this.Controls.Add(this.lblLastError);
            this.Controls.Add(this.lstInfo);
            this.Controls.Add(this.lblerror);
            this.Controls.Add(this.imageXView1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Menu = this.mainMenu1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Simple Annotations";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

		}
		#endregion


		#region Pegasus Imaging Sample Application Standard Functions

		System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private const System.String strCommonImagesDirectory = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
		string strDefaultImageFilter = ("All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" +
			"" + (";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" + ("2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" + ("t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" + ("e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" + (")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" + ("cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" + ("s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" + "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"))))))));

    
		private string GetFileName(string FullName) 
		{
			return FullName.Substring((FullName.LastIndexOf("\\") + 1), (FullName.Length 
				- (FullName.LastIndexOf("\\") + 1)));
		}
    
		private static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = (ErrorException.Message + ("" + ("\n" 
				+ ((ErrorException.Source + "") 
				+ "\n"))));
		}
    
		private static void PegasusError(PegasusImaging.WinForms.ImagXpress9.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = (ErrorException.Message + ("" + ("\n" 
				+ (ErrorException.Source + ("" + ("\n" + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))));
		}
    
		private string PegasusOpenFile(string sTitle, string sFileFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = sTitle;
			dlg.Filter = sFileFilter;
			dlg.InitialDirectory = strCurrentDir;
			if ((dlg.ShowDialog() == DialogResult.OK)) 
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
				return dlg.FileName;
			}
			else 
			{
				return "";
			}
		}
    
		private string PegasusSaveFile(string sTitle, string sFilter) 
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.Title = sTitle;
			dlg.Filter = sFilter;
			dlg.InitialDirectory = strCurrentDir;
			if ((dlg.ShowDialog() == DialogResult.OK)) 
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
				return dlg.FileName;
			}
			else 
			{
				return "";
			}
		}

		#endregion


		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new MainForm());
		}

		private void LoadFile() 
		{
			try 
			{
                //Load the image into ImagXpress
				PegasusImaging.WinForms.ImagXpress9.ImageX oldImage = imageXView1.Image;
				imagX1 = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strImageFileName, loIXLoadOptions);
				imageXView1.Image = imagX1;
				lblerror.Text = "";
				if (oldImage != null)
				{
					oldImage.Dispose();
					oldImage = null;
				}
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
		}


		
		private void Form1_Load(object sender, System.EventArgs e)
		{
			try 
			{
				//**The UnlockRuntime function must be called to distribute the runtime**
				//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);

                Application.EnableVisualStyles();

				// Create a new load options object so we can recieve events from the images we load
				loIXLoadOptions = new PegasusImaging.WinForms.ImagXpress9.LoadOptions();
				soIXSaveOptions = new PegasusImaging.WinForms.ImagXpress9.SaveOptions();
				soIXSaveOptions.Tiff.Compression = PegasusImaging.WinForms.ImagXpress9.Compression.Group4;
				soNXSaveOptions = new PegasusImaging.WinForms.NotateXpress9.SaveOptions();
				soNXLoadOptions = new PegasusImaging.WinForms.NotateXpress9.LoadOptions();
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}

			//here we set the current directory and image so that the file open dialog box works well
			strCurrentDir = System.IO.Directory.GetCurrentDirectory ().ToString (cultNumber);
			strCurrentImage = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\benefits.tif");
			strCurrentDir = strCommonImagesDirectory;
			
			if (System.IO.File.Exists(strCurrentImage)) 
			{
				try 
				{
                    //Load the image and connect ImagXpress to NotateXpress
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress9.ImageX.FromFile(imagXpress1, strCurrentImage, loIXLoadOptions);
					imageXView1.AutoScroll = true;
					notateXpress1.ClientWindow = imageXView1.Handle;
					notateXpress1.ToolbarDefaults.ToolbarActivated = true;
					layer = new PegasusImaging.WinForms.NotateXpress9.Layer();
					// ****This is a workaround issue ***
					layer.Active = true;
					notateXpress1.Layers.Add(layer);
					// disable the toolbar 
					notateXpress1.ToolbarDefaults.ToolbarActivated = false;
					notateXpress1.MenuSetEnabled(PegasusImaging.WinForms.NotateXpress9.MenuType.Context, PegasusImaging.WinForms.NotateXpress9.AnnotationTool.NoTool, true);
				}
				catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException eX) 
				{
					PegasusError(eX,lblerror);
				}
			}

            notateXpress1.ToolbarDefaults.ToolbarActivated = true;
		}

		private void mnuOpen_Click(object sender, System.EventArgs e)
		{
            try
            {
                strImageFileName = PegasusOpenFile("Select an Image File", strDefaultImageFilter);
                if (strImageFileName.Length != 0)
                {
                    //If it is valid, we set our internal image filename equal to it
                    LoadFile();
                }

                notateXpress1.Layers.Clear();

                layer = new PegasusImaging.WinForms.NotateXpress9.Layer();
                layer.Active = true;
                notateXpress1.Layers.Add(layer);
                mnuNotate1_Click(sender, e);

                notateXpress1.ToolbarDefaults.ToolbarActivated = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
            //save the image to a file using ImagXpres
			try 
			{
				strSaveFileName = PegasusSaveFile("Save TIFF File", "Tiff File (*.tif) | *.tif");
				imagX1.Save(strSaveFileName, soIXSaveOptions);
			}
			catch (PegasusImaging.WinForms.ImagXpress9.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
			catch (System.Exception ex) 
			{
				PegasusError(ex, lblerror);
			}


		}

		private void mnuWang_Click(object sender, System.EventArgs e)
		{
            //set the save options for Wang annotations
            try
            {
			if (soNXSaveOptions.PreserveWangLayers) 
			{
				soNXSaveOptions.PreserveWangLayers = false;
				mnuWang.Checked = false;
			}
			else 
			{
				soNXSaveOptions.PreserveWangLayers = true;
				mnuWang.Checked = true;
			}
                       }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

		}

		private void mnuSaveA_Click(object sender, System.EventArgs e)
		{
			strSaveFileName = PegasusSaveFile("Save Annotation File", "NotateXpress File (*.nxp) | *.nxp");
			try 
			{
				notateXpress1.Layers.Comments = "This file was saved today";
				notateXpress1.Layers.Subject = "testing 123";
				notateXpress1.Layers.Title = "One awesome annotation file";
				notateXpress1.Layers.SaveToFile(strSaveFileName, soNXSaveOptions);
			}
			catch (PegasusImaging.WinForms.NotateXpress9.NotateXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
			catch (System.Exception ex) 
			{
				PegasusError(ex, lblerror);
			}

		}

		private void mnuLoadAnno_Click(object sender, System.EventArgs e)
		{
			strNXPFileName = PegasusOpenFile("Open NotateXpress File", "NotateXpress File (*.nxp) | *.nxp");
			try 
			{
				notateXpress1.Layers.FromFile(strNXPFileName, soNXLoadOptions);
				mnuNotate1_Click(sender, e);
			}
			catch (PegasusImaging.WinForms.NotateXpress9.NotateXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
			catch (System.Exception ex) 
			{
				PegasusError(ex, lblerror);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.Application.Exit();
		}

        private void mnuTxt_Click(object sender, System.EventArgs e)
        {
            //add a text annotation
            try
            {
                System.Drawing.Rectangle rect = new System.Drawing.Rectangle();
                rect.X = 30;
                rect.Y = 30;
                rect.Width = 280;
                rect.Height = 300;
                PegasusImaging.WinForms.NotateXpress9.TextTool text = new PegasusImaging.WinForms.NotateXpress9.TextTool();
                text.TextJustification = PegasusImaging.WinForms.NotateXpress9.TextJustification.Center;
                text.Text = "NotateXpress";
                text.BackColor = System.Drawing.Color.Red;
                text.BoundingRectangle = rect;
                text.PenWidth = 2;
                text.BackStyle = PegasusImaging.WinForms.NotateXpress9.BackStyle.Translucent;
                text.PenWidth = 2;
                text.ToolTipText = "Text";
                // Add the annotation to the image
                layer.Elements.Add(text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mnuRedac_Click(object sender, System.EventArgs e)
        {
            //add a redaction annotation
            try
            {
                DateTime saveNow = DateTime.Now;


                System.Drawing.Rectangle rect = new System.Drawing.Rectangle(100, 100, 150, 150);
                PegasusImaging.WinForms.NotateXpress9.TextTool text = new PegasusImaging.WinForms.NotateXpress9.TextTool();
                text.Text = ("Approved by Pegasus Imaging Corp."
                    + "  " + DateTime.Now.Date.ToString());
                text.TextJustification = PegasusImaging.WinForms.NotateXpress9.TextJustification.Center;
                text.TextColor = System.Drawing.Color.Red;
                text.PenWidth = 1;
                text.PenColor = System.Drawing.Color.White;
                text.BackStyle = PegasusImaging.WinForms.NotateXpress9.BackStyle.Opaque;
                text.BackColor = System.Drawing.Color.White;
                text.BoundingRectangle = rect;
                text.ToolTipText = "Redaction";
                layer.Elements.Add(text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void mnuRuler_Click(object sender, System.EventArgs e)
        {
            //add a ruler annotation
            try
            {
                System.Drawing.Rectangle rect = new System.Drawing.Rectangle(25, 50, 200, 100);
                PegasusImaging.WinForms.NotateXpress9.RulerTool ruler = new PegasusImaging.WinForms.NotateXpress9.RulerTool();
                ruler.BoundingRectangle = rect;
                ruler.GaugeLength = 20;
                ruler.MeasurementUnit = PegasusImaging.WinForms.NotateXpress9.MeasurementUnit.Pixels;
                ruler.Precision = 1;
                ruler.BackColor = System.Drawing.Color.Blue;
                ruler.ToolTipText = "Ruler";
                layer.Elements.Add(ruler);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void mnuRects_Click(object sender, System.EventArgs e)
        {
            //add rectangle annotations
            try
            {
                int counter;
                System.Drawing.Rectangle rect = new System.Drawing.Rectangle();
                rect.X = 30;
                rect.Y = 30;
                rect.Width = 60;
                rect.Height = 60;
                System.Random rand = new System.Random();
                for (counter = 1; (counter <= 10); counter++)
                {
                    PegasusImaging.WinForms.NotateXpress9.RectangleTool rectangle = new PegasusImaging.WinForms.NotateXpress9.RectangleTool();
                    rectangle.BoundingRectangle = rect;
                    rectangle.PenColor = System.Drawing.Color.FromArgb((int)((rand.NextDouble() * 255)), (int)((rand.NextDouble() * 255)), (int)((rand.NextDouble() * 255)));
                    rectangle.FillColor = System.Drawing.Color.FromArgb((int)((rand.NextDouble() * 255)), (int)((rand.NextDouble() * 255)), (int)((rand.NextDouble() * 255)));
                    rectangle.ToolTipText = "Rectangle" + counter.ToString();
                    layer.Elements.Add(rectangle);
                    rect.X = (rect.X + 5);
                    rect.Y = (rect.Y + 5);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

		private void mnuNotate1_Click(object sender, System.EventArgs e)
		{
            //toggle the toolbar showing or not
            try
            {
                if ((notateXpress1.ToolbarDefaults.ToolbarActivated == true))
                {
                    mnuNotate1.Text = "Show";
                    notateXpress1.ToolbarDefaults.ToolbarActivated = false;
                }
                else
                {
                    mnuNotate1.Text = "Hide";
                    notateXpress1.ToolbarDefaults.ToolbarActivated = true;
                }
            }
                       
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
		}


        private void NoteMenuItem_Click(object sender, EventArgs e)
        {
            //add a note annotation
            try
            {
                PegasusImaging.WinForms.NotateXpress9.NoteTool note = new PegasusImaging.WinForms.NotateXpress9.NoteTool();

                note.BackColor = Color.Red;
                note.BoundingRectangle = new Rectangle(50, 50, 200, 200);
                note.Text = "Note Tool";
                note.TextColor = Color.PowderBlue;
                note.ToolTipText = "Note Tool";
                layer.Elements.Add(note);

            }
            catch (PegasusImaging.WinForms.NotateXpress9.NotateXpressException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void HighlightMenuItem_Click(object sender, EventArgs e)
        {
            //add a blockhighlight annotation
            try
            {
                PegasusImaging.WinForms.NotateXpress9.BlockHighlightTool highlight = new BlockHighlightTool();

                highlight.BoundingRectangle = new Rectangle(100, 100, 300, 300);
                highlight.FillColor = Color.Blue;
                highlight.ToolTipText = "Block Highlight";
                layer.Elements.Add(highlight);
            }
            catch (PegasusImaging.WinForms.NotateXpress9.NotateXpressException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            imagXpress1.AboutBox();
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            notateXpress1.AboutBox();
        }

        private void lblLoadStatus_Click(object sender, EventArgs e)
        {

        }			
	}
}

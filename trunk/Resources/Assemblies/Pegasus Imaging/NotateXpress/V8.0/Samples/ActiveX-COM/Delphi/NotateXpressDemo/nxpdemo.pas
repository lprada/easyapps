unit nxpdemo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, ExtCtrls, AxCtrls, PegasusImagingActiveXImagXpress8_TLB, OleCtrls,
  PegasusImagingActiveXNotateXpress8_TLB,ComObj, ActiveX, StdCtrls;

type
  TForm1 = class(TForm)
    MyMenu: TMainMenu;
    File1: TMenuItem;
    Loadimagefile1: TMenuItem;
    N1: TMenuItem;
    Loadannotationfile1: TMenuItem;
    Layers1: TMenuItem;
    Createnewlayer1: TMenuItem;
    Setcurrentlayer1: TMenuItem;
    Deletecurrentlayer1: TMenuItem;
    N2: TMenuItem;
    Hidecurrentlayer1: TMenuItem;
    ToggleToolPalettevisibility1: TMenuItem;
    N3: TMenuItem;
    Setlayerdescription1: TMenuItem;
    Getlayerdescription1: TMenuItem;
    N4: TMenuItem;
    Setlayername1: TMenuItem;
    Getlayername1: TMenuItem;
    Objects1: TMenuItem;
    Reversezorder1: TMenuItem;
    Create20objects1: TMenuItem;
    CreatetranslucentRectangle1: TMenuItem;
    CreateanImageobject1: TMenuItem;
    Nudgeup1: TMenuItem;
    Nudgedn1: TMenuItem;
    Inflateme1: TMenuItem;
    N5: TMenuItem;
    CreatelargeTextobject1: TMenuItem;
    N6: TMenuItem;
    Brand1: TMenuItem;
    OpenDialog1: TOpenDialog;
    Modify1: TMenuItem;
    Rectangle1: TMenuItem;
    Ellipse1: TMenuItem;
    Text1: TMenuItem;
    Stamp1: TMenuItem;
    Line1: TMenuItem;
    Polyline1: TMenuItem;
    Polygon1: TMenuItem;
    Pencolor1: TMenuItem;
    Penwidth1: TMenuItem;
    Fillcolor1: TMenuItem;
    Backstyle1: TMenuItem;
    N01: TMenuItem;
    N11: TMenuItem;
    N21: TMenuItem;
    N31: TMenuItem;
    N41: TMenuItem;
    N51: TMenuItem;
    Opaque1: TMenuItem;
    Translucent1: TMenuItem;
    Transparent1: TMenuItem;
    Pencolor2: TMenuItem;
    Penwidth2: TMenuItem;
    Fillcolor2: TMenuItem;
    Backstyle2: TMenuItem;
    N02: TMenuItem;
    N12: TMenuItem;
    N22: TMenuItem;
    N32: TMenuItem;
    N42: TMenuItem;
    N52: TMenuItem;
    Opaque2: TMenuItem;
    Translucent2: TMenuItem;
    Transparent2: TMenuItem;
    Font1: TMenuItem;
    Textcolor1: TMenuItem;
    Penwidth3: TMenuItem;
    Pencolor3: TMenuItem;
    BackStyle3: TMenuItem;
    N03: TMenuItem;
    N13: TMenuItem;
    N23: TMenuItem;
    N33: TMenuItem;
    N43: TMenuItem;
    N53: TMenuItem;
    Opaque3: TMenuItem;
    Transparent3: TMenuItem;
    Translucent3: TMenuItem;
    Textcolor2: TMenuItem;
    Font2: TMenuItem;
    Penwidth4: TMenuItem;
    Pencolor4: TMenuItem;
    Backstyle4: TMenuItem;
    N04: TMenuItem;
    N14: TMenuItem;
    N24: TMenuItem;
    N34: TMenuItem;
    N44: TMenuItem;
    N54: TMenuItem;
    Opaque4: TMenuItem;
    Transparent4: TMenuItem;
    Translucent4: TMenuItem;
    Penwidth5: TMenuItem;
    Pencolor5: TMenuItem;
    N15: TMenuItem;
    N25: TMenuItem;
    N35: TMenuItem;
    N45: TMenuItem;
    N55: TMenuItem;
    Pencolor6: TMenuItem;
    Penwidth6: TMenuItem;
    Pencolor7: TMenuItem;
    Penwidth7: TMenuItem;
    Fillcolor3: TMenuItem;
    Backstyle5: TMenuItem;
    N05: TMenuItem;
    N16: TMenuItem;
    N26: TMenuItem;
    N36: TMenuItem;
    N46: TMenuItem;
    N56: TMenuItem;
    Opaque5: TMenuItem;
    Translucent5: TMenuItem;
    Transparent5: TMenuItem;
    N17: TMenuItem;
    N27: TMenuItem;
    N37: TMenuItem;
    N47: TMenuItem;
    N57: TMenuItem;
    Freehand1: TMenuItem;
    Pencolor8: TMenuItem;
    Penwidth8: TMenuItem;
    N18: TMenuItem;
    N28: TMenuItem;
    N38: TMenuItem;
    N48: TMenuItem;
    N58: TMenuItem;
    SaveDialog1: TSaveDialog;
    AutoSelect: TMenuItem;
    N8: TMenuItem;
    UseMLE: TMenuItem;
    N9: TMenuItem;
    IXDibLoader: TImagXpress;
    Label1: TLabel;
    mnuWang: TMenuItem;
    mnuNXP: TMenuItem;
    mnuTMS: TMenuItem;
    mnuSaveImage: TMenuItem;
    mnuLoadIX: TMenuItem;
    mnuSaveIX: TMenuItem;
    mnuPreserveWang: TMenuItem;
    N7: TMenuItem;
    N10: TMenuItem;
    SaveAnnotationToFileNXP1: TMenuItem;
    N19: TMenuItem;
    LoadAnnotationfromFileNXP1: TMenuItem;
    mnuExit: TMenuItem;
    mnuAbout: TMenuItem;
    Xpress1: TImagXpress;
    Notate1: TNotateXpress;
    procedure OnActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Createnewlayer1Click(Sender: TObject);
    procedure Deletecurrentlayer1Click(Sender: TObject);
    procedure Getlayerdescription1Click(Sender: TObject);
    procedure OnCurrentLayerChange(Sender: TObject; newLayer: Integer);
    procedure Getlayername1Click(Sender: TObject);
    procedure Setlayerdescription1Click(Sender: TObject);
    procedure Setlayername1Click(Sender: TObject);
    procedure Hidecurrentlayer1Click(Sender: TObject);
    procedure OnLayersClick(Sender: TObject);
    procedure ToggleToolPalettevisibility1Click(Sender: TObject);
    procedure Create20objects1Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure CreatetranslucentRectangle1Click(Sender: TObject);
    procedure CreateanImageobject1Click(Sender: TObject);
    
    procedure Nudgeup1Click(Sender: TObject);
    procedure Nudgedn1Click(Sender: TObject);
    procedure OnObjectsClick(Sender: TObject);
    procedure CreatelargeTextobject1Click(Sender: TObject);
    procedure Loadimagefile1Click(Sender: TObject);
    procedure Reversezorder1Click(Sender: TObject);
    procedure OnFileClick(Sender: TObject);
    procedure OnBrandClick(Sender: TObject);
    procedure OnStamping(Sender: TObject);
    procedure OnModifyClick(Sender: TObject);
    procedure OnExitClick(Sender: TObject);
    procedure OnBackstyleClick(Sender: TObject);
    procedure PenColor(Sender: TObject);
    procedure OnPenWidth(Sender: TObject);
    procedure OnTextColor(Sender: TObject);
    procedure OnPenColor(Sender: TObject);
    procedure OnFillColor(Sender: TObject);
    procedure OnFont(Sender: TObject);
    procedure Saveannotationfile1Click(Sender: TObject);
    procedure OnAutoSelectClick(Sender: TObject);
    procedure OnUseMLE(Sender: TObject);
 
    procedure mnuWangClick(Sender: TObject);
    procedure mnuNXPClick(Sender: TObject);
    procedure mnuTMSClick(Sender: TObject);
    procedure mnuSaveImageClick(Sender: TObject);
    procedure mnuPreserveWangClick(Sender: TObject);
    procedure mnuLoadIXClick(Sender: TObject);
    procedure mnuSaveIXClick(Sender: TObject);
    procedure mnuExitClick(Sender: TObject);
    procedure SaveAnnotationToFileNXP1Click(Sender: TObject);
    procedure LoadAnnotationfromFileNXP1Click(Sender: TObject);
  
    procedure mnuAboutClick(Sender: TObject);
    procedure Notate1AnnotateEvent(ASender: TObject; lType,
      lData: Integer);
    procedure Notate1CurrentLayerChange(ASender: TObject;
      newLayer: Integer);
    procedure Inflateme1Click(Sender: TObject);



  private
    bActivated: Boolean;
    bAutoSelect: Boolean;
    dwLayer: Integer;
    dwSeqNum: Integer; // the last selected object
    { Private declarations }
  protected
     procedure CommandMessage(var Message: TWMCommand); message WM_COMMAND;

  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}


procedure TForm1.CommandMessage(var Message: TWMCommand);
Var n, i : Integer;
begin

   // check to see if the Set Current Layer menu item
   //   has 'children' -- which would mean there is a
   //   layer list attached

   n := Setcurrentlayer1.Count;

   if (n > 0) then
      begin
         for i:=0 to n-1 do
            begin
               if (Setcurrentlayer1.Items[i].Command = Message.ItemID) then
                  Notate1.SetCurrentLayer(Setcurrentlayer1.Items[i].Tag);
            end;

      end;
  inherited;

end;

procedure TForm1.OnActivate(Sender: TObject);
begin
   if (bActivated = False) then
      begin
         bActivated := True;
         Notate1.ToolbarSetToolTip  (NX_EllipseTool, 'The Ellipse-o-Matic!');
         Notate1.ToolbarSetToolTip  (NX_FreehandTool, 'Draw with this!');
         Notate1.ToolbarSetToolTip  (NX_LineTool, 'Liney, very, very Liney');
         Notate1.ToolbarSetToolTip  (NX_PointerTool, 'Point ''em out!');
         Notate1.ToolbarSetToolTip  (NX_PolygonTool, 'Poly want a cracker?');
         Notate1.ToolbarSetToolTip  (NX_RectangleTool, 'Rects r us');
         Notate1.ToolbarSetToolTip  (NX_StampTool, 'Stamp out tool tips!');
         Notate1.ToolbarSetToolTip  (NX_TextTool, 'Write me a masterpiece!');
         Notate1.ToolbarSetToolTip  (NX_PolyLineTool, 'PolyLine');
         Notate1.ToolbarSetToolTip  (NX_ImageTool, 'Buttons r us');
         Notate1.ToolbarSetToolTip  (NX_RulerTool, 'Hey, I''m the new guy in town!');
         // Set up Button Tool
         Notate1.SetToolDefaultAttribute (NX_ButtonTool, 5, 4);
         Notate1.DefaultButtonText := 'Click me';
         // Set up Image Tool
         Notate1.SetToolDefaultAttribute (NX_ImageTool, 16, 1);
         Notate1.SetToolDefaultAttribute (NX_ImageTool, 17, IXDibLoader.CopyDib);

         // connect up ImagXpress and NotateXpress
         Notate1.SetClientWindow(Xpress1.Hwnd);
         // create an annotation layer, make tool palette visible
         dwlayer := Notate1.CreateLayer;
         Notate1.ToolbarSetVisible(dwLayer, True);

         Notate1.Tool := NX_FreehandTool;

         // Set up Stamp Tool
         Notate1.SetText (dwLayer, NX_StampTool, 'This is evocative text');
         Notate1.SetTextColor (dwLayer, NX_StampTool, RGB(255, 0, 0));
      end;

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
   bActivated := False;
   bAutoSelect := True;
   Xpress1.FileName := ExtractFilePath(Application.EXEName) + '..\..\..\..\..\..\Common\Images\benefits.tif';
   // this 2nd ImagXpress is used to set the default image for the image tool
   // (see TForm1.Activate event handler)
   IXDibLoader.Filename := ExtractFilePath(Application.EXEName) + '..\..\..\..\..\..\Common\Images\vermont.jpg';

   end;

procedure TForm1.Createnewlayer1Click(Sender: TObject);
begin
   Notate1.CreateLayer;
   if Notate1.InteractMode = 0 then
   begin
   Notate1.ToolbarSetVisible(dwLayer, true);
   Notate1.Active := true;
   end
end;

procedure TForm1.Deletecurrentlayer1Click(Sender: TObject);
begin
      Notate1.DeleteLayer(dwlayer);
end;

procedure TForm1.Getlayerdescription1Click(Sender: TObject);
begin
   If (Notate1.CurrentLayer<>0) Then
    ShowMessage(Notate1.Description);

end;

procedure TForm1.OnCurrentLayerChange(Sender: TObject; newLayer: Integer);
begin
   dwLayer := newLayer;
   dwSeqNum := 0;
end;

procedure TForm1.Getlayername1Click(Sender: TObject);
begin

   If (Notate1.CurrentLayer<> 0) Then
      ShowMessage(Notate1.LayerName);

end;

procedure TForm1.Setlayerdescription1Click(Sender: TObject);
Var desc: string;
begin

   desc := InputBox('Layer Description', 'Enter description', Notate1.Description);
   Notate1.Description := desc;

end;

procedure TForm1.Setlayername1Click(Sender: TObject);
Var layerName: string;
begin

   layerName := InputBox('Layer Name', 'Enter name', Notate1.LayerName);
   Notate1.LayerName := layerName;

end;

procedure TForm1.Hidecurrentlayer1Click(Sender: TObject);
begin

   Notate1.Visible := Not Notate1.Visible;

end;

procedure TForm1.OnLayersClick(Sender: TObject);
Var i, n, layerHandle: Integer;
   desc: string;
   bEnabled: Boolean;
   menuItem: TMenuItem;
begin

   bEnabled := (Notate1.CurrentLayer <> 0);
   Deletecurrentlayer1.Enabled := bEnabled;
   Hidecurrentlayer1.Enabled := bEnabled;
   Setlayerdescription1.Enabled := bEnabled;
   Getlayerdescription1.Enabled := bEnabled;
   Setlayername1.Enabled := bEnabled;
   Getlayername1.Enabled := bEnabled;
   ToggleToolPalettevisibility1.Enabled := (bEnabled And (Notate1.ToolbarActivated = True));

   If (bEnabled) Then
      begin
         If (Notate1.Visible) Then
           Hidecurrentlayer1.Caption := 'Hide current layer'
         Else
           Hidecurrentlayer1.Caption := 'Show current layer';
      End;

   n := Setcurrentlayer1.Count;
   if (n > 0) then
      begin
         For i := 1 To n do
           Setcurrentlayer1.Delete(0);
      end;
   
   If (Notate1.NumLayers < 2) Then
      begin
         Setcurrentlayer1.Enabled := False
      end
   Else
      begin
         Setcurrentlayer1.Enabled := True;
         //---------------
         // add the description for each layer under this submenu
         //---------------
         layerHandle := Notate1.GetFirstLayer;

         While (layerHandle <> 0) do
            begin
              desc := Notate1.GetDescription(layerHandle);
              menuItem := TMenuItem.Create(Self);
              menuItem.Caption := desc;
              menuItem.Tag := layerHandle;
              Setcurrentlayer1.Insert(0,menuItem);
              layerHandle := Notate1.GetNextLayer;
            end;
      end;

end;

procedure TForm1.ToggleToolPalettevisibility1Click(Sender: TObject);
begin
   Notate1.ToolbarSetVisible(Notate1.CurrentLayer, Not Notate1.ToolbarGetVisible(Notate1.CurrentLayer));
end;

procedure TForm1.Create20objects1Click(Sender: TObject);
Var e, i, top, left, right, bottom : Integer;
begin

   top := 15;
   left := 15;
   right := 115;
   bottom := 115;
   // create a 'programmatic' element
   e := Notate1.ElementCreate;
   // set pen width attribute, then create 100 objects
   Notate1.ElementSetPenWidth (e, 1);

   For i := 1 To 20 do
      begin
        Notate1.ElementSetBoundingRect(e, top, left, right, bottom);
        Notate1.ElementSetPenColor(e, RGB(Random(256), Random(256), Random(256)));
        Notate1.ElementSetFillColor(e, RGB(Random(256), Random(256), Random(256)));
        If (i Mod 2 <> 0) Then
          Notate1.ElementSetType(e, NX_RectangleTool)
        Else
          Notate1.ElementSetType(e, 4099);

        // create object based on element template
        Notate1.ElementAppend(e);

        top := top + 2;
        left := left + 2;
        right := right + 2;
        bottom := bottom + 2;
      end;

   // destroy the 'programmatic' element
   Notate1.ElementDestroy(e);


end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
   // cleanup resources
   Notate1.Client := '';
   Notate1.SetClientWindow(0);
   Xpress1.FileName := '';
end;

procedure TForm1.CreatetranslucentRectangle1Click(Sender: TObject);
Var e: Integer;
begin

With Notate1 do
   begin
     // create a programmatic element
     e := ElementCreate;
     // make it a Rectangle template
     ElementSetType(e, NX_RectangleTool);
     // set some Rectangle properties
     ElementSetPenWidth(e, 1);
     ElementSetPenColor(e, clBlue);
     ElementSetBackStyle(e, 3);
     ElementSetFillColor(e, clYellow);
     ElementSetBoundingRect(e, 100, 100, 200, 200);
     // tell NotateXpress to create an object from this template
     ElementAppend(e);
     // destroy the programmtic element
     ElementDestroy(e);
   end;


end;

procedure TForm1.CreateanImageobject1Click(Sender: TObject);
Var e: Integer;
begin

   e := Notate1.ElementCreate;
   Notate1.ElementSetType(e, NX_ImageTool);
   Notate1.ElementSetDIBHandle(e, IXDibLoader.CopyDib);
   Notate1.ElementSetBoundingRect(e, 100, 100, 100 + IXDibLoader.IWidth, 100 + IXDibLoader.IHeight);
   Notate1.ElementAppend(e);
   Notate1.ElementDestroy(e);

end;

procedure TForm1.Nudgeup1Click(Sender: TObject);
begin
  Notate1.NudgeUp(dwLayer, dwSeqNum);
end;

procedure TForm1.Nudgedn1Click(Sender: TObject);
begin
   Notate1.NudgeDn(dwLayer, dwSeqNum);
end;

procedure TForm1.OnObjectsClick(Sender: TObject);
Var i, iCount : Integer;
   thisItem : NX_AnnotationTool;
begin

   If (Notate1.CurrentLayer = 0) Then
      begin
         //------------
         // disable all menu selections on this menu
         //------------
         For i := 1 to Objects1.Count do
            Objects1.Items[i-1].Enabled := False;
         Exit;
      end;

   iCount := Notate1.PrgGetItemCount(Notate1.CurrentLayer);
   Reversezorder1.Enabled := (iCount > 1);
   Brand1.Enabled := (iCount > 0);
   Nudgeup1.Enabled := (Notate1.IsSelection And (iCount > 1));
   Nudgedn1.Enabled := Nudgeup1.Enabled;
   CreatetranslucentRectangle1.Enabled := True;
   Inflateme1.Enabled := False;

   If (dwSeqNum <> 0) Then
      begin
         thisItem := Notate1.PrgGetItemType(dwlayer, dwSeqNum);
         If ((thisItem = NX_TextTool) or (thisItem = NX_EllipseTool) Or (thisItem = NX_RectangleTool) Or (thisItem = NX_ButtonTool)) Then
            Inflateme1.Enabled := True;
      end;

end;
procedure TForm1.CreatelargeTextobject1Click(Sender: TObject);
Var e : Integer;
   t : String;
   f : TFont;
   NewFont : IFontDisp;

begin

   f := TFont.Create();
   f.Name := 'Times New Roman';
   f.Size := 12;
   GetOleFont(f, NewFont);
   e := Notate1.ElementCreate;
   Notate1.ElementSetType(e, NX_TextTool);
   Notate1.ElementSetBoundingRect(e, 30, 30, 280, 300);
   Notate1.ElementSetBackColor(e, clYellow);
   Notate1.ElementSetTextColor(e, clBlue);
   Notate1.ElementSetBackstyle(e, 3);
   Notate1.ElementSetPenWidth(e, 3);
   Notate1.ElementSetPenColor(e, clRed);

   t := 'NotateXpress version 8' + Chr(13) + Chr(10) + Chr(13) + Chr(10);
   t := t + 'NotateXpress version 8''s programmatic annotation features let you enter, edit, and query all types of annotation objects programmatically. ';
   Notate1.ElementSetText(e, t);
   Notate1.ElementSetFont(e, NewFont);
   Notate1.ElementAppend(e);
   Notate1.ElementDestroy(e);
end;

procedure TForm1.Loadimagefile1Click(Sender: TObject);
begin
   OpenDialog1.Filter := 'Tiff files (*.tif)|*.tif|Jpeg files (*.jpg)|*.jpg';
   OpenDialog1.Filter :=  OpenDialog1.Filter;
   OpenDialog1.InitialDir := '..\..\..\..\..\..\Common\Images\';
   if OpenDialog1.Execute then

   Xpress1.Filename := OpenDialog1.FileName;
end;

procedure TForm1.Reversezorder1Click(Sender: TObject);
begin
   Notate1.Reverse(Notate1.CurrentLayer);
end;

procedure TForm1.OnFileClick(Sender: TObject);
begin
   //Saveannotationfile1.Enabled := ((Notate1.CurrentLayer <> 0) and (Notate1.PrgGetItemCount(Notate1.CurrentLayer) > 0));
end;

procedure TForm1.OnBrandClick(Sender: TObject);
begin
   // Brand to 24bits
   Notate1.Brand(24);
end;

procedure TForm1.OnStamping(Sender: TObject);
begin
   Notate1.TextColor := RGB(Random(256),Random(256),Random(256));
   Notate1.Text := 'Approved ' + DateTimeToStr(Now);
end;

procedure TForm1.OnModifyClick(Sender: TObject);
Var i, iCount : Integer;
   thisItem : NX_annotationTOOL;
begin

   //------------
   // disable all menu selections on this menu
   //------------
   For i := 1 to Modify1.Count do
      Modify1.Items[i-1].Enabled := False;

   // if no object is selected, we're done
   if (dwSeqNum = 0) then Exit;

   // otherwise, enable the appropriate item type selection
   thisItem := Notate1.PrgGetItemType(dwlayer, dwSeqNum);

   case thisItem of
      NX_RectangleTool:
         Rectangle1.Enabled := True;
      NX_EllipseTool:
         Ellipse1.Enabled := True;
      NX_StampTool:
         Stamp1.Enabled := True;
      NX_TextTool:
         Text1.Enabled := True;
      NX_LineTool:
         Line1.Enabled := True;
      NX_PolylineTool:
         Polyline1.Enabled := True;
      NX_PolygonTool:
         Polygon1.Enabled := True;
      NX_FreehandTool:
         Freehand1.Enabled := True;
   end;

end;

procedure TForm1.OnExitClick(Sender: TObject);
begin
   Application.Terminate;

end;

procedure TForm1.OnBackstyleClick(Sender: TObject);
begin

   Notate1.PrgSetItemBackstyle(Notate1.CurrentLayer, dwSeqNum, TMenuItem(Sender).Tag);

end;

procedure TForm1.PenColor(Sender: TObject);
begin
   Notate1.UserItemColorSelect(Notate1.CurrentLayer, dwSeqNum, 1, 'Select pen color', 0);
end;

procedure TForm1.OnPenWidth(Sender: TObject);
begin
   Notate1.PrgSetItemPenWidth(Notate1.CurrentLayer, dwSeqNum, TMenuItem(Sender).Tag);
end;

procedure TForm1.OnTextColor(Sender: TObject);
begin
   Notate1.UserItemColorSelect(Notate1.CurrentLayer, dwSeqNum, 2, 'Select text color', Form1.Handle);
end;

procedure TForm1.OnPenColor(Sender: TObject);
begin
   Notate1.UserItemColorSelect(Notate1.CurrentLayer, dwSeqNum, 1, 'Select pen color', Form1.Handle);
end;

procedure TForm1.OnFillColor(Sender: TObject);
begin
    Notate1.UserItemColorSelect(Notate1.CurrentLayer, dwSeqNum, 4, 'Select fill color', Form1.Handle);
end;

procedure TForm1.OnFont(Sender: TObject);
begin
   Notate1.UserItemFontSelect(Notate1.CurrentLayer, dwSeqNum, 'Select Font', Form1.Handle);
end;

procedure TForm1.Saveannotationfile1Click(Sender: TObject);
Var filename : String;
begin
   SaveDialog1.Filter := 'NotateXpress files (*.nxp)|*.nxp';
   if SaveDialog1.Execute then
      begin
         filename := SaveDialog1.FileName;
         if ((StrPos(StrLower(PChar(filename)), '.nxp')) = nil) then
            filename := filename + '.nxp';
         Notate1.StoreAnnotation(filename);
      end;
end;

procedure TForm1.OnAutoSelectClick(Sender: TObject);
begin
   bAutoSelect := not bAutoSelect;
   AutoSelect.Checked := bAutoSelect;
end;

procedure TForm1.OnUseMLE(Sender: TObject);
Var bUseMLE: Boolean;
begin

   bUseMLE := Not Notate1.MLECompatible;
   Notate1.MLECompatible := bUseMLE;
   UseMLE.Checked := bUseMLE;

end;

procedure TForm1.mnuWangClick(Sender: TObject);
begin

  //Wang Compatible
  Notate1.TIFFAnnotationType := 0;
  mnuWAng.Checked := Not  mnuWAng.Checked;
  mnuTMS.Checked := false;
  mnuNXP.Checked := false;

end;

procedure TForm1.mnuNXPClick(Sender: TObject);
begin

  //NXP Compatible
  Notate1.TIFFAnnotationType := 1;

  mnuNXP.Checked := Not  mnuNXP.Checked;
  mnuwang.Checked := false;
  mnuTMS.Checked := false;
end;

procedure TForm1.mnuTMSClick(Sender: TObject);
begin

    //TMS Compatible
  Notate1.TIFFAnnotationType := 2;
  mnuTMS.Checked := Not  mnuTMS.Checked;
  mnuwang.Checked := false;
  mnuNXP.Checked := false;
end;

procedure TForm1.mnuSaveImageClick(Sender: TObject);
begin
   SaveDialog1.Filter := 'Tiff files (*.tif)|*.tif|Jpeg files (*.jpg)|*.jpg';
   SaveDialog1.Filter :=  SaveDialog1.Filter;
   SaveDialog1.InitialDir := '..\..\..\..\..\..\Common\Images\';
   if  SaveDialog1.Execute then

  Xpress1.SaveFileName := SaveDialog1.FileName;
  Xpress1.SaveTIFFCompression := 4;
  Xpress1.SaveFile;

end;

procedure TForm1.mnuPreserveWangClick(Sender: TObject);
begin
        //preserver/not preserver Wang layerss
        Notate1.PreserveWTLayers := Not Notate1.PreserveWTLayers;
        mnuPreserveWang.Checked := Not mnuPreserveWang.Checked;
end;

procedure TForm1.mnuLoadIXClick(Sender: TObject);
begin
      //Load or Not Load Annotations with IX TIFF image
        Notate1.LoadAnnotations := Not Notate1.LoadAnnotations;
        mnuLoadIX.Checked := Not mnuLoadIX.Checked;
end;

procedure TForm1.mnuSaveIXClick(Sender: TObject);
begin
   //Save/Not Save Annotations with IX
    Notate1.SaveAnnotations := Not Notate1.SaveAnnotations;
    mnuSaveIX.Checked := Not  mnuSaveIX.Checked;
end;

procedure TForm1.mnuExitClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TForm1.SaveAnnotationToFileNXP1Click(Sender: TObject);
begin
  //Save the annotations seperately in a file
   SaveDialog1.Filter := 'NXP files (*.nxp)|*.nxp|';
   SaveDialog1.Filter :=  SaveDialog1.Filter;
    SaveDialog1.InitialDir := '..\..\..\..\..\..\Common\Images\';
   if  SaveDialog1.Execute then
       Notate1.Description := 'NotateXpress';
       Notate1.Title :=  'An annotation file';
       Notate1.StoreAnnotation(SaveDialog1.FileName);

end;

procedure TForm1.LoadAnnotationfromFileNXP1Click(Sender: TObject);
begin
     //Save the annotations seperately in a file
   OpenDialog1.Filter := 'NXP files (*.nxp)|*.nxp|';
   OpenDialog1.Filter :=   OpenDialog1.Filter;
    OpenDialog1.InitialDir := '..\..\..\..\..\..\Common\Images\';
   if  OpenDialog1.Execute then
       Notate1.RetrieveAnnotation(OpenDialog1.FileName);
end;

procedure TForm1.mnuAboutClick(Sender: TObject);
begin
  Notate1.AboutBox;
end;

procedure TForm1.Notate1AnnotateEvent(ASender: TObject; lType,
  lData: Integer);
begin
     case lType of
   //Annotation Selected
      1004:
         dwSeqNum := lData;
     // NXP_AnnotationAdded
     1001:
         if (bAutoSelect) then
            begin
               Notate1.PrgSetItemSelected(dwlayer, lData, True);
               Notate1.Tool := NX_PointerTool;
            end;
   end;
end;

procedure TForm1.Notate1CurrentLayerChange(ASender: TObject;
  newLayer: Integer);
begin

   ///track any changes here
   dwlayer := newLayer;
   dwSeqNum := 0;
end;

procedure TForm1.Inflateme1Click(Sender: TObject);
var
x : longint;
y : longint;
x2 : longint;
y2 : longint;
i : integer;
begin
      Notate1.PrgGetItemRect(dwlayer,dwSeqNum,x,y,x2,y2);
      for i := 1 to 100 do
      begin
      Notate1.PrgSetItemRect(dwlayer, dwSeqNum, x - i, y - i, x2 + i, y2 + i);
      Application.ProcessMessages;
      end
      end;

end.

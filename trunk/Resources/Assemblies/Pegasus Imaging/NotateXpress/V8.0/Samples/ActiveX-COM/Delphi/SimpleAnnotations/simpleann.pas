unit simpleann;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PegasusImagingActiveXNotateXpress8_TLB, StdCtrls, AxCtrls,ActiveX,
  OleCtrls, PegasusImagingActiveXImagXpress8_TLB, Menus;

type
  TForm1 = class(TForm)
    MyMenu: TMainMenu;
    File1: TMenuItem;
    mnuPreserveWang: TMenuItem;
    Loadimagefile1: TMenuItem;
    mnuSaveImage: TMenuItem;
    SaveAnnotationToFileNXP1: TMenuItem;
    LoadAnnotationfromFileNXP1: TMenuItem;
    mnuExit: TMenuItem;
    mnuCreateAnn: TMenuItem;
    mnuTextAnn: TMenuItem;
    mnuRedactionAnn: TMenuItem;
    mnuRulerAnn: TMenuItem;
    mnuRectanglesAnn: TMenuItem;
    mnuAbout: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    ImagXpress1: TImagXpress;
    ListBox1: TListBox;
    NotateXpress1: TNotateXpress;
    mnuToggleToolPalette: TMenuItem;
   // procedure OnActivate(Sender: TObject);
    procedure mnuPreserveWangClick(Sender: TObject);
    procedure Loadimagefile1Click(Sender: TObject);
    procedure mnuSaveImageClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure mnuRedactionAnnClick(Sender: TObject);
    procedure mnuTextAnnClick(Sender: TObject);
    procedure mnuAboutClick(Sender: TObject);
    procedure mnuToggleToolPaletteClick(Sender: TObject);
    procedure mnuRulerAnnClick(Sender: TObject);
    procedure mnuRectanglesAnnClick(Sender: TObject);
    procedure SaveAnnotationToFileNXP1Click(Sender: TObject);
    procedure LoadAnnotationfromFileNXP1Click(Sender: TObject);
    procedure mnuExitClick(Sender: TObject);
    
  private
    { Private declarations }
    dwLayer: Integer;


  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.mnuPreserveWangClick(Sender: TObject);
begin
//preserver/not preserver Wang layerss
        NotateXpress1.PreserveWTLayers := Not NotateXpress1.PreserveWTLayers;
        mnuPreserveWang.Checked := Not mnuPreserveWang.Checked;
end;

procedure TForm1.Loadimagefile1Click(Sender: TObject);
begin
 OpenDialog1.Filter := 'Tiff files (*.tif)|*.tif|Jpeg files (*.jpg)|*.jpg';
   OpenDialog1.Filter :=  OpenDialog1.Filter;
   OpenDialog1.InitialDir := '..\..\..\..\..\..\Common\Images\';
   if OpenDialog1.Execute then

      NotateXpress1.Clear();
      dwLayer := NotateXpress1.CreateLayer;
      ImagXpress1.Filename := OpenDialog1.FileName;

      NotateXpress1.ToolbarActivated := true;
   
end;

procedure TForm1.mnuSaveImageClick(Sender: TObject);

 begin
   
   SaveDialog1.Filter := 'Tiff files (*.tif)|*.tif|Jpeg files (*.jpg)|*.jpg';
   SaveDialog1.DefaultExt := 'tif';
   SaveDialog1.InitialDir := '..\..\..\..\..\..\Common\Images\';


   if  SaveDialog1.Execute then
    begin
     
      ImagXpress1.SaveFileName :=  SaveDialog1.FileName;
      ImagXpress1.SaveTIFFCompression := 4;
      ImagXpress1.SaveFile();


    end;
end;

procedure TForm1.FormActivate(Sender: TObject);
begin

   NotateXpress1.PreserveWTLayers := mnuPreserveWang.Checked;
    NotateXpress1.TIFFAnnotationType := NX_I4W_Compatible;
   NotateXpress1.SetClientWindow(ImagXpress1.hWnd);

   dwLayer := NotateXpress1.CreateLayer;
   NotateXpress1.ToolbarActivated := true;


end;

procedure TForm1.FormCreate(Sender: TObject);
begin

 //use your ImagXpress unlock codes here 
 ImagXpress1.UnlockRuntime(1234,1234,1234,1234);

//Set starting defaults.

 ImagXpress1.ScrollBars:=SB_Both;
 
  ImagXpress1.FileName := ExtractFilePath(Application.EXEName) + '..\..\..\..\..\..\Common\Images\benefits.tif';
 
end;

procedure TForm1.mnuRedactionAnnClick(Sender: TObject);
Var e : Integer;
   
begin
//this code demonstrates how to perform redaction on the image which is often used in _

    e := NotateXpress1.ElementCreate;
   NotateXpress1.ElementSetType(e, NX_TextTool);
   NotateXpress1.ElementSetText(e,'Approved by Pegasus Imaging Corp.  ' + DateToStr(Date));
   NotateXpress1.ElementSetBoundingRect(e, 100, 100, 300, 200);
   NotateXpress1.ElementSetBackColor(e, clWhite);
   NotateXpress1.ElementSetTextColor(e, clRed);
   NotateXpress1.ElementSetBackstyle(e, 2);
   NotateXpress1.ElementSetPenWidth(e, 3);
   NotateXpress1.ElementSetPenColor(e, clWhite);
   NotateXpress1.ElementSetTextJustification(e, NX_Justify_Center);
   NotateXpress1.ElementSetFillColor(e,clWhite);

   NotateXpress1.ElementAppend(e);
   NotateXpress1.ElementDestroy(e);
   NotateXpress1.Tool := NX_PointerTool;

end;

procedure TForm1.mnuTextAnnClick(Sender: TObject);
Var e : Integer;
   t : String;
   f : TFont;
   NewFont : IFontDisp;

begin

   f := TFont.Create();
   f.Name := 'Times New Roman';
   f.Size := 12;
   GetOleFont(f, NewFont);
   e := NotateXpress1.ElementCreate;
   NotateXpress1.ElementSetType(e, NX_TextTool);
   NotateXpress1.ElementSetBoundingRect(e, 30, 30, 280, 300);
   NotateXpress1.ElementSetBackColor(e, clYellow);
   NotateXpress1.ElementSetTextColor(e, clBlue);
   NotateXpress1.ElementSetBackstyle(e, 3);
   NotateXpress1.ElementSetPenWidth(e, 3);
   NotateXpress1.ElementSetPenColor(e, clRed);

   t := 'NotateXpress version 8''s programmatic annotation features let you enter, edit, and query all types of annotation objects programmatically. ';
   t := t + 'Using NotateXpress''s programmatic capabilities is fun, fascinating, and has a New Ruler Tool! Try it today. ';
   NotateXpress1.ElementSetText(e, t);
   NotateXpress1.ElementSetFont(e, NewFont);
   NotateXpress1.ElementAppend(e);
   NotateXpress1.ElementDestroy(e);

end;

procedure TForm1.mnuAboutClick(Sender: TObject);
begin
 NotateXpress1.AboutBox;
end;



procedure TForm1.mnuToggleToolPaletteClick(Sender: TObject);
begin
NotateXpress1.ToolbarSetVisible(NotateXpress1.CurrentLayer, Not mnuToggleToolPalette.Checked);
mnuToggleToolPalette.Checked := Not mnuToggleToolPalette.Checked;

end;

procedure TForm1.mnuRulerAnnClick(Sender: TObject);
Var e : Integer;

begin
    e := NotateXpress1.ElementCreate;
    NotateXpress1.ElementSetPenWidth(e, 4);
    //set the annotation type to the Ruler annotation
    NotateXpress1.ElementSetType(e, NX_RulerTool);
    NotateXpress1.ElementSetBoundingRect (e, 25, 50, 200, 100);
    //set the size of the gauge
    NotateXpress1.ElementSetGaugeLength(e, 20);
    //set the unit of measurement being used for the ruler
    NotateXpress1.ElementSetMeasure(e, NX_Measure_Pixels);
    //set the precision being used for the ruler
    NotateXpress1.ElementSetPrecision(e, 1);
    //set the color for the units of measurement
    NotateXpress1.ElementSetTextColor(e, ClBlue);
    //set the annotation to selected
    NotateXpress1.ElementSetSelected(e, True);
    //append the ruler to image
    NotateXpress1.ElementAppend(e);

end;

procedure TForm1.mnuRectanglesAnnClick(Sender: TObject);
Var e, i, top, left, right, bottom : Integer;
begin

   top := 20;
   left := 20;
   right := 120;
   bottom := 120;
   // create a 'programmatic' element
   e := NotateXpress1.ElementCreate;
   // set pen width attribute, then create 10 Rectangles
   NotateXpress1.ElementSetPenWidth (e, 1);

   For i := 1 To 10 do
      begin
        NotateXpress1.ElementSetBoundingRect(e, top, left, right, bottom);
        NotateXpress1.ElementSetPenColor(e, RGB(Random(256), Random(256), Random(256)));
        NotateXpress1.ElementSetFillColor(e, RGB(Random(256), Random(256), Random(256)));
        If (i Mod 2 <> 0) Then
          NotateXpress1.ElementSetType(e, NX_RectangleTool)
        Else
          NotateXpress1.ElementSetType(e, 4099);

        // create object based on element template
        NotateXpress1.ElementAppend(e);

        top := top + 2;
        left := left + 2;
        right := right + 2;
        bottom := bottom + 2;
      end;

   // destroy the 'programmatic' element
   NotateXpress1.ElementDestroy(e);

end;

procedure TForm1.SaveAnnotationToFileNXP1Click(Sender: TObject);
Var filename : String;
begin
   SaveDialog1.Filter := 'NotateXpress files (*.nxp)|*.nxp';
   if SaveDialog1.Execute then
      begin
         filename := SaveDialog1.FileName;
         if ((StrPos(StrLower(PChar(filename)), '.nxp')) = nil) then
            filename := filename + '.nxp';
         NotateXpress1.StoreAnnotation(filename);
      end;
end;

procedure TForm1.LoadAnnotationfromFileNXP1Click(Sender: TObject);
begin
 //Save the annotations seperately in a file
   OpenDialog1.Filter := 'NXP files (*.nxp)|*.nxp|';
   OpenDialog1.Filter :=   OpenDialog1.Filter;
    OpenDialog1.InitialDir := '..\..\..\..\..\..\Common\Images\';
   if  OpenDialog1.Execute then
       NotateXpress1.RetrieveAnnotation(OpenDialog1.FileName);
       NotateXpress1.ToolbarSetVisible(NotateXpress1.CurrentLayer,mnuToggleToolPalette.Checked);

end;

procedure TForm1.mnuExitClick(Sender: TObject);
begin
Application.Terminate;
end;

end.

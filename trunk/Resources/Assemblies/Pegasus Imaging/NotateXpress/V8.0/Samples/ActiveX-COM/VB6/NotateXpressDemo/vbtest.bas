Attribute VB_Name = "vbtestMod"
Option Explicit


Private Declare Function CallWindowProc Lib "user32" _
   Alias "CallWindowProcA" (ByVal lpPrevWndFunc As Long, _
   ByVal hwnd As Long, ByVal Msg As Long, _
   ByVal wParam As Long, ByVal lParam As Long) As Long

Global gLoadAnnName As String

'--------------
' subclassing variable
'--------------
Global lpPrevWndProc As Long


Global hStuffMenu As Long

Public Const WM_COMMAND = &H111
Public Const WM_MENUSELECT = &H11F

Public Const GWL_WNDPROC = -4

Function WindowProc(ByVal hw As Long, ByVal uMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
Dim i As Integer

    'Check for a menu selection message.
    
    If (uMsg = WM_MENUSELECT) Then
        If (lParam = hStuffMenu) Then
            Dim n As Long
            n = wParam And &HFFFF
            frmDemo.Output.Caption = "Selected item " + CStr(n)
        
        End If
    End If
    
    
    
    
    If uMsg = WM_COMMAND Then
        If ((wParam And &HFFFF0000) = 0) Then
            '------------
            ' it's from a menu
            '------------
            Dim whichLayer As Long, layerHandle As Long
            
            '---------------
            ' when we set up the submenu under layers, we set the menu id to be
            '    1000 + the nth layer (so the first layer would be 1000, etc)
            '---------------
            ' so here we're just subtracting off the 1000 to get which nth layer
            '    it is. The 1000 is arbitary, just to make sure we don't get a
            '    collision with another WM_COMMAND menu id
            '---------------
            whichLayer = (wParam And &HFFFF) - 1000
            
            If (whichLayer >= 0) Then
               '------------
               ' from the Layers menu
               '------------
               layerHandle = frmDemo.NotateXpress1.GetFirstLayer
               If (whichLayer = 0) Then
                    frmDemo.NotateXpress1.SetCurrentLayer layerHandle
               Else
                  For i = 0 To whichLayer - 1
                    layerHandle = frmDemo.NotateXpress1.GetNextLayer
                  Next
                  frmDemo.NotateXpress1.SetCurrentLayer layerHandle
               End If
            End If
        End If
    End If

    'Pass the message on to Windows for processing.
    WindowProc = CallWindowProc(lpPrevWndProc, hw, uMsg, wParam, lParam)
    
End Function

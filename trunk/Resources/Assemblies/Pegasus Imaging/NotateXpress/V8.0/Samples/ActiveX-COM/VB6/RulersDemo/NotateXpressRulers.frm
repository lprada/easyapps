VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{A711DB49-6663-44B3-8804-487B8EA09BE1}#1.0#0"; "PegasusImaging.ActiveX.NotateXpress8.dll"
Begin VB.Form NotateXpressRulers 
   Caption         =   "NotateXpress 8 Rulers Demo"
   ClientHeight    =   8700
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   14340
   LinkTopic       =   "Form1"
   ScaleHeight     =   8700
   ScaleWidth      =   14340
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   1815
      Left            =   11160
      TabIndex        =   21
      Top             =   1680
      Width           =   3135
   End
   Begin VB.ListBox List2 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   645
      ItemData        =   "NotateXpressRulers.frx":0000
      Left            =   240
      List            =   "NotateXpressRulers.frx":000A
      TabIndex        =   20
      Top             =   120
      Width           =   13935
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress Xpress 
      Height          =   7215
      Left            =   240
      TabIndex        =   19
      Top             =   1080
      Width           =   7695
      _ExtentX        =   13573
      _ExtentY        =   12726
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1418347871
      ErrInfo         =   388088647
      Persistence     =   -1  'True
      _cx             =   13573
      _cy             =   12726
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.CommandButton cmdClearList 
      Caption         =   "Clear List"
      Height          =   495
      Left            =   13080
      TabIndex        =   16
      Top             =   6360
      Width           =   1095
   End
   Begin VB.ListBox lstRulerLengths 
      Height          =   1035
      Left            =   11280
      TabIndex        =   15
      Top             =   7200
      Width           =   2895
   End
   Begin VB.CommandButton cmdGetRulerLen 
      Caption         =   "Get Ruler Lengths"
      Height          =   495
      Left            =   11400
      TabIndex        =   14
      Top             =   6360
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Caption         =   "Ruler Settings"
      Height          =   7335
      Left            =   8040
      TabIndex        =   0
      Top             =   960
      Width           =   3015
      Begin VB.OptionButton optMiles 
         Caption         =   "Miles"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   3720
         Width           =   1935
      End
      Begin VB.OptionButton optMicroMet 
         Caption         =   "MicroMeters"
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   1440
         Width           =   1335
      End
      Begin VB.CheckBox chkShowHideGauge 
         Caption         =   "Show/Hide Ruler's Gauge"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   13
         Top             =   5520
         Value           =   1  'Checked
         Width           =   2175
      End
      Begin VB.CheckBox chkShowAbbrev 
         Caption         =   "Show Abbreviations"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   4920
         Width           =   2175
      End
      Begin MSComctlLib.Slider sldUnitsOfPrec 
         Height          =   375
         Left            =   120
         TabIndex        =   10
         Top             =   6600
         Width           =   2655
         _ExtentX        =   4683
         _ExtentY        =   661
         _Version        =   393216
         Min             =   1
         SelStart        =   1
         Value           =   1
      End
      Begin VB.CheckBox chkShowHideUnits 
         Caption         =   "Show/Hide Unit Of Measurement"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   240
         TabIndex        =   9
         Top             =   4200
         Value           =   1  'Checked
         Width           =   2535
      End
      Begin VB.OptionButton optTwips 
         Caption         =   "Twips"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   3360
         Width           =   1335
      End
      Begin VB.OptionButton optPixels 
         Caption         =   "Pixels"
         Height          =   375
         Left            =   240
         TabIndex        =   7
         Top             =   2880
         Width           =   1335
      End
      Begin VB.OptionButton optMM 
         Caption         =   "Millimeters"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   2640
         Width           =   1335
      End
      Begin VB.OptionButton optMeters 
         Caption         =   "Meters"
         Height          =   375
         Left            =   240
         TabIndex        =   5
         Top             =   2160
         Width           =   1335
      End
      Begin VB.OptionButton optKM 
         Caption         =   "Kilometers"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1800
         Width           =   1335
      End
      Begin VB.OptionButton optInches 
         Caption         =   "Inches"
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   960
         Width           =   1335
      End
      Begin VB.OptionButton optFeet 
         Caption         =   "Feet"
         Height          =   375
         Left            =   240
         TabIndex        =   2
         Top             =   600
         Width           =   1335
      End
      Begin VB.OptionButton optCent 
         Caption         =   "Centimeters"
         Height          =   375
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   1335
      End
      Begin VB.Line Line1 
         X1              =   0
         X2              =   3000
         Y1              =   4080
         Y2              =   4080
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Set Ruler's Units of Precision"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   6120
         Width           =   2655
      End
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   11160
      TabIndex        =   24
      Top             =   1080
      Width           =   1815
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   495
      Left            =   11160
      TabIndex        =   23
      Top             =   3600
      Width           =   2415
   End
   Begin VB.Label lblerror 
      Height          =   1935
      Left            =   11160
      TabIndex        =   22
      Top             =   4200
      Width           =   3015
   End
   Begin PegasusImagingActiveXNotateXpress8Ctl.NotateXpress NX 
      Left            =   9480
      Top             =   8040
      _cx             =   847
      _cy             =   847
      Client          =   ""
      DefaultPenColor =   16711680
      DefaultFillColor=   12632256
      DefaultPenWidth =   1
      DefaultTextColor=   0
      DefaultTool     =   4103
      DefaultBackColor=   16777215
      BeginProperty DefaultTextFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DefaultStampFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Subject         =   ""
      Author          =   ""
      Comments        =   ""
      Title           =   ""
      DefaultHighlightFill=   0   'False
      DefaultHighlightBack=   0   'False
      InteractMode    =   0
      SaveAnnotations =   -1  'True
      DefaultStampText=   ""
      BeginProperty DefaultButtonFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultButtonText=   ""
      PreserveWTLayers=   0   'False
      TIFFAnnotationType=   0
      AllowPaint      =   -1  'True
      UnicodeMode     =   0   'False
      NXPEditors      =   -1  'True
      LoadAnnotations =   -1  'True
      MLECompatible   =   0   'False
      FontScaling     =   0
      TextToolArrows  =   -1  'True
      RecalibrateXDPI =   -1
      RecalibrateYDPI =   -1
      BeginProperty DefaultRulerFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RaiseExceptions =   -1  'True
      ErrorLevel      =   0
      Debug           =   0   'False
      DebugLogFile    =   "c:\NotateXpress8.log"
      ToolbarActivated=   -1  'True
      ToolbarLargeIcons=   -1  'True
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Index           =   1
      Begin VB.Menu mnuOpen 
         Caption         =   "Load Image..."
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu munRuler 
      Caption         =   "Rulers"
      Index           =   1
      Begin VB.Menu mnuCreate 
         Caption         =   "Create Ruler"
      End
      Begin VB.Menu mnuDelete 
         Caption         =   "Delete All Rulers"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "About"
   End
End
Attribute VB_Name = "NotateXpressRulers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit

Dim imgFileName As String
Dim imgParentDir As String
Dim dwlayer As Long
Dim Counter As Integer
Dim iNumRulers As Long

Private Sub cmdGetRulerLen_Click()
    iNumRulers = 0
    lstRulerLengths.Clear
    For Counter = 1 To NX.PrgGetItemCount(dwlayer)
    
        If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
            iNumRulers = iNumRulers + 1
            lstRulerLengths.AddItem "Ruler#" & iNumRulers & Space(5) & NX.PrgGetRulerLength(dwlayer, Counter)
        End If
    Next
End Sub

Private Sub chkShowHideUnits_Click()
            
    For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
        If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
            NX.PrgSetShowLength NX.CurrentLayer, Counter, chkShowHideUnits.Value
        End If
    Next
    
End Sub

Private Sub chkShowAbbrev_Click()

     For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
         If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
              NX.PrgSetShowAbbr NX.CurrentLayer, Counter, chkShowAbbrev.Value
         End If
     Next
End Sub

Private Sub chkShowHideGauge_Click()
   
    For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
        If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
            NX.PrgSetShowGauge NX.CurrentLayer, Counter, chkShowHideGauge.Value
        End If
    Next
End Sub


Private Sub cmdClearList_Click()
    lstRulerLengths.Clear
End Sub

Private Sub LoadFile()
        
    NX.Clear
           
    Xpress.FileName = imgFileName
    dwlayer = NX.CreateLayer
        
    Err = Xpress.ImagError
    PegasusError Err, lblerror
    
    NX.ToolbarActivated = True
   
End Sub

Private Sub Form_Load()
      
    Xpress.EventSetEnabled EVENT_PROGRESS, True
    Xpress.ScrollBars = SB_Both
    
    
    imgParentDir = App.Path
    imgFileName = imgParentDir & "\..\..\..\..\..\..\Common\Images\benefits.tif"
   
    NX.Client = "Xpress"
    
    LoadFile
    NX.ToolbarActivated = True
End Sub



Private Sub Form_Unload(Cancel As Integer)
 ' *Always* do this to force NotateXpress resource cleanup.
    NX.Client = ""
End Sub

Private Sub Xpress_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub
Private Sub mnuAbout_Click()
    NX.AboutBox
End Sub

Private Sub mnuCreate_Click()
   
    
    Dim annotation As Long
    annotation = NX.ElementCreate
    NX.ElementSetPenWidth annotation, 4
    'set the annotation type to the Ruler annotation
    NX.ElementSetType annotation, NX_RulerTool
    NX.ElementSetBoundingRect annotation, 25, 50, 100, 100
    'set the size of the gauge
    NX.ElementSetGaugeLength annotation, 20
    'set the unit of measurement being used for the ruler
    NX.ElementSetMeasure annotation, NX_Measure_Inches
    optInches.Value = True
    
    'set the precision being used for the ruler
    NX.ElementSetPrecision annotation, 1
    'set the color for the units of measurement
    NX.ElementSetTextColor annotation, vbBlue
    'set the annotation to selected
    NX.ElementSetSelected annotation, True
    'append the ruler to image
    NX.ElementAppend annotation
End Sub

Private Sub mnuDelete_Click()
    NX.DeleteLayer NX.CurrentLayer
    NX.CreateLayer
End Sub

Private Sub mnuExit_Click()
    End
End Sub

Private Sub mnuOpen_Click()
Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
               
        LoadFile
    End If
End Sub

Private Sub optCent_Click()

    If optCent.Value = True Then
        For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
            If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
                NX.PrgSetMeasure NX.CurrentLayer, Counter, NX_Measure_Centimeters
            End If
        Next
    End If
End Sub

Private Sub optMiles_Click()
     
     If optMiles.Value = True Then
        For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
            If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
                NX.PrgSetMeasure NX.CurrentLayer, Counter, NX_Measure_Miles
            End If
        Next
    End If
End Sub

Private Sub optFeet_Click()
    
    If optFeet.Value = True Then
        For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
             If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
                NX.PrgSetMeasure NX.CurrentLayer, Counter, NX_Measure_Feet
             End If
        Next
    End If
End Sub

Private Sub optInches_Click()
   
    If optInches.Value = True Then
        For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
            If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
                NX.PrgSetMeasure NX.CurrentLayer, Counter, NX_Measure_Inches
            End If
        Next
    End If
End Sub

Private Sub optKM_Click()
    
    If optKM.Value = True Then
        For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
            If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
                NX.PrgSetMeasure NX.CurrentLayer, Counter, NX_Measure_Kilometers
            End If
        Next
    End If
End Sub

Private Sub optMeters_Click()
    
    If optMeters.Value = True Then
        For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
            If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
                NX.PrgSetMeasure NX.CurrentLayer, Counter, NX_Measure_Meters
            End If
        Next
    End If
End Sub

Private Sub optMM_Click()
    
    If optMM.Value = True Then
        For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
             If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
                NX.PrgSetMeasure NX.CurrentLayer, Counter, NX_Measure_Millimeters
             End If
        Next
    End If
End Sub

Private Sub optPixels_Click()
     
     If optPixels.Value = True Then
        For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
             If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
                NX.PrgSetMeasure NX.CurrentLayer, Counter, NX_Measure_Pixels
             End If
        Next
    End If
End Sub

Private Sub optTwips_Click()
   
    If optTwips.Value = True Then
        For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
             If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
                NX.PrgSetMeasure NX.CurrentLayer, Counter, NX_Measure_Twips
             End If
        Next
    End If
End Sub


Private Sub optMicroMet_Click()
   
    If optMicroMet.Value = True Then
        For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
             If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
                NX.PrgSetMeasure NX.CurrentLayer, Counter, NX_Measure_MicroMeters
             End If
        Next
    End If
End Sub

Private Sub sldUnitsOfPrec_Change()
   
    For Counter = 1 To NX.PrgGetItemCount(NX.CurrentLayer)
         If NX.PrgGetItemType(dwlayer, Counter) = NX_RulerTool Then
                NX.PrgSetPrecision NX.CurrentLayer, Counter, sldUnitsOfPrec.Value
         End If
    Next
End Sub

Private Sub NX_AnnotateEvent(ByVal lType As Long, ByVal lData As Long)
        
    If lType = 1001 Then
        NX.PrgSetItemSelected NX.CurrentLayer, lData, True
        NX.Tool = NX_PointerTool
    End If
End Sub


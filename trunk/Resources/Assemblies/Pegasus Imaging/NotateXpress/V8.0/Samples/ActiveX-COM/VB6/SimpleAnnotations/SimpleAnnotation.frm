VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{A711DB49-6663-44B3-8804-487B8EA09BE1}#1.0#0"; "PegasusImaging.ActiveX.NotateXpress8.dll"
Begin VB.Form SimpleAnnotation 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "NotateXpress Simple Annotation Sample"
   ClientHeight    =   6780
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   13080
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6780
   ScaleWidth      =   13080
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstStatus 
      Height          =   1815
      Left            =   9840
      TabIndex        =   3
      Top             =   1800
      Width           =   3135
   End
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "SimpleAnnotation.frx":0000
      Left            =   120
      List            =   "SimpleAnnotation.frx":000A
      TabIndex        =   1
      Top             =   120
      Width           =   12855
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress Xpress 
      Height          =   5175
      Left            =   120
      TabIndex        =   0
      Top             =   1200
      Width           =   9615
      _ExtentX        =   16960
      _ExtentY        =   9128
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1981577526
      ErrInfo         =   -103366663
      Persistence     =   -1  'True
      _cx             =   16960
      _cy             =   9128
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   3
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label lblerror 
      Height          =   1935
      Left            =   9840
      TabIndex        =   6
      Top             =   4320
      Width           =   3015
   End
   Begin VB.Label Label1 
      Height          =   1935
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   2775
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   495
      Left            =   9840
      TabIndex        =   4
      Top             =   3720
      Width           =   2415
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   9840
      TabIndex        =   2
      Top             =   1200
      Width           =   1815
   End
   Begin PegasusImagingActiveXNotateXpress8Ctl.NotateXpress NX 
      Left            =   12360
      Top             =   1200
      _cx             =   847
      _cy             =   847
      Client          =   "<No Client>"
      DefaultPenColor =   16711680
      DefaultFillColor=   12632256
      DefaultPenWidth =   1
      DefaultTextColor=   0
      DefaultTool     =   4103
      DefaultBackColor=   16777215
      BeginProperty DefaultTextFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DefaultStampFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Subject         =   ""
      Author          =   ""
      Comments        =   ""
      Title           =   ""
      DefaultHighlightFill=   0   'False
      DefaultHighlightBack=   0   'False
      InteractMode    =   0
      SaveAnnotations =   -1  'True
      DefaultStampText=   ""
      BeginProperty DefaultButtonFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultButtonText=   ""
      PreserveWTLayers=   0   'False
      TIFFAnnotationType=   0
      AllowPaint      =   -1  'True
      UnicodeMode     =   0   'False
      NXPEditors      =   -1  'True
      LoadAnnotations =   -1  'True
      MLECompatible   =   0   'False
      FontScaling     =   0
      TextToolArrows  =   -1  'True
      RecalibrateXDPI =   -1
      RecalibrateYDPI =   -1
      BeginProperty DefaultRulerFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RaiseExceptions =   -1  'True
      ErrorLevel      =   0
      Debug           =   0   'False
      DebugLogFile    =   "c:\NotateXpress8.log"
      ToolbarActivated=   -1  'True
      ToolbarLargeIcons=   -1  'True
   End
   Begin VB.Menu mnuFile 
      Caption         =   "File"
      Begin VB.Menu mnuLoad 
         Caption         =   "Load Image"
      End
      Begin VB.Menu mnuSave 
         Caption         =   "Save Image"
      End
      Begin VB.Menu mnuWang 
         Caption         =   "Wang Compatibility"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuSaveAnno 
         Caption         =   "Save Annotation(NXP)"
      End
      Begin VB.Menu mnuLoadAnno 
         Caption         =   "LoadAnnotation(NXP)"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuAnnotation 
      Caption         =   "Create Annotations"
      Begin VB.Menu mnuText 
         Caption         =   "Add Text Annotation"
      End
      Begin VB.Menu mnuRedaction 
         Caption         =   "Add Redaction Annotation"
      End
      Begin VB.Menu mnuRuler 
         Caption         =   "Add Ruler Annotation"
      End
      Begin VB.Menu mnuRect 
         Caption         =   "Add Rectangles"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "About"
   End
End
Attribute VB_Name = "SimpleAnnotation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Dim dwlayer As Long
Dim imgFileName As String
Dim imgParentDir As String
Private Sub Form_Load()
    
     Xpress.EventSetEnabled EVENT_PROGRESS, True
     Xpress.ScrollBars = SB_Both
     
     imgParentDir = App.Path
     imgFileName = imgParentDir & "\..\..\..\..\..\..\Common\Images\benefits.tif"
       
     
     
     NX.Client = "Xpress"
     LoadFile
     
     NX.ToolbarActivated = True
            
End Sub

Private Sub mnuAbout_Click()
    NX.AboutBox
End Sub

Private Sub mnuExit_Click()
    End
End Sub
Private Sub LoadFile()
        
    NX.Clear
            
    Xpress.FileName = imgFileName
    dwlayer = NX.CreateLayer
        
    Err = Xpress.ImagError
    PegasusError Err, lblerror
    
    NX.ToolbarActivated = True
   
End Sub
Private Sub Xpress_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub
Private Sub mnuLoad_Click()
    Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
               
        LoadFile
    End If
End Sub

Private Sub mnuLoadAnno_Click()
    Dim annFile As String
    
    annFile = PegasusOpenFilePF(imgParentDir, "NotateXpress File (*.nxp)" & vbNullChar & "*.nxp" & vbNullChar & vbNullChar)
    If Len(annFile) <> 0 Then
       
          NX.RetrieveAnnotation annFile
          NX.ToolbarActivated = True
      
    End If
End Sub


Private Sub mnuRect_Click()
    Dim e As Long
    Dim i As Integer
    Dim top, left, right, bottom As Integer
    Dim x, y As Integer
    
    top = 20
    left = 20
    right = 120
    bottom = 120
    
    e = NX.ElementCreate
    NX.ElementSetType e, NX_RectangleTool
        
    NX.ElementSetPenWidth e, 1
    For i = 1 To 10
      NX.ElementSetBoundingRect e, left, top, right, bottom
      NX.ElementSetPenColor e, RGB(Int(255 * Rnd), Int(255 * Rnd), Int(255 * Rnd))
      NX.ElementSetFillColor e, RGB(Int(255 * Rnd), Int(255 * Rnd), Int(255 * Rnd))
          
      x = Int(400 * Rnd)
      y = Int(400 * Rnd)
        
      NX.ElementAppend e
      top = top + 2
      left = left + 2
      right = right + 2
      bottom = bottom + 2 '
    Next
    
    NX.ElementDestroy e
End Sub

Private Sub mnuRedaction_Click()
    'this code demonstrates how to perform redaction on the image which is often used in _
    legal field
     Dim e As Long
    e = NX.ElementCreate
    NX.ElementSetType e, NX_TextTool
    NX.ElementSetText e, "Approved by Pegasus Imaging Corp." & Space(2) & Now
    
    NX.ElementSetTextJustification e, NX_Justify_Center
    NX.ElementSetTextColor e, vbRed
    NX.ElementSetPenWidth e, 1
    NX.ElementSetPenColor e, vbWhite
    NX.ElementSetBackstyle e, NX_Opaque
    NX.ElementSetFillColor e, vbWhite
    NX.ElementSetBoundingRect e, 100, 100, 300, 200
    NX.ElementAppend e
    NX.ElementDestroy e
    NX.Tool = NX_PointerTool
End Sub

Private Sub mnuRuler_Click()
    Dim e As Long
    e = NX.ElementCreate
    NX.ElementSetPenWidth e, 4
    'set the annotation type to the Ruler annotation
    NX.ElementSetType e, NX_RulerTool
    NX.ElementSetBoundingRect e, 25, 50, 200, 100
    'set the size of the gauge
    NX.ElementSetGaugeLength e, 20
    'set the unit of measurement being used for the ruler
    NX.ElementSetMeasure e, NX_Measure_Pixels
    'set the precision being used for the ruler
    NX.ElementSetPrecision e, 1
    'set the color for the units of measurement
    NX.ElementSetTextColor e, vbBlue
    'set the annotation to selected
    NX.ElementSetSelected e, True
    'append the ruler to image
    NX.ElementAppend e
End Sub

Private Sub mnuSave_Click()

    Dim sTiffFile As String
    sTiffFile = PegasusSaveFilePF(imgParentDir, "Tiff File (*.tif)" & vbNullChar & "*.tif" & vbNullChar & vbNullChar, "tif")
    If Len(sTiffFile) Then
       Xpress.SaveFileName = sTiffFile
       Xpress.SaveFile
       NX.Clear
       
       dwlayer = NX.CreateLayer
        
       Err = Xpress.ImagError
       PegasusError Err, lblerror
    
       NX.ToolbarActivated = True
    End If
End Sub

Private Sub mnuSaveAnno_Click()
    Dim annFile As String
    
    annFile = PegasusSaveFilePF(imgParentDir, "NotateXpress File (*.nxp)" & vbNullChar & "*.nxp" & vbNullChar & vbNullChar, "ann")
    If Len(annFile) Then
       NX.Comments = Now
       NX.Description = Now
       NX.Title = "One awesome annotation file"
       NX.StoreAnnotation annFile
    End If
End Sub

Private Sub mnuText_Click()
    Dim e As Long
    Dim t As String
    Dim s As StdFont
    
    Set s = New StdFont
    s.Bold = False
    s.Name = "Times New Roman"
    s.Size = 12
    
    e = NX.ElementCreate
    NX.ElementSetTextJustification e, NX_Justify_Center
    NX.ElementSetType e, NX_TextTool
    NX.ElementSetBoundingRect e, 30, 30, 280, 300
    NX.ElementSetBackColor e, RGB(255, 0, 0)
    NX.ElementSetTextColor e, RGB(0, 0, 0)
    NX.ElementSetBackstyle e, NX_Translucent
    NX.ElementSetPenWidth e, 2
    NX.ElementSetPenColor e, RGB(0, 0, 255)
    
   t = "NotateXpress version 8" + Chr(13) + Chr(10) + Chr(13) + Chr(10)
t = t + "NotateXpress version 8's programmatic annotation features let you enter, edit, and query all types of annotation objects programmatically. "
t = t + "Using NotateXpress's programmatic capabilities is fun and fascinating! "
t = t + "New features in NotateXpress version 8 include ANN Annotation File Format Support and Medical Annotation Data Locking. Try it today."
    
    NX.ElementSetText e, t
    NX.ElementSetFont e, s
    NX.ElementAppend e
    NX.ElementDestroy e

End Sub

Private Sub mnuWang_Click()
   
    If (NX.TIFFAnnotationType = NX_I4W_Compatible) Then
        NX.TIFFAnnotationType = NX_NXP_Compatible
        mnuWang.Checked = False
    Else
        NX.TIFFAnnotationType = NX_I4W_Compatible
        mnuWang.Checked = True
    End If

End Sub

Private Sub NX_AnnotateEvent(ByVal lType As Long, ByVal lData As Long)
        
    If lType = 1001 Then
        NX.PrgSetItemSelected NX.CurrentLayer, lData, True
        NX.Tool = NX_PointerTool
    End If
End Sub

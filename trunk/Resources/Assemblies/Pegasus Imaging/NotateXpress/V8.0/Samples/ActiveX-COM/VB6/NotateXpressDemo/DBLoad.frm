VERSION 5.00
Begin VB.Form DBLoad 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Load from Database list"
   ClientHeight    =   4305
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4830
   ControlBox      =   0   'False
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4305
   ScaleWidth      =   4830
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Cancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   495
      Left            =   3480
      TabIndex        =   3
      Top             =   2160
      Width           =   1095
   End
   Begin VB.CommandButton Load 
      Caption         =   "Load"
      Height          =   495
      Left            =   3480
      TabIndex        =   2
      Top             =   1440
      Width           =   1095
   End
   Begin VB.ListBox List1 
      Height          =   3375
      Left            =   360
      TabIndex        =   0
      Top             =   600
      Width           =   2895
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "Select annotation to load:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      TabIndex        =   1
      Top             =   240
      Width           =   2535
   End
End
Attribute VB_Name = "DBLoad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Cancel_Click()
Unload Me
End Sub

Private Sub Form_Load()
Me.Move frmDemo.left + ((frmDemo.Width - Me.Width) / 2), frmDemo.top + ((frmDemo.Height - Me.Height) / 2)

End Sub

Private Sub List1_DblClick()
If List1.ListIndex <> -1 Then
   gLoadAnnName = List1.List(List1.ListIndex)
   Unload Me
End If
End Sub

Private Sub Load_Click()
If List1.ListIndex <> -1 Then
   gLoadAnnName = List1.List(List1.ListIndex)
End If

Unload Me
End Sub

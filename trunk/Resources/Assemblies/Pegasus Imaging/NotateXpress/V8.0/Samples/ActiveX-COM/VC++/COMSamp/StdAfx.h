// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__4DFF1051_6E0B_4B90_9C5A_D3F0E5A2791E__INCLUDED_)
#define AFX_STDAFX_H__4DFF1051_6E0B_4B90_9C5A_D3F0E5A2791E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT

#include <atlbase.h> // so that CComBstr class can be used.

#import <PegasusImaging.ActiveX.ImagXpress8.dll>
#import <PegasusImaging.ActiveX.NotateXpress8.dll>
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__4DFF1051_6E0B_4B90_9C5A_D3F0E5A2791E__INCLUDED_)

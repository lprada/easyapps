// ComSampDlg.h : header file
//

#if !defined(AFX_COMSAMPDLG_H__75C8330B_0835_4694_8DAB_ED62DBFFEAD8__INCLUDED_)
#define AFX_COMSAMPDLG_H__75C8330B_0835_4694_8DAB_ED62DBFFEAD8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "..\..\..\..\..\..\imagxpress\v8.0\Samples\ActiveX-COM\VC++\Include\ImagXpress8Events.h"
#include "..\include\NotateXpress8Events.h"

/////////////////////////////////////////////////////////////////////////////
// CComSampDlg dialog


class CComSampDlg : public CDialog
{
	// Construction
public:
	
	CComSampDlg(CWnd* pParent = NULL);	// standard constructor
	
	static HRESULT AnnotationEvent(DWORD instancePtr, DWORD objID, long lType, long lData);
	static HRESULT LayerChangeEvent(DWORD instancePtr, DWORD objID, long newLayer);
    static HRESULT NotateMenuSelectEvent(DWORD classPtr, DWORD objID, NX_MenuType Menu, NX_AnnotationTool Tool, long menuID, long subMenuID, long User1, long User2, long layerHandle, long sequenceNumber);
	
private:
	
	CImagXpress *ppCImagXpress1;
	IImagXpressPtr pImagXpress1;
	CNotateXpress *ppCNotateXpress1;
	INotateXpressPtr pNotateXpress1;
	int totLayers;    // used just to supply a name ('Layer ' + totLayers) to each new layer
	long currentLayer;   // tracks the current layer -- set in the LayerChangeEvent
	bool bSetupStamp;
	bool bAutoSelect;
	long NXProgElem; //programmable element to be used throughout the dialog's event handlers etc
	
	void Build_Menus();
	
	// Dialog Data
	//{{AFX_DATA(CComSampDlg)
	enum { IDD = IDD_COMSAMP_DIALOG };
	// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA
	
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CComSampDlg)
protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL
	
	// Implementation
protected:
	HICON m_hIcon;
	
	// Generated message map functions
	//{{AFX_MSG(CComSampDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnDestroy();
	afx_msg void OnFileLoadimage();
	afx_msg void OnFileLoadannotation();
	afx_msg void OnInitMenuPopup(CMenu *pPopupMenu, UINT nIndex, BOOL bSysMenu);
	afx_msg void OnFileSaveannotation();
	afx_msg void OnFilePrint();
	afx_msg void OnFileExit();
	afx_msg void OnLayersCreatelayer();
	afx_msg void OnLayersDeletelayer();
	afx_msg void OnLayersVisible();
	afx_msg void OnLayersActive();
	afx_msg void OnUpdateLayersVisible(CCmdUI* pCmdUI);
	afx_msg void OnLayersClear();
	afx_msg void OnUpdateLayersDeletelayer(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLayersActive(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLayersClear(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileSaveannotation(CCmdUI* pCmdUI);
	afx_msg void OnOptionsBrandannotations();
	afx_msg void OnUpdateOptionsBrandannotations(CCmdUI* pCmdUI);
	afx_msg void OnProgrammableApproveit();
	afx_msg void OnProgrammableCreateobjects();
	afx_msg void OnProgrammableCreatehighlight();
	afx_msg void OnProgrammableItemize();
	afx_msg void OnUpdateProgrammableItemize(CCmdUI* pCmdUI);
	afx_msg void OnOptionsToolpalette();
	afx_msg void OnUpdateOptionsToolpalette(CCmdUI* pCmdUI);
	afx_msg void OnSetstamp();
	afx_msg void OnUpdateSetstamp(CCmdUI* pCmdUI);
	afx_msg void OnAutoselect();
	afx_msg void OnUpdateAutoselect(CCmdUI* pCmdUI);
	afx_msg void OnUsemle();
	afx_msg void OnUpdateUsemle(CCmdUI* pCmdUI);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMSAMPDLG_H__75C8330B_0835_4694_8DAB_ED62DBFFEAD8__INCLUDED_)

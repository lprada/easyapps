// ComSamp.h : main header file for the COMSAMP application
//

#if !defined(AFX_COMSAMP_H__57DB4D74_101E_4620_82FB_BF8E67FDAA7C__INCLUDED_)
#define AFX_COMSAMP_H__57DB4D74_101E_4620_82FB_BF8E67FDAA7C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CComSampApp:
// See ComSamp.cpp for the implementation of this class
//

class CComSampApp : public CWinApp
{
public:
	CComSampApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CComSampApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CComSampApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMSAMP_H__57DB4D74_101E_4620_82FB_BF8E67FDAA7C__INCLUDED_)

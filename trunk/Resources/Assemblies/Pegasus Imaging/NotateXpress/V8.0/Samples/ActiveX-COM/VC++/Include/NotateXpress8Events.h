//-------------------------------------
// Copyright Pegasus Imaging Corp., 1997 - 2006
//   Header Created on 03/13/2006
//     For NotateXpress from Version 8.0.0.4
//         
//         
// Modification History
// ---------------------
//                      
//-------------------------------------


#ifndef NOTATEXPRESS8EVENTS_H
#define NOTATEXPRESS8EVENTS_H


#ifndef __BORLANDC__
using namespace PegasusImagingActiveXNotateXpress8;	
#endif


class CNotateXpressEventHandler : public IDispatch
{
public:
	long m_dwRef;

	CNotateXpressEventHandler()
	{
		m_dwRef = 0;
	}

	~CNotateXpressEventHandler()
	{
	}

	long __stdcall GetTypeInfoCount(unsigned int *)
	{
		return 0;
	}

	long __stdcall GetTypeInfo(unsigned int,unsigned long,struct ITypeInfo ** )
	{
		return 0;
	}

	long __stdcall GetIDsOfNames(const struct _GUID &, BSTR * ,unsigned int,unsigned long,long *)
	{
		return 0;
	}

	long __stdcall GetIID()
	{
		return 0;
	}

	HRESULT __stdcall Invoke (DISPID dispidMember, REFIID riid, LCID lcid, WORD wFlags, DISPPARAMS FAR* pdispparams, VARIANT FAR* pvarResult, EXCEPINFO FAR* pexcepinfo, UINT FAR* puArgErr)
	{
		// this is where all the events come -- just call directly into your methods exactly
		//   as the import wrapper will show you
		switch (dispidMember)
		{
		case 1:
			AnnotateEvent(pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
			break;

		case 2:
			Stamping(   );
			break;

		case 3:
			LayerRestored(pdispparams->rgvarg[0].lVal   );
			break;

		case 4:
			RequestLayerPassword(pdispparams->rgvarg[1].bstrVal, pdispparams->rgvarg[0].bstrVal   );
			break;

		case 5:
			CurrentLayerChange(pdispparams->rgvarg[0].lVal   );
			break;

		case 6:
			Click(pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
			break;

		case 7:
			MenuSelect((NX_MenuType)pdispparams->rgvarg[7].lVal, (NX_AnnotationTool)pdispparams->rgvarg[6].lVal, pdispparams->rgvarg[5].lVal, pdispparams->rgvarg[4].lVal, pdispparams->rgvarg[3].lVal, pdispparams->rgvarg[2].lVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
			break;

		case 8:
			MouseDown(pdispparams->rgvarg[5].lVal, pdispparams->rgvarg[4].lVal, pdispparams->rgvarg[3].iVal, pdispparams->rgvarg[2].iVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
			break;

		case 9:
			MouseUp(pdispparams->rgvarg[5].lVal, pdispparams->rgvarg[4].lVal, pdispparams->rgvarg[3].iVal, pdispparams->rgvarg[2].iVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
			break;

		case 10:
			DoubleClick(pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
			break;

		case 11:
			MouseMove(pdispparams->rgvarg[5].lVal, pdispparams->rgvarg[4].lVal, pdispparams->rgvarg[3].iVal, pdispparams->rgvarg[2].iVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
			break;

		case 12:
			UserDraw(pdispparams->rgvarg[7].lVal, pdispparams->rgvarg[6].lVal, pdispparams->rgvarg[5].lVal, pdispparams->rgvarg[4].lVal, pdispparams->rgvarg[3].lVal, pdispparams->rgvarg[2].lVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
			break;

		case 13:
			ItemChanged(pdispparams->rgvarg[2].lVal, pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
			break;

		case 14:
			ToolbarSelect((NX_AnnotationTool)pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
			break;

		case 15:
			UserGroupCreated(pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
			break;

		case 16:
			UserGroupDestroyed(pdispparams->rgvarg[1].lVal, pdispparams->rgvarg[0].lVal   );
			break;

		}
		return S_OK;
	}

	HRESULT __stdcall QueryInterface(REFIID a_riid, void** a_ppv)
	{
		if (a_ppv == NULL)
			return E_POINTER;
		*a_ppv = NULL;
#ifdef __BORLANDC__
		if (a_riid == DIID__INotateXpressEvents)
#else
		if (a_riid == __uuidof(_INotateXpressEvents))
#endif
			*a_ppv = static_cast<CNotateXpressEventHandler*>(this);
		else if (a_riid == IID_IUnknown)
			*a_ppv = static_cast<IUnknown *>(this);
		else
			return E_NOINTERFACE;
		AddRef();
		return S_OK;
	}

	ULONG __stdcall AddRef()
	{
		return InterlockedIncrement(&m_dwRef);
	}

	ULONG __stdcall Release()
	{
		if (InterlockedDecrement(&m_dwRef) == 0)
			return 0;
		return m_dwRef;
	}

	virtual HRESULT AnnotateEvent(long lType, long lData   )
	{
		return S_OK;
	}

	virtual HRESULT Stamping(   )
	{
		return S_OK;
	}

	virtual HRESULT LayerRestored(long layerHandle   )
	{
		return S_OK;
	}

	virtual HRESULT RequestLayerPassword(BSTR LayerName, BSTR password   )
	{
		return S_OK;
	}

	virtual HRESULT CurrentLayerChange(long newLayer   )
	{
		return S_OK;
	}

	virtual HRESULT Click(long layerHandle, long seqNum   )
	{
		return S_OK;
	}

	virtual HRESULT MenuSelect(NX_MenuType Menu, NX_AnnotationTool Tool, long TopMenuID, long SubMenuID, long User1, long User2, long layerHandle, long sequenceNumber   )
	{
		return S_OK;
	}

	virtual HRESULT MouseDown(long layerHandle, long seqNum, short Button, short Shift, long x, long y   )
	{
		return S_OK;
	}

	virtual HRESULT MouseUp(long layerHandle, long seqNum, short Button, short Shift, long x, long y   )
	{
		return S_OK;
	}

	virtual HRESULT DoubleClick(long layerHandle, long seqNum   )
	{
		return S_OK;
	}

	virtual HRESULT MouseMove(long layerHandle, long seqNum, short Button, short Shift, long x, long y   )
	{
		return S_OK;
	}

	virtual HRESULT UserDraw(long hDC, long x, long y, long Width, long height, long layerHandle, long seqNum, long userLong   )
	{
		return S_OK;
	}

	virtual HRESULT ItemChanged(long layerHandle, long seqNum, long attributeID   )
	{
		return S_OK;
	}

	virtual HRESULT ToolbarSelect(NX_AnnotationTool Tool, long layerHandle   )
	{
		return S_OK;
	}

	virtual HRESULT UserGroupCreated(long layerHandle, long groupID   )
	{
		return S_OK;
	}

	virtual HRESULT UserGroupDestroyed(long layerHandle, long groupID   )
	{
		return S_OK;
	}

};

class CNotateXpress : CNotateXpressEventHandler
{
private:
	DWORD m_dwCookie;

	// Event Handlers
	HRESULT (*pAnnotateEvent) (DWORD classPtr, DWORD objID, long lType, long lData);
	HRESULT (*pStamping) (DWORD classPtr, DWORD objID);
	HRESULT (*pLayerRestored) (DWORD classPtr, DWORD objID, long layerHandle);
	HRESULT (*pRequestLayerPassword) (DWORD classPtr, DWORD objID, BSTR LayerName, BSTR password);
	HRESULT (*pCurrentLayerChange) (DWORD classPtr, DWORD objID, long newLayer);
	HRESULT (*pClick) (DWORD classPtr, DWORD objID, long layerHandle, long seqNum);
	HRESULT (*pMenuSelect) (DWORD classPtr, DWORD objID, NX_MenuType Menu, NX_AnnotationTool Tool, long TopMenuID, long SubMenuID, long User1, long User2, long layerHandle, long sequenceNumber);
	HRESULT (*pMouseDown) (DWORD classPtr, DWORD objID, long layerHandle, long seqNum, short Button, short Shift, long x, long y);
	HRESULT (*pMouseUp) (DWORD classPtr, DWORD objID, long layerHandle, long seqNum, short Button, short Shift, long x, long y);
	HRESULT (*pDoubleClick) (DWORD classPtr, DWORD objID, long layerHandle, long seqNum);
	HRESULT (*pMouseMove) (DWORD classPtr, DWORD objID, long layerHandle, long seqNum, short Button, short Shift, long x, long y);
	HRESULT (*pUserDraw) (DWORD classPtr, DWORD objID, long hDC, long x, long y, long Width, long height, long layerHandle, long seqNum, long userLong);
	HRESULT (*pItemChanged) (DWORD classPtr, DWORD objID, long layerHandle, long seqNum, long attributeID);
	HRESULT (*pToolbarSelect) (DWORD classPtr, DWORD objID, NX_AnnotationTool Tool, long layerHandle);
	HRESULT (*pUserGroupCreated) (DWORD classPtr, DWORD objID, long layerHandle, long groupID);
	HRESULT (*pUserGroupDestroyed) (DWORD classPtr, DWORD objID, long layerHandle, long groupID);

public:
	CNotateXpress()
	{
		CNotateXpress(0,0);
	}

	CNotateXpress(DWORD classPtr, DWORD objID)
	{
		// Call the event connection function in the NotateXpress ATL COM Control
		typedef bool (__cdecl* LPFN_IX_CONNECTEVENTS)(long, DWORD *, IUnknown *, long *);
		LPFN_IX_CONNECTEVENTS  lpfn_IX_ConnectEvents;

		HINSTANCE hDLL = LoadLibrary("PegasusImaging.ActiveX.NotateXpress8.dll");   // NotateXpress ATL COM control
		if (hDLL != NULL)
		{
			lpfn_IX_ConnectEvents = (LPFN_IX_CONNECTEVENTS)GetProcAddress(hDLL, "NX_ConnectEvents");
			if (lpfn_IX_ConnectEvents)
			{
				lpfn_IX_ConnectEvents((long)(&pNotateXpress), &m_dwCookie, this, &m_dwRef);
			}
			FreeLibrary(hDLL);
		}

		// Initialize our instance pointer and object id.
		m_classPtr = classPtr;
		m_objID    = objID;

		// NULL out the Event procedures

		pAnnotateEvent = NULL;
		pStamping = NULL;
		pLayerRestored = NULL;
		pRequestLayerPassword = NULL;
		pCurrentLayerChange = NULL;
		pClick = NULL;
		pMenuSelect = NULL;
		pMouseDown = NULL;
		pMouseUp = NULL;
		pDoubleClick = NULL;
		pMouseMove = NULL;
		pUserDraw = NULL;
		pItemChanged = NULL;
		pToolbarSelect = NULL;
		pUserGroupCreated = NULL;
		pUserGroupDestroyed = NULL;

	}

	~CNotateXpress()
	{
		// Call the event disconnection function in the NotateXpress ATL COM Control
		typedef bool (__cdecl* LPFN_IX_DISCONNECTEVENTS)(long, DWORD);
		LPFN_IX_DISCONNECTEVENTS  lpfn_IX_DisConnectEvents;

		HINSTANCE hDLL = LoadLibrary("PegasusImaging.ActiveX.NotateXpress8.dll");   // NotateXpress ATL COM control
		if (hDLL != NULL)
		{
			lpfn_IX_DisConnectEvents = (LPFN_IX_DISCONNECTEVENTS)GetProcAddress(hDLL, "IX_DisConnectEvents");
			if (lpfn_IX_DisConnectEvents)
			{
				lpfn_IX_DisConnectEvents((long)(static_cast<INotateXpress *>(pNotateXpress)), m_dwCookie);
			}
			FreeLibrary(hDLL);
		}
	}

	void SetAnnotateEventEvent(HRESULT (*pMyAnnotateEvent) (DWORD classPtr, DWORD objID, long lType, long lData))
	{
		pAnnotateEvent = pMyAnnotateEvent;
	}

	HRESULT AnnotateEvent(long lType, long lData)
	{
		if (pAnnotateEvent)
			(*pAnnotateEvent)(m_classPtr, m_objID, lType, lData);
		return S_OK;
	}

	void SetStampingEvent(HRESULT (*pMyStamping) (DWORD classPtr, DWORD objID))
	{
		pStamping = pMyStamping;
	}

	HRESULT Stamping()
	{
		if (pStamping)
			(*pStamping)(m_classPtr, m_objID);
		return S_OK;
	}

	void SetLayerRestoredEvent(HRESULT (*pMyLayerRestored) (DWORD classPtr, DWORD objID, long layerHandle))
	{
		pLayerRestored = pMyLayerRestored;
	}

	HRESULT LayerRestored(long layerHandle)
	{
		if (pLayerRestored)
			(*pLayerRestored)(m_classPtr, m_objID, layerHandle);
		return S_OK;
	}

	void SetRequestLayerPasswordEvent(HRESULT (*pMyRequestLayerPassword) (DWORD classPtr, DWORD objID, BSTR LayerName, BSTR password))
	{
		pRequestLayerPassword = pMyRequestLayerPassword;
	}

	HRESULT RequestLayerPassword(BSTR LayerName, BSTR password)
	{
		if (pRequestLayerPassword)
			(*pRequestLayerPassword)(m_classPtr, m_objID, LayerName, password);
		return S_OK;
	}

	void SetCurrentLayerChangeEvent(HRESULT (*pMyCurrentLayerChange) (DWORD classPtr, DWORD objID, long newLayer))
	{
		pCurrentLayerChange = pMyCurrentLayerChange;
	}

	HRESULT CurrentLayerChange(long newLayer)
	{
		if (pCurrentLayerChange)
			(*pCurrentLayerChange)(m_classPtr, m_objID, newLayer);
		return S_OK;
	}

	void SetClickEvent(HRESULT (*pMyClick) (DWORD classPtr, DWORD objID, long layerHandle, long seqNum))
	{
		pClick = pMyClick;
	}

	HRESULT Click(long layerHandle, long seqNum)
	{
		if (pClick)
			(*pClick)(m_classPtr, m_objID, layerHandle, seqNum);
		return S_OK;
	}

	void SetMenuSelectEvent(HRESULT (*pMyMenuSelect) (DWORD classPtr, DWORD objID, NX_MenuType Menu, NX_AnnotationTool Tool, long TopMenuID, long SubMenuID, long User1, long User2, long layerHandle, long sequenceNumber))
	{
		pMenuSelect = pMyMenuSelect;
	}

	HRESULT MenuSelect(NX_MenuType Menu, NX_AnnotationTool Tool, long TopMenuID, long SubMenuID, long User1, long User2, long layerHandle, long sequenceNumber)
	{
		if (pMenuSelect)
			(*pMenuSelect)(m_classPtr, m_objID, Menu, Tool, TopMenuID, SubMenuID, User1, User2, layerHandle, sequenceNumber);
		return S_OK;
	}

	void SetMouseDownEvent(HRESULT (*pMyMouseDown) (DWORD classPtr, DWORD objID, long layerHandle, long seqNum, short Button, short Shift, long x, long y))
	{
		pMouseDown = pMyMouseDown;
	}

	HRESULT MouseDown(long layerHandle, long seqNum, short Button, short Shift, long x, long y)
	{
		if (pMouseDown)
			(*pMouseDown)(m_classPtr, m_objID, layerHandle, seqNum, Button, Shift, x, y);
		return S_OK;
	}

	void SetMouseUpEvent(HRESULT (*pMyMouseUp) (DWORD classPtr, DWORD objID, long layerHandle, long seqNum, short Button, short Shift, long x, long y))
	{
		pMouseUp = pMyMouseUp;
	}

	HRESULT MouseUp(long layerHandle, long seqNum, short Button, short Shift, long x, long y)
	{
		if (pMouseUp)
			(*pMouseUp)(m_classPtr, m_objID, layerHandle, seqNum, Button, Shift, x, y);
		return S_OK;
	}

	void SetDoubleClickEvent(HRESULT (*pMyDoubleClick) (DWORD classPtr, DWORD objID, long layerHandle, long seqNum))
	{
		pDoubleClick = pMyDoubleClick;
	}

	HRESULT DoubleClick(long layerHandle, long seqNum)
	{
		if (pDoubleClick)
			(*pDoubleClick)(m_classPtr, m_objID, layerHandle, seqNum);
		return S_OK;
	}

	void SetMouseMoveEvent(HRESULT (*pMyMouseMove) (DWORD classPtr, DWORD objID, long layerHandle, long seqNum, short Button, short Shift, long x, long y))
	{
		pMouseMove = pMyMouseMove;
	}

	HRESULT MouseMove(long layerHandle, long seqNum, short Button, short Shift, long x, long y)
	{
		if (pMouseMove)
			(*pMouseMove)(m_classPtr, m_objID, layerHandle, seqNum, Button, Shift, x, y);
		return S_OK;
	}

	void SetUserDrawEvent(HRESULT (*pMyUserDraw) (DWORD classPtr, DWORD objID, long hDC, long x, long y, long Width, long height, long layerHandle, long seqNum, long userLong))
	{
		pUserDraw = pMyUserDraw;
	}

	HRESULT UserDraw(long hDC, long x, long y, long Width, long height, long layerHandle, long seqNum, long userLong)
	{
		if (pUserDraw)
			(*pUserDraw)(m_classPtr, m_objID, hDC, x, y, Width, height, layerHandle, seqNum, userLong);
		return S_OK;
	}

	void SetItemChangedEvent(HRESULT (*pMyItemChanged) (DWORD classPtr, DWORD objID, long layerHandle, long seqNum, long attributeID))
	{
		pItemChanged = pMyItemChanged;
	}

	HRESULT ItemChanged(long layerHandle, long seqNum, long attributeID)
	{
		if (pItemChanged)
			(*pItemChanged)(m_classPtr, m_objID, layerHandle, seqNum, attributeID);
		return S_OK;
	}

	void SetToolbarSelectEvent(HRESULT (*pMyToolbarSelect) (DWORD classPtr, DWORD objID, NX_AnnotationTool Tool, long layerHandle))
	{
		pToolbarSelect = pMyToolbarSelect;
	}

	HRESULT ToolbarSelect(NX_AnnotationTool Tool, long layerHandle)
	{
		if (pToolbarSelect)
			(*pToolbarSelect)(m_classPtr, m_objID, Tool, layerHandle);
		return S_OK;
	}

	void SetUserGroupCreatedEvent(HRESULT (*pMyUserGroupCreated) (DWORD classPtr, DWORD objID, long layerHandle, long groupID))
	{
		pUserGroupCreated = pMyUserGroupCreated;
	}

	HRESULT UserGroupCreated(long layerHandle, long groupID)
	{
		if (pUserGroupCreated)
			(*pUserGroupCreated)(m_classPtr, m_objID, layerHandle, groupID);
		return S_OK;
	}

	void SetUserGroupDestroyedEvent(HRESULT (*pMyUserGroupDestroyed) (DWORD classPtr, DWORD objID, long layerHandle, long groupID))
	{
		pUserGroupDestroyed = pMyUserGroupDestroyed;
	}

	HRESULT UserGroupDestroyed(long layerHandle, long groupID)
	{
		if (pUserGroupDestroyed)
			(*pUserGroupDestroyed)(m_classPtr, m_objID, layerHandle, groupID);
		return S_OK;
	}

	DWORD m_objID;
	DWORD m_classPtr;
	INotateXpressPtr pNotateXpress;

};
#endif
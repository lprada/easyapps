VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{A711DB49-6663-44B3-8804-487B8EA09BE1}#1.0#0"; "PegasusImaging.ActiveX.NotateXpress8.dll"
Begin VB.Form frmDemo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "NotateXpress 8 Demo"
   ClientHeight    =   9555
   ClientLeft      =   1095
   ClientTop       =   1890
   ClientWidth     =   13260
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   9555
   ScaleWidth      =   13260
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox List1 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1230
      ItemData        =   "NotateDemo.frx":0000
      Left            =   240
      List            =   "NotateDemo.frx":0010
      TabIndex        =   9
      Top             =   120
      Width           =   12855
   End
   Begin VB.ListBox lstStatus 
      Height          =   1815
      Left            =   9480
      TabIndex        =   5
      Top             =   2040
      Width           =   3135
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress ImagXpress1 
      Height          =   6735
      Left            =   240
      TabIndex        =   4
      Top             =   1440
      Width           =   8775
      _ExtentX        =   15478
      _ExtentY        =   11880
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1904089674
      ErrInfo         =   -654048866
      Persistence     =   -1  'True
      _cx             =   15478
      _cy             =   11880
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   1
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress IXDibLoader 
      Height          =   615
      Left            =   120
      TabIndex        =   3
      Top             =   7440
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   1085
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   1904089674
      ErrInfo         =   -654048866
      Persistence     =   -1  'True
      _cx             =   1931
      _cy             =   1085
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      DrawStyle       =   0
      DrawFillStyle   =   0
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   2
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   0
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   -1  'True
      EvalMode        =   0
   End
   Begin VB.CommandButton ZoomOut 
      Caption         =   "Zoom Image Out"
      Height          =   495
      Left            =   6720
      TabIndex        =   2
      Top             =   8880
      Width           =   2295
   End
   Begin VB.CommandButton ZoomIn 
      Caption         =   "Zoom Image In"
      Height          =   495
      Left            =   6720
      TabIndex        =   1
      Top             =   8280
      Width           =   2295
   End
   Begin VB.Label lblCoor 
      Caption         =   "Annotation Coordinates:"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   8280
      Width           =   5895
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   9480
      TabIndex        =   8
      Top             =   1440
      Width           =   1815
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   495
      Left            =   9480
      TabIndex        =   7
      Top             =   3960
      Width           =   2415
   End
   Begin VB.Label lblerror 
      Height          =   1935
      Left            =   9480
      TabIndex        =   6
      Top             =   4560
      Width           =   3015
   End
   Begin PegasusImagingActiveXNotateXpress8Ctl.NotateXpress NotateXpress1 
      Left            =   4320
      Top             =   7080
      _cx             =   847
      _cy             =   847
      Client          =   ""
      DefaultPenColor =   16711680
      DefaultFillColor=   12632256
      DefaultPenWidth =   1
      DefaultTextColor=   0
      DefaultTool     =   4103
      DefaultBackColor=   16777215
      BeginProperty DefaultTextFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DefaultStampFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Lucida Console"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Subject         =   ""
      Author          =   ""
      Comments        =   ""
      Title           =   ""
      DefaultHighlightFill=   0   'False
      DefaultHighlightBack=   0   'False
      InteractMode    =   0
      SaveAnnotations =   -1  'True
      DefaultStampText=   ""
      BeginProperty DefaultButtonFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultButtonText=   ""
      PreserveWTLayers=   0   'False
      TIFFAnnotationType=   0
      AllowPaint      =   -1  'True
      UnicodeMode     =   0   'False
      NXPEditors      =   -1  'True
      LoadAnnotations =   -1  'True
      MLECompatible   =   0   'False
      FontScaling     =   0
      TextToolArrows  =   -1  'True
      RecalibrateXDPI =   -1
      RecalibrateYDPI =   -1
      BeginProperty DefaultRulerFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RaiseExceptions =   -1  'True
      ErrorLevel      =   0
      Debug           =   0   'False
      DebugLogFile    =   "c:\NotateXpress8.log"
      ToolbarActivated=   -1  'True
      ToolbarLargeIcons=   -1  'True
   End
   Begin VB.Label Output 
      BorderStyle     =   1  'Fixed Single
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   120
      TabIndex        =   0
      Top             =   8760
      Width           =   6135
   End
   Begin VB.Menu mnu_file 
      Caption         =   "File"
      Begin VB.Menu mnuLoad 
         Caption         =   "Load Image..."
      End
      Begin VB.Menu mnu_file_sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_file_save 
         Caption         =   "Save Image..."
      End
      Begin VB.Menu mnu_file_sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_file_loadannotationtype 
         Caption         =   "Load/Save Annotation Type"
         Begin VB.Menu mnu_ann_wang 
            Caption         =   "Imaging For Windows (Wang)"
            Checked         =   -1  'True
         End
         Begin VB.Menu mnu_ann_notate 
            Caption         =   "NotateXpress (NXP)"
         End
         Begin VB.Menu mnu_ann_tms 
            Caption         =   "ViewDirector - TMSSeqoia (ANN)"
         End
      End
      Begin VB.Menu mnu_file_saveannotationNXP 
         Caption         =   "Save annotation file(NXP)..."
      End
      Begin VB.Menu mnu_file_loadannotationNXP 
         Caption         =   "Load annotation file(NXP)..."
      End
      Begin VB.Menu mnu_file_saveannotationANN 
         Caption         =   "Save annotation file(ANN)..."
      End
      Begin VB.Menu mnu_file_loadannotationANN 
         Caption         =   "Load annotation file(ANN)..."
      End
      Begin VB.Menu mnuPrint 
         Caption         =   "Print Image and Annotations"
      End
      Begin VB.Menu mnu_file_sep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_fileIResY 
         Caption         =   "IResY FontScaling"
      End
      Begin VB.Menu mfSep 
         Caption         =   "-"
      End
      Begin VB.Menu mnuItemToggleAllowPaint 
         Caption         =   "Toggle Allow Paint"
         Checked         =   -1  'True
      End
      Begin VB.Menu mfSeperater 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_file_exit 
         Caption         =   "&Exit"
      End
   End
   Begin VB.Menu mnu_layers 
      Caption         =   "Layer Management"
      Begin VB.Menu mnu_layers_create 
         Caption         =   "Create new layer"
      End
      Begin VB.Menu mnu_layers_deletecurrent 
         Caption         =   "Delete current layer"
      End
      Begin VB.Menu mnu_layers_sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_layers_togglehide 
         Caption         =   "Hide current layer"
      End
      Begin VB.Menu mnu_layers_togglepalette 
         Caption         =   "Toggle toolbar visibility"
      End
      Begin VB.Menu mnuDisableToolbar 
         Caption         =   "Disable Toolbar"
      End
      Begin VB.Menu mnu_layers_sep14 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_objects_reverse 
         Caption         =   "Reverse current ZOrder"
      End
      Begin VB.Menu mnuItemSendToBack 
         Caption         =   "Current Selected To Back of ZOrder"
      End
      Begin VB.Menu mnu_layers_sep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_layers_setdescription 
         Caption         =   "Set layer description"
      End
      Begin VB.Menu mnu_layers_getdescription 
         Caption         =   "Get layer description"
      End
      Begin VB.Menu mnu_layers_sep4 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_layers_setname 
         Caption         =   "Set layer name"
      End
      Begin VB.Menu mnu_layers_getname 
         Caption         =   "Get layer name"
      End
      Begin VB.Menu mnu_layers_sep5 
         Caption         =   "-"
      End
      Begin VB.Menu mnuItemDeleteSelected 
         Caption         =   "Delete Selected"
      End
   End
   Begin VB.Menu mnu_objects 
      Caption         =   "Objects"
      Begin VB.Menu mnu_objects_autoselect 
         Caption         =   "Auto-select when created"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnu_objects_sep3 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_objects_allowgrouping 
         Caption         =   "Allow user grouping"
      End
      Begin VB.Menu mnu_objects_set2 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_objects_create10 
         Caption         =   "Create 10 objects"
      End
      Begin VB.Menu mnu_objects_rects 
         Caption         =   "Create 10 rects"
      End
      Begin VB.Menu mnu_objects_buttons 
         Caption         =   "Create 10 buttons"
      End
      Begin VB.Menu mnu_objects_createtransrect 
         Caption         =   "Create translucent rectangle"
      End
      Begin VB.Menu mnu_objects_createImage 
         Caption         =   "Create an Image object"
      End
      Begin VB.Menu mnu_objects_nudgeup 
         Caption         =   "Nudge up"
      End
      Begin VB.Menu mnu_objects_nudgedn 
         Caption         =   "Nudge dn"
      End
      Begin VB.Menu mnu_objects_inflate 
         Caption         =   "Inflate me"
      End
      Begin VB.Menu mnu_objects_sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuRedaction 
         Caption         =   "Create Redaction"
      End
      Begin VB.Menu mnuArrow 
         Caption         =   "Create Arrow Line"
      End
      Begin VB.Menu mnuRuler 
         Caption         =   "Add Ruler Annotation"
      End
      Begin VB.Menu mnu_objects_largetext 
         Caption         =   "Create large text object"
      End
      Begin VB.Menu mnu_objects_useMLE 
         Caption         =   "Use MLE to edit existing text"
      End
      Begin VB.Menu mnu_objects_sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_objects_brand 
         Caption         =   "Brand"
      End
   End
   Begin VB.Menu mnu_orient 
      Caption         =   "Orientation"
      Begin VB.Menu mnu_orient_ltbr 
         Caption         =   "Rotate 90 clockwise"
      End
      Begin VB.Menu mnu_orient_brtl 
         Caption         =   "Invert"
      End
      Begin VB.Menu mnu_orient_rblt 
         Caption         =   "Rotate 90 counterclockwise"
      End
      Begin VB.Menu mnu_orient_tlbr 
         Caption         =   "Mirror"
      End
      Begin VB.Menu mnu_orientation_rotate180 
         Caption         =   "Rotate 180"
      End
   End
   Begin VB.Menu mnu_stuff 
      Caption         =   "Other Annotation Attributes"
      Begin VB.Menu mnu_stuff_settext 
         Caption         =   "Set item text"
      End
      Begin VB.Menu mnu_stuff_backcolor 
         Caption         =   "Set item backcolor"
      End
      Begin VB.Menu mnu_stuff_pencolor 
         Caption         =   "Set item pen color"
      End
      Begin VB.Menu mnu_stuff_penwidth 
         Caption         =   "Set item pen width"
      End
      Begin VB.Menu mnu_stuff_bevelshadow 
         Caption         =   "Set item bevelshadowcolor"
      End
      Begin VB.Menu mnu_stuff_bevellight 
         Caption         =   "Set item bevellightcolor"
      End
      Begin VB.Menu mnu_stuff_setfont 
         Caption         =   "Set item font..."
      End
      Begin VB.Menu mnu_stuff_getitemtext 
         Caption         =   "Get item text"
      End
      Begin VB.Menu mnu_stuff_hb 
         Caption         =   "Toggle HighlightBack"
      End
      Begin VB.Menu mnu_stuff_hf 
         Caption         =   "Toggle HighlightFill"
      End
      Begin VB.Menu mnu_stuff_moveable 
         Caption         =   "Toggle item moveable"
      End
      Begin VB.Menu mnu_stuff_fixed 
         Caption         =   "Toggle item fixed"
      End
      Begin VB.Menu mnu_stuff_sizeable 
         Caption         =   "Toggle item sizeable"
      End
      Begin VB.Menu mnu_stuff_getitemfont 
         Caption         =   "Get item font"
      End
   End
   Begin VB.Menu mnu_mode 
      Caption         =   "Mode"
      Begin VB.Menu mnu_mode_editmode 
         Caption         =   "Set Edit mode"
      End
      Begin VB.Menu mnu_mode_interactive 
         Caption         =   "Set Interactive mode"
      End
   End
   Begin VB.Menu mnu_cm 
      Caption         =   "Context Menu"
      Begin VB.Menu mnu_cm_enable 
         Caption         =   "Enable"
      End
   End
   Begin VB.Menu mnu_variants 
      Caption         =   "Variants"
      Begin VB.Menu mnu_variants_getlayer 
         Caption         =   "Save layer to database"
      End
      Begin VB.Menu mnu_variants_setlayer 
         Caption         =   "Retrieve layer from database"
      End
      Begin VB.Menu mnu_variants_sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnu_variants_savedb 
         Caption         =   "Save annotations to database..."
      End
      Begin VB.Menu mnu_variants_getdb 
         Caption         =   "Retrieve annotations from database"
      End
   End
   Begin VB.Menu mnu_help 
      Caption         =   "&Help"
      Begin VB.Menu mnu_help_aboutnx 
         Caption         =   "About &NotateXpress..."
      End
      Begin VB.Menu mnu_help_aboutix 
         Caption         =   "About &ImagXpress..."
      End
   End
End
Attribute VB_Name = "frmDemo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit


' Structures
Private Type myMenuItem
    itemID As Long
    NXAttribute As Long
End Type

Private Type POINT
  x As Long
  y As Long
End Type



' Globals
Dim g_layer As Long
Dim g_sequenceNumber As Long
Dim g_stuffMenuItems(0 To 13) As myMenuItem
Dim g_selectedType As NX_AnnotationTool
Dim g_updateMoved As Boolean

Dim db As Database
'Dim gLoadLayerName As String

Dim imgFileName As String 'last full-path name of tiff loaded -- for database stuff
Dim imgParentDir As String
Dim dwlayer As Long

Dim layersMenu As Long

Dim bAutoSelect As Boolean



' Windows API constants and function declarations
Private Const MF_MENUBARBREAK = &H20& ' columns with a separator line
Private Const MF_MENUBREAK = &H40&    ' columns w/o a separator line
Private Const MF_STRING = &H0&
Private Const MF_HELP = &H4000&
Private Const MF_BYPOSITION = &H400&
Private Const MFS_DEFAULT = &H1000&
Private Const MF_ENABLED = &H0&
Private Const MF_DISABLED = &H2&
Private Const MF_GRAYED = &H1&

Private Declare Function AppendMenu Lib "user32" Alias "AppendMenuA" (ByVal hMenu As Long, ByVal wFlags As Long, ByVal wIDNewItem As Long, ByVal lpNewItem As Any) As Long
Private Declare Function DeleteMenu Lib "user32" (ByVal hMenu As Long, ByVal nPosition As Long, ByVal wFlags As Long) As Long
Private Declare Function CreatePopupMenu Lib "user32" () As Long
Private Declare Function GetSubMenu Lib "user32" (ByVal hMenu As Long, ByVal nPos As Long) As Long
Private Declare Function GetMenuItemCount Lib "user32" (ByVal hMenu As Long) As Long
Private Declare Function EnableMenuItem Lib "user32" (ByVal hMenu As Long, ByVal wIDEnableItem As Long, ByVal wEnable As Long) As Long
Private Declare Function GetMenu Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function DrawMenuBar Lib "user32" (ByVal hwnd As Long) As Long
Private Declare Function GetMenuItemID Lib "user32" (ByVal hMenu As Long, ByVal nPos As Long) As Long

Public Sub LoadAnnotationFromDatabase(AnnName As String)
    Dim query As String
    query = "Select * from Annotations where AnnName = '" & AnnName & "'"

    Dim rs As Recordset
    Set rs = db.OpenRecordset(query, dbOpenSnapshot)
    If (rs Is Nothing) Then Exit Sub
    rs.MoveFirst

    '----------------------------
    ' read the record into the variant
    '----------------------------
    Dim tmpV As Variant
    tmpV = rs!NXPData.GetChunk(0, rs!NXPData.FieldSize)

    '------------------
    ' try to load the same image file into ImagXpress
    '-----------------
    ImagXpress1.FileName = rs!imgfile

    '----------------------------
    ' tell NotateXpress to retrieve annotations from
    '   this variant
    '----------------------------
    NotateXpress1.SetAnnFromVariant tmpV
    ImagXpress1.Refresh
    

    If Not (rs Is Nothing) Then
        rs.Close
        Set rs = Nothing
    End If
End Sub
Public Sub LoadLayerFromDatabase(layerName As String)
    Dim query As String
    query = "Select * from Layers where LayerName = '" & layerName & "'"

    Dim rs As Recordset
    Set rs = db.OpenRecordset(query, dbOpenSnapshot)
    If (rs Is Nothing) Then Exit Sub
    rs.MoveFirst

    '----------------------------
    ' read the record into the variant
    '----------------------------
    Dim tmpV As Variant
    tmpV = rs!NXPData.GetChunk(0, rs!NXPData.FieldSize)

    '----------------------------
    ' tell NotateXpress to retrieve annotations from
    '   this variant
    '----------------------------
    Dim layerHandle As Long
    NotateXpress1.SetLayerFromVariant tmpV, layerHandle

    If (layerHandle) Then
        NotateXpress1.SetCurrentLayer layerHandle
        g_layer = layerHandle
    End If

    If Not (rs Is Nothing) Then
       rs.Close
       Set rs = Nothing
    End If
End Sub

Private Sub CreateDB()
Dim TD As New TableDef
Dim TD1 As New TableDef
Dim Indx As index
Dim fld() As New Field
Dim indxfld As Field
Dim DBName As String
Dim i As Integer

On Error GoTo DBErr

DBName = App.Path & "\annotation.mdb"

If Dir(DBName) <> "" Then
  Kill DBName
End If

Screen.MousePointer = 11 ' Hourglass

' ----------------------------------------------
' Create the New Database
' Our Table contains 2 fields; one for an RTF string
' and one for the image data
' ----------------------------------------------
Set db = CreateDatabase(DBName, dbLangGeneral)
TD.Name = "Annotations"   ' Set the table name.

' Create Fields.
ReDim fld(1 To 3)
fld(1).Name = "NXPData"
fld(1).Type = dbLongBinary
fld(2).Name = "ImgFile"
fld(2).Type = dbText
fld(3).Name = "AnnName"
fld(3).Type = dbText
' Add the Fields
For i = 1 To 3
  TD.Fields.Append fld(i)
Next i
Set Indx = TD.CreateIndex("PrimaryKey")
With Indx
   .Name = "PrimaryKey"
   .Primary = True
   .Required = True
   .IgnoreNulls = False
End With

Set indxfld = Indx.CreateField("AnnName")
Indx.Fields.Append indxfld
TD.Indexes.Append Indx
' Append the Table
db.TableDefs.Append TD


TD1.Name = "Layers"   ' Set the table name.
' Create Fields.
ReDim fld(1 To 2)
fld(1).Name = "NXPData"
fld(1).Type = dbLongBinary
fld(2).Name = "LayerName"
fld(2).Type = dbText
' Add the Fields
For i = 1 To 2
  TD1.Fields.Append fld(i)
Next i
Set Indx = TD1.CreateIndex("PrimaryKey")
With Indx
   .Name = "PrimaryKey"
   .Primary = True
   .Required = True
   .IgnoreNulls = False
End With

Set indxfld = Indx.CreateField("LayerName")
With indxfld
    'AllowZeroLength = True -- if DAO >= 3.6
End With
Indx.Fields.Append indxfld
TD1.Indexes.Append Indx

' Append the Table
db.TableDefs.Append TD1

' Close the Database
db.Close
Set db = OpenDatabase(DBName, False, False)

DBErr:
Screen.MousePointer = vbDefault '

End Sub

Private Sub Form_Load()


   ImagXpress1.EventSetEnabled EVENT_PROGRESS, True

    ' Override default settings for the IX controls.
    With IXDibLoader
        .Notify = True
        .DrawStyle = 0
        .DrawFillStyle = 0
        .ManagePalette = False
        .DisplayError = True
    End With
    With ImagXpress1
        .Notify = True
        .DrawStyle = 0
        .DrawFillStyle = 0
        .BorderType = 1
        .ScrollBars = 3
        .ManagePalette = False
        .DisplayError = True
    End With

    ImagXpress1.MenuSetEnabled Menu_Context, TOOL_None, False
    Dim txt As String

    
    Dim f As StdFont

    SetStuffMenuTags

    '--------------
    ' get handle to the 'stuff' menu
    '--------------
    hStuffMenu = GetSubMenu(GetMenu(Me.hwnd), 4)

    '--------------
    ' get handle to the layers submenu
    '--------------
    layersMenu = GetSubMenu(GetMenu(Me.hwnd), 1)
    layersMenu = GetSubMenu(layersMenu, 1)

   
    imgParentDir = App.Path
    imgFileName = imgParentDir & "\..\..\..\..\..\..\Common\Images\benefits.tif"
     
     LoadFile

    NotateXpress1.ToolbarSetToolTip NX_EllipseTool, "The Ellipse-o-Matic!"
    NotateXpress1.ToolbarSetToolTip NX_FreehandTool, "Draw with this!"
    NotateXpress1.ToolbarSetToolTip NX_LineTool, "Liney, very, very Liney"
    NotateXpress1.ToolbarSetToolTip NX_PointerTool, "Point 'em out!"
    NotateXpress1.ToolbarSetToolTip NX_PolygonTool, "Polygons aren't gone"
    NotateXpress1.ToolbarSetToolTip NX_RectangleTool, "Rectangle, we got Rectangle"
    NotateXpress1.ToolbarSetToolTip NX_StampTool, "Stamp Tool!"
    NotateXpress1.ToolbarSetToolTip NX_TextTool, "Write a masterpiece!"
    NotateXpress1.ToolbarSetToolTip NX_PolyLineTool, "PolyLine"
    NotateXpress1.ToolbarSetToolTip NX_ButtonTool, "Button Tool"
    NotateXpress1.ToolbarSetToolTip NX_ImageTool, "Image Annotation Tool"
    NotateXpress1.ToolbarSetToolTip NX_RulerTool, "Ruler Tool"

    NotateXpress1.SetToolDefaultAttribute NX_ButtonTool, NX_PenWidth, 4
    NotateXpress1.DefaultButtonText = "Click me"
    NotateXpress1.SetToolDefaultAttribute NX_ImageTool, NX_BackStyle, NX_Transparent
    NotateXpress1.SetToolDefaultAttribute NX_RulerTool, NX_PenWidth, 4
    
    NotateXpress1.TIFFAnnotationType = NX_I4W_Compatible
    
    mnu_file_saveannotationANN.Enabled = False
    mnu_file_loadannotationANN.Enabled = False

    'this loads an image annotation from a second ImagXpress control
    IXDibLoader.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\vermont.jpg"
    NotateXpress1.SetToolDefaultAttribute NX_ImageTool, NX_DibPtr, IXDibLoader.CopyDIB
    NotateXpress1.SetToolDefaultAttribute NX_StampTool, NX_TextColor, RGB(0, 0, 255)
    NotateXpress1.Client = "ImagXpress1"
    g_layer = NotateXpress1.CreateLayer
    NotateXpress1.Description = "Layer created " & Now
    NotateXpress1.Tool = NX_FreehandTool
    NotateXpress1.Active = True
    NotateXpress1.ToolbarSetVisible g_layer, True
    NotateXpress1.SetText g_layer, NX_StampTool, "Pegasus Imaging Corp."
    Set f = NotateXpress1.GetFont(g_layer, NX_StampTool)
    MoveWindow NotateXpress1.ToolbarHwnd, 750, 400, 120, 180, 1
    SetWindowText NotateXpress1.ToolbarHwnd, "ToolPalette"


    f.Size = 16
    f.Bold = True
    f.Italic = True
    NotateXpress1.SetFont g_layer, NX_StampTool, f

    bAutoSelect = True
    g_updateMoved = True
End Sub

Private Sub ImagXpress1_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub

Private Sub Form_Unload(Cancel As Integer)
    ' *Always* do this to force NotateXpress resource cleanup.
    NotateXpress1.Client = ""
End Sub


Private Sub mnu_ann_notate_Click()
    NotateXpress1.TIFFAnnotationType = NX_NXP_Compatible
    mnu_ann_wang.Checked = False
    mnu_ann_notate.Checked = True
    mnu_ann_tms.Checked = False
End Sub

Private Sub mnu_ann_tms_Click()
    NotateXpress1.TIFFAnnotationType = NX_TMS_Compatible
    mnu_ann_wang.Checked = False
    mnu_ann_notate.Checked = False
    mnu_ann_tms.Checked = True
    
    mnu_file_saveannotationANN.Enabled = True
    mnu_file_loadannotationANN.Enabled = True
    
    mnu_file_saveannotationNXP.Enabled = False
    mnu_file_loadannotationNXP.Enabled = False
End Sub

Private Sub mnu_ann_wang_Click()
    NotateXpress1.TIFFAnnotationType = NX_I4W_Compatible
    mnu_ann_wang.Checked = True
    mnu_ann_notate.Checked = False
    mnu_ann_tms.Checked = False
    
     mnu_file_saveannotationANN.Enabled = False
    mnu_file_loadannotationANN.Enabled = False
    
    mnu_file_saveannotationNXP.Enabled = True
    mnu_file_loadannotationNXP.Enabled = True
    
End Sub

Private Sub mnu_cm_Click()
    If NotateXpress1.MenuGetEnabled(Menu_Context, 0) Then
        mnu_cm_enable.Caption = "Disable"
    Else
        mnu_cm_enable.Caption = "Enable"
    End If
End Sub

Private Sub mnu_cm_enable_Click()
    Dim contextMenuEnabled As Boolean
    contextMenuEnabled = NotateXpress1.MenuGetEnabled(NX_Menu_Context, 0)
    NotateXpress1.MenuSetEnabled NX_Menu_Context, 0, contextMenuEnabled
End Sub

Private Sub mnu_file_exit_Click()
    Unload Me
End Sub

Private Sub mnu_file_loadannotationANN_Click()
    Dim annFile As String
    
    annFile = PegasusOpenFilePF(imgParentDir, "TMS Annotation File (*.ann)" & vbNullChar & "*.ann" & vbNullChar & vbNullChar)
    If Len(annFile) <> 0 Then
    
       
          NotateXpress1.RetrieveAnnotation annFile
                             
          Err = ImagXpress1.ImagError
          PegasusError Err, lblerror
    
          NotateXpress1.ToolbarActivated = True
      
    End If
End Sub

Private Sub mnu_file_loadannotationNXP_Click()
Dim annFile As String
    
    annFile = PegasusOpenFilePF(imgParentDir, "NotateXpress File (*.nxp)" & vbNullChar & "*.nxp" & vbNullChar & vbNullChar)
    If Len(annFile) <> 0 Then
    
       
          NotateXpress1.RetrieveAnnotation annFile
                             
          Err = ImagXpress1.ImagError
          PegasusError Err, lblerror
    
          NotateXpress1.ToolbarActivated = True
      
    End If
End Sub

Private Sub mnu_file_save_Click()
 Dim sTiffFile As String
    sTiffFile = PegasusSaveFilePF(imgParentDir, "Tiff File (*.tif)" & vbNullChar & "*.tif" & vbNullChar & vbNullChar, "tif")
    If Len(sTiffFile) Then
       ImagXpress1.SaveFileName = sTiffFile
       ImagXpress1.SaveFile
    End If
End Sub

Private Sub mnu_file_saveannotationANN_Click()
    Dim annFile As String
    
    annFile = PegasusSaveFilePF(imgParentDir, "TMS Annotation File (*.ann)" & vbNullChar & "*.ann" & vbNullChar & vbNullChar, "ann")
    If Len(annFile) Then
       NotateXpress1.Comments = Now
       NotateXpress1.Description = Now
       NotateXpress1.Title = "One awesome annotation file"
       NotateXpress1.StoreAnnotation annFile
    End If
End Sub

Private Sub mnu_file_saveannotationNXP_Click()
 Dim annFile As String
    
    annFile = PegasusSaveFilePF(imgParentDir, "NotateXpress File (*.nxp)" & vbNullChar & "*.nxp" & vbNullChar & vbNullChar, "ann")
    If Len(annFile) Then
       NotateXpress1.Comments = Now
       NotateXpress1.Description = Now
       NotateXpress1.Title = "One awesome annotation file"
       NotateXpress1.StoreAnnotation annFile
    End If
End Sub

Private Sub mnu_fileIResY_Click()

If (NotateXpress1.FontScaling = NX_FontScaling_IResY) Then
   NotateXpress1.FontScaling = NX_FontScaling_Normal
Else
   NotateXpress1.FontScaling = NX_FontScaling_IResY
End If

mnu_fileIResY.Checked = (NotateXpress1.FontScaling = NX_FontScaling_IResY)

End Sub

Private Sub mnu_layers_Click()
Dim rc As Long, n As Long
Dim layerHandle As Long
Dim desc As String
Dim bEnabled As Boolean

bEnabled = (NotateXpress1.CurrentLayer <> 0)

mnu_layers_deletecurrent.Enabled = bEnabled
mnu_layers_togglehide.Enabled = bEnabled
mnu_layers_setdescription.Enabled = bEnabled
mnu_layers_getdescription.Enabled = bEnabled
mnu_layers_setname.Enabled = bEnabled
mnu_layers_getname.Enabled = bEnabled
mnu_layers_togglepalette.Enabled = (bEnabled And NotateXpress1.ToolbarActivated)

If (bEnabled) Then
   If (NotateXpress1.Visible) Then
        mnu_layers_togglehide.Caption = "Hide current layer"
    Else
        mnu_layers_togglehide.Caption = "Show current layer"
    End If
Else

End If

If (NotateXpress1.NumLayers < 2) Then
   ' mnu_layers_setcurrent.Enabled = False
Else
    'mnu_layers_setcurrent.Enabled = True
    '---------------
    ' clean layers menu
    '---------------
    rc = DeleteMenu(layersMenu, 0, MF_BYPOSITION)
    While rc
        rc = DeleteMenu(layersMenu, 0, MF_BYPOSITION)
    Wend

    '---------------
    ' add the description for each layer under this submenu
    '---------------
    layerHandle = NotateXpress1.GetFirstLayer
    n = 0
    While layerHandle <> 0
        desc = NotateXpress1.GetDescription(layerHandle)
        AppendMenu layersMenu, MF_STRING Or MF_ENABLED, n + 1000, desc
        layerHandle = NotateXpress1.GetNextLayer
        n = n + 1
    Wend
    DrawMenuBar Me.hwnd

End If

End Sub

Private Sub mnu_layers_create_Click()
NotateXpress1.CreateLayer
If (NotateXpress1.InteractMode = NX_EditMode) Then
    NotateXpress1.ToolbarSetVisible g_layer, True
    NotateXpress1.Active = True
End If
NotateXpress1.Description = "Layer created " & Now

End Sub

Private Sub mnu_layers_deletecurrent_Click()
    NotateXpress1.DeleteLayer g_layer
End Sub

Private Sub mnu_layers_getdescription_Click()
If (NotateXpress1.CurrentLayer) Then
   Dim s As String
   s = NotateXpress1.Description
   MsgBox NotateXpress1.Description
End If

End Sub

Private Sub mnu_layers_getname_Click()
If (NotateXpress1.CurrentLayer) Then MsgBox NotateXpress1.layerName

End Sub

Private Sub mnu_layers_setdescription_Click()
Dim p As String

p = InputBox("Enter layer description:", "Layer description")
NotateXpress1.Description = p

End Sub

Private Sub mnu_layers_setname_Click()
Dim p As String

p = InputBox("Enter layer name:", "Layer name")
NotateXpress1.layerName = p

End Sub

Private Sub mnu_layers_togglehide_Click()
NotateXpress1.Visible = Not NotateXpress1.Visible
End Sub

Private Sub mnu_layers_togglepalette_Click()
If NotateXpress1.ToolbarGetVisible(g_layer) Then
    NotateXpress1.ToolbarSetVisible g_layer, False
Else
    NotateXpress1.ToolbarSetVisible g_layer, True
End If

End Sub
Private Sub LoadFile()
        
    NotateXpress1.Clear
            
    
    dwlayer = NotateXpress1.CreateLayer
    ImagXpress1.FileName = imgFileName
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblerror
    
    NotateXpress1.ToolbarActivated = True
   
End Sub

Private Sub mnu_stuff_fixed_Click()
Dim m As Boolean
    m = NotateXpress1.PrgGetItemFixed(g_layer, g_sequenceNumber)
    m = Not m
    NotateXpress1.PrgSetItemFixed g_layer, g_sequenceNumber, m
End Sub

Private Sub mnuDisableToolbar_Click()
    
     If NotateXpress1.ToolbarGetEnabled(NotateXpress1.CurrentLayer) Then
        mnuDisableToolbar.Caption = "Enable Toolbar"
        NotateXpress1.ToolbarSetEnabled NotateXpress1.CurrentLayer, False
    Else
        mnuDisableToolbar.Caption = "Disable Toolbar"
        NotateXpress1.ToolbarSetEnabled NotateXpress1.CurrentLayer, True
    End If
     
    
    Err = ImagXpress1.ImagError
    PegasusError Err, lblerror
  
      
End Sub

Private Sub mnuItemDeleteSelected_Click()
    NotateXpress1.DeleteSelected
End Sub

Private Sub mnuItemSendToBack_Click()
    If (g_layer) Then NotateXpress1.SendItemToBack g_layer, g_sequenceNumber
End Sub

Private Sub mnuItemToggleAllowPaint_Click()

    NotateXpress1.AllowPaint = Not NotateXpress1.AllowPaint
    mnuItemToggleAllowPaint.Checked = NotateXpress1.AllowPaint

End Sub

Private Sub mnuLoad_Click()
 Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
       
        LoadFile
    End If
End Sub


Private Sub mnu_mode_editmode_Click()
NotateXpress1.InteractMode = NX_EditMode

End Sub

Private Sub mnu_mode_interactive_Click()
NotateXpress1.InteractMode = NX_InteractiveMode

End Sub

Private Sub mnu_objects_allowgrouping_Click()
Dim bUserGrouping

bUserGrouping = Not NotateXpress1.AllowUserGrouping
NotateXpress1.AllowUserGrouping = bUserGrouping
mnu_objects_allowgrouping.Checked = bUserGrouping

End Sub

Private Sub mnu_objects_autoselect_Click()
bAutoSelect = Not bAutoSelect
mnu_objects_autoselect.Checked = bAutoSelect
End Sub

Private Sub mnu_objects_brand_Click()
NotateXpress1.Brand 24
End Sub

Private Sub mnu_objects_buttons_Click()
Dim e As Long
Dim i As Integer
Dim top, left, right, bottom As Integer
Dim x, y As Integer

'---------------
' initialize object location
'---------------
top = 10
left = 10
right = 100
bottom = 100

'---------------
' create a programmable element
'---------------
e = NotateXpress1.ElementCreate

'---------------
' set some programmable element properties
'---------------
NotateXpress1.ElementSetPenWidth e, 4
NotateXpress1.ElementSetType e, NX_ButtonTool

'--------------
' now using this one programmable element,
'   create 10 buttons
'--------------
For i = 1 To 10
  NotateXpress1.ElementSetBoundingRect e, left, top, right, bottom
  NotateXpress1.ElementSetText e, "Button " + CStr(i)
  x = Int(400 * Rnd)
  y = Int(400 * Rnd)

  NotateXpress1.ElementAppend e
  top = top + 2
  left = left + 2
  right = right + 2
  bottom = bottom + 2 '
Next

NotateXpress1.ElementDestroy e

End Sub

Private Sub mnu_objects_Click()
If (NotateXpress1.CurrentLayer = 0) Then

   Dim objMenu As Long, n As Long, rc As Long

   objMenu = GetSubMenu(GetMenu(Me.hwnd), 2)
   n = GetMenuItemCount(objMenu)
   n = n - 1
   While n >= 0
      rc = EnableMenuItem(objMenu, n, MF_BYPOSITION + MF_DISABLED + MF_GRAYED)
      n = n - 1
   Wend
   DrawMenuBar Me.hwnd
   Exit Sub
End If

Dim iCount As Integer

iCount = NotateXpress1.PrgGetItemCount(NotateXpress1.CurrentLayer)
mnu_objects_reverse.Enabled = (iCount > 1)
mnu_objects_brand.Enabled = (iCount > 0)
mnu_objects_nudgeup.Enabled = NotateXpress1.IsSelection And iCount > 1
mnu_objects_nudgedn.Enabled = NotateXpress1.IsSelection And iCount > 1
mnu_objects_createtransrect.Enabled = True
mnu_objects_inflate.Enabled = False

If (g_sequenceNumber <> 0) Then
   Dim thisItem As NX_AnnotationTool

   thisItem = NotateXpress1.PrgGetItemType(g_layer, g_sequenceNumber)
   If (thisItem = NX_EllipseTool Or thisItem = NX_RectangleTool Or thisItem = NX_TextTool Or thisItem = NX_ButtonTool) Then
       mnu_objects_inflate.Enabled = True
   End If
End If




End Sub

Private Sub mnu_objects_create10_Click()
Dim e As Long
Dim i As Integer
Dim top, left, right, bottom As Integer
Dim x, y As Integer

top = 15
left = 15
right = 115
bottom = 115

e = NotateXpress1.ElementCreate

NotateXpress1.ElementSetPenWidth e, 1
For i = 1 To 10
  NotateXpress1.ElementSetBoundingRect e, left, top, right, bottom
  NotateXpress1.ElementSetPenColor e, RGB(Int(255 * Rnd), Int(255 * Rnd), Int(255 * Rnd))
  NotateXpress1.ElementSetFillColor e, RGB(Int(255 * Rnd), Int(255 * Rnd), Int(255 * Rnd))
  If (i Mod 2) Then
    NotateXpress1.ElementSetType e, NX_RectangleTool
  Else
    NotateXpress1.ElementSetType e, NX_EllipseTool
  End If
  x = Int(400 * Rnd)
  y = Int(400 * Rnd)

  NotateXpress1.ElementAppend e
  top = top + 2
  left = left + 2
  right = right + 2
  bottom = bottom + 2 '
Next

NotateXpress1.ElementDestroy e


End Sub

Private Sub mnu_objects_createImage_Click()
Dim e As Long

e = NotateXpress1.ElementCreate
IXDibLoader.FileName = App.Path & "\..\..\..\..\..\..\Common\Images\vermont.jpg"
NotateXpress1.ElementSetType e, NX_ImageTool
NotateXpress1.ElementSetDIBHandle e, IXDibLoader.CopyDIB
NotateXpress1.ElementSetBoundingRect e, 100, 100, 100 + IXDibLoader.IWidth, 100 + IXDibLoader.IHeight
NotateXpress1.ElementAppend e
NotateXpress1.ElementDestroy e

End Sub

Private Sub mnu_objects_createtransrect_Click()
Dim e As Long
With NotateXpress1
    e = .ElementCreate
    .ElementSetType e, NX_RectangleTool
    .ElementSetPenWidth e, 1
    .ElementSetPenColor e, vbBlue
    .ElementSetHighlightFill e, True ' ****** Outline is "permanent" without this line
    .ElementSetFillColor e, vbYellow
    .ElementSetBoundingRect e, 100, 100, 200, 200
    .ElementAppend e
    .ElementDestroy e
    .Tool = NX_PointerTool
End With

End Sub

Private Sub mnu_objects_inflate_Click()
Dim x As Long, y As Long, x2 As Long, y2 As Long
Dim i As Integer

NotateXpress1.PrgGetItemRect g_layer, g_sequenceNumber, x, y, x2, y2

For i = 1 To 100
    NotateXpress1.PrgSetItemRect g_layer, g_sequenceNumber, x - i, y - i, x2 + i, y2 + i
    DoEvents
Next

End Sub

Private Sub mnu_objects_largetext_Click()
Dim e As Long
Dim t As String
Dim s As StdFont

Set s = New StdFont
s.Bold = False
s.Name = "Times New Roman"
s.Size = 12

e = NotateXpress1.ElementCreate
NotateXpress1.ElementSetTextJustification e, NX_Justify_Center
NotateXpress1.ElementSetType e, NX_TextTool
NotateXpress1.ElementSetBoundingRect e, 30, 30, 280, 300
NotateXpress1.ElementSetBackColor e, RGB(255, 255, 0)
NotateXpress1.ElementSetTextColor e, RGB(0, 0, 0)
NotateXpress1.ElementSetBackstyle e, NX_Translucent
NotateXpress1.ElementSetPenWidth e, 2
NotateXpress1.ElementSetPenColor e, RGB(0, 0, 255)

t = "NotateXpress version 8" + Chr(13) + Chr(10) + Chr(13) + Chr(10)
t = t + "NotateXpress version 8's programmatic annotation features let you enter, edit, and query all types of annotation objects programmatically. "
t = t + "Using NotateXpress's programmatic capabilities is fun and fascinating! "
t = t + "New features in NotateXpress version 8 include ANN Annotation File Format Support and Medical Annotation Data Locking.  Try it today."

NotateXpress1.ElementSetText e, t
NotateXpress1.ElementSetFont e, s
NotateXpress1.ElementAppend e
NotateXpress1.ElementDestroy e

End Sub

Private Sub mnu_objects_nudgedn_Click()
  NotateXpress1.NudgeDn g_layer, g_sequenceNumber

End Sub

Private Sub mnu_objects_nudgeup_Click()
  NotateXpress1.NudgeUp g_layer, g_sequenceNumber

End Sub

Private Sub mnu_objects_rects_Click()
Dim e As Long
Dim i As Integer
Dim top, left, right, bottom As Integer
Dim x, y As Integer

top = 20
left = 20
right = 120
bottom = 120

e = NotateXpress1.ElementCreate
NotateXpress1.ElementSetType e, NX_RectangleTool

NotateXpress1.ElementSetPenWidth e, 1
For i = 1 To 10
  NotateXpress1.ElementSetBoundingRect e, left, top, right, bottom
  NotateXpress1.ElementSetPenColor e, RGB(Int(255 * Rnd), Int(255 * Rnd), Int(255 * Rnd))
  NotateXpress1.ElementSetFillColor e, RGB(Int(255 * Rnd), Int(255 * Rnd), Int(255 * Rnd))
  x = Int(400 * Rnd)
  y = Int(400 * Rnd)

  NotateXpress1.ElementAppend e
  top = top + 2
  left = left + 2
  right = right + 2
  bottom = bottom + 2 '
Next

NotateXpress1.ElementDestroy e


End Sub

Private Sub mnu_objects_reverse_Click()
If (g_layer) Then NotateXpress1.Reverse g_layer

End Sub


Private Sub mnu_objects_useMLE_Click()
  NotateXpress1.MLECompatible = Not NotateXpress1.MLECompatible
  mnu_objects_useMLE.Checked = NotateXpress1.MLECompatible

End Sub

Private Sub mnu_orient_brtl_Click()
ImagXpress1.Flip

End Sub

Private Sub mnu_orient_ltbr_Click()
ImagXpress1.Rotate 270
End Sub

Private Sub mnu_orient_rblt_Click()
ImagXpress1.Rotate 90
End Sub

Private Sub mnu_orient_tlbr_Click()
ImagXpress1.Mirror
End Sub

Private Sub mnu_orientation_rotate180_Click()
ImagXpress1.Rotate 180
End Sub



Private Sub mnu_stuff_backcolor_Click()

NotateXpress1.PrgSetItemColor g_layer, g_sequenceNumber, NX_BackColor, RGB(0, 255, 0)
End Sub

Private Sub mnu_stuff_bevellight_Click()
NotateXpress1.PrgSetItemColor g_layer, g_sequenceNumber, NX_BevelLightColor, RGB(0, 255, 2550)

End Sub

Private Sub mnu_stuff_bevelshadow_Click()
NotateXpress1.PrgSetItemColor g_layer, g_sequenceNumber, NX_BevelShadowColor, RGB(0, 255, 0)
End Sub

Private Sub mnu_stuff_Click()
Dim stuffMenu As Long, n As Long
Dim i As Integer

stuffMenu = GetSubMenu(GetMenu(Me.hwnd), 4)
n = GetMenuItemCount(stuffMenu)

If ((NotateXpress1.CurrentLayer = 0) Or (g_sequenceNumber = 0)) Then
   '------------
   ' disable all menu selections on this menu
   '------------
   Dim rc As Long

   n = n - 1
   While n >= 0
      rc = EnableMenuItem(stuffMenu, n, MF_BYPOSITION + MF_DISABLED + MF_GRAYED)
      n = n - 1
   Wend
   DrawMenuBar Me.hwnd
   Exit Sub
End If

'-----------------
' Enable menu items as appropriate for this menu
'-----------------

Dim Tool As NX_AnnotationTool

Tool = NotateXpress1.PrgGetItemType(g_layer, g_sequenceNumber)

For i = 0 To 13
  If (NotateXpress1.IsValidToolAttribute(Tool, g_stuffMenuItems(i).NXAttribute)) Then
     EnableMenuItem stuffMenu, i, MF_BYPOSITION + MF_ENABLED
  Else
     EnableMenuItem stuffMenu, i, MF_BYPOSITION + MF_DISABLED + MF_GRAYED
  End If
Next

DrawMenuBar Me.hwnd



End Sub

Private Sub mnu_stuff_getitemfont_Click()
    Dim f As StdFont
    Set f = NotateXpress1.PrgGetItemFont(g_layer, g_sequenceNumber)
    MsgBox f.Name + " " + CStr(f.Size)
End Sub

Private Sub mnu_stuff_getitemtext_Click()
    Dim itemText As String
    itemText = NotateXpress1.PrgGetItemText(g_layer, g_sequenceNumber)
    MsgBox itemText
End Sub

Private Sub mnu_stuff_hb_Click()
    Dim hb As Boolean
    hb = NotateXpress1.PrgGetItemHighlightBack(g_layer, g_sequenceNumber)
    hb = Not hb
    NotateXpress1.PrgSetItemHighlightBack g_layer, g_sequenceNumber, hb
End Sub

Private Sub mnu_stuff_hf_Click()
    Dim hf As Boolean
    hf = NotateXpress1.PrgGetItemHighlightFill(g_layer, g_sequenceNumber)
    hf = Not hf
    NotateXpress1.PrgSetItemHighlightFill g_layer, g_sequenceNumber, hf
End Sub

Private Sub mnu_stuff_moveable_Click()
    Dim m As Boolean
    m = NotateXpress1.PrgGetItemMoveable(g_layer, g_sequenceNumber)
    m = Not m
    NotateXpress1.PrgSetItemMoveable g_layer, g_sequenceNumber, m
End Sub

Private Sub mnu_stuff_pencolor_Click()
    NotateXpress1.PrgSetItemColor g_layer, g_sequenceNumber, NX_PenColor, RGB(0, 255, 0)
End Sub

Private Sub mnu_stuff_penwidth_Click()
    NotateXpress1.PrgSetItemPenWidth g_layer, g_sequenceNumber, 15
End Sub

Private Sub mnu_stuff_setfont_Click()
    NotateXpress1.UserItemFontSelect g_layer, g_sequenceNumber, "Select new font", 0
End Sub

Private Sub mnu_stuff_settext_Click()
    NotateXpress1.PrgSetItemText g_layer, g_sequenceNumber, "This is a test and only a test. Had this been something other than a test, something significant would have happened!"
End Sub

Private Sub mnu_stuff_sizeable_Click()
    Dim s As Boolean
    s = NotateXpress1.PrgGetItemSizeable(g_layer, g_sequenceNumber)
    s = Not s
    NotateXpress1.PrgSetItemSizeable g_layer, g_sequenceNumber, s
End Sub

Private Sub mnu_variants_Click()
    If NotateXpress1.CurrentLayer = 0 Then
        mnu_variants_getlayer.Enabled = False
    Else
        mnu_variants_getlayer.Enabled = True
    End If
End Sub

Private Sub mnu_variants_getdb_Click()
Dim rs As Recordset
Dim tmpV As Variant
Dim query As String
Dim count As Integer
Dim i As Integer

gLoadAnnName = ""

On Error GoTo DBLoadErr

If db Is Nothing Then
   Set db = OpenDatabase(App.Path & "\annotation.mdb", False, False)
End If

Set rs = db.OpenRecordset("Select AnnName from Annotations", dbOpenDynaset)
If (rs Is Nothing) Then
   MsgBox "There are no saved annotations", vbInformation, "Empty database"
   Exit Sub
End If

rs.MoveLast
count = rs.RecordCount
rs.MoveFirst

Load DBLoad
For i = 1 To count
   DBLoad.List1.AddItem rs!AnnName
   If (i <> count) Then rs.MoveNext
Next

DBLoad.Show 1

If (Len(gLoadAnnName)) Then
   LoadAnnotationFromDatabase (gLoadAnnName)
End If

DBLoadErr:
If Not (rs Is Nothing) Then
   rs.Close
   Set rs = Nothing
End If
End Sub

Private Sub mnu_variants_getlayer_Click()
Dim layerName As String

If db Is Nothing Then CreateDB
If db Is Nothing Then
   MsgBox "Database creation error"
   Exit Sub
End If

If NotateXpress1.layerName = "" Then
   layerName = InputBox("Please name this layer")
   If layerName = "" Then Exit Sub
   NotateXpress1.layerName = layerName
End If

Dim v As Variant
Dim c As Long
Dim rs As Recordset
Dim editExisting As Integer
Dim rc As Integer

editExisting = False

Set rs = db.OpenRecordset("Layers", dbOpenTable)
rs.index = "PrimaryKey"
rs.Seek "=", NotateXpress1.layerName
If Not (rs.NoMatch) Then
   rc = MsgBox("Overwrite existing layer?", vbOKCancel, "Duplicate Layer Name")
   If rc = vbCancel Then
      rs.Close
      Set rs = Nothing
      Exit Sub
   Else
      editExisting = True
   End If
End If
rs.Close


NotateXpress1.GetLayerIntoVariant v, NotateXpress1.CurrentLayer, c

'--------------------------------
' once the layer data is in a variant,
'   you can store it into a database, etc.
'--------------------------------
'MsgBox "Length of data = " & Len(v)
'--------------------------------
Set rs = db.OpenRecordset("Layers", dbOpenDynaset)
If editExisting Then
   rs.FindFirst "LayerName = '" & NotateXpress1.layerName & "'"
   rs.Edit
Else
   rs.AddNew
End If
rs!layerName = NotateXpress1.layerName
rs!NXPData.AppendChunk v
rs.Update
rs.Close
Set rs = Nothing

End Sub

Private Sub mnu_variants_savedb_Click()
Dim AnnName As String
Dim rc As Integer
Dim editExisting As Integer

AnnName = InputBox("Annotation name:", "Set Annotation Name", "AnnotationOne")
If Len(AnnName) = 0 Then Exit Sub

If db Is Nothing Then CreateDB
If db Is Nothing Then
   MsgBox "Database creation error"
   Exit Sub
End If

Dim v As Variant
Dim c As Long
Dim rs As Recordset

editExisting = False

Set rs = db.OpenRecordset("Annotations", dbOpenTable)
rs.index = "PrimaryKey"
rs.Seek "=", AnnName
If Not (rs.NoMatch) Then
   rc = MsgBox("Overwrite existing annotation?", vbOKCancel, "Duplicate Annotation Name")
   If rc = vbCancel Then
      rs.Close
      Set rs = Nothing
      Exit Sub
   Else
      editExisting = True
   End If
End If
rs.Close

NotateXpress1.GetAnnIntoVariant v, c
'--------------------------------
' once the annotation data is in a variant,
'   you can store it into a database, etc.
'--------------------------------
' Everything about the annotation is retained
'   in this variant array EXCEPT:
'      Comments
'      Author
'      Title
'      Subject
'--------------------------------
' if you want to save these, you should access
'    these values from the respective properties
'--------------------------------
'MsgBox "Length of data = " & Len(v)
'--------------------------------
Set rs = db.OpenRecordset("Annotations", dbOpenDynaset)

If editExisting Then
   rs.FindFirst "AnnName = '" & AnnName & "'"
   rs.Edit
Else
   rs.AddNew
End If
rs!AnnName = AnnName
rs!imgfile = imgFileName
rs!NXPData.AppendChunk v
rs.Update
rs.Close
Set rs = Nothing
End Sub

Private Sub mnu_variants_setlayer_Click()
Dim rs As Recordset
Dim tmpV As Variant
Dim query As String
Dim count As Integer
Dim i As Integer

gLoadAnnName = ""

On Error GoTo DBLoadErr

If db Is Nothing Then
   Set db = OpenDatabase(App.Path & "\annotation.mdb", False, False)
End If

Set rs = db.OpenRecordset("Select LayerName from Layers", dbOpenSnapshot)
If (rs Is Nothing) Then
   MsgBox "There are no saved layers", vbInformation, "Empty database"
   Exit Sub
End If

rs.MoveLast
count = rs.RecordCount
rs.MoveFirst

Load DBLoad
For i = 1 To count
   DBLoad.List1.AddItem rs!layerName
   If (i <> count) Then rs.MoveNext
Next

DBLoad.Show 1

If (Len(gLoadAnnName)) Then
   LoadLayerFromDatabase (gLoadAnnName)
   ImagXpress1.Refresh
End If

DBLoadErr:
If Not (rs Is Nothing) Then
   rs.Close
   Set rs = Nothing
End If
End Sub

Private Sub mnuArrow_Click()
    Dim e As Long
    e = NotateXpress1.ElementCreate
    NotateXpress1.ElementSetPenWidth e, 5
    NotateXpress1.ElementSetLineEndStyle e, NX_LineEnd_Arrow
    NotateXpress1.ElementSetBoundingRect e, 25, 50, 75, 100
    NotateXpress1.ElementSetType e, NX_LineTool
    NotateXpress1.ElementAppend e
End Sub


Private Sub mnuPrint_Click()
    Dim ImgHgt As Integer, ImgWid As Integer
    Dim AreaHgt As Integer, AreaWid As Integer
    Dim H As Integer, W As Integer
    Dim WScl As Double, HScl As Double

    Printer.ScaleMode = vbTwips
    ImgHgt = ImagXpress1.IHeight
    ImgWid = ImagXpress1.IWidth
    AreaHgt = Printer.ScaleHeight
    AreaWid = Printer.ScaleWidth
    HScl = AreaHgt / ImgHgt
    WScl = AreaWid / ImgWid
    If WScl < HScl Then
        W = AreaWid
        H = Int(ImgHgt * WScl)
    Else
        H = AreaHgt
        W = Int(ImgWid * HScl)
    End If
    If H <= 0 Then H = 1
    If W <= 0 Then W = 1
    ImagXpress1.PrinterHeight = H
    ImagXpress1.PrinterTop = Int((Printer.ScaleHeight - H) / 2) - 1
    ImagXpress1.PrinterWidth = W
    ImagXpress1.PrinterLeft = Int((Printer.ScaleWidth - W) / 2) - 1
    Printer.Print ""  ' Initialize Printer
    ImagXpress1.PrinterhDC = Printer.hDC
    Printer.EndDoc
End Sub

Private Sub mnuRedaction_Click()
    'this code demonstrates how to perform redaction on the image which is often used in _
    legal field
    Dim e As Long
    e = NotateXpress1.ElementCreate
    NotateXpress1.ElementSetType e, NX_TextTool
    NotateXpress1.ElementSetText e, "Approved by Pegasus Imaging Corp." & Space(2) & Now
    NotateXpress1.ElementSetTextJustification e, NX_Justify_Center
    NotateXpress1.ElementSetTextColor e, vbRed
    NotateXpress1.ElementSetPenWidth e, 1
    NotateXpress1.ElementSetPenColor e, vbWhite
    NotateXpress1.ElementSetBackstyle e, NX_Opaque
    NotateXpress1.ElementSetFillColor e, vbWhite
    NotateXpress1.ElementSetBoundingRect e, 100, 100, 300, 200
    NotateXpress1.ElementAppend e
    NotateXpress1.ElementDestroy e
    NotateXpress1.Tool = NX_PointerTool
End Sub

Private Sub mnuRuler_Click()
    Dim element As Long
    element = NotateXpress1.ElementCreate
    NotateXpress1.ElementSetPenWidth element, 4
    ' Set the annotation type to the Ruler annotation.
    NotateXpress1.ElementSetType element, NX_RulerTool
    NotateXpress1.ElementSetBoundingRect element, 25, 50, 200, 100
    ' Set the size of the gauge.
    NotateXpress1.ElementSetGaugeLength element, 20
    ' Set the unit of measurement being used for the ruler.
    NotateXpress1.ElementSetMeasure element, NX_Measure_Pixels
    ' Set the precision being used for the ruler.
    NotateXpress1.ElementSetPrecision element, 1
    ' Set the color for the units of measurement.
    NotateXpress1.ElementSetTextColor element, vbBlue
    ' Set the annotation to selected.
    NotateXpress1.ElementSetSelected element, True
    ' Append the ruler to image.
    NotateXpress1.ElementAppend element
End Sub

Private Sub NotateXpress1_AnnotateEvent(ByVal lType As Long, ByVal lData As Long)
    Select Case lType
    Case NX_AnnotationEndSelected:
        ' Clear the selection.
        g_sequenceNumber = 0

    Case NX_AnnotationSelected:
        g_sequenceNumber = lData
        g_selectedType = NotateXpress1.PrgGetItemType(g_layer, g_sequenceNumber)

    Case NX_AnnotationStamping:
        NotateXpress1.SetText g_layer, NX_StampTool, "The time is: " & Time

    Case NX_AnnotationAdded:
    
        If (bAutoSelect) Then
            NotateXpress1.PrgSetItemSelected g_layer, lData, True
            NotateXpress1.Tool = NX_PointerTool
        Else
            ' Clear the selection.
            g_sequenceNumber = 0
        End If

    Case NX_ItemRightClick:
        Output.Caption = "Element " & CStr(lData) & " right click"

    Case NX_AnnotationDeleted:
        ' Clear the selection.
        g_sequenceNumber = 0

    Case NX_AnnotationMoved:
        If (g_updateMoved) Then
            Dim l As Long, t As Long, r As Long, b As Long
            NotateXpress1.PrgGetItemRect g_layer, lData, l, t, r, b
            Output.Caption = "Element " & CStr(lData) & " moved to " & CStr(l) & "," & CStr(t) & "," & CStr(r) & "," & CStr(b)
        End If

    Case NX_ToolChange:

    End Select
End Sub

Private Sub NotateXpress1_Click(ByVal layerHandle As Long, ByVal seqNum As Long)
    Output.Caption = "Mouse Click: layer " + CStr(layerHandle) + ", ID = " + CStr(seqNum)
End Sub

Private Sub NotateXpress1_CurrentLayerChange(ByVal newLayer As Long)
    g_layer = newLayer
    g_sequenceNumber = 0
End Sub

Private Sub NotateXpress1_MouseDown(ByVal layerHandle As Long, ByVal seqNum As Long, ByVal button As Integer, ByVal shift As Integer, ByVal x As Long, ByVal y As Long)
    Output.Caption = "Mouse Down: Button = " & CStr(button) & ", Shift = " & CStr(shift) & ",  x = " & CStr(x) & ", y = " & CStr(y)
End Sub

Private Sub NotateXpress1_MouseUp(ByVal layerHandle As Long, ByVal seqNum As Long, ByVal button As Integer, ByVal shift As Integer, ByVal x As Long, ByVal y As Long)
    Output.Caption = "Mouse Up: Button = " & CStr(button) & ", Shift = " & CStr(shift) & ",  x = " & CStr(x) & ", y = " & CStr(y)
End Sub

Private Sub NotateXpress1_RequestLayerPassword(ByVal layerName As String, password As String)
    password = InputBox("Password for layer " + layerName, ", please:")
End Sub



Private Sub ZoomIn_Click()
    ImagXpress1.ZoomFactor = ImagXpress1.ZoomFactor * (4 / 3)
End Sub

Private Sub ZoomOut_Click()
    ImagXpress1.ZoomFactor = ImagXpress1.ZoomFactor * (3 / 4)
End Sub

Private Sub SetStuffMenuTags()
    Dim itemAttributesInMenuOrder(0 To 14) As Long
    itemAttributesInMenuOrder(0) = NX_Text
    itemAttributesInMenuOrder(1) = NX_BackColor
    itemAttributesInMenuOrder(2) = NX_PenColor
    itemAttributesInMenuOrder(3) = NX_PenWidth
    itemAttributesInMenuOrder(4) = NX_BevelShadowColor
    itemAttributesInMenuOrder(5) = NX_BevelLightColor
    itemAttributesInMenuOrder(6) = NX_Logfont
    itemAttributesInMenuOrder(7) = NX_Text
    itemAttributesInMenuOrder(8) = NX_HighlightBack
    itemAttributesInMenuOrder(9) = NX_HighlightFill
    itemAttributesInMenuOrder(10) = NX_Moveable
    itemAttributesInMenuOrder(11) = NX_Sizeable
    itemAttributesInMenuOrder(12) = NX_Logfont
    itemAttributesInMenuOrder(13) = NX_DibPtr
    itemAttributesInMenuOrder(14) = NX_Fixed
    

    Dim index As Integer
    For index = 0 To 13
        g_stuffMenuItems(index).itemID = GetMenuItemID(hStuffMenu, index)
        g_stuffMenuItems(index).NXAttribute = itemAttributesInMenuOrder(index)
    Next
End Sub

Private Sub mnu_help_aboutnx_Click()
    NotateXpress1.AboutBox
End Sub

Private Sub mnu_help_aboutix_Click()
    ImagXpress1.AboutBox
End Sub


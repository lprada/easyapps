// ComSampDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ComSamp.h"
#include "ComSampDlg.h"
#include "mfc_menu.h"
#include <winspool.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include "..\..\..\..\..\..\imagxpress\v8.0\Samples\ActiveX-COM\VC++\Include\ix8_open.cpp"

/////////////////////////////////////////////////////////////////////////////
// CComSampDlg dialog

// generates a random number between 0 and constraint
int Random(int constraint)
{
	int n;
	div_t div_result;
	
	
	n = rand();
	div_result = div( n, constraint);
	return(div_result.rem);
	
}

// get default printer DC
HDC GetPrinterDC ()
{
	PRINTER_INFO_5 pinfo5[20];
	DWORD dwNeeded, dwReturned, dwPlatformID;
	HDC hdcPrint = (HDC)NULL;
	OSVERSIONINFO osVersion;
	
	
	osVersion.dwOSVersionInfoSize = sizeof (OSVERSIONINFO);
	GetVersionEx (&osVersion);
	dwPlatformID = osVersion.dwPlatformId;
	
	switch (dwPlatformID)
	{
		PRINTDLG printDlg;
		
	   case VER_PLATFORM_WIN32_WINDOWS:
		   if (EnumPrinters (PRINTER_ENUM_DEFAULT | PRINTER_ENUM_LOCAL, NULL, 5, (LPBYTE) pinfo5, sizeof (pinfo5), &dwNeeded, &dwReturned))
			   if (pinfo5[0].pPrinterName)
				   hdcPrint = CreateDC (NULL, pinfo5[0].pPrinterName, NULL, NULL);
			   break;
			   
	   case VER_PLATFORM_WIN32_NT:
		   ZeroMemory(&printDlg, sizeof(PRINTDLG));
		   printDlg.lStructSize = sizeof(PRINTDLG);
		   printDlg.Flags = PD_RETURNDEFAULT;
		   PrintDlg(&printDlg);
		   if (printDlg.hDevNames)
		   {
			   LPDEVNAMES lpDevNames = (LPDEVNAMES) GlobalLock(printDlg.hDevNames);
			   if (lpDevNames)
               {
				   if (lpDevNames->wDefault)
				   {
					   LPSTR lpPrinterName = (LPSTR) (((LPBYTE)lpDevNames)+lpDevNames->wDeviceOffset);
					   
					   if (lpPrinterName)
						   hdcPrint = CreateDC(NULL, lpPrinterName, NULL, NULL);
				   }
				   GlobalUnlock(printDlg.hDevNames);
				   GlobalFree(printDlg.hDevNames);
               }
		   }
		   
		   break;
	}
	
	
	return hdcPrint;
}

CComSampDlg::CComSampDlg(CWnd* pParent /*=NULL*/)
: CDialog(CComSampDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CComSampDlg)
	// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CComSampDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CComSampDlg)
	// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CComSampDlg, CDialog)
//{{AFX_MSG_MAP(CComSampDlg)
ON_WM_PAINT()
ON_WM_QUERYDRAGICON()
ON_WM_DESTROY()
ON_COMMAND(ID_FILE_LOADIMAGE, OnFileLoadimage)
ON_COMMAND(ID_FILE_LOADANNOTATION, OnFileLoadannotation)
ON_WM_INITMENUPOPUP()
ON_COMMAND(ID_FILE_SAVEANNOTATION, OnFileSaveannotation)
ON_COMMAND(ID_FILE_PRINT, OnFilePrint)
ON_COMMAND(ID_FILE_EXIT, OnFileExit)
ON_COMMAND(ID_LAYERS_CREATELAYER, OnLayersCreatelayer)
ON_COMMAND(ID_LAYERS_DELETELAYER, OnLayersDeletelayer)
ON_COMMAND(ID_LAYERS_VISIBLE, OnLayersVisible)
ON_COMMAND(ID_LAYERS_ACTIVE, OnLayersActive)
ON_UPDATE_COMMAND_UI(ID_LAYERS_VISIBLE, OnUpdateLayersVisible)
ON_COMMAND(ID_LAYERS_CLEAR, OnLayersClear)
ON_UPDATE_COMMAND_UI(ID_LAYERS_DELETELAYER, OnUpdateLayersDeletelayer)
ON_UPDATE_COMMAND_UI(ID_LAYERS_ACTIVE, OnUpdateLayersActive)
ON_UPDATE_COMMAND_UI(ID_LAYERS_CLEAR, OnUpdateLayersClear)
ON_UPDATE_COMMAND_UI(ID_FILE_SAVEANNOTATION, OnUpdateFileSaveannotation)
ON_COMMAND(ID_OPTIONS_BRANDANNOTATIONS, OnOptionsBrandannotations)
ON_UPDATE_COMMAND_UI(ID_OPTIONS_BRANDANNOTATIONS, OnUpdateOptionsBrandannotations)
ON_COMMAND(ID_PROGRAMMABLE_APPROVEIT, OnProgrammableApproveit)
ON_COMMAND(ID_PROGRAMMABLE_CREATEOBJECTS, OnProgrammableCreateobjects)
ON_COMMAND(ID_PROGRAMMABLE_CREATEHIGHLIGHT, OnProgrammableCreatehighlight)
ON_COMMAND(ID_PROGRAMMABLE_ITEMIZE, OnProgrammableItemize)
ON_UPDATE_COMMAND_UI(ID_PROGRAMMABLE_ITEMIZE, OnUpdateProgrammableItemize)
ON_COMMAND(ID_OPTIONS_TOOLPALETTE, OnOptionsToolpalette)
ON_UPDATE_COMMAND_UI(ID_OPTIONS_TOOLPALETTE, OnUpdateOptionsToolpalette)
ON_COMMAND(ID_SETSTAMP, OnSetstamp)
ON_UPDATE_COMMAND_UI(ID_SETSTAMP, OnUpdateSetstamp)
ON_COMMAND(ID_AUTOSELECT, OnAutoselect)
ON_UPDATE_COMMAND_UI(ID_AUTOSELECT, OnUpdateAutoselect)
ON_COMMAND(ID_USEMLE, OnUsemle)
ON_UPDATE_COMMAND_UI(ID_USEMLE, OnUpdateUsemle)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()

void CComSampDlg::Build_Menus()
{
	// add submenu items that pop up when user right clicks on a tool in the tool palette!
	
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTPENCOLOR, 0, "Text pen color...", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTPENWIDTH, 0, "Text pen width", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTPENWIDTH, ID_TEXT_TEXTPENWIDTH_1, "1", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTPENWIDTH, ID_TEXT_TEXTPENWIDTH_2, "2", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTPENWIDTH, ID_TEXT_TEXTPENWIDTH_3, "3", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTPENWIDTH, ID_TEXT_TEXTPENWIDTH_4, "4", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTPENWIDTH, ID_TEXT_TEXTPENWIDTH_5, "5", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTPENWIDTH, ID_TEXT_TEXTPENWIDTH_6, "6", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTPENWIDTH, ID_TEXT_TEXTPENWIDTH_7, "7", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTPENWIDTH, ID_TEXT_TEXTPENWIDTH_8, "8", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTPENWIDTH, ID_TEXT_TEXTPENWIDTH_9, "9", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTPENWIDTH, ID_TEXT_TEXTPENWIDTH_10, "10", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTFONT, 0, "Text pen font...", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_TextTool, ID_TEXT_TEXTBACKCOLOR, 0, "Text back color...", 0, 0 );
	
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLEPENCOLOR, 0, "Rectangle pen color...", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLEPENWIDTH, 0, "Rectangle pen width", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLEPENWIDTH, ID_RECTANGLEPENWIDTH_1, "1", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLEPENWIDTH, ID_RECTANGLEPENWIDTH_2, "2", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLEPENWIDTH, ID_RECTANGLEPENWIDTH_3, "3", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLEPENWIDTH, ID_RECTANGLEPENWIDTH_4, "4", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLEPENWIDTH, ID_RECTANGLEPENWIDTH_5, "5", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLEPENWIDTH, ID_RECTANGLEPENWIDTH_6, "6", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLEPENWIDTH, ID_RECTANGLEPENWIDTH_7, "7", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLEPENWIDTH, ID_RECTANGLEPENWIDTH_8, "8", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLEPENWIDTH, ID_RECTANGLEPENWIDTH_9, "9", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLEPENWIDTH, ID_RECTANGLEPENWIDTH_10, "10", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_RectangleTool, ID_RECTANGLE_FILLCOLOR, 0, "Rectangle fill color...", 0, 0 );
	
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_EllipseTool, ID_ELLIPSE_ELLIPSEPENCOLOR, 0, "Ellipse pen color...", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_EllipseTool, ID_ELLIPSE_ELLIPSEPENWIDTH, 0, "Ellipse pen width", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_EllipseTool, ID_ELLIPSE_ELLIPSEPENWIDTH, ID_ELLIPSE_ELLIPSEPENWIDTH_1, "1", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_EllipseTool, ID_ELLIPSE_ELLIPSEPENWIDTH, ID_ELLIPSE_ELLIPSEPENWIDTH_2, "2", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_EllipseTool, ID_ELLIPSE_ELLIPSEPENWIDTH, ID_ELLIPSE_ELLIPSEPENWIDTH_3, "3", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_EllipseTool, ID_ELLIPSE_ELLIPSEPENWIDTH, ID_ELLIPSE_ELLIPSEPENWIDTH_4, "4", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_EllipseTool, ID_ELLIPSE_ELLIPSEPENWIDTH, ID_ELLIPSE_ELLIPSEPENWIDTH_5, "5", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_EllipseTool, ID_ELLIPSE_ELLIPSEPENWIDTH, ID_ELLIPSE_ELLIPSEPENWIDTH_6, "6", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_EllipseTool, ID_ELLIPSE_ELLIPSEPENWIDTH, ID_ELLIPSE_ELLIPSEPENWIDTH_7, "7", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_EllipseTool, ID_ELLIPSE_ELLIPSEPENWIDTH, ID_ELLIPSE_ELLIPSEPENWIDTH_8, "8", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_EllipseTool, ID_ELLIPSE_ELLIPSEPENWIDTH, ID_ELLIPSE_ELLIPSEPENWIDTH_9, "9", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_EllipseTool, ID_ELLIPSE_ELLIPSEPENWIDTH, ID_ELLIPSE_ELLIPSEPENWIDTH_10, "10", 0, 0 );
	
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolygonTool, ID_POLYGON_POLYGONFILLCOLOR, 0, "Polygon fill color...", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolygonTool, ID_POLYGON_POLYGONPENWIDTH, 0, "Polygon pen width", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolygonTool, ID_POLYGON_POLYGONPENWIDTH, ID_POLYGON_POLYGONPENWIDTH_1, "1", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolygonTool, ID_POLYGON_POLYGONPENWIDTH, ID_POLYGON_POLYGONPENWIDTH_2, "2", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolygonTool, ID_POLYGON_POLYGONPENWIDTH, ID_POLYGON_POLYGONPENWIDTH_3, "3", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolygonTool, ID_POLYGON_POLYGONPENWIDTH, ID_POLYGON_POLYGONPENWIDTH_4, "4", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolygonTool, ID_POLYGON_POLYGONPENWIDTH, ID_POLYGON_POLYGONPENWIDTH_5, "5", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolygonTool, ID_POLYGON_POLYGONPENWIDTH, ID_POLYGON_POLYGONPENWIDTH_6, "6", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolygonTool, ID_POLYGON_POLYGONPENWIDTH, ID_POLYGON_POLYGONPENWIDTH_7, "7", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolygonTool, ID_POLYGON_POLYGONPENWIDTH, ID_POLYGON_POLYGONPENWIDTH_8, "8", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolygonTool, ID_POLYGON_POLYGONPENWIDTH, ID_POLYGON_POLYGONPENWIDTH_9, "9", 0, 0 );
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolygonTool, ID_POLYGON_POLYGONPENWIDTH, ID_POLYGON_POLYGONPENWIDTH_10, "10", 0, 0 );
	
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolyLineTool, ID_POLYLINE_POLYLINEPENCOLOR, 0, "Polyline pen color...", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolyLineTool, ID_POLYLINE_POLYLINEPENWIDTH, 0, "Polyline pen width", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolyLineTool, ID_POLYLINE_POLYLINEPENWIDTH, ID_POLYLINE_POLYLINEPENWIDTH_1, "1", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolyLineTool, ID_POLYLINE_POLYLINEPENWIDTH, ID_POLYLINE_POLYLINEPENWIDTH_2, "2", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolyLineTool, ID_POLYLINE_POLYLINEPENWIDTH, ID_POLYLINE_POLYLINEPENWIDTH_3, "3", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolyLineTool, ID_POLYLINE_POLYLINEPENWIDTH, ID_POLYLINE_POLYLINEPENWIDTH_4, "4", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolyLineTool, ID_POLYLINE_POLYLINEPENWIDTH, ID_POLYLINE_POLYLINEPENWIDTH_5, "5", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolyLineTool, ID_POLYLINE_POLYLINEPENWIDTH, ID_POLYLINE_POLYLINEPENWIDTH_6, "6", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolyLineTool, ID_POLYLINE_POLYLINEPENWIDTH, ID_POLYLINE_POLYLINEPENWIDTH_7, "7", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolyLineTool, ID_POLYLINE_POLYLINEPENWIDTH, ID_POLYLINE_POLYLINEPENWIDTH_8, "8", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolyLineTool, ID_POLYLINE_POLYLINEPENWIDTH, ID_POLYLINE_POLYLINEPENWIDTH_9, "9", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_PolyLineTool, ID_POLYLINE_POLYLINEPENWIDTH, ID_POLYLINE_POLYLINEPENWIDTH_10, "10", 0, 0 );  
	
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_LineTool, ID_LINE_LINEPENCOLOR, 0, "Line pen color...", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_LineTool, ID_LINE_LINEPENWIDTH, 0, "Line pen width", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_LineTool, ID_LINE_LINEPENWIDTH, ID_LINE_LINEPENWIDTH_1, "1", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_LineTool, ID_LINE_LINEPENWIDTH, ID_LINE_LINEPENWIDTH_2, "2", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_LineTool, ID_LINE_LINEPENWIDTH, ID_LINE_LINEPENWIDTH_3, "3", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_LineTool, ID_LINE_LINEPENWIDTH, ID_LINE_LINEPENWIDTH_4, "4", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_LineTool, ID_LINE_LINEPENWIDTH, ID_LINE_LINEPENWIDTH_5, "5", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_LineTool, ID_LINE_LINEPENWIDTH, ID_LINE_LINEPENWIDTH_6, "6", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_LineTool, ID_LINE_LINEPENWIDTH, ID_LINE_LINEPENWIDTH_7, "7", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_LineTool, ID_LINE_LINEPENWIDTH, ID_LINE_LINEPENWIDTH_8, "8", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_LineTool, ID_LINE_LINEPENWIDTH, ID_LINE_LINEPENWIDTH_9, "9", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_LineTool, ID_LINE_LINEPENWIDTH, ID_LINE_LINEPENWIDTH_10, "10", 0, 0 );  
	
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_FreehandTool, ID_FREEHAND_FREEHANDPENCOLOR, 0, "Freehand pen color...", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_FreehandTool, ID_FREEHAND_FREEHANDPENWIDTH, 0, "Freehand pen width", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_FreehandTool, ID_FREEHAND_FREEHANDPENWIDTH, ID_FREEHAND_FREEHANDPENWIDTH_1, "1", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_FreehandTool, ID_FREEHAND_FREEHANDPENWIDTH, ID_FREEHAND_FREEHANDPENWIDTH_2, "2", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_FreehandTool, ID_FREEHAND_FREEHANDPENWIDTH, ID_FREEHAND_FREEHANDPENWIDTH_3, "3", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_FreehandTool, ID_FREEHAND_FREEHANDPENWIDTH, ID_FREEHAND_FREEHANDPENWIDTH_4, "4", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_FreehandTool, ID_FREEHAND_FREEHANDPENWIDTH, ID_FREEHAND_FREEHANDPENWIDTH_5, "5", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_FreehandTool, ID_FREEHAND_FREEHANDPENWIDTH, ID_FREEHAND_FREEHANDPENWIDTH_6, "6", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_FreehandTool, ID_FREEHAND_FREEHANDPENWIDTH, ID_FREEHAND_FREEHANDPENWIDTH_7, "7", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_FreehandTool, ID_FREEHAND_FREEHANDPENWIDTH, ID_FREEHAND_FREEHANDPENWIDTH_8, "8", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_FreehandTool, ID_FREEHAND_FREEHANDPENWIDTH, ID_FREEHAND_FREEHANDPENWIDTH_9, "9", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_FreehandTool, ID_FREEHAND_FREEHANDPENWIDTH, ID_FREEHAND_FREEHANDPENWIDTH_10, "10", 0, 0 );  
	
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPTEXTCOLOR, 0, "Stamp text color...", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPBACKCOLOR, 0, "Stamp back color...", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPPENCOLOR, 0, "Stamp pen color...", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPPENWIDTH, 0, "Stamp pen width", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPPENWIDTH, ID_STAMP_STAMPPENWIDTH_1, "1", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPPENWIDTH, ID_STAMP_STAMPPENWIDTH_2, "2", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPPENWIDTH, ID_STAMP_STAMPPENWIDTH_3, "3", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPPENWIDTH, ID_STAMP_STAMPPENWIDTH_4, "4", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPPENWIDTH, ID_STAMP_STAMPPENWIDTH_5, "5", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPPENWIDTH, ID_STAMP_STAMPPENWIDTH_6, "6", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPPENWIDTH, ID_STAMP_STAMPPENWIDTH_7, "7", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPPENWIDTH, ID_STAMP_STAMPPENWIDTH_8, "8", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPPENWIDTH, ID_STAMP_STAMPPENWIDTH_9, "9", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPPENWIDTH, ID_STAMP_STAMPPENWIDTH_10, "10", 0, 0 );  
	pNotateXpress1->MenuAddItem( NX_Menu_Toolbar, NX_StampTool, ID_STAMP_STAMPFONT, 0, "Stamp font...", 0, 0 );  
	
	pNotateXpress1->MenuSetEnabled( NX_Menu_Toolbar, NX_TextTool, TRUE );
	
}
/////////////////////////////////////////////////////////////////////////////
// CComSampDlg message handlers

BOOL CComSampDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	RECT rect;
	
	srand( (unsigned)time( NULL ) );
	
	// Create an ImagXpress instance
	HINSTANCE hDLL = IX_Open();
	
	GetClientRect(&rect);
	ppCImagXpress1 = new CImagXpress((DWORD)this, 1, (long)m_hWnd, 10, 30, rect.right-20, rect.bottom-60);
	pImagXpress1 = ppCImagXpress1->pImagXpress;
	// From now on, the library will be referenced by the new 
	// ImagXpress COM instance, so we can do a FreeLibrary to remove our 
	// LoadLibrary reference.  NOTE:  Do not call FreeLibrary before 
	// you create the first "new" ImagXpress object.
	IX_Close(hDLL);
	
	// ImagXpress Initialization
	if (pImagXpress1)
	{
		pImagXpress1->BorderType = BORD_None;		
		pImagXpress1->ScrollBars = SB_Both;
		pImagXpress1->FileName = "..\\..\\..\\..\\..\\..\\Common\\Images\\benefits.tif";
	}	
	
	// NotateXpress Initialization
	
	ppCNotateXpress1 = new CNotateXpress((DWORD)this, 1);
	pNotateXpress1 = ppCNotateXpress1->pNotateXpress;
	
	// Setup the annotation link between NotateXpress and ImagXpress
	//   this call should always be balanced by a
	//   SetClientWindow(0) to prevent memory leaks. See the
	//   OnCancel method
	
	pNotateXpress1->SetClientWindow(pImagXpress1->hWnd);
	
	
	// map the NotateXpress LayerChange Event to our LayerChangeEvent method
	// if you're using this to keep track of the current layer, make sure you
	//    map this event BEFORE you create a layer	
	ppCNotateXpress1->SetCurrentLayerChangeEvent(LayerChangeEvent);
	
	// Create a layer so it will be available on start	
	pNotateXpress1->CreateLayer();
	
	// totLayers keeps track of how many layers have been created
	//   so I can give each layer a distinctive name	
	totLayers = 1;
	pNotateXpress1->SetLayerName(currentLayer, L"Layer 1");
	
	// just to help you distinquish your layers I'll set the stamp text
	//   to be the name of this layer	
	pNotateXpress1->SetText(currentLayer, NX_StampTool, L"Layer 1");
	
	// set this layer active -- by default, new layers are created inactive	
	pNotateXpress1->Active = VARIANT_TRUE;
	
	// create a programmable element
	//   programmable elements are used to create annotation items programmatically
	NXProgElem = pNotateXpress1->ElementCreate();
	
	// map the NotateXpress AnnotateEvent to our AnnotationEvent method
	ppCNotateXpress1->SetAnnotateEventEvent(AnnotationEvent);
	
	// create menus to pop up on right click of tool palette buttons!	
	Build_Menus();
	
	//ppCNotateXpress1->SetPaletteMenuSelectEvent(PaletteMenuSelectEvent);
	//ppCNotateXpress1->S
	//
	ppCNotateXpress1->SetMenuSelectEvent(NotateMenuSelectEvent);

	
	// center me in the screen!	
	RECT meRect, deskRect, palRect;	
	::GetWindowRect(m_hWnd, &meRect);
	::GetWindowRect(::GetDesktopWindow(), &deskRect);	
	MoveWindow((deskRect.right - (meRect.right - meRect.left))/2, (deskRect.bottom - (meRect.bottom - meRect.top))/2, meRect.right-meRect.left, meRect.bottom - meRect.top);
	
	// lets set the tool palette visible and locate it right next to me	
	HWND hWndTool;	
	hWndTool = (HWND) pNotateXpress1->ToolbarHwnd;
	::GetWindowRect(hWndTool, &palRect);
	::GetWindowRect(m_hWnd, &meRect);  // remember, i got moved!	
	::MoveWindow(hWndTool, meRect.right+3, meRect.top, palRect.right-palRect.left, palRect.bottom-palRect.top, FALSE);
	
	//pNotateXpress1->PutEnableContextMenu(VARIANT_TRUE);
	//pNotateXpress1->put_EnableContextMenu(VARIANT_TRUE);
	//**********************************************************************************
	pNotateXpress1->MenuSetEnabled(NX_Menu_Context,NX_NoTool,VARIANT_TRUE);
	
	pNotateXpress1->ToolbarSetVisible(currentLayer, TRUE);
	
	// bSetupStamp tracks whether to setup the stamp tool from a selected text element
	bSetupStamp = FALSE;
	
	pNotateXpress1->PrgSetImageMappingDimensions(pImagXpress1->IWidth, pImagXpress1->IHeight);
	
	return TRUE;  // Let the system set the focus
	
	
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CComSampDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting
		
		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);
		
		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;
		
		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CComSampDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


HRESULT CComSampDlg::NotateMenuSelectEvent(DWORD classPtr, DWORD objID, NX_MenuType Menu, NX_AnnotationTool Tool, long menuID, long subMenuID, long User1, long User2, long layerHandle, long sequenceNumber) 
{
	
	long layer;
	CComSampDlg *dlgPtr = (CComSampDlg*)classPtr;
	
	
	layer = dlgPtr->pNotateXpress1->CurrentLayer;
	switch (menuID)
	{
	case ID_RECTANGLEPENCOLOR:
		dlgPtr->pNotateXpress1->UserColorSelect(layer, NX_RectangleTool, NX_PenColor, (long)dlgPtr->m_hWnd);
		break;
		
	case ID_RECTANGLEPENWIDTH:			
		dlgPtr->pNotateXpress1->SetAttributeWidth(layer, NX_RectangleTool, NX_PenWidth, subMenuID - ID_RECTANGLEPENWIDTH_1 +1);
		break;
		
	case ID_RECTANGLE_FILLCOLOR:
		dlgPtr->pNotateXpress1->UserColorSelect(layer, NX_RectangleTool, NX_FillColor, (long)dlgPtr->m_hWnd);
		break;
		
	case ID_FREEHAND_FREEHANDPENCOLOR:
		dlgPtr->pNotateXpress1->UserColorSelect(layer, NX_FreehandTool, NX_PenColor, (long)dlgPtr->m_hWnd);
		break;		
		
	case ID_FREEHAND_FREEHANDPENWIDTH:
		dlgPtr->pNotateXpress1->SetAttributeWidth(layer, NX_FreehandTool, NX_PenWidth, subMenuID - ID_FREEHAND_FREEHANDPENWIDTH_1 +1);
		break;
		
	case ID_ELLIPSE_ELLIPSEPENCOLOR:    
		dlgPtr->pNotateXpress1->UserColorSelect(layer, NX_EllipseTool, NX_PenColor, (long)dlgPtr->m_hWnd);
		break;
		
	case ID_ELLIPSE_ELLIPSEPENWIDTH:
		dlgPtr->pNotateXpress1->SetAttributeWidth(layer, NX_EllipseTool, NX_PenWidth, subMenuID - ID_ELLIPSE_ELLIPSEPENWIDTH_1 +1);
		break;		
		
	case ID_POLYGON_POLYGONFILLCOLOR:    
		dlgPtr->pNotateXpress1->UserColorSelect(layer, NX_PolygonTool, NX_FillColor, (long)dlgPtr->m_hWnd);
		break;		
		
	case ID_POLYGON_POLYGONPENWIDTH:  
		dlgPtr->pNotateXpress1->SetAttributeWidth(layer, NX_PolygonTool, NX_PenWidth, subMenuID - ID_POLYGON_POLYGONPENWIDTH_1 +1);
		break;
		
	case ID_POLYLINE_POLYLINEPENCOLOR:  
		dlgPtr->pNotateXpress1->UserColorSelect(layer, NX_PolyLineTool, NX_PenColor, (long)dlgPtr->m_hWnd);
		break;
		
	case ID_POLYLINE_POLYLINEPENWIDTH:
		dlgPtr->pNotateXpress1->SetAttributeWidth(layer, NX_PolyLineTool, NX_PenWidth, subMenuID - ID_POLYLINE_POLYLINEPENWIDTH_1 +1);
		break;		
		
	case ID_LINE_LINEPENCOLOR:        
		dlgPtr->pNotateXpress1->UserColorSelect(layer, NX_LineTool, NX_PenColor, (long)dlgPtr->m_hWnd);
		break;		
		
	case ID_LINE_LINEPENWIDTH:        
		dlgPtr->pNotateXpress1->SetAttributeWidth(layer, NX_LineTool, NX_PenWidth, subMenuID - ID_LINE_LINEPENWIDTH_1 +1);
		break;
		
	case ID_STAMP_STAMPTEXTCOLOR:        
		dlgPtr->pNotateXpress1->UserColorSelect(layer, NX_StampTool, NX_TextColor, (long)dlgPtr->m_hWnd);
		break;
		
	case ID_STAMP_STAMPBACKCOLOR:        
		dlgPtr->pNotateXpress1->UserColorSelect(layer, NX_StampTool, NX_BackColor, (long)dlgPtr->m_hWnd);
		break;
		
	case ID_STAMP_STAMPPENCOLOR:         
		dlgPtr->pNotateXpress1->UserColorSelect(layer, NX_StampTool, NX_PenColor, (long)dlgPtr->m_hWnd);
		break;		
		
	case ID_STAMP_STAMPPENWIDTH:  
		dlgPtr->pNotateXpress1->SetAttributeWidth(layer, NX_StampTool, NX_PenWidth, subMenuID - ID_STAMP_STAMPPENWIDTH_1 +1);
		break;
		
	case ID_STAMP_STAMPFONT:
		dlgPtr->pNotateXpress1->UserFontSelect(layer, NX_StampTool, (long)dlgPtr->m_hWnd);
		break;
		
	case ID_TEXT_TEXTPENCOLOR:
		dlgPtr->pNotateXpress1->UserColorSelect(layer, NX_TextTool, NX_PenColor, (long)dlgPtr->m_hWnd);
		break;
		
	case ID_TEXT_TEXTFONT:
		dlgPtr->pNotateXpress1->UserFontSelect(layer, NX_TextTool, (long)dlgPtr->m_hWnd);
		break;
		
	case ID_TEXT_TEXTPENWIDTH:
		dlgPtr->pNotateXpress1->SetAttributeWidth(layer, NX_TextTool, NX_PenWidth, subMenuID - ID_TEXT_TEXTPENWIDTH_1 +1);
		break;
		
	case ID_TEXT_TEXTBACKCOLOR:        
		dlgPtr->pNotateXpress1->UserColorSelect(layer, NX_TextTool, NX_BackColor, (long)dlgPtr->m_hWnd);
		break;		
		
	}
	
	return S_OK;
	
}
//---------------------------------
// Annotation Event Handler
//   In this demo, we're only looking for the ToolPalette right click
//     so we can popup a tool context menu
//---------------------------------
HRESULT CComSampDlg::AnnotationEvent(DWORD instancePtr, DWORD objID, long lType, long lData)
{
	
	CComSampDlg *p = (CComSampDlg *) instancePtr;
	long objType;
	
	switch(lType) // event specifics
	{
		
	case NX_AnnotationSelected:{ // if it's a text annotation and bSetupStamp is True, we'll set the stamp tool like this text
		objType = p->pNotateXpress1->PrgGetItemType(p->currentLayer, lData);
		if (objType == NX_TextTool && p->bSetupStamp) 
		{
			long tempElement;
			// setup the stamp tool just like this text entry
			
			// create a temporary programmable element to receive
			//   the text attributes
			tempElement = p->pNotateXpress1->ElementCreate();
			// get the attributes
			p->pNotateXpress1->PrgGetItemAttributes(p->currentLayer, lData, tempElement);
			// now uses these attributes to set the same attributes to the StampTool
			p->pNotateXpress1->SetPenColor(p->currentLayer, NX_StampTool, p->pNotateXpress1->ElementGetPenColor(tempElement));
			p->pNotateXpress1->SetText(p->currentLayer, NX_StampTool, p->pNotateXpress1->ElementGetText(tempElement));
			p->pNotateXpress1->SetAttributeColor(p->currentLayer, NX_StampTool, NX_BackColor, p->pNotateXpress1->ElementGetBackColor(tempElement));
			p->pNotateXpress1->SetPenWidth(p->currentLayer, NX_StampTool, p->pNotateXpress1->ElementGetPenWidth(tempElement));
			p->pNotateXpress1->SetTextColor(p->currentLayer, NX_StampTool, p->pNotateXpress1->ElementGetTextColor(tempElement));
			p->pNotateXpress1->SetFont(p->currentLayer, NX_StampTool, p->pNotateXpress1->ElementGetFont(tempElement));
			p->pNotateXpress1->ElementDestroy(tempElement);
		}         
								}
		break;
		
	case NX_AnnotationAdded:{
		if (p->bAutoSelect)
		{
			p->pNotateXpress1->PrgSetItemSelected(p->currentLayer, lData, VARIANT_TRUE);
			p->pNotateXpress1->Tool = NX_PointerTool;
		}
							 }
		break;        
	}
	
	
	return S_OK;
}


// -----------------------------------------
// LayerChange Event handler
//-------------------------------------------
HRESULT CComSampDlg::LayerChangeEvent(DWORD instancePtr, DWORD objID, long newLayer)
{
	CComSampDlg *p = (CComSampDlg *) instancePtr;
	
	// this is all the code that's required to keep track of the current layer
	p->currentLayer = newLayer;
	// if you need to query anything about the current layer or do something
	//   special in your app when the layer change, here would be a likely
	//   place to do it.
	return S_OK;
}


void CComSampDlg::OnDestroy() 
{
	// unconnect NotateXpress
	pNotateXpress1->SetClientWindow(0);
	
	// destroy the programmable element
	pNotateXpress1->ElementDestroy(NXProgElem);
	
	pNotateXpress1 = NULL;
	delete ppCNotateXpress1;
	
	pImagXpress1 = NULL;
	delete ppCImagXpress1;	
	
	CDialog::OnDestroy();	
}

void CComSampDlg::OnFileLoadimage() 
{
	OPENFILENAME ofn;
	char szSource[_MAX_PATH+1];
	
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ZeroMemory(szSource, sizeof(szSource));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.lpstrFilter = "JPEG files (*.jpg)\0*.jpg\0All files (*.*)\0*.*\0\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szSource;
	ofn.nMaxFile = _MAX_PATH;
	ofn.lpstrTitle = "Load image"; ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.nFileOffset = 0; ofn.nFileExtension = 0;
	ofn.lCustData = 0L; ofn.lpfnHook = NULL;
	ofn.lpTemplateName = NULL; ofn.lpstrDefExt = "jpg";
	if (GetOpenFileName(&ofn))
	{
		pImagXpress1->PutFileName(szSource);
		pNotateXpress1->PrgSetImageMappingDimensions(pImagXpress1->IWidth, pImagXpress1->IHeight);
	}	
}

void CComSampDlg::OnFileLoadannotation() 
{
	OPENFILENAME ofn;
	char szSource[_MAX_PATH+1];
	
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ZeroMemory(szSource, sizeof(szSource));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.lpstrFilter = "NotateXpress files (*.nxp)\0*.nxp\0All files (*.*)\0*.*\0\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szSource;
	ofn.nMaxFile = _MAX_PATH;
	ofn.lpstrTitle = "Load annotation"; ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;
	ofn.lpTemplateName = NULL; ofn.lpstrDefExt = "nxp";
	if (GetOpenFileName(&ofn))
		pNotateXpress1->RetrieveAnnotation(szSource);
	
}




void CComSampDlg::OnFileSaveannotation() 
{
	OPENFILENAME ofn;
	char szSource[_MAX_PATH+1];
	
	ZeroMemory(&ofn, sizeof(OPENFILENAME));
	ZeroMemory(szSource, sizeof(szSource));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.lpstrFilter = "NotateXpress files (*.nxp)\0*.nxp\0All files (*.*)\0*.*\0\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFile = szSource;
	ofn.nMaxFile = _MAX_PATH;
	ofn.lpstrTitle = "Save annotation"; ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_HIDEREADONLY;
	ofn.lpTemplateName = NULL; ofn.lpstrDefExt = "nxp";
	if (GetSaveFileName(&ofn))
		pNotateXpress1->StoreAnnotation(szSource);
	
}

void CComSampDlg::OnFilePrint() 
{
	HANDLE hPrinter;
	
	// Compute the Page Width & Height
	//   we'll assume here 8 1/2 x 11 paper
	int TwipsPerInch = 1440;
	int PageWidth  = 8 * TwipsPerInch;
	int PageHeight = int(10.5 * (float)TwipsPerInch);
	
	// Get the Printer DC
	
	HDC hdcPrinter = GetPrinterDC();
	if (!hdcPrinter)
		return ;
	
	// Set the Print area and center the image  
	pImagXpress1->PrinterWidth = (4 * TwipsPerInch);  // 4 Inches Wide
	pImagXpress1->PrinterHeight = (3 * TwipsPerInch); // 3 Inches High
	pImagXpress1->PrinterLeft = ((PageWidth-pImagXpress1->GetPrinterWidth())/2);  // Center the image
	pImagXpress1->PrinterTop = ((PageHeight-pImagXpress1->GetPrinterHeight())/2); // Center the Image
	
	// Open the Printer
	OpenPrinter(NULL, &hPrinter, NULL);
	DOCINFO dInfo;
	memset ((PVOID)&dInfo, 0, sizeof (DOCINFO));
	dInfo.cbSize = sizeof (DOCINFO);
	dInfo.lpszDocName = NULL;
	StartDoc(hdcPrinter, &dInfo);
	StartPage(hdcPrinter);
	
	// Print the image
	//pImagXpress1->PutPrinterhDC((long)hdcPrinter);
	pImagXpress1->PrinterhDC = ((long)hdcPrinter);
	
	// Cleanup/Close Printer
	EndPage(hdcPrinter);
	EndDoc(hdcPrinter);
	ClosePrinter(hPrinter);	
}

void CComSampDlg::OnFileExit() 
{
	// we're done
	EndDialog(0);	
}

void CComSampDlg::OnLayersCreatelayer() 
{
	CComBSTR cbstr;
	char szLayer[8];
	BSTR regBstr;
	
	// create the layer and set it active and the tool palette visible
	pNotateXpress1->CreateLayer();
	pNotateXpress1->Active = VARIANT_TRUE;
	//pNotateXpress1->SetToolPaletteVisible(currentLayer, TRUE);
	//***************************************************************************************************
	pNotateXpress1->MenuSetEnabled(NX_Menu_Toolbar,NX_NoTool, TRUE);
	// increment count of layers to make a distinct name
	totLayers++;
	cbstr = L"Layer ";
	wsprintf(szLayer, "%d", totLayers);
	cbstr.Append(szLayer);
	regBstr = cbstr;
	
	pNotateXpress1->SetLayerName(currentLayer, regBstr);
	// just to help you distinquish your layers I'll set the stamp text
	//   to be the name of this layer
	pNotateXpress1->SetText(currentLayer, NX_StampTool, regBstr);
	
	
	
}

void CComSampDlg::OnLayersDeletelayer() 
{
	pNotateXpress1->DeleteLayer(currentLayer);	
}

void CComSampDlg::OnLayersVisible() 
{
	VARIANT_BOOL bVisible;
	
	bVisible = pNotateXpress1->Visible;
	
	if (bVisible)
		bVisible = VARIANT_FALSE;
	else
		bVisible = VARIANT_TRUE;
	pNotateXpress1->Visible = bVisible;	
}

void CComSampDlg::OnUpdateLayersVisible(CCmdUI* pCmdUI) 
{
	if(currentLayer!=0)
	{
		pCmdUI->Enable(TRUE);
		if(pNotateXpress1->Visible) pCmdUI->SetText("Hide Current Layer");
		else pCmdUI->SetText("Show Current Layer");
	}
	else
		pCmdUI->Enable(FALSE);
}

void CComSampDlg::OnLayersActive() 
{
	VARIANT_BOOL bActive;
	
	bActive = pNotateXpress1->Active;
	
	if (bActive)
		bActive = VARIANT_FALSE;
	else
		bActive = VARIANT_TRUE;
	pNotateXpress1->Active = bActive;	
}

void CComSampDlg::OnUpdateLayersActive(CCmdUI* pCmdUI) 
{
	if(currentLayer!=0)
	{
		pCmdUI->Enable(TRUE);
		if(pNotateXpress1->Active) pCmdUI->SetText("Disable Current Layer");
		else pCmdUI->SetText("Enable Current Layer");
	}
	else
		pCmdUI->Enable(FALSE);
	
}

void CComSampDlg::OnLayersClear() 
{
	pNotateXpress1->Clear();	
}

void CComSampDlg::OnUpdateLayersDeletelayer(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable((currentLayer!=0));
}

void CComSampDlg::OnUpdateLayersClear(CCmdUI* pCmdUI) 
{
	pCmdUI->Enable((pNotateXpress1->NumLayers!=0));
}

void CComSampDlg::OnUpdateFileSaveannotation(CCmdUI* pCmdUI) 
{
	BOOL bSave = FALSE;
	long numItems, layer;
	
	if (pNotateXpress1->NumLayers != 0)
	{	               
		layer = pNotateXpress1->GetFirstLayer();
		while (layer) // pNotateXpress1->GetNextLayer() returns 0 when done iterating layers
		{
			numItems = pNotateXpress1->PrgGetItemCount(layer);
			// we'll only enable the Save annotation menu select
			//   when there's at least 1 item in 1 layer
			if (numItems)
				bSave = TRUE;
			
			layer = pNotateXpress1->GetNextLayer();
		}
	}
	
	pCmdUI->Enable(bSave);
	
}

void CComSampDlg::OnOptionsBrandannotations() 
{
	pNotateXpress1->Brand(24);	
}

void CComSampDlg::OnUpdateOptionsBrandannotations(CCmdUI* pCmdUI) 
{
	BOOL bBrand = FALSE;
	long numItems, layer;
	
	if (pNotateXpress1->NumLayers != 0)
	{	               
		layer = pNotateXpress1->GetFirstLayer();
		while (layer) // pNotateXpress1->GetNextLayer() returns 0 when done iterating layers
		{
			numItems = pNotateXpress1->PrgGetItemCount(layer);
			// we'll only enable the Save annotation menu select
			//   when there's at least 1 item in 1 layer
			if (numItems)
				if (pNotateXpress1->GetVisible(layer))
					bBrand = TRUE;
				
				layer = pNotateXpress1->GetNextLayer();
		}
	}
	
	pCmdUI->Enable(bBrand);
}

void CComSampDlg::OnProgrammableApproveit() 
{
    char userName[255];
    DWORD bufSize;
    CComPtr<IFontDisp> pFont;
    CComBSTR bstr;
    BSTR regBstr;
    long id, height,width;
    CY fontSize;
	
	
    height = pImagXpress1->IHeight;//rect.bottom - rect.top;
    width = pImagXpress1->IWidth;//rect.right - rect.left;
	//---------------------------------------
	// set the bounding rect top/left to the point
	//   we want the text to go
	//---------------------------------------
	// in version 1.5.0, only the top/left coordinates are used
	pNotateXpress1->ElementSetBoundingRect(NXProgElem, 100,100,width/4,height/4);
	
	//---------------------------------------
	// remember to set the element type to Text
	//---------------------------------------
	pNotateXpress1->ElementSetType (NXProgElem, NX_TextTool);
	//---------------------------------------
	// set various other properties
	//---------------------------------------
	pNotateXpress1->ElementSetHighlightBack (NXProgElem, TRUE);
	pNotateXpress1->ElementSetFillColor (NXProgElem, RGB(0, 0, 0));
	pNotateXpress1->ElementSetPenColor (NXProgElem, RGB(255, 0, 0));
	pNotateXpress1->ElementSetTextColor (NXProgElem, RGB(255, 0, 0));
	pNotateXpress1->ElementSetPenWidth (NXProgElem, 1);
	
	//---------------------------------------
	// set the element's font to the default Text font
	//---------------------------------------
	pFont = pNotateXpress1->GetDefaultTextFont();
	
	// just for fun we'll bump up the pointsize
	if (pFont)
	{
		CComQIPtr<IFont, &IID_IFont> p(pFont);
		if (p)
		{
			BOOL bItalic;
			
			p->get_Size(&fontSize);
			fontSize.Lo = fontSize.Lo + 20000; //bump by 2 points
			p->put_Size(fontSize);
			// and make it italic if it's not already
			p->get_Italic(&bItalic);
			if (!bItalic)
				p->put_Italic(TRUE);
		}
		
		pNotateXpress1->ElementSetFont (NXProgElem, pFont);
	}
	//---------------------------------------
	// get who you are
	//---------------------------------------
	bufSize =  sizeof(userName);
	GetUserName (userName, &bufSize);
	//---------------------------------------
	// and say you approved it!
	//---------------------------------------
	
	if (userName[0] == '\0')
		lstrcpy(userName, "Nobody");
	
	bstr = L"Approved by ";
	bstr.Append(userName);
	regBstr = bstr;
	
	pNotateXpress1->ElementSetText (NXProgElem, regBstr);
	//---------------------------------------
	// finally, append the element to the current layer
	//--------------------------------------
	id = pNotateXpress1->ElementAppend(NXProgElem);	
}

void CComSampDlg::OnProgrammableCreateobjects() 
{
	
	long top, left, right, bottom, x, y, e;
	//-----------------------------------------
	//-----------------------------------------
	
	top = left = 15;
	right = bottom = 115;
	
	
	e = pNotateXpress1->ElementCreate();
	
	pNotateXpress1->ElementSetPenWidth( e, 1);
	for (int i = 1; i <= 10; i++)
	{
		pNotateXpress1->ElementSetBoundingRect (e, top, left, right, bottom);
		pNotateXpress1->ElementSetPenColor (e, RGB(Random(255), Random(255), Random(255)));
		pNotateXpress1->ElementSetFillColor (e, RGB(Random(255), Random(255), Random(255)));
		if (i % 2) 
			pNotateXpress1->ElementSetType (e, NX_RectangleTool);
		else
			pNotateXpress1->ElementSetType (e, NX_EllipseTool);
		
		x = Random(400);
		y = Random(400);
		
		pNotateXpress1->ElementAppend (e);
		top = top + 2;
		left = left + 2;
		right = right + 2;
		bottom = bottom + 2;
	}
	
	pNotateXpress1->ElementDestroy (e);	
}

void CComSampDlg::OnProgrammableCreatehighlight() 
{
	long e;
	
	e = pNotateXpress1->ElementCreate();
	
	pNotateXpress1->ElementSetType(e, NX_RectangleTool);
	pNotateXpress1->ElementSetPenWidth( e, 1);
	pNotateXpress1->ElementSetBoundingRect (e, 10, 10, 100, 100);
	pNotateXpress1->ElementSetPenColor (e, RGB(Random(255), Random(255), Random(255)));
	pNotateXpress1->ElementSetFillColor (e, RGB(255, 255, 0));
	pNotateXpress1->ElementSetBackstyle(e, NX_Translucent);
	pNotateXpress1->ElementAppend (e);
	pNotateXpress1->ElementDestroy(e);	
}

void CComSampDlg::OnProgrammableItemize() 
{
	int i, numItems, polygons, ellipses, polylines, lines, frees, tboxes, stamps, rects, seqnum, itemType; 
	char szMessage[160];
	
	
	numItems = pNotateXpress1->PrgGetItemCount(currentLayer);
	
	polygons = ellipses = polylines = lines = frees = tboxes = rects = stamps = 0;
	
	// for each item
	//   get the item's layer-unique sequence number
	//   and then request the type of item
	for (i= 1; i <= numItems; i++)
	{
		seqnum = pNotateXpress1->PrgSequenceFromEntry(currentLayer, i);
		itemType = pNotateXpress1->PrgGetItemType(currentLayer, seqnum);
		switch(itemType)
		{
		case NX_FreehandTool:
            frees++;
            break;
			
		case NX_TextTool:
            tboxes++;
            break;
			
		case NX_RectangleTool:
            rects++;
            break;
			
		case NX_LineTool:
            lines++;
            break;
			
		case NX_PolyLineTool:
            polylines++;
            break;
			
		case NX_PolygonTool:
            polygons++;
            break;
			
		case NX_EllipseTool:
            ellipses++;
            break;
			
		case NX_StampTool:
            stamps++;
            break;
			
		}
	}
	
	wsprintf(szMessage, "You have %d Line(s), %d Polyline(s), %d Polygon(s), %d Text(s), %d Rectangle(s), %d Ellipse(s), %d Freehand(s), and %d Stamp(s)", lines, polylines, polygons, tboxes, rects, ellipses, frees, stamps);   
	MessageBox(szMessage);   	
}

void CComSampDlg::OnUpdateProgrammableItemize(CCmdUI* pCmdUI) 
{
	if( currentLayer != 0)
	{
		pCmdUI->Enable( (pNotateXpress1->PrgGetItemCount(currentLayer)!=0) );
	}
	else
		pCmdUI->Enable( FALSE );	
}

void CComSampDlg::OnOptionsToolpalette() 
{
	if( currentLayer !=0 )
	{
		VARIANT_BOOL bVisible;
		
		
		//*****************************************************************************************
		//bVisible = pNotateXpress1->GetToolPaletteVisible(currentLayer);
		//bVisible = pNotateXpress1->GetToolbarActivated();
		bVisible = pNotateXpress1->MenuGetItemVisible(NX_Menu_Toolbar,NX_NoTool,0,0);
		
		if (bVisible)
			bVisible = VARIANT_FALSE;
		else
			bVisible = VARIANT_TRUE;
		
		//******************************************************************************************
		//pNotateXpress1->SetToolPaletteVisible(currentLayer, bVisible);	
		pNotateXpress1->MenuSetItemVisible(NX_Menu_Toolbar,NX_NoTool,0,0,bVisible,FALSE);
	}
}

void CComSampDlg::OnUpdateOptionsToolpalette(CCmdUI* pCmdUI) 
{
	if( currentLayer != 0 )
	{
		pCmdUI->Enable( TRUE );
		
		//***********************************************************************************************
		/*if( pNotateXpress1->GetToolPaletteVisible(currentLayer) ) pCmdUI->SetCheck(1);
		else pCmdUI->SetCheck(0);*/		
		if(pNotateXpress1->MenuGetItemVisible(NX_Menu_Toolbar,NX_NoTool,0,0)) pCmdUI->SetCheck(1);
		else pCmdUI->SetCheck(0);
		
		
	}
}

void CComSampDlg::OnSetstamp() 
{
	bSetupStamp = !bSetupStamp;	
}

void CComSampDlg::OnUpdateSetstamp(CCmdUI* pCmdUI) 
{
	if(bSetupStamp) pCmdUI->SetCheck(1);
	else pCmdUI->SetCheck(0);
}

void CComSampDlg::OnAutoselect() 
{
	bAutoSelect = !bAutoSelect;	
}

void CComSampDlg::OnUpdateAutoselect(CCmdUI* pCmdUI) 
{
	if(bAutoSelect) pCmdUI->SetCheck(1);
	else pCmdUI->SetCheck(0);
}

void CComSampDlg::OnUsemle() 
{
	if (pNotateXpress1->MLECompatible)
		pNotateXpress1->MLECompatible = false;
	else
	{
		pNotateXpress1->MLECompatible = true;		
	}
	
}

void CComSampDlg::OnUpdateUsemle(CCmdUI* pCmdUI) 
{
	if (pNotateXpress1->MLECompatible) pCmdUI->SetCheck(1);	
	else pCmdUI->SetCheck(0);	
}

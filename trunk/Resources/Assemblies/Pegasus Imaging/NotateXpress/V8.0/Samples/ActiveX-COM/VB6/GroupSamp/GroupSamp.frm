VERSION 5.00
Object = "{83C34652-53A4-45F8-BAE4-B64AFCE2A5AF}#1.0#0"; "PegasusImaging.ActiveX.ImagXpress8.dll"
Object = "{A711DB49-6663-44B3-8804-487B8EA09BE1}#1.0#0"; "PegasusImaging.ActiveX.NotateXpress8.dll"
Begin VB.Form Group 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "NotateXpress 8 Groups Sample"
   ClientHeight    =   8310
   ClientLeft      =   150
   ClientTop       =   435
   ClientWidth     =   13305
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8310
   ScaleWidth      =   13305
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstInfo 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   840
      ItemData        =   "GroupSamp.frx":0000
      Left            =   120
      List            =   "GroupSamp.frx":000A
      TabIndex        =   2
      Top             =   120
      Width           =   12975
   End
   Begin VB.ListBox lstStatus 
      Height          =   2010
      Left            =   9360
      TabIndex        =   1
      Top             =   1800
      Width           =   3135
   End
   Begin PegasusImagingActiveXImagXpress8Ctl.ImagXpress Xpress 
      Height          =   6495
      Left            =   120
      TabIndex        =   0
      Top             =   1440
      Width           =   9135
      _ExtentX        =   16113
      _ExtentY        =   11456
      ErrStr          =   "FA4E77A979FB30D2235DFA505B11D146"
      ErrCode         =   466355488
      ErrInfo         =   -2135781102
      Persistence     =   -1  'True
      _cx             =   16113
      _cy             =   11456
      AutoSize        =   5
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      SaveTransparencyColor=   0
      OLEDropMode     =   0
      SaveTIFFCompression=   0
      SaveTransparent =   0
      SaveJPEGProgressive=   0   'False
      SaveJPEGGrayscale=   0   'False
      SaveJPEGLumFactor=   10
      SaveJPEGChromFactor=   10
      SaveJPEGSubSampling=   2
      ViewAntialias   =   -1  'True
      ViewDithered    =   -1  'True
      AlignH          =   1
      AlignV          =   1
      LoadRotated     =   0
      JPEGEnhDecomp   =   -1  'True
      WMFConvert      =   0   'False
      ProcessImageID  =   23
      OwnDIB          =   -1  'True
      FileTimeout     =   10000
      AsyncPriority   =   0
      ViewUpdate      =   -1  'True
      SavePDFCompression=   8
      ViewProgressive =   0   'False
      SaveTIFFByteOrder=   1
      FTPUserName     =   ""
      FTPPassword     =   ""
      ProxyServer     =   ""
      SaveEXIFThumbnailSize=   0
      SaveLJPPrediction=   1
      PDFSwapBlackandWhite=   0   'False
      SaveTIFFRowsPerStrip=   0
      TIFFIFDOffset   =   0
      ViewGrayMode    =   0
      SaveWSQQuant    =   1
      DisplayError    =   0   'False
      EvalMode        =   0
   End
   Begin VB.Label lblLoadStatus 
      Caption         =   "Load Status:"
      Height          =   375
      Left            =   9360
      TabIndex        =   5
      Top             =   1320
      Width           =   1815
   End
   Begin VB.Label lblLastError 
      Caption         =   "Last Error:"
      Height          =   495
      Left            =   9360
      TabIndex        =   4
      Top             =   4800
      Width           =   2415
   End
   Begin VB.Label lblerror 
      Height          =   2295
      Left            =   9360
      TabIndex        =   3
      Top             =   5520
      Width           =   3135
   End
   Begin PegasusImagingActiveXNotateXpress8Ctl.NotateXpress NX 
      Left            =   11880
      Top             =   1200
      _cx             =   847
      _cy             =   847
      Client          =   ""
      DefaultPenColor =   16711680
      DefaultFillColor=   12632256
      DefaultPenWidth =   1
      DefaultTextColor=   0
      DefaultTool     =   4103
      DefaultBackColor=   16777215
      BeginProperty DefaultTextFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty DefaultStampFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Subject         =   ""
      Author          =   ""
      Comments        =   ""
      Title           =   ""
      DefaultHighlightFill=   0   'False
      DefaultHighlightBack=   0   'False
      InteractMode    =   0
      SaveAnnotations =   -1  'True
      DefaultStampText=   ""
      BeginProperty DefaultButtonFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      DefaultButtonText=   ""
      PreserveWTLayers=   0   'False
      TIFFAnnotationType=   0
      AllowPaint      =   -1  'True
      UnicodeMode     =   0   'False
      NXPEditors      =   -1  'True
      LoadAnnotations =   -1  'True
      MLECompatible   =   0   'False
      FontScaling     =   0
      TextToolArrows  =   -1  'True
      RecalibrateXDPI =   -1
      RecalibrateYDPI =   -1
      BeginProperty DefaultRulerFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      RaiseExceptions =   -1  'True
      ErrorLevel      =   0
      Debug           =   0   'False
      DebugLogFile    =   "c:\NotateXpress8.log"
      ToolbarActivated=   -1  'True
      ToolbarLargeIcons=   -1  'True
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Load Image..."
         Shortcut        =   ^O
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "&Save Image..."
         Shortcut        =   ^S
      End
      Begin VB.Menu mnuFileOpenAnnotation 
         Caption         =   "LoadAnnotation(NXP)..."
      End
      Begin VB.Menu mnuFileSaveAnnotation 
         Caption         =   "Save Annotation(NXP)..."
      End
      Begin VB.Menu mnuFileExit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu mnuGroup 
      Caption         =   "Group"
      Begin VB.Menu mnuGroupPutSelected 
         Caption         =   "Put selected into group"
      End
      Begin VB.Menu mnuGroupAddAll 
         Caption         =   "Add all to group..."
         Begin VB.Menu mnuCircles 
            Caption         =   "Circles"
         End
         Begin VB.Menu mnuGroupAddFreehands 
            Caption         =   "Freehands"
         End
         Begin VB.Menu mnuGroupAddLines 
            Caption         =   "Lines"
         End
         Begin VB.Menu mnuGroupAddPolygons 
            Caption         =   "Polygons"
         End
         Begin VB.Menu mnuGroupAddPolylines 
            Caption         =   "PolyLines"
         End
         Begin VB.Menu mnuGroupAddRectangles 
            Caption         =   "Rectangles"
         End
         Begin VB.Menu mnuGroupAddTexts 
            Caption         =   "Texts"
         End
         Begin VB.Menu mnuRulers 
            Caption         =   "Rulers"
         End
      End
      Begin VB.Menu mnuGroupBar1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuGroupInvert 
         Caption         =   "Invert"
      End
      Begin VB.Menu mnuGroupMirror 
         Caption         =   "Mirror"
      End
      Begin VB.Menu mnuGroupBar2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuGroupRemoveAll 
         Caption         =   "Remove all from group"
      End
   End
   Begin VB.Menu mnuOptions 
      Caption         =   "&Options"
      Begin VB.Menu mnuOptionsLoadAnnotations 
         Caption         =   "Load annotations in Tif files"
      End
      Begin VB.Menu mnuOptionsWangCompatibility 
         Caption         =   "Wang compatibility"
      End
      Begin VB.Menu mnuOptionsYoked 
         Caption         =   "Group yoked"
      End
      Begin VB.Menu mnu_OptionsAllowUserGroupCreation 
         Caption         =   "AllowUserGrouping"
      End
   End
   Begin VB.Menu mnuAbout 
      Caption         =   "About"
   End
End
Attribute VB_Name = "Group"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Option Explicit
Dim curLayer As Long
Dim imgFileName As String
Dim imgParentDir As String

Private Sub Form_Load()
    
     Dim groupid As Long
     Xpress.ScrollBars = SB_Both
     
     
     Xpress.EventSetEnabled EVENT_PROGRESS, True
     Xpress.ScrollBars = SB_Both
     
     imgParentDir = App.Path
     imgFileName = imgParentDir & "\..\..\..\..\..\..\Common\Images\benefits.tif"
               
     NX.Client = "Xpress"
     LoadFile
     
     NX.ToolbarActivated = True
        
     If (NX.GrpGetFirstGroupID(curLayer) = 0) Then
       groupid = NX.GrpCreateGroup(curLayer, "", 0)
     End If
    
      
      ShowCustomToolbar
     
End Sub
Private Sub LoadFile()
        
    NX.Clear
            
    Xpress.FileName = imgFileName
    curLayer = NX.CreateLayer
        
    Err = Xpress.ImagError
    PegasusError Err, lblerror
    
    NX.ToolbarActivated = True
   
End Sub

Private Sub Form_Unload(Cancel As Integer)
 ' *Always* do this to force NotateXpress resource cleanup.
    NX.Client = ""
End Sub

Private Sub mnuAbout_Click()
    NX.AboutBox
End Sub

Private Sub Xpress_Progress(ByVal ImageID As Long, ByVal OperationID As Long, ByVal BytesProcessed As Long, ByVal TotalBytes As Long, ByVal PctDone As Long, ByVal bDone As Boolean, ByVal bAsync As Boolean, ByVal Error As Long)
    lstStatus.AddItem Str(PctDone) + "% Loaded."
    If bDone Then
        lstStatus.AddItem Str(TotalBytes) + " Bytes Completed Loading."
    End If
    lstStatus.ListIndex = lstStatus.ListCount - 1
End Sub
Private Sub mnu_OptionsAllowUserGroupCreation_Click()
    NX.AllowUserGrouping = Not NX.AllowUserGrouping
    mnu_OptionsAllowUserGroupCreation.Checked = NX.AllowUserGrouping
End Sub

Private Sub mnuCircles_Click()
    Dim groupid As Long

    groupid = NX.GrpGetFirstGroupID(curLayer)
    NX.GrpAddType curLayer, groupid, NX_EllipseTool
       
End Sub

Private Sub mnuFileExit_Click()
    NX.Client = ""
    'unload the form
    Unload Me
End Sub


Private Sub mnuFileOpen_Click()
    Dim groupid As Long
    Dim tmpFN As String
    tmpFN = PegasusOpenFileP(imgParentDir)
    If Len(tmpFN) <> 0 Then
        imgFileName = tmpFN
        imgParentDir = PathFromFile(imgFileName)
               
        LoadFile
                
        ShowCustomToolbar
         
        If (NX.GrpGetFirstGroupID(curLayer) = 0) Then
           groupid = NX.GrpCreateGroup(curLayer, "", 0)
        End If
    End If

End Sub


Private Sub mnuFileOpenAnnotation_Click()

Dim annFile As String
    
    annFile = PegasusOpenFilePF(imgParentDir, "NotateXpress File (*.nxp)" & vbNullChar & "*.nxp" & vbNullChar & vbNullChar)
    If Len(annFile) <> 0 Then
       
          NX.RetrieveAnnotation annFile
           ShowCustomToolbar
    End If
 
End Sub


Private Sub mnuFileSave_Click()
    
    Dim sTiffFile As String
    sTiffFile = PegasusSaveFilePF(imgParentDir, "Tiff File (*.tif)" & vbNullChar & "*.tif" & vbNullChar & vbNullChar, "tif")
    If Len(sTiffFile) Then
       Xpress.SaveFileName = sTiffFile
       Xpress.SaveFile
       NX.Clear
       curLayer = NX.CreateLayer
       NX.ToolbarActivated = True
    End If

End Sub


Private Sub mnuFileSaveAnnotation_Click()
    
   Dim annFile As String
    
    annFile = PegasusSaveFilePF(imgParentDir, "NotateXpress File (*.nxp)" & vbNullChar & "*.nxp" & vbNullChar & vbNullChar, "ann")
    If Len(annFile) Then
       NX.Comments = Now
       NX.Description = Now
       NX.Title = "One awesome annotation file"
       NX.StoreAnnotation annFile
    End If

End Sub

Private Sub mnuGroup_Click()
    Dim groupid As Long
    Dim itemCount As Long
    
    If (NX.CurrentLayer) Then
       mnuGroupPutSelected.Enabled = NX.IsSelection
       groupid = NX.GrpGetFirstGroupID(NX.CurrentLayer)
       
       If (groupid = 0) Then
            groupid = NX.GrpCreateGroup(curLayer, "", 0)
       End If
       
       itemCount = NX.GrpGetItemCount(NX.CurrentLayer, groupid)
       mnuGroupRemoveAll.Enabled = (itemCount > 0)
       mnuGroupInvert.Enabled = (itemCount > 0)
       mnuGroupMirror.Enabled = (itemCount > 0)
       mnuGroupAddAll.Enabled = True
    Else
       mnuGroupPutSelected.Enabled = False
       mnuGroupRemoveAll.Enabled = False
       mnuGroupMirror.Enabled = False
       mnuGroupInvert.Enabled = False
       mnuGroupAddAll.Enabled = False
    End If

End Sub

Private Sub mnuGroupAddFreehands_Click()
    Dim groupid As Long
    
    groupid = NX.GrpGetFirstGroupID(curLayer)
    NX.GrpAddType curLayer, groupid, NX_FreehandTool

End Sub



Private Sub mnuGroupAddLines_Click()
    Dim groupid As Long
    
    groupid = NX.GrpGetFirstGroupID(curLayer)
    NX.GrpAddType curLayer, groupid, NX_LineTool

End Sub

Private Sub mnuGroupAddPolygons_Click()
    Dim groupid As Long
    
    groupid = NX.GrpGetFirstGroupID(curLayer)
    NX.GrpAddType curLayer, groupid, NX_PolygonTool

End Sub

Private Sub mnuGroupAddPolylines_Click()
    Dim groupid As Long
    
    groupid = NX.GrpGetFirstGroupID(curLayer)
    NX.GrpAddType curLayer, groupid, NX_PolyLineTool

End Sub

Private Sub mnuGroupAddRectangles_Click()
    Dim groupid As Long
    
    groupid = NX.GrpGetFirstGroupID(curLayer)
    NX.GrpAddType curLayer, groupid, NX_RectangleTool

End Sub

Private Sub mnuGroupAddStamps_Click()
    Dim groupid As Long
    
    groupid = NX.GrpGetFirstGroupID(curLayer)
    NX.GrpAddType curLayer, groupid, NX_StampTool

End Sub

Private Sub mnuGroupAddTexts_Click()
    Dim groupid As Long
    
    groupid = NX.GrpGetFirstGroupID(curLayer)
    NX.GrpAddType curLayer, groupid, NX_TextTool

End Sub

Private Sub mnuGroupInvert_Click()
    Dim groupid As Long
    
    groupid = NX.GrpGetFirstGroupID(NX.CurrentLayer)
    NX.GrpInvert NX.CurrentLayer, groupid
End Sub

Private Sub mnuGroupMirror_Click()
    Dim groupid As Long
    
    groupid = NX.GrpGetFirstGroupID(NX.CurrentLayer)
    NX.GrpMirror NX.CurrentLayer, groupid

End Sub

Private Sub mnuGroupPutSelected_Click()
    Dim groupid As Long
    Dim cLayer As Long
    Dim seqNum As Long
    
    If (NX.IsSelection) Then
       cLayer = NX.CurrentLayer
       groupid = NX.GrpGetFirstGroupID(cLayer)
       NX.GrpRemoveAll cLayer, groupid
       NX.GrpAddSelected cLayer, groupid
    End If
   
End Sub

Private Sub mnuGroupRemoveAll_Click()
    Dim groupid As Long
    
    groupid = NX.GrpGetFirstGroupID(NX.CurrentLayer)
    NX.GrpRemoveAll NX.CurrentLayer, groupid
End Sub

Private Sub mnuOptions_Click()
    Dim groupid As Long
    
    mnuOptionsYoked.Enabled = (curLayer <> 0)
    If (curLayer) Then
       groupid = NX.GrpGetFirstGroupID(curLayer)
       
       If (groupid = 0) Then
            groupid = NX.GrpCreateGroup(curLayer, "", 0)
       End If
       
       mnuOptionsYoked.Checked = NX.GrpGetYoked(curLayer, groupid)
    End If

End Sub

Private Sub mnuOptionsLoadAnnotations_Click()
    NX.LoadAnnotations = Not NX.LoadAnnotations
    mnuOptionsLoadAnnotations.Checked = NX.LoadAnnotations
End Sub

Private Sub mnuOptionsWangCompatibility_Click()
    If NX.TIFFAnnotationType = NX_NXP_Compatible Then
       NX.TIFFAnnotationType = NX_I4W_Compatible
       mnuOptionsWangCompatibility.Checked = True
    Else
       NX.TIFFAnnotationType = NX_NXP_Compatible
       mnuOptionsWangCompatibility.Checked = False
    End If
End Sub

Private Sub mnuOptionsYoked_Click()
    Dim groupid As Long
    Dim yoked As Boolean
    
    groupid = NX.GrpGetFirstGroupID(curLayer)
    yoked = NX.GrpGetYoked(curLayer, groupid)
    yoked = Not yoked
    NX.GrpSetYoked curLayer, groupid, yoked
    mnuOptionsYoked.Checked = yoked
End Sub

Private Sub mnuRulers_Click()
    Dim groupid As Long
    
    groupid = NX.GrpGetFirstGroupID(curLayer)
    NX.GrpAddType curLayer, groupid, NX_RulerTool
End Sub

Private Sub NX_AnnotateEvent(ByVal lType As Long, ByVal lData As Long)
    If lType = 1001 Then
        NX.PrgSetItemSelected NX.CurrentLayer, lData, True
        NX.Tool = NX_PointerTool
    End If
End Sub

Private Sub NX_CurrentLayerChange(ByVal newLayer As Long)
    curLayer = newLayer
End Sub

Private Sub NX_LayerRestored(ByVal layerHandle As Long)
    If (NX.PrgGetGroupCount(layerHandle) = 0) Then
       NX.GrpCreateGroup layerHandle, "", 0
    End If

End Sub

Private Sub NX_UserGroupCreated(ByVal layerHandle As Long, ByVal groupid As Long)
    'MsgBox "Bingo: User created group: " + CStr(groupID) + " in layer " + CStr(layerHandle)
End Sub

Private Sub NX_UserGroupDestroyed(ByVal layerHandle As Long, ByVal groupid As Long)
'MsgBox "Bingo: User destroyed group: " + CStr(groupID) + " in layer " + CStr(layerHandle)

'We want to make sure there's at least one group available, so we'll make that check here
Dim n As Integer

n = NX.PrgGetGroupCount(layerHandle)
If (n = 0) Then 'we better create one
   NX.GrpCreateGroup layerHandle, "", 0
End If

End Sub

Private Sub ShowCustomToolbar()

   NX.ToolbarActivated = True
   NX.ToolbarDeleteTool NX.CurrentLayer, NX_StampTool
   NX.ToolbarDeleteTool NX.CurrentLayer, NX_ImageTool
   NX.ToolbarDeleteTool NX.CurrentLayer, NX_ButtonTool


End Sub






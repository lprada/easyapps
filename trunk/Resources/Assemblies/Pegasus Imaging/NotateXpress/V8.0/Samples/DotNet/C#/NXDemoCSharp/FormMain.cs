/*****************************************************************'
* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;
using PegasusImaging.WinForms.ImagXpress8;
using PegasusImaging.WinForms.NotateXpress8;

///////////////////////// Distribution Note  //////////////
///  This code should be updated to resolve all TODO comments
///  before it is distributed as sample code
///////////////////////////////////////////////////////////

namespace NXDemoCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class FormMain : System.Windows.Forms.Form
	{
		[DllImport("kernel32.dll")]
		private static extern IntPtr GlobalFree( IntPtr hMem );

		public System.Windows.Forms.MainMenu mainMenu;
		public System.Windows.Forms.MenuItem mnu_file;
		public System.Windows.Forms.MenuItem mnu_load_tiff;
		public System.Windows.Forms.MenuItem mnu_file_sep1;
		public System.Windows.Forms.MenuItem mnu_file_save;
		public System.Windows.Forms.MenuItem mnu_file_sep2;
		public System.Windows.Forms.MenuItem mnu_file_wangcompatible;
		public System.Windows.Forms.MenuItem mnu_file_sep3;
		public System.Windows.Forms.MenuItem mnu_fileIResY;
		public System.Windows.Forms.MenuItem mfsep;
		public System.Windows.Forms.MenuItem mnu_file_exit;
		public System.Windows.Forms.MenuItem mnu_layers;
		public System.Windows.Forms.MenuItem mnu_layers_create;
		public System.Windows.Forms.MenuItem mnu_layers_setcurrent;
		public System.Windows.Forms.MenuItem mnu_layers_deletecurrent;
		public System.Windows.Forms.MenuItem mnu_layers_togglehide;
		public System.Windows.Forms.MenuItem mnu_layers_toggletoolbar;
		public System.Windows.Forms.MenuItem mnu_layers_setdescription;
		public System.Windows.Forms.MenuItem mnu_layers_setname;
		public System.Windows.Forms.MenuItem mnu_objects;
		public System.Windows.Forms.MenuItem mnu_objects_autoselect;
		public System.Windows.Forms.MenuItem mnu_objects_sep3;
		public System.Windows.Forms.MenuItem mnu_objects_allowgrouping;
		public System.Windows.Forms.MenuItem mnu_objects_set2;
		public System.Windows.Forms.MenuItem mnu_objects_reverse;
		public System.Windows.Forms.MenuItem mnu_objects_create10;
		public System.Windows.Forms.MenuItem mnu_objects_rects;
		public System.Windows.Forms.MenuItem mnu_objects_buttons;
		public System.Windows.Forms.MenuItem mnu_objects_createtransrect;
		public System.Windows.Forms.MenuItem mnu_objects_createImage;
		public System.Windows.Forms.MenuItem mnu_objects_nudgeup;
		public System.Windows.Forms.MenuItem mnu_objects_nudgedn;
		public System.Windows.Forms.MenuItem mnu_objects_inflate;
		public System.Windows.Forms.MenuItem mnu_objects_sep1;
		public System.Windows.Forms.MenuItem mnu_objects_largetext;
		public System.Windows.Forms.MenuItem mnu_objects_useMLE;
		public System.Windows.Forms.MenuItem mnu_objects_sep2;
		public System.Windows.Forms.MenuItem mnu_objects_brand;
		public System.Windows.Forms.MenuItem mnu_orient;
		public System.Windows.Forms.MenuItem mnu_orient_ltbr;
		public System.Windows.Forms.MenuItem mnu_orient_brtl;
		public System.Windows.Forms.MenuItem mnu_orient_rblt;
		public System.Windows.Forms.MenuItem mnu_orient_tlbr;
		public System.Windows.Forms.MenuItem mnu_orientation_rotate180;
		public System.Windows.Forms.MenuItem mnu_stuff;
		public System.Windows.Forms.MenuItem mnu_stuff_settext;
		public System.Windows.Forms.MenuItem mnu_stuff_backcolor;
		public System.Windows.Forms.MenuItem mnu_stuff_pencolor;
		public System.Windows.Forms.MenuItem mnu_stuff_penwidth;
		public System.Windows.Forms.MenuItem mnu_stuff_bevelshadow;
		public System.Windows.Forms.MenuItem mnu_stuff_bevellight;
		public System.Windows.Forms.MenuItem mnu_stuff_setfont;
		public System.Windows.Forms.MenuItem mnu_stuff_getitemtext;
		public System.Windows.Forms.MenuItem mnu_stuff_hb;
		public System.Windows.Forms.MenuItem mnu_stuff_hf;
		public System.Windows.Forms.MenuItem mnu_stuff_moveable;
		public System.Windows.Forms.MenuItem mnu_stuff_sizeable;
		public System.Windows.Forms.MenuItem mnu_stuff_getitemfont;
		public System.Windows.Forms.MenuItem mnu_stuff_setnewdib;
		public System.Windows.Forms.MenuItem mnu_mode;
		public System.Windows.Forms.MenuItem mnu_mode_editmode;
		public System.Windows.Forms.MenuItem mnu_mode_interactive;
		public System.Windows.Forms.MenuItem mnu_cm;
		public System.Windows.Forms.MenuItem mnu_cm_enable;
		public System.Windows.Forms.Button ZoomOut;
		public System.Windows.Forms.Button ZoomIn;
		private System.Windows.Forms.OpenFileDialog openFileDialog;
		private System.Windows.Forms.SaveFileDialog saveFileDialog;
		private System.Windows.Forms.FontDialog fontDialog1;
		private PegasusImaging.WinForms.NotateXpress8.SaveOptions m_saveOptions = new PegasusImaging.WinForms.NotateXpress8.SaveOptions();
		private PegasusImaging.WinForms.NotateXpress8.LoadOptions m_loadOptions = new PegasusImaging.WinForms.NotateXpress8.LoadOptions();
        
		
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		/// <summary>
		/// Data variables
		/// </summary>
		/// 
		private int iPageNum;
		private string strNXPFileName;

		private bool _bAutoSelect;
		private bool _bUpdateMoved;
		private System.IO.MemoryStream _SavedAnnotationMemoryStream = null;
		private byte [] _SavedAnnotationByteArray = null;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private PegasusImaging.WinForms.ImagXpress8.ImageXView imagXView1;
		private PegasusImaging.WinForms.ImagXpress8.ImageX imageObject;
		private System.Windows.Forms.MenuItem menuItemGroups;
		private System.Windows.Forms.MenuItem menuItemCreateGroup;
		private System.Windows.Forms.MenuItem menuItemDeleteGroup;
		private System.Windows.Forms.MenuItem menuItemSetGroupEmpty;
		private System.Windows.Forms.MenuItem menuItemSelectGroupItems;
		private System.Windows.Forms.MenuItem menuItemGroupAddSelectedItems;
		private System.Windows.Forms.MenuItem menuItemGroupRemoveSelectedItems;
		private System.Windows.Forms.MenuItem menuItemYokeGroupItems;
		private System.Windows.Forms.MenuItem menuItemSetGroupUserString;
		private System.Windows.Forms.MenuItem menuItemToggleAllowPaint;
		private System.Windows.Forms.MenuItem menuItemSendToBack;
		private System.Windows.Forms.MenuItem menuItemBringToFront;
		private System.Windows.Forms.MenuItem menuItemDeactivateCurrentLayer;
		private System.Windows.Forms.MenuItem menuItemDeleteSelected;
		private System.Windows.Forms.MenuItem menuItemSaveByteArray;
		private System.Windows.Forms.MenuItem menuItemSaveMemoryStream;
		private System.Windows.Forms.MenuItem menuItemLoadByteArray;
		private System.Windows.Forms.MenuItem menuItemLoadMemoryStream;
		private System.Windows.Forms.MenuItem menuItemDisableToolbar;
		private System.Windows.Forms.MenuItem menuItem6;
		private System.Windows.Forms.MenuItem menuItem8;
		private System.Windows.Forms.MenuItem menuItemMirrorYoked;
		private System.Windows.Forms.MenuItem menuItemInvertYoked;
		private System.Windows.Forms.MenuItem menuItemLoadAnnotations;
		private System.Windows.Forms.MenuItem menuItemSaveAnnotations;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.MenuItem menuItemCreateNewNX8;
		private System.Windows.Forms.MenuItem menuItem7;
		private System.Windows.Forms.MenuItem menuItem9;
		private System.Windows.Forms.MenuItem menuItemImagingForWindows;
		private System.Windows.Forms.MenuItem menuItemNotateAnnotations;
		private System.Windows.Forms.MenuItem menuItemViewDirectorAnnotations;
		private System.Windows.Forms.MenuItem menuItemUnicodeMode;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItemSaveCurrentLayer;
		private System.Windows.Forms.MenuItem menuItemToolBar;
		private System.Windows.Forms.MenuItem menuItemToolbarRulerTool;
		private System.Windows.Forms.MenuItem menuItemRulerToolVisible;
		private System.Windows.Forms.MenuItem menuItemRulerToolEnabeled;
		private System.Windows.Forms.MenuItem menuItemToolbarFreehandTool;
		private System.Windows.Forms.MenuItem menuItemToolbarEllipseTool;
		private System.Windows.Forms.MenuItem menuItemToolbarTextTool;
		private System.Windows.Forms.MenuItem menuItemToolbarRectangleTool;
		private System.Windows.Forms.MenuItem menuItemToolbarStampTool;
		private System.Windows.Forms.MenuItem menuItemToolbarPolylineTool;
		private System.Windows.Forms.MenuItem menuItemToolbarLineTool;
		private System.Windows.Forms.MenuItem menuItemToolbarImageTool;
		private System.Windows.Forms.MenuItem menuItemFreehandToolEnabled;
		private System.Windows.Forms.MenuItem menuItemFreehandToolVisible;
		private System.Windows.Forms.MenuItem menuItemEllipseToolEnabeld;
		private System.Windows.Forms.MenuItem menuItemEllipseToolVisible;
		private System.Windows.Forms.MenuItem menuItemTextToolEnabled;
		private System.Windows.Forms.MenuItem menuItemTextToolVisible;
		private System.Windows.Forms.MenuItem menuItemRectangleToolEnabled;
		private System.Windows.Forms.MenuItem menuItemRectangleToolVisible;
		private System.Windows.Forms.MenuItem menuItemStampToolEnabled;
		private System.Windows.Forms.MenuItem menuItemStampToolVisible;
		private System.Windows.Forms.MenuItem menuItemPolylineToolVisible;
		private System.Windows.Forms.MenuItem menuItemPolylineToolEnabled;
		private System.Windows.Forms.MenuItem menuItemLineToolEnabled;
		private System.Windows.Forms.MenuItem menuItemLineToolVisible;
		private System.Windows.Forms.MenuItem menuItemImageToolEnabled;
		private System.Windows.Forms.MenuItem menuItemImageToolVisible;
		private System.Windows.Forms.MenuItem menuItemToolbarButtonTool;
		private System.Windows.Forms.MenuItem menuItemToolbarPolygonTool;
		private System.Windows.Forms.MenuItem menuItemButtonToolEnabled;
		private System.Windows.Forms.MenuItem menuItemButtonToolVisible;
		private System.Windows.Forms.MenuItem menuItemPolygonToolEnabled;
		private System.Windows.Forms.MenuItem menuItemPolygonToolVisible;
		private System.Windows.Forms.MenuItem menuItem11;
		private System.Windows.Forms.MenuItem menuItem10;
		private System.Windows.Forms.MenuItem menuItemBoundingRectangle;
		private System.Windows.Forms.ListBox OutputList;
		private System.Windows.Forms.MenuItem mnu_layers_setpassword;
		private System.Windows.Forms.MenuItem mnu_stuff_toggleuserdraw;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.MenuItem menuItemHelp;
		private System.Windows.Forms.MenuItem menuItemAboutNX;
		private System.Windows.Forms.MenuItem menuItemAboutIX;
		private PegasusImaging.WinForms.NotateXpress8.NotateXpress notateXpress1;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		internal System.Windows.Forms.ListBox lstInfo;
		internal System.Windows.Forms.ListBox lstStatus;
		internal System.Windows.Forms.Label lblLoadStatus;
		internal System.Windows.Forms.Label lblLastError;
		internal System.Windows.Forms.Label lblerror;
		public System.Windows.Forms.MenuItem mnu_file_saveann2fileNXP;
		private System.Windows.Forms.MenuItem mnu_file_saveann2fileANN;
		private System.Windows.Forms.MenuItem mnu_file_loadannotationANN;
		public System.Windows.Forms.MenuItem mnu_file_loadannotationNXP;
		private System.Windows.Forms.MenuItem mnu_stuff_fixed;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox txtPageNum;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private PegasusImaging.WinForms.ImagXpress8.Processor imagXProcessor;

		public FormMain()
		{
			// call the unlock function with dummy values
			// PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345, 12345, 12345, 12345);

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (imageObject != null)
				{
					imageObject.Dispose();
					imageObject = null;
				}

				// Don't forget to dispose IX
				//

				if (imagXProcessor != null)
				{
					if (imagXProcessor.Image != null)
					{
						imagXProcessor.Image.Dispose();
						imagXProcessor.Image = null;
					}
					imagXProcessor.Dispose();
					imagXProcessor = null;
				}

				if (notateXpress1  != null)
				{
					notateXpress1.Dispose();
					notateXpress1 = null;
				}

				if (imagXView1.Image != null)
				{
					imagXView1.Image.Dispose();
					imagXView1.Image = null;
				}
			
                
				if (imagXpress1 != null)
				{
					imagXpress1.Dispose();
					imagXpress1 = null;
				}               
                
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

        #region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
        
		private void InitializeComponent()
		{
			this.mainMenu = new System.Windows.Forms.MainMenu();
			this.mnu_file = new System.Windows.Forms.MenuItem();
			this.menuItem9 = new System.Windows.Forms.MenuItem();
			this.menuItemImagingForWindows = new System.Windows.Forms.MenuItem();
			this.menuItemNotateAnnotations = new System.Windows.Forms.MenuItem();
			this.menuItemViewDirectorAnnotations = new System.Windows.Forms.MenuItem();
			this.mnu_file_wangcompatible = new System.Windows.Forms.MenuItem();
			this.menuItem7 = new System.Windows.Forms.MenuItem();
			this.menuItemLoadAnnotations = new System.Windows.Forms.MenuItem();
			this.mnu_load_tiff = new System.Windows.Forms.MenuItem();
			this.mnu_file_sep1 = new System.Windows.Forms.MenuItem();
			this.menuItemSaveAnnotations = new System.Windows.Forms.MenuItem();
			this.mnu_file_save = new System.Windows.Forms.MenuItem();
			this.mnu_file_sep2 = new System.Windows.Forms.MenuItem();
			this.mnu_file_saveann2fileNXP = new System.Windows.Forms.MenuItem();
			this.mnu_file_saveann2fileANN = new System.Windows.Forms.MenuItem();
			this.menuItemSaveByteArray = new System.Windows.Forms.MenuItem();
			this.menuItemSaveMemoryStream = new System.Windows.Forms.MenuItem();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItemUnicodeMode = new System.Windows.Forms.MenuItem();
			this.mnu_file_loadannotationNXP = new System.Windows.Forms.MenuItem();
			this.mnu_file_loadannotationANN = new System.Windows.Forms.MenuItem();
			this.menuItemLoadByteArray = new System.Windows.Forms.MenuItem();
			this.menuItemLoadMemoryStream = new System.Windows.Forms.MenuItem();
			this.mnu_file_sep3 = new System.Windows.Forms.MenuItem();
			this.mnu_fileIResY = new System.Windows.Forms.MenuItem();
			this.mfsep = new System.Windows.Forms.MenuItem();
			this.menuItemToggleAllowPaint = new System.Windows.Forms.MenuItem();
			this.menuItem11 = new System.Windows.Forms.MenuItem();
			this.mnu_file_exit = new System.Windows.Forms.MenuItem();
			this.mnu_layers = new System.Windows.Forms.MenuItem();
			this.mnu_layers_create = new System.Windows.Forms.MenuItem();
			this.menuItem8 = new System.Windows.Forms.MenuItem();
			this.mnu_layers_setcurrent = new System.Windows.Forms.MenuItem();
			this.mnu_layers_deletecurrent = new System.Windows.Forms.MenuItem();
			this.mnu_layers_togglehide = new System.Windows.Forms.MenuItem();
			this.menuItemDeactivateCurrentLayer = new System.Windows.Forms.MenuItem();
			this.mnu_layers_setname = new System.Windows.Forms.MenuItem();
			this.mnu_layers_setdescription = new System.Windows.Forms.MenuItem();
			this.mnu_layers_setpassword = new System.Windows.Forms.MenuItem();
			this.mnu_objects_reverse = new System.Windows.Forms.MenuItem();
			this.menuItemSendToBack = new System.Windows.Forms.MenuItem();
			this.menuItemBringToFront = new System.Windows.Forms.MenuItem();
			this.menuItemDeleteSelected = new System.Windows.Forms.MenuItem();
			this.menuItemSaveCurrentLayer = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.mnu_layers_toggletoolbar = new System.Windows.Forms.MenuItem();
			this.menuItemDisableToolbar = new System.Windows.Forms.MenuItem();
			this.mnu_objects = new System.Windows.Forms.MenuItem();
			this.mnu_objects_autoselect = new System.Windows.Forms.MenuItem();
			this.mnu_objects_sep3 = new System.Windows.Forms.MenuItem();
			this.mnu_objects_create10 = new System.Windows.Forms.MenuItem();
			this.mnu_objects_rects = new System.Windows.Forms.MenuItem();
			this.mnu_objects_buttons = new System.Windows.Forms.MenuItem();
			this.mnu_objects_createtransrect = new System.Windows.Forms.MenuItem();
			this.mnu_objects_createImage = new System.Windows.Forms.MenuItem();
			this.mnu_objects_nudgeup = new System.Windows.Forms.MenuItem();
			this.mnu_objects_nudgedn = new System.Windows.Forms.MenuItem();
			this.mnu_objects_inflate = new System.Windows.Forms.MenuItem();
			this.mnu_objects_sep1 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.mnu_objects_largetext = new System.Windows.Forms.MenuItem();
			this.mnu_objects_useMLE = new System.Windows.Forms.MenuItem();
			this.mnu_objects_sep2 = new System.Windows.Forms.MenuItem();
			this.mnu_objects_brand = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItemCreateNewNX8 = new System.Windows.Forms.MenuItem();
			this.mnu_orient = new System.Windows.Forms.MenuItem();
			this.mnu_orient_ltbr = new System.Windows.Forms.MenuItem();
			this.mnu_orient_brtl = new System.Windows.Forms.MenuItem();
			this.mnu_orient_rblt = new System.Windows.Forms.MenuItem();
			this.mnu_orient_tlbr = new System.Windows.Forms.MenuItem();
			this.mnu_orientation_rotate180 = new System.Windows.Forms.MenuItem();
			this.mnu_stuff = new System.Windows.Forms.MenuItem();
			this.menuItemBoundingRectangle = new System.Windows.Forms.MenuItem();
			this.menuItem10 = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_settext = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_backcolor = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_pencolor = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_penwidth = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_bevelshadow = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_bevellight = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_setfont = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_getitemtext = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_hb = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_hf = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_moveable = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_fixed = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_sizeable = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_getitemfont = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_setnewdib = new System.Windows.Forms.MenuItem();
			this.mnu_stuff_toggleuserdraw = new System.Windows.Forms.MenuItem();
			this.mnu_mode = new System.Windows.Forms.MenuItem();
			this.mnu_mode_editmode = new System.Windows.Forms.MenuItem();
			this.mnu_mode_interactive = new System.Windows.Forms.MenuItem();
			this.mnu_cm = new System.Windows.Forms.MenuItem();
			this.mnu_cm_enable = new System.Windows.Forms.MenuItem();
			this.menuItemGroups = new System.Windows.Forms.MenuItem();
			this.mnu_objects_allowgrouping = new System.Windows.Forms.MenuItem();
			this.mnu_objects_set2 = new System.Windows.Forms.MenuItem();
			this.menuItemCreateGroup = new System.Windows.Forms.MenuItem();
			this.menuItemDeleteGroup = new System.Windows.Forms.MenuItem();
			this.menuItemSetGroupEmpty = new System.Windows.Forms.MenuItem();
			this.menuItemSelectGroupItems = new System.Windows.Forms.MenuItem();
			this.menuItemGroupAddSelectedItems = new System.Windows.Forms.MenuItem();
			this.menuItemGroupRemoveSelectedItems = new System.Windows.Forms.MenuItem();
			this.menuItemYokeGroupItems = new System.Windows.Forms.MenuItem();
			this.menuItemMirrorYoked = new System.Windows.Forms.MenuItem();
			this.menuItemInvertYoked = new System.Windows.Forms.MenuItem();
			this.menuItemSetGroupUserString = new System.Windows.Forms.MenuItem();
			this.menuItemToolBar = new System.Windows.Forms.MenuItem();
			this.menuItemToolbarRulerTool = new System.Windows.Forms.MenuItem();
			this.menuItemRulerToolEnabeled = new System.Windows.Forms.MenuItem();
			this.menuItemRulerToolVisible = new System.Windows.Forms.MenuItem();
			this.menuItemToolbarFreehandTool = new System.Windows.Forms.MenuItem();
			this.menuItemFreehandToolEnabled = new System.Windows.Forms.MenuItem();
			this.menuItemFreehandToolVisible = new System.Windows.Forms.MenuItem();
			this.menuItemToolbarEllipseTool = new System.Windows.Forms.MenuItem();
			this.menuItemEllipseToolEnabeld = new System.Windows.Forms.MenuItem();
			this.menuItemEllipseToolVisible = new System.Windows.Forms.MenuItem();
			this.menuItemToolbarTextTool = new System.Windows.Forms.MenuItem();
			this.menuItemTextToolEnabled = new System.Windows.Forms.MenuItem();
			this.menuItemTextToolVisible = new System.Windows.Forms.MenuItem();
			this.menuItemToolbarRectangleTool = new System.Windows.Forms.MenuItem();
			this.menuItemRectangleToolEnabled = new System.Windows.Forms.MenuItem();
			this.menuItemRectangleToolVisible = new System.Windows.Forms.MenuItem();
			this.menuItemToolbarStampTool = new System.Windows.Forms.MenuItem();
			this.menuItemStampToolEnabled = new System.Windows.Forms.MenuItem();
			this.menuItemStampToolVisible = new System.Windows.Forms.MenuItem();
			this.menuItemToolbarPolylineTool = new System.Windows.Forms.MenuItem();
			this.menuItemPolylineToolEnabled = new System.Windows.Forms.MenuItem();
			this.menuItemPolylineToolVisible = new System.Windows.Forms.MenuItem();
			this.menuItemToolbarLineTool = new System.Windows.Forms.MenuItem();
			this.menuItemLineToolEnabled = new System.Windows.Forms.MenuItem();
			this.menuItemLineToolVisible = new System.Windows.Forms.MenuItem();
			this.menuItemToolbarImageTool = new System.Windows.Forms.MenuItem();
			this.menuItemImageToolEnabled = new System.Windows.Forms.MenuItem();
			this.menuItemImageToolVisible = new System.Windows.Forms.MenuItem();
			this.menuItemToolbarButtonTool = new System.Windows.Forms.MenuItem();
			this.menuItemButtonToolEnabled = new System.Windows.Forms.MenuItem();
			this.menuItemButtonToolVisible = new System.Windows.Forms.MenuItem();
			this.menuItemToolbarPolygonTool = new System.Windows.Forms.MenuItem();
			this.menuItemPolygonToolEnabled = new System.Windows.Forms.MenuItem();
			this.menuItemPolygonToolVisible = new System.Windows.Forms.MenuItem();
			this.menuItemHelp = new System.Windows.Forms.MenuItem();
			this.menuItemAboutNX = new System.Windows.Forms.MenuItem();
			this.menuItemAboutIX = new System.Windows.Forms.MenuItem();
			this.ZoomOut = new System.Windows.Forms.Button();
			this.ZoomIn = new System.Windows.Forms.Button();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.fontDialog1 = new System.Windows.Forms.FontDialog();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.imagXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imagXProcessor = new PegasusImaging.WinForms.ImagXpress8.Processor();
			this.OutputList = new System.Windows.Forms.ListBox();
			this.label2 = new System.Windows.Forms.Label();
			this.notateXpress1 = new PegasusImaging.WinForms.NotateXpress8.NotateXpress();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.lstStatus = new System.Windows.Forms.ListBox();
			this.lblLoadStatus = new System.Windows.Forms.Label();
			this.lblLastError = new System.Windows.Forms.Label();
			this.lblerror = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.txtPageNum = new System.Windows.Forms.TextBox();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// mainMenu
			// 
			this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnu_file,
																					 this.mnu_layers,
																					 this.mnu_objects,
																					 this.mnu_orient,
																					 this.mnu_stuff,
																					 this.mnu_mode,
																					 this.mnu_cm,
																					 this.menuItemGroups,
																					 this.menuItemToolBar,
																					 this.menuItemHelp});
			// 
			// mnu_file
			// 
			this.mnu_file.Index = 0;
			this.mnu_file.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.menuItem9,
																					 this.mnu_file_wangcompatible,
																					 this.menuItem7,
																					 this.menuItemLoadAnnotations,
																					 this.mnu_load_tiff,
																					 this.mnu_file_sep1,
																					 this.menuItemSaveAnnotations,
																					 this.mnu_file_save,
																					 this.mnu_file_sep2,
																					 this.mnu_file_saveann2fileNXP,
																					 this.mnu_file_saveann2fileANN,
																					 this.menuItemSaveByteArray,
																					 this.menuItemSaveMemoryStream,
																					 this.menuItem1,
																					 this.menuItemUnicodeMode,
																					 this.mnu_file_loadannotationNXP,
																					 this.mnu_file_loadannotationANN,
																					 this.menuItemLoadByteArray,
																					 this.menuItemLoadMemoryStream,
																					 this.mnu_file_sep3,
																					 this.mnu_fileIResY,
																					 this.mfsep,
																					 this.menuItemToggleAllowPaint,
																					 this.menuItem11,
																					 this.mnu_file_exit});
			this.mnu_file.Text = "File";
			this.mnu_file.Select += new System.EventHandler(this.mnu_file_select);
			// 
			// menuItem9
			// 
			this.menuItem9.Index = 0;
			this.menuItem9.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItemImagingForWindows,
																					  this.menuItemNotateAnnotations,
																					  this.menuItemViewDirectorAnnotations});
			this.menuItem9.Text = "Load/Save Annotation Type";
			// 
			// menuItemImagingForWindows
			// 
			this.menuItemImagingForWindows.Index = 0;
			this.menuItemImagingForWindows.Text = "Imaging For Windows (Wang)";
			this.menuItemImagingForWindows.Click += new System.EventHandler(this.menuItemImagingForWindows_click);
			// 
			// menuItemNotateAnnotations
			// 
			this.menuItemNotateAnnotations.Index = 1;
			this.menuItemNotateAnnotations.Text = "NotateXpress (NXP)";
			this.menuItemNotateAnnotations.Click += new System.EventHandler(this.menuItemNotateAnnotations_click);
			// 
			// menuItemViewDirectorAnnotations
			// 
			this.menuItemViewDirectorAnnotations.Index = 2;
			this.menuItemViewDirectorAnnotations.Text = "ViewDirector - TMSSequoia (ANN)";
			this.menuItemViewDirectorAnnotations.Click += new System.EventHandler(this.menuItemViewDirectorAnnotations_click);
			// 
			// mnu_file_wangcompatible
			// 
			this.mnu_file_wangcompatible.Checked = true;
			this.mnu_file_wangcompatible.Index = 1;
			this.mnu_file_wangcompatible.Text = "Toggle Load/Save Preserve Wang";
			this.mnu_file_wangcompatible.Click += new System.EventHandler(this.mnu_file_wangcompatible_Click);
			// 
			// menuItem7
			// 
			this.menuItem7.Index = 2;
			this.menuItem7.Text = "-";
			// 
			// menuItemLoadAnnotations
			// 
			this.menuItemLoadAnnotations.Index = 3;
			this.menuItemLoadAnnotations.Text = "Toggle Load Annotations thru IX";
			this.menuItemLoadAnnotations.Click += new System.EventHandler(this.menuItemLoadAnnotations_Click);
			// 
			// mnu_load_tiff
			// 
			this.mnu_load_tiff.Index = 4;
			this.mnu_load_tiff.Text = "Load Image...";
			this.mnu_load_tiff.Click += new System.EventHandler(this.mnu_load_tiff_Click);
			// 
			// mnu_file_sep1
			// 
			this.mnu_file_sep1.Index = 5;
			this.mnu_file_sep1.Text = "-";
			// 
			// menuItemSaveAnnotations
			// 
			this.menuItemSaveAnnotations.Index = 6;
			this.menuItemSaveAnnotations.Text = "Toggle Save Annotations thru IX";
			this.menuItemSaveAnnotations.Click += new System.EventHandler(this.menuItemSaveAnnotations_Click);
			// 
			// mnu_file_save
			// 
			this.mnu_file_save.Index = 7;
			this.mnu_file_save.Text = "Save Image...";
			this.mnu_file_save.Click += new System.EventHandler(this.mnu_file_save_Click);
			// 
			// mnu_file_sep2
			// 
			this.mnu_file_sep2.Index = 8;
			this.mnu_file_sep2.Text = "-";
			// 
			// mnu_file_saveann2fileNXP
			// 
			this.mnu_file_saveann2fileNXP.Index = 9;
			this.mnu_file_saveann2fileNXP.Text = "Save annotation to file(NXP)";
			this.mnu_file_saveann2fileNXP.Click += new System.EventHandler(this.mnu_file_saveann2fileNXP_Click);
			// 
			// mnu_file_saveann2fileANN
			// 
			this.mnu_file_saveann2fileANN.Index = 10;
			this.mnu_file_saveann2fileANN.Text = "Save annotation to file(ANN)";
			this.mnu_file_saveann2fileANN.Click += new System.EventHandler(this.mnu_file_saveann2fileANN_Click);
			// 
			// menuItemSaveByteArray
			// 
			this.menuItemSaveByteArray.Index = 11;
			this.menuItemSaveByteArray.Text = "Save annotation to ByteArray";
			this.menuItemSaveByteArray.Click += new System.EventHandler(this.menuItemSaveByteArray_Click);
			// 
			// menuItemSaveMemoryStream
			// 
			this.menuItemSaveMemoryStream.Index = 12;
			this.menuItemSaveMemoryStream.Text = "Save annotation to MemoryStream";
			this.menuItemSaveMemoryStream.Click += new System.EventHandler(this.menuItemSaveMemoryStream_Click);
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 13;
			this.menuItem1.Text = "-";
			// 
			// menuItemUnicodeMode
			// 
			this.menuItemUnicodeMode.Checked = true;
			this.menuItemUnicodeMode.Index = 14;
			this.menuItemUnicodeMode.Text = "Toggle Load in Unicode Mode";
			this.menuItemUnicodeMode.Click += new System.EventHandler(this.menuItemUnicodeMode_Click);
			// 
			// mnu_file_loadannotationNXP
			// 
			this.mnu_file_loadannotationNXP.Index = 15;
			this.mnu_file_loadannotationNXP.Text = "Load annotation from file(NXP)";
			this.mnu_file_loadannotationNXP.Click += new System.EventHandler(this.mnu_file_loadannotationNXP_Click);
			// 
			// mnu_file_loadannotationANN
			// 
			this.mnu_file_loadannotationANN.Index = 16;
			this.mnu_file_loadannotationANN.Text = "Load Annotation from file(ANN)";
			this.mnu_file_loadannotationANN.Click += new System.EventHandler(this.mnu_file_loadannotationANN_Click);
			// 
			// menuItemLoadByteArray
			// 
			this.menuItemLoadByteArray.Index = 17;
			this.menuItemLoadByteArray.Text = "Load annotation from ByteArray";
			this.menuItemLoadByteArray.Click += new System.EventHandler(this.menuItemLoadByteArray_Click);
			// 
			// menuItemLoadMemoryStream
			// 
			this.menuItemLoadMemoryStream.Index = 18;
			this.menuItemLoadMemoryStream.Text = "Load annotation from MemoryStream";
			this.menuItemLoadMemoryStream.Click += new System.EventHandler(this.menuItemLoadMemoryStream_Click);
			// 
			// mnu_file_sep3
			// 
			this.mnu_file_sep3.Index = 19;
			this.mnu_file_sep3.Text = "-";
			// 
			// mnu_fileIResY
			// 
			this.mnu_fileIResY.Index = 20;
			this.mnu_fileIResY.Text = "IResY FontScaling";
			this.mnu_fileIResY.Click += new System.EventHandler(this.mnu_fileIResY_Click);
			// 
			// mfsep
			// 
			this.mfsep.Index = 21;
			this.mfsep.Text = "-";
			// 
			// menuItemToggleAllowPaint
			// 
			this.menuItemToggleAllowPaint.Index = 22;
			this.menuItemToggleAllowPaint.Text = "Toggle Allow Paint";
			this.menuItemToggleAllowPaint.Click += new System.EventHandler(this.menuItemToggleAllowPain_Click);
			// 
			// menuItem11
			// 
			this.menuItem11.Index = 23;
			this.menuItem11.Text = "-";
			// 
			// mnu_file_exit
			// 
			this.mnu_file_exit.Index = 24;
			this.mnu_file_exit.Text = "E&xit";
			this.mnu_file_exit.Click += new System.EventHandler(this.mnu_file_exit_Click);
			// 
			// mnu_layers
			// 
			this.mnu_layers.Index = 1;
			this.mnu_layers.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnu_layers_create,
																					   this.menuItem8,
																					   this.mnu_layers_setcurrent,
																					   this.mnu_layers_deletecurrent,
																					   this.mnu_layers_togglehide,
																					   this.menuItemDeactivateCurrentLayer,
																					   this.mnu_layers_setname,
																					   this.mnu_layers_setdescription,
																					   this.mnu_layers_setpassword,
																					   this.mnu_objects_reverse,
																					   this.menuItemSendToBack,
																					   this.menuItemBringToFront,
																					   this.menuItemDeleteSelected,
																					   this.menuItemSaveCurrentLayer,
																					   this.menuItem6,
																					   this.mnu_layers_toggletoolbar,
																					   this.menuItemDisableToolbar});
			this.mnu_layers.Text = "Layers";
			this.mnu_layers.Select += new System.EventHandler(this.mnu_layers_Select);
			// 
			// mnu_layers_create
			// 
			this.mnu_layers_create.Index = 0;
			this.mnu_layers_create.Text = "Create...";
			this.mnu_layers_create.Click += new System.EventHandler(this.mnu_layers_create_Click);
			// 
			// menuItem8
			// 
			this.menuItem8.Index = 1;
			this.menuItem8.Text = "-";
			// 
			// mnu_layers_setcurrent
			// 
			this.mnu_layers_setcurrent.Index = 2;
			this.mnu_layers_setcurrent.Text = "Set current";
			this.mnu_layers_setcurrent.Click += new System.EventHandler(this.mnu_layers_setcurrent_Click_1);
			// 
			// mnu_layers_deletecurrent
			// 
			this.mnu_layers_deletecurrent.Index = 3;
			this.mnu_layers_deletecurrent.Text = "Delete current";
			this.mnu_layers_deletecurrent.Click += new System.EventHandler(this.mnu_layers_deletecurrent_Click);
			// 
			// mnu_layers_togglehide
			// 
			this.mnu_layers_togglehide.Index = 4;
			this.mnu_layers_togglehide.Text = "Hide current";
			this.mnu_layers_togglehide.Click += new System.EventHandler(this.mnu_layers_togglehide_Click);
			// 
			// menuItemDeactivateCurrentLayer
			// 
			this.menuItemDeactivateCurrentLayer.Index = 5;
			this.menuItemDeactivateCurrentLayer.Text = "Deactivate current";
			this.menuItemDeactivateCurrentLayer.Click += new System.EventHandler(this.menuItemDeactivateCurrentLayer_Click);
			// 
			// mnu_layers_setname
			// 
			this.mnu_layers_setname.Index = 6;
			this.mnu_layers_setname.Text = "Current name...";
			this.mnu_layers_setname.Click += new System.EventHandler(this.mnu_layers_setname_Click);
			// 
			// mnu_layers_setdescription
			// 
			this.mnu_layers_setdescription.Index = 7;
			this.mnu_layers_setdescription.Text = "Current description...";
			this.mnu_layers_setdescription.Click += new System.EventHandler(this.mnu_layers_setdescription_Click);
			// 
			// mnu_layers_setpassword
			// 
			this.mnu_layers_setpassword.Index = 8;
			this.mnu_layers_setpassword.Text = "Current password...";
			this.mnu_layers_setpassword.Click += new System.EventHandler(this.mnu_layers_setpassword_Click);
			// 
			// mnu_objects_reverse
			// 
			this.mnu_objects_reverse.Index = 9;
			this.mnu_objects_reverse.Text = "Reverse current ZOrder";
			this.mnu_objects_reverse.Click += new System.EventHandler(this.mnu_objects_reverse_Click);
			// 
			// menuItemSendToBack
			// 
			this.menuItemSendToBack.Index = 10;
			this.menuItemSendToBack.Text = "Current selected To Back of ZOrder";
			this.menuItemSendToBack.Click += new System.EventHandler(this.menuItemSendToBack_Click);
			// 
			// menuItemBringToFront
			// 
			this.menuItemBringToFront.Index = 11;
			this.menuItemBringToFront.Text = "Current selected To Front of ZOrder";
			this.menuItemBringToFront.Click += new System.EventHandler(this.menuItemBringToFront_Click);
			// 
			// menuItemDeleteSelected
			// 
			this.menuItemDeleteSelected.Index = 12;
			this.menuItemDeleteSelected.Text = "Current delete Selected";
			this.menuItemDeleteSelected.Click += new System.EventHandler(this.menuItemDeleteSelected_Click);
			// 
			// menuItemSaveCurrentLayer
			// 
			this.menuItemSaveCurrentLayer.Index = 13;
			this.menuItemSaveCurrentLayer.Text = "Save Current...";
			this.menuItemSaveCurrentLayer.Click += new System.EventHandler(this.menuItemSaveCurrentLayer_click);
			// 
			// menuItem6
			// 
			this.menuItem6.Index = 14;
			this.menuItem6.Text = "-";
			// 
			// mnu_layers_toggletoolbar
			// 
			this.mnu_layers_toggletoolbar.Index = 15;
			this.mnu_layers_toggletoolbar.Text = "Toggle toolbar visibility";
			this.mnu_layers_toggletoolbar.Click += new System.EventHandler(this.mnu_layers_toggletoolbar_Click);
			// 
			// menuItemDisableToolbar
			// 
			this.menuItemDisableToolbar.Index = 16;
			this.menuItemDisableToolbar.Text = "Disable toolbar";
			this.menuItemDisableToolbar.Click += new System.EventHandler(this.menuItemDisableToolbar_Click);
			// 
			// mnu_objects
			// 
			this.mnu_objects.Index = 2;
			this.mnu_objects.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.mnu_objects_autoselect,
																						this.mnu_objects_sep3,
																						this.mnu_objects_create10,
																						this.mnu_objects_rects,
																						this.mnu_objects_buttons,
																						this.mnu_objects_createtransrect,
																						this.mnu_objects_createImage,
																						this.mnu_objects_nudgeup,
																						this.mnu_objects_nudgedn,
																						this.mnu_objects_inflate,
																						this.mnu_objects_sep1,
																						this.menuItem3,
																						this.menuItem2,
																						this.mnu_objects_largetext,
																						this.mnu_objects_useMLE,
																						this.mnu_objects_sep2,
																						this.mnu_objects_brand,
																						this.menuItem4,
																						this.menuItemCreateNewNX8});
			this.mnu_objects.Text = "Objects";
			this.mnu_objects.Select += new System.EventHandler(this.mnu_objects_Select);
			// 
			// mnu_objects_autoselect
			// 
			this.mnu_objects_autoselect.Checked = true;
			this.mnu_objects_autoselect.Index = 0;
			this.mnu_objects_autoselect.Text = "Auto-select when created";
			this.mnu_objects_autoselect.Click += new System.EventHandler(this.mnu_objects_autoselect_Click);
			// 
			// mnu_objects_sep3
			// 
			this.mnu_objects_sep3.Index = 1;
			this.mnu_objects_sep3.Text = "-";
			// 
			// mnu_objects_create10
			// 
			this.mnu_objects_create10.Index = 2;
			this.mnu_objects_create10.Text = "Create 10 objects";
			this.mnu_objects_create10.Click += new System.EventHandler(this.mnu_objects_create10_Click);
			// 
			// mnu_objects_rects
			// 
			this.mnu_objects_rects.Index = 3;
			this.mnu_objects_rects.Text = "Create 10 rects";
			this.mnu_objects_rects.Click += new System.EventHandler(this.mnu_objects_rects_Click);
			// 
			// mnu_objects_buttons
			// 
			this.mnu_objects_buttons.Index = 4;
			this.mnu_objects_buttons.Text = "Create 10 buttons";
			this.mnu_objects_buttons.Click += new System.EventHandler(this.mnu_objects_buttons_Click);
			// 
			// mnu_objects_createtransrect
			// 
			this.mnu_objects_createtransrect.Index = 5;
			this.mnu_objects_createtransrect.Text = "Create translucent rectangle";
			this.mnu_objects_createtransrect.Click += new System.EventHandler(this.mnu_objects_createtransrect_Click);
			// 
			// mnu_objects_createImage
			// 
			this.mnu_objects_createImage.Index = 6;
			this.mnu_objects_createImage.Text = "Create an Image object";
			this.mnu_objects_createImage.Click += new System.EventHandler(this.mnu_objects_createImage_Click);
			// 
			// mnu_objects_nudgeup
			// 
			this.mnu_objects_nudgeup.Index = 7;
			this.mnu_objects_nudgeup.Text = "Nudge up";
			this.mnu_objects_nudgeup.Click += new System.EventHandler(this.mnu_objects_nudgeup_Click);
			// 
			// mnu_objects_nudgedn
			// 
			this.mnu_objects_nudgedn.Index = 8;
			this.mnu_objects_nudgedn.Text = "Nudge dn";
			this.mnu_objects_nudgedn.Click += new System.EventHandler(this.mnu_objects_nudgedn_Click);
			// 
			// mnu_objects_inflate
			// 
			this.mnu_objects_inflate.Index = 9;
			this.mnu_objects_inflate.Text = "Inflate me";
			this.mnu_objects_inflate.Click += new System.EventHandler(this.mnu_objects_inflate_Click);
			// 
			// mnu_objects_sep1
			// 
			this.mnu_objects_sep1.Index = 10;
			this.mnu_objects_sep1.Text = "-";
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 11;
			this.menuItem3.Text = "Redaction";
			this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 12;
			this.menuItem2.Text = "Add Ruler Annotation";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// mnu_objects_largetext
			// 
			this.mnu_objects_largetext.Index = 13;
			this.mnu_objects_largetext.Text = "Create large text object";
			this.mnu_objects_largetext.Click += new System.EventHandler(this.mnu_objects_largetext_Click);
			// 
			// mnu_objects_useMLE
			// 
			this.mnu_objects_useMLE.Index = 14;
			this.mnu_objects_useMLE.Text = "Use MLE to edit existing text";
			this.mnu_objects_useMLE.Click += new System.EventHandler(this.mnu_objects_useMLE_Click);
			// 
			// mnu_objects_sep2
			// 
			this.mnu_objects_sep2.Index = 15;
			this.mnu_objects_sep2.Text = "-";
			// 
			// mnu_objects_brand
			// 
			this.mnu_objects_brand.Index = 16;
			this.mnu_objects_brand.Text = "Brand";
			this.mnu_objects_brand.Click += new System.EventHandler(this.mnu_objects_brand_Click);
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 17;
			this.menuItem4.Text = "-";
			// 
			// menuItemCreateNewNX8
			// 
			this.menuItemCreateNewNX8.Index = 18;
			this.menuItemCreateNewNX8.Text = "Create new NX8 component";
			this.menuItemCreateNewNX8.Click += new System.EventHandler(this.menuItemCreateNewNX8_click);
			// 
			// mnu_orient
			// 
			this.mnu_orient.Index = 3;
			this.mnu_orient.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnu_orient_ltbr,
																					   this.mnu_orient_brtl,
																					   this.mnu_orient_rblt,
																					   this.mnu_orient_tlbr,
																					   this.mnu_orientation_rotate180});
			this.mnu_orient.Text = "Orientation";
			// 
			// mnu_orient_ltbr
			// 
			this.mnu_orient_ltbr.Index = 0;
			this.mnu_orient_ltbr.Text = "Rotate 90 clockwise";
			this.mnu_orient_ltbr.Click += new System.EventHandler(this.mnu_orient_ltbr_Click);
			// 
			// mnu_orient_brtl
			// 
			this.mnu_orient_brtl.Index = 1;
			this.mnu_orient_brtl.Text = "Invert";
			this.mnu_orient_brtl.Click += new System.EventHandler(this.mnu_orient_brtl_Click);
			// 
			// mnu_orient_rblt
			// 
			this.mnu_orient_rblt.Index = 2;
			this.mnu_orient_rblt.Text = "Rotate 90 counterclockwise";
			this.mnu_orient_rblt.Click += new System.EventHandler(this.mnu_orient_rblt_Click);
			// 
			// mnu_orient_tlbr
			// 
			this.mnu_orient_tlbr.Index = 3;
			this.mnu_orient_tlbr.Text = "Mirror";
			this.mnu_orient_tlbr.Click += new System.EventHandler(this.mnu_orient_tlbr_Click);
			// 
			// mnu_orientation_rotate180
			// 
			this.mnu_orientation_rotate180.Index = 4;
			this.mnu_orientation_rotate180.Text = "Rotate 180";
			this.mnu_orientation_rotate180.Click += new System.EventHandler(this.mnu_orientation_rotate180_Click);
			// 
			// mnu_stuff
			// 
			this.mnu_stuff.Index = 4;
			this.mnu_stuff.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItemBoundingRectangle,
																					  this.menuItem10,
																					  this.mnu_stuff_settext,
																					  this.mnu_stuff_backcolor,
																					  this.mnu_stuff_pencolor,
																					  this.mnu_stuff_penwidth,
																					  this.mnu_stuff_bevelshadow,
																					  this.mnu_stuff_bevellight,
																					  this.mnu_stuff_setfont,
																					  this.mnu_stuff_getitemtext,
																					  this.mnu_stuff_hb,
																					  this.mnu_stuff_hf,
																					  this.mnu_stuff_moveable,
																					  this.mnu_stuff_fixed,
																					  this.mnu_stuff_sizeable,
																					  this.mnu_stuff_getitemfont,
																					  this.mnu_stuff_setnewdib,
																					  this.mnu_stuff_toggleuserdraw});
			this.mnu_stuff.Text = "Stuff";
			this.mnu_stuff.Select += new System.EventHandler(this.mnu_stuff_Select);
			// 
			// menuItemBoundingRectangle
			// 
			this.menuItemBoundingRectangle.Index = 0;
			this.menuItemBoundingRectangle.Text = "Bounding Rectangle...";
			this.menuItemBoundingRectangle.Click += new System.EventHandler(this.menuItemBoundingRectangle_Click);
			// 
			// menuItem10
			// 
			this.menuItem10.Index = 1;
			this.menuItem10.Text = "-";
			// 
			// mnu_stuff_settext
			// 
			this.mnu_stuff_settext.Index = 2;
			this.mnu_stuff_settext.Text = "Set item text";
			this.mnu_stuff_settext.Click += new System.EventHandler(this.mnu_stuff_settext_Click);
			// 
			// mnu_stuff_backcolor
			// 
			this.mnu_stuff_backcolor.Index = 3;
			this.mnu_stuff_backcolor.Text = "Set item backcolor";
			this.mnu_stuff_backcolor.Click += new System.EventHandler(this.mnu_stuff_backcolor_Click);
			// 
			// mnu_stuff_pencolor
			// 
			this.mnu_stuff_pencolor.Index = 4;
			this.mnu_stuff_pencolor.Text = "Set item pen color";
			this.mnu_stuff_pencolor.Click += new System.EventHandler(this.mnu_stuff_pencolor_Click);
			// 
			// mnu_stuff_penwidth
			// 
			this.mnu_stuff_penwidth.Index = 5;
			this.mnu_stuff_penwidth.Text = "Set item pen width";
			this.mnu_stuff_penwidth.Click += new System.EventHandler(this.mnu_stuff_penwidth_Click);
			// 
			// mnu_stuff_bevelshadow
			// 
			this.mnu_stuff_bevelshadow.Index = 6;
			this.mnu_stuff_bevelshadow.Text = "Set item bevelshadowcolor";
			this.mnu_stuff_bevelshadow.Click += new System.EventHandler(this.mnu_stuff_bevelshadow_Click);
			// 
			// mnu_stuff_bevellight
			// 
			this.mnu_stuff_bevellight.Index = 7;
			this.mnu_stuff_bevellight.Text = "Set item bevellightcolor";
			this.mnu_stuff_bevellight.Click += new System.EventHandler(this.mnu_stuff_bevellight_Click);
			// 
			// mnu_stuff_setfont
			// 
			this.mnu_stuff_setfont.Index = 8;
			this.mnu_stuff_setfont.Text = "Set item font...";
			this.mnu_stuff_setfont.Click += new System.EventHandler(this.mnu_stuff_setfont_Click);
			// 
			// mnu_stuff_getitemtext
			// 
			this.mnu_stuff_getitemtext.Index = 9;
			this.mnu_stuff_getitemtext.Text = "Get item text";
			this.mnu_stuff_getitemtext.Click += new System.EventHandler(this.mnu_stuff_getitemtext_Click);
			// 
			// mnu_stuff_hb
			// 
			this.mnu_stuff_hb.Index = 10;
			this.mnu_stuff_hb.Text = "Toggle HighlightBack";
			this.mnu_stuff_hb.Click += new System.EventHandler(this.mnu_stuff_hb_Click);
			// 
			// mnu_stuff_hf
			// 
			this.mnu_stuff_hf.Index = 11;
			this.mnu_stuff_hf.Text = "Toggle HighlightFill";
			this.mnu_stuff_hf.Click += new System.EventHandler(this.mnu_stuff_hf_Click);
			// 
			// mnu_stuff_moveable
			// 
			this.mnu_stuff_moveable.Index = 12;
			this.mnu_stuff_moveable.Text = "Toggle item moveable";
			this.mnu_stuff_moveable.Click += new System.EventHandler(this.mnu_stuff_moveable_Click);
			// 
			// mnu_stuff_fixed
			// 
			this.mnu_stuff_fixed.Index = 13;
			this.mnu_stuff_fixed.Text = "Toggle item fixed";
			this.mnu_stuff_fixed.Click += new System.EventHandler(this.mnu_stuff_fixed_Click);
			// 
			// mnu_stuff_sizeable
			// 
			this.mnu_stuff_sizeable.Index = 14;
			this.mnu_stuff_sizeable.Text = "Toggle item sizeable";
			this.mnu_stuff_sizeable.Click += new System.EventHandler(this.mnu_stuff_sizeable_Click);
			// 
			// mnu_stuff_getitemfont
			// 
			this.mnu_stuff_getitemfont.Index = 15;
			this.mnu_stuff_getitemfont.Text = "Get item font";
			this.mnu_stuff_getitemfont.Click += new System.EventHandler(this.mnu_stuff_getitemfont_Click);
			// 
			// mnu_stuff_setnewdib
			// 
			this.mnu_stuff_setnewdib.Index = 16;
			this.mnu_stuff_setnewdib.Text = "Set new dib into Image object";
			this.mnu_stuff_setnewdib.Click += new System.EventHandler(this.mnu_stuff_setnewdib_Click);
			// 
			// mnu_stuff_toggleuserdraw
			// 
			this.mnu_stuff_toggleuserdraw.Index = 17;
			this.mnu_stuff_toggleuserdraw.Text = "Toggle user draw";
			this.mnu_stuff_toggleuserdraw.Click += new System.EventHandler(this.mnu_stuff_toggleuserdraw_Click);
			// 
			// mnu_mode
			// 
			this.mnu_mode.Index = 5;
			this.mnu_mode.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					 this.mnu_mode_editmode,
																					 this.mnu_mode_interactive});
			this.mnu_mode.Text = "Mode";
			// 
			// mnu_mode_editmode
			// 
			this.mnu_mode_editmode.Index = 0;
			this.mnu_mode_editmode.Text = "Set Edit mode";
			this.mnu_mode_editmode.Click += new System.EventHandler(this.mnu_mode_editmode_Click);
			// 
			// mnu_mode_interactive
			// 
			this.mnu_mode_interactive.Index = 1;
			this.mnu_mode_interactive.Text = "Set Interactive mode";
			this.mnu_mode_interactive.Click += new System.EventHandler(this.mnu_mode_interactive_Click);
			// 
			// mnu_cm
			// 
			this.mnu_cm.Index = 6;
			this.mnu_cm.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																				   this.mnu_cm_enable});
			this.mnu_cm.Text = "Context Menu";
			this.mnu_cm.Select += new System.EventHandler(this.mnu_cm_Select);
			// 
			// mnu_cm_enable
			// 
			this.mnu_cm_enable.Index = 0;
			this.mnu_cm_enable.Text = "Enable";
			this.mnu_cm_enable.Click += new System.EventHandler(this.mnu_cm_enable_Click);
			// 
			// menuItemGroups
			// 
			this.menuItemGroups.Index = 7;
			this.menuItemGroups.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						   this.mnu_objects_allowgrouping,
																						   this.mnu_objects_set2,
																						   this.menuItemCreateGroup,
																						   this.menuItemDeleteGroup,
																						   this.menuItemSetGroupEmpty,
																						   this.menuItemSelectGroupItems,
																						   this.menuItemGroupAddSelectedItems,
																						   this.menuItemGroupRemoveSelectedItems,
																						   this.menuItemYokeGroupItems,
																						   this.menuItemMirrorYoked,
																						   this.menuItemInvertYoked,
																						   this.menuItemSetGroupUserString});
			this.menuItemGroups.Text = "Groups";
			this.menuItemGroups.Click += new System.EventHandler(this.menuItemGroups_Click);
			this.menuItemGroups.Select += new System.EventHandler(this.menuItemGroups_Click);
			// 
			// mnu_objects_allowgrouping
			// 
			this.mnu_objects_allowgrouping.Index = 0;
			this.mnu_objects_allowgrouping.Text = "Toggle Allow Grouping";
			this.mnu_objects_allowgrouping.Click += new System.EventHandler(this.mnu_objects_allowgrouping_Click);
			// 
			// mnu_objects_set2
			// 
			this.mnu_objects_set2.Index = 1;
			this.mnu_objects_set2.Text = "-";
			// 
			// menuItemCreateGroup
			// 
			this.menuItemCreateGroup.Index = 2;
			this.menuItemCreateGroup.Text = "Create...";
			this.menuItemCreateGroup.Click += new System.EventHandler(this.menuItemCreateGroup_Click);
			// 
			// menuItemDeleteGroup
			// 
			this.menuItemDeleteGroup.Index = 3;
			this.menuItemDeleteGroup.Text = "Delete";
			this.menuItemDeleteGroup.Click += new System.EventHandler(this.menuItemDeleteGroup_Click);
			// 
			// menuItemSetGroupEmpty
			// 
			this.menuItemSetGroupEmpty.Index = 4;
			this.menuItemSetGroupEmpty.Text = "Set Empty";
			this.menuItemSetGroupEmpty.Click += new System.EventHandler(this.menuItemSetGroupEmpty_Click);
			// 
			// menuItemSelectGroupItems
			// 
			this.menuItemSelectGroupItems.Index = 5;
			this.menuItemSelectGroupItems.Text = "Show Items Selected";
			this.menuItemSelectGroupItems.Click += new System.EventHandler(this.menuItemSelectGroupItems_Click);
			// 
			// menuItemGroupAddSelectedItems
			// 
			this.menuItemGroupAddSelectedItems.Index = 6;
			this.menuItemGroupAddSelectedItems.Text = "Add Selected Items";
			this.menuItemGroupAddSelectedItems.Click += new System.EventHandler(this.menuItemGroupAddSelectedItems_Click);
			// 
			// menuItemGroupRemoveSelectedItems
			// 
			this.menuItemGroupRemoveSelectedItems.Index = 7;
			this.menuItemGroupRemoveSelectedItems.Text = "Remove Selected Items";
			this.menuItemGroupRemoveSelectedItems.Click += new System.EventHandler(this.menuItemGroupRemoveSelectedItems_Click);
			// 
			// menuItemYokeGroupItems
			// 
			this.menuItemYokeGroupItems.Index = 8;
			this.menuItemYokeGroupItems.Text = "Toggle Yoked";
			this.menuItemYokeGroupItems.Click += new System.EventHandler(this.menuItemYokeGroupItems_Click);
			// 
			// menuItemMirrorYoked
			// 
			this.menuItemMirrorYoked.Index = 9;
			this.menuItemMirrorYoked.Text = "Mirror Yoked Items";
			this.menuItemMirrorYoked.Click += new System.EventHandler(this.menuItemMirrorYoked_click);
			// 
			// menuItemInvertYoked
			// 
			this.menuItemInvertYoked.Index = 10;
			this.menuItemInvertYoked.Text = "Invert Yoked Items";
			this.menuItemInvertYoked.Click += new System.EventHandler(this.menuItemInvertYoked_click);
			// 
			// menuItemSetGroupUserString
			// 
			this.menuItemSetGroupUserString.Index = 11;
			this.menuItemSetGroupUserString.Text = " User String...";
			this.menuItemSetGroupUserString.Click += new System.EventHandler(this.SetGroupUserString_Select);
			// 
			// menuItemToolBar
			// 
			this.menuItemToolBar.Index = 8;
			this.menuItemToolBar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							this.menuItemToolbarRulerTool,
																							this.menuItemToolbarFreehandTool,
																							this.menuItemToolbarEllipseTool,
																							this.menuItemToolbarTextTool,
																							this.menuItemToolbarRectangleTool,
																							this.menuItemToolbarStampTool,
																							this.menuItemToolbarPolylineTool,
																							this.menuItemToolbarLineTool,
																							this.menuItemToolbarImageTool,
																							this.menuItemToolbarButtonTool,
																							this.menuItemToolbarPolygonTool});
			this.menuItemToolBar.Text = "ToolBar";
			this.menuItemToolBar.Select += new System.EventHandler(this.menuItemToolBar_Select);
			// 
			// menuItemToolbarRulerTool
			// 
			this.menuItemToolbarRulerTool.Index = 0;
			this.menuItemToolbarRulerTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																									 this.menuItemRulerToolEnabeled,
																									 this.menuItemRulerToolVisible});
			this.menuItemToolbarRulerTool.Text = "RulerTool";
			// 
			// menuItemRulerToolEnabeled
			// 
			this.menuItemRulerToolEnabeled.Index = 0;
			this.menuItemRulerToolEnabeled.Text = "Enabled";
			this.menuItemRulerToolEnabeled.Click += new System.EventHandler(this.menuItemRulerToolEnabeled_Click);
			// 
			// menuItemRulerToolVisible
			// 
			this.menuItemRulerToolVisible.Index = 1;
			this.menuItemRulerToolVisible.Text = "Visible";
			this.menuItemRulerToolVisible.Click += new System.EventHandler(this.menuItemRulerToolVisible_Click);
			// 
			// menuItemToolbarFreehandTool
			// 
			this.menuItemToolbarFreehandTool.Index = 1;
			this.menuItemToolbarFreehandTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																										this.menuItemFreehandToolEnabled,
																										this.menuItemFreehandToolVisible});
			this.menuItemToolbarFreehandTool.Text = "Freehand Tool";
			// 
			// menuItemFreehandToolEnabled
			// 
			this.menuItemFreehandToolEnabled.Index = 0;
			this.menuItemFreehandToolEnabled.Text = "Enabled";
			this.menuItemFreehandToolEnabled.Click += new System.EventHandler(this.menuItemFreehandToolEnabled_Click);
			// 
			// menuItemFreehandToolVisible
			// 
			this.menuItemFreehandToolVisible.Index = 1;
			this.menuItemFreehandToolVisible.Text = "Visible";
			this.menuItemFreehandToolVisible.Click += new System.EventHandler(this.menuItemFreehandToolVisible_Click);
			// 
			// menuItemToolbarEllipseTool
			// 
			this.menuItemToolbarEllipseTool.Index = 2;
			this.menuItemToolbarEllipseTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																									   this.menuItemEllipseToolEnabeld,
																									   this.menuItemEllipseToolVisible});
			this.menuItemToolbarEllipseTool.Text = "Elliplse Tool";
			// 
			// menuItemEllipseToolEnabeld
			// 
			this.menuItemEllipseToolEnabeld.Index = 0;
			this.menuItemEllipseToolEnabeld.Text = "Enabled";
			this.menuItemEllipseToolEnabeld.Click += new System.EventHandler(this.menuItemEllipseToolEnabeld_Click);
			// 
			// menuItemEllipseToolVisible
			// 
			this.menuItemEllipseToolVisible.Index = 1;
			this.menuItemEllipseToolVisible.Text = "Visible";
			this.menuItemEllipseToolVisible.Click += new System.EventHandler(this.menuItemEllipseToolVisible_Click);
			// 
			// menuItemToolbarTextTool
			// 
			this.menuItemToolbarTextTool.Index = 3;
			this.menuItemToolbarTextTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																									this.menuItemTextToolEnabled,
																									this.menuItemTextToolVisible});
			this.menuItemToolbarTextTool.Text = "Text Tool";
			// 
			// menuItemTextToolEnabled
			// 
			this.menuItemTextToolEnabled.Index = 0;
			this.menuItemTextToolEnabled.Text = "Enabled";
			this.menuItemTextToolEnabled.Click += new System.EventHandler(this.menuItemTextToolEnabled_Click);
			// 
			// menuItemTextToolVisible
			// 
			this.menuItemTextToolVisible.Index = 1;
			this.menuItemTextToolVisible.Text = "Visible";
			this.menuItemTextToolVisible.Click += new System.EventHandler(this.menuItemTextToolVisible_Click);
			// 
			// menuItemToolbarRectangleTool
			// 
			this.menuItemToolbarRectangleTool.Index = 4;
			this.menuItemToolbarRectangleTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																										 this.menuItemRectangleToolEnabled,
																										 this.menuItemRectangleToolVisible});
			this.menuItemToolbarRectangleTool.Text = "Rectangle Tool";
			// 
			// menuItemRectangleToolEnabled
			// 
			this.menuItemRectangleToolEnabled.Index = 0;
			this.menuItemRectangleToolEnabled.Text = "Enabled";
			this.menuItemRectangleToolEnabled.Click += new System.EventHandler(this.menuItemRectangleToolEnabled_Click);
			// 
			// menuItemRectangleToolVisible
			// 
			this.menuItemRectangleToolVisible.Index = 1;
			this.menuItemRectangleToolVisible.Text = "Visible";
			this.menuItemRectangleToolVisible.Click += new System.EventHandler(this.menuItemRectangleToolVisible_Click);
			// 
			// menuItemToolbarStampTool
			// 
			this.menuItemToolbarStampTool.Index = 5;
			this.menuItemToolbarStampTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																									 this.menuItemStampToolEnabled,
																									 this.menuItemStampToolVisible});
			this.menuItemToolbarStampTool.Text = "Stamp Tool";
			// 
			// menuItemStampToolEnabled
			// 
			this.menuItemStampToolEnabled.Index = 0;
			this.menuItemStampToolEnabled.Text = "Enabled";
			this.menuItemStampToolEnabled.Click += new System.EventHandler(this.menuItemStampToolEnabled_Click);
			// 
			// menuItemStampToolVisible
			// 
			this.menuItemStampToolVisible.Index = 1;
			this.menuItemStampToolVisible.Text = "Visible";
			this.menuItemStampToolVisible.Click += new System.EventHandler(this.menuItemStampToolVisible_Click);
			// 
			// menuItemToolbarPolylineTool
			// 
			this.menuItemToolbarPolylineTool.Index = 6;
			this.menuItemToolbarPolylineTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																										this.menuItemPolylineToolEnabled,
																										this.menuItemPolylineToolVisible});
			this.menuItemToolbarPolylineTool.Text = "Polyline Tool";
			// 
			// menuItemPolylineToolEnabled
			// 
			this.menuItemPolylineToolEnabled.Index = 0;
			this.menuItemPolylineToolEnabled.Text = "Enabled";
			this.menuItemPolylineToolEnabled.Click += new System.EventHandler(this.menuItemPolylineToolEnabled_Click);
			// 
			// menuItemPolylineToolVisible
			// 
			this.menuItemPolylineToolVisible.Index = 1;
			this.menuItemPolylineToolVisible.Text = "Visible";
			this.menuItemPolylineToolVisible.Click += new System.EventHandler(this.menuItemPolylineToolVisible_Click);
			// 
			// menuItemToolbarLineTool
			// 
			this.menuItemToolbarLineTool.Index = 7;
			this.menuItemToolbarLineTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																									this.menuItemLineToolEnabled,
																									this.menuItemLineToolVisible});
			this.menuItemToolbarLineTool.Text = "Line Tool";
			// 
			// menuItemLineToolEnabled
			// 
			this.menuItemLineToolEnabled.Index = 0;
			this.menuItemLineToolEnabled.Text = "Enabled";
			this.menuItemLineToolEnabled.Click += new System.EventHandler(this.menuItemLineToolEnabled_Click);
			// 
			// menuItemLineToolVisible
			// 
			this.menuItemLineToolVisible.Index = 1;
			this.menuItemLineToolVisible.Text = "Visible";
			this.menuItemLineToolVisible.Click += new System.EventHandler(this.menuItemLineToolVisible_Click);
			// 
			// menuItemToolbarImageTool
			// 
			this.menuItemToolbarImageTool.Index = 8;
			this.menuItemToolbarImageTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																									 this.menuItemImageToolEnabled,
																									 this.menuItemImageToolVisible});
			this.menuItemToolbarImageTool.Text = "Image Tool";
			// 
			// menuItemImageToolEnabled
			// 
			this.menuItemImageToolEnabled.Index = 0;
			this.menuItemImageToolEnabled.Text = "Enabled";
			this.menuItemImageToolEnabled.Click += new System.EventHandler(this.menuItemImageToolEnabled_Click);
			// 
			// menuItemImageToolVisible
			// 
			this.menuItemImageToolVisible.Index = 1;
			this.menuItemImageToolVisible.Text = "Visible";
			this.menuItemImageToolVisible.Click += new System.EventHandler(this.menuItemImageToolVisible_Click);
			// 
			// menuItemToolbarButtonTool
			// 
			this.menuItemToolbarButtonTool.Index = 9;
			this.menuItemToolbarButtonTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																									  this.menuItemButtonToolEnabled,
																									  this.menuItemButtonToolVisible});
			this.menuItemToolbarButtonTool.Text = "Button Tool";
			// 
			// menuItemButtonToolEnabled
			// 
			this.menuItemButtonToolEnabled.Index = 0;
			this.menuItemButtonToolEnabled.Text = "Enabled";
			this.menuItemButtonToolEnabled.Click += new System.EventHandler(this.menuItemButtonToolEnabled_Click);
			// 
			// menuItemButtonToolVisible
			// 
			this.menuItemButtonToolVisible.Index = 1;
			this.menuItemButtonToolVisible.Text = "Visible";
			this.menuItemButtonToolVisible.Click += new System.EventHandler(this.menuItemButtonToolVisible_Click);
			// 
			// menuItemToolbarPolygonTool
			// 
			this.menuItemToolbarPolygonTool.Index = 10;
			this.menuItemToolbarPolygonTool.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																									   this.menuItemPolygonToolEnabled,
																									   this.menuItemPolygonToolVisible});
			this.menuItemToolbarPolygonTool.Text = "Polygon Tool";
			// 
			// menuItemPolygonToolEnabled
			// 
			this.menuItemPolygonToolEnabled.Index = 0;
			this.menuItemPolygonToolEnabled.Text = "Enabled";
			this.menuItemPolygonToolEnabled.Click += new System.EventHandler(this.menuItemPolygonToolEnabled_Click);
			// 
			// menuItemPolygonToolVisible
			// 
			this.menuItemPolygonToolVisible.Index = 1;
			this.menuItemPolygonToolVisible.Text = "Visible";
			this.menuItemPolygonToolVisible.Click += new System.EventHandler(this.menuItemPolygonToolVisible_Click);
			// 
			// menuItemHelp
			// 
			this.menuItemHelp.Index = 9;
			this.menuItemHelp.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this.menuItemAboutNX,
																						 this.menuItemAboutIX});
			this.menuItemHelp.Text = "&Help";
			// 
			// menuItemAboutNX
			// 
			this.menuItemAboutNX.Index = 0;
			this.menuItemAboutNX.Text = "About &NotateXpress..";
			this.menuItemAboutNX.Click += new System.EventHandler(this.menuItemAboutNX_Click);
			// 
			// menuItemAboutIX
			// 
			this.menuItemAboutIX.Index = 1;
			this.menuItemAboutIX.Text = "About &ImagXpress...";
			this.menuItemAboutIX.Click += new System.EventHandler(this.menuItemAboutIX_Click);
			// 
			// ZoomOut
			// 
			this.ZoomOut.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.ZoomOut.BackColor = System.Drawing.SystemColors.Control;
			this.ZoomOut.Cursor = System.Windows.Forms.Cursors.Default;
			this.ZoomOut.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.ZoomOut.ForeColor = System.Drawing.SystemColors.ControlText;
			this.ZoomOut.Location = new System.Drawing.Point(616, 528);
			this.ZoomOut.Name = "ZoomOut";
			this.ZoomOut.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ZoomOut.Size = new System.Drawing.Size(65, 33);
			this.ZoomOut.TabIndex = 8;
			this.ZoomOut.Text = "Zoom Out";
			this.ZoomOut.Click += new System.EventHandler(this.ZoomOut_Click);
			// 
			// ZoomIn
			// 
			this.ZoomIn.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.ZoomIn.BackColor = System.Drawing.SystemColors.Control;
			this.ZoomIn.Cursor = System.Windows.Forms.Cursors.Default;
			this.ZoomIn.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.ZoomIn.ForeColor = System.Drawing.SystemColors.ControlText;
			this.ZoomIn.Location = new System.Drawing.Point(512, 528);
			this.ZoomIn.Name = "ZoomIn";
			this.ZoomIn.RightToLeft = System.Windows.Forms.RightToLeft.No;
			this.ZoomIn.Size = new System.Drawing.Size(65, 33);
			this.ZoomIn.TabIndex = 14;
			this.ZoomIn.Text = "Zoom In";
			this.ZoomIn.Click += new System.EventHandler(this.ZoomIn_Click);
			// 
			// saveFileDialog
			// 
			this.saveFileDialog.FileName = "doc1";
			// 
			// imagXView1
			// 
			this.imagXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imagXView1.Location = new System.Drawing.Point(16, 88);
			this.imagXView1.Name = "imagXView1";
			this.imagXView1.Size = new System.Drawing.Size(672, 424);
			this.imagXView1.TabIndex = 7;
			// 
			// imagXProcessor
			// 
			this.imagXProcessor.BackgroundColor = System.Drawing.Color.Black;
			this.imagXProcessor.ContrastMethod = PegasusImaging.WinForms.ImagXpress8.ContrastMethod.DefaultContrast;
			this.imagXProcessor.ProgressPercent = 10;
			this.imagXProcessor.Redeyes = null;
			// 
			// OutputList
			// 
			this.OutputList.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.OutputList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.OutputList.HorizontalScrollbar = true;
			this.OutputList.Location = new System.Drawing.Point(16, 576);
			this.OutputList.Name = "OutputList";
			this.OutputList.Size = new System.Drawing.Size(896, 108);
			this.OutputList.TabIndex = 15;
			// 
			// label2
			// 
			this.label2.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left);
			this.label2.Location = new System.Drawing.Point(16, 520);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 16);
			this.label2.TabIndex = 16;
			this.label2.Text = "Events:";
			// 
			// notateXpress1
			// 
			this.notateXpress1.AllowPaint = true;
			this.notateXpress1.Debug = false;
			this.notateXpress1.DebugLogFile = "c:\\NotateXpress8.log";
			this.notateXpress1.ErrorLevel = PegasusImaging.WinForms.NotateXpress8.ErrorLevelInfo.Production;
			this.notateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress8.FontScaling.Normal;
			this.notateXpress1.ImagXpressLoad = true;
			this.notateXpress1.ImagXpressSave = true;
			this.notateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress8.AnnotationMode.Edit;
			this.notateXpress1.MultiLineEdit = false;
			this.notateXpress1.MouseMove += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.MouseMoveEventHandler(this.notateXpress1_MouseMove);
			this.notateXpress1.Click += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.ClickEventHandler(this.notateXpress1_Click);
			this.notateXpress1.AnnotationDeleted += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.AnnotationDeletedEventHandler(this.notateXpress1_AnnotationDeleted);
			this.notateXpress1.AnnotationAdded += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.AnnotationAddedEventHandler(this.notateXpress1_AnnotationAdded);
			this.notateXpress1.UserGroupDestroyed += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.UserGroupDestroyedEventHandler(this.notateXpress1_UserGroupDestroyed);
			this.notateXpress1.LayerRestored += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.LayerRestoredEventHandler(this.notateXpress1_LayerRestored);
			this.notateXpress1.RequestLayerPassword += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.GetLayerPasswordEventHandler(this.notateXpress1_RequestLayerPassword);
			this.notateXpress1.AnnotationMoved += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.AnnotationMovedEventHandler(this.notateXpress1_AnnotationMoved);
			this.notateXpress1.UserGroupCreated += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.UserGroupCreatedEventHandler(this.notateXpress1_UserGroupCreated);
			this.notateXpress1.MenuSelect += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.MenuEventHandler(this.notateXpress1_MenuSelect);
			this.notateXpress1.AnnotationEndSelected += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.AnnotationEndSelectedEventHandler(this.notateXpress1_AnnotationEndSelected);
			this.notateXpress1.UserDraw += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.UserDrawEventHandler(this.notateXpress1_UserDraw);
			this.notateXpress1.ToolbarSelect += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.ToolbarEventHandler(this.notateXpress1_ToolbarSelect);
			this.notateXpress1.MouseUp += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.MouseUpEventHandler(this.notateXpress1_MouseUp);
			this.notateXpress1.ItemChanged += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.ItemChangedEventHandler(this.notateXpress1_ItemChanged);
			this.notateXpress1.DoubleClick += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.DoubleClickEventHandler(this.notateXpress1_DoubleClick);
			this.notateXpress1.CurrentLayerChange += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.CurrentLayerChangeEventHandler(this.notateXpress1_CurrentLayerChange);
			this.notateXpress1.AnnotationSelected += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.AnnotationSelectedEventHandler(this.notateXpress1_AnnotationSelected);
			this.notateXpress1.MouseDown += new PegasusImaging.WinForms.NotateXpress8.NotateXpress.MouseDownEventHandler(this.notateXpress1_MouseDown);
			// 
			// lstInfo
			// 
			this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following:",
														 "1)Adding various annotation types including redaction and ruler types.",
														 "2)Creating,deleting and hiding layers.",
														 "3)Saving annotations in the Pegasus NXP format and the Kodak Wang Imaging format." +
														 ""});
			this.lstInfo.Location = new System.Drawing.Point(16, 8);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(904, 69);
			this.lstInfo.TabIndex = 21;
			// 
			// lstStatus
			// 
			this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstStatus.Location = new System.Drawing.Point(696, 136);
			this.lstStatus.Name = "lstStatus";
			this.lstStatus.Size = new System.Drawing.Size(232, 82);
			this.lstStatus.TabIndex = 20;
			// 
			// lblLoadStatus
			// 
			this.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblLoadStatus.Location = new System.Drawing.Point(704, 88);
			this.lblLoadStatus.Name = "lblLoadStatus";
			this.lblLoadStatus.Size = new System.Drawing.Size(192, 40);
			this.lblLoadStatus.TabIndex = 19;
			this.lblLoadStatus.Text = "Load Status:";
			// 
			// lblLastError
			// 
			this.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.lblLastError.Location = new System.Drawing.Point(696, 240);
			this.lblLastError.Name = "lblLastError";
			this.lblLastError.Size = new System.Drawing.Size(216, 32);
			this.lblLastError.TabIndex = 18;
			this.lblLastError.Text = "Last Error:";
			// 
			// lblerror
			// 
			this.lblerror.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.lblerror.Location = new System.Drawing.Point(704, 288);
			this.lblerror.Name = "lblerror";
			this.lblerror.Size = new System.Drawing.Size(224, 128);
			this.lblerror.TabIndex = 17;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(696, 432);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(80, 32);
			this.button1.TabIndex = 22;
			this.button1.Text = "Go to Below Page";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// txtPageNum
			// 
			this.txtPageNum.Location = new System.Drawing.Point(696, 472);
			this.txtPageNum.Name = "txtPageNum";
			this.txtPageNum.Size = new System.Drawing.Size(80, 20);
			this.txtPageNum.TabIndex = 23;
			this.txtPageNum.Text = "1";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(816, 424);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(64, 48);
			this.button2.TabIndex = 24;
			this.button2.Text = "Save Ann To NXP File";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(688, 504);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(128, 48);
			this.button3.TabIndex = 25;
			this.button3.Text = "Save Current Page to Temp File with Wang Layers Preserved";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(832, 488);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(80, 64);
			this.button4.TabIndex = 26;
			this.button4.Text = "Create new Multi-page TIFF from Temp files";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// FormMain
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(936, 705);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.button4,
																		  this.button3,
																		  this.button2,
																		  this.txtPageNum,
																		  this.button1,
																		  this.lstInfo,
																		  this.lstStatus,
																		  this.lblLoadStatus,
																		  this.lblLastError,
																		  this.lblerror,
																		  this.label2,
																		  this.OutputList,
																		  this.imagXView1,
																		  this.ZoomOut,
																		  this.ZoomIn});
			this.Menu = this.mainMenu;
			this.Name = "FormMain";
			this.Text = "NotateXpress 8 C# Demo";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.ResumeLayout(false);

		}
        #endregion


		#region Pegasus Imaging Sample Application Standard Functions

		System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private System.String strCurrentDir = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
		string strDefaultImageFilter = ("All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" +
			"" + (";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" + ("2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" + ("t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" + ("e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" + (")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" + ("cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" + ("s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" + "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"))))))));

    
		private string GetFileName(string FullName) 
		{
			return FullName.Substring((FullName.LastIndexOf("\\") + 1), (FullName.Length 
				- (FullName.LastIndexOf("\\") + 1)));
		}
    
		private static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = (ErrorException.Message + ("" + ("\n" 
				+ ((ErrorException.Source + "") 
				+ "\n"))));
		}
    
		private static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = (ErrorException.Message + ("" + ("\n" 
				+ (ErrorException.Source + ("" + ("\n" + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))));
		}
    
		private string PegasusOpenFile(string sTitle, string sFileFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = sTitle;
			dlg.Filter = sFileFilter;
			dlg.InitialDirectory = strCurrentDir;
			if ((dlg.ShowDialog() == DialogResult.OK)) 
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
				return dlg.FileName;
			}
			else 
			{
				return "";
			}
		}
    
		private string PegasusSaveFile(string sTitle, string sFilter) 
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.Title = sTitle;
			dlg.Filter = sFilter;
			dlg.InitialDirectory = strCurrentDir;
			if ((dlg.ShowDialog() == DialogResult.OK)) 
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
				return dlg.FileName;
			}
			else 
			{
				return "";
			}
		}

		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.Run(new FormMain());
		}

		private void mnu_load_tiff_Click(object sender, System.EventArgs e)
		{
			openFileDialog.Title = "Select an Image File";
			openFileDialog.Filter = strDefaultImageFilter;
			openFileDialog.DefaultExt = ".tif";
			openFileDialog.Multiselect = false;
			openFileDialog.InitialDirectory = strCurrentDir;
			openFileDialog.ShowDialog (this);

			string strFilePath = openFileDialog.FileName;
			if (strFilePath != "" && strFilePath != null)
			{
				
				//clear out the error in case there was an error from a previous operation
				lblerror.Text = "";

				// ---------------
				// clear existing annotations and layers
				// ---------------
				notateXpress1.Layers.Clear ();

				PegasusImaging.WinForms.NotateXpress8.Layer theNewLayer = createNewLayer("Layer1", System.IntPtr.Zero);
				
				if (imagXView1.Image != null)
				{
					imagXView1.Image.Dispose();
					imagXView1.Image = null;
				}
				try
				{
					
					imagXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strFilePath);
					this.Text = "Image = " + imagXView1.Image.ImageXData.Width.ToString () + " x " + imagXView1.Image.ImageXData.Height.ToString ();
					theNewLayer.Toolbar.Visible = menuItemToolBar.Enabled;
					theNewLayer.Toolbar.Enabled = menuItemToolBar.Enabled;

					
				}
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
				{
					PegasusError(ex, lblerror);
				}
			}
			
		}

		private PegasusImaging.WinForms.NotateXpress8.Layer createNewLayer(string strLayerName, System.IntPtr DibHandle)
		{
			// ---------------
			// create new layer
			// ---------------
			PegasusImaging.WinForms.NotateXpress8.Layer layer = new Layer();

			// ---------------
			// Add the new layer to the collection and make it the selected layer
			// ---------------
			notateXpress1.Layers.Add(layer);
			layer.Select();

			layer.Name = strLayerName;
			layer.Description = "Layer created " + System.DateTime.Now.ToLongTimeString ();
			//layer.Active = true; // probably default value
			layer.Visible = true; // probably default value
			if(notateXpress1.InteractMode == AnnotationMode.Edit)
				layer.Toolbar.Visible = true;
			else
			{
				layer.Toolbar.Visible = false;
				menuItemToolBar.Enabled = false;
			}

			// ---------------
			// set the button tool text and pen width for this demo
			// ---------------
			layer.Toolbar.ButtonToolbarDefaults.PenWidth = 4;
			layer.Toolbar.ButtonToolbarDefaults.Text = "Click me";

			// ---------------
			// set the image tool defaults for this demo
			// ---------------
			layer.Toolbar.ImageToolbarDefaults.BackStyle = BackStyle.Transparent;
			if(DibHandle != IntPtr.Zero)
				layer.Toolbar.ImageToolbarDefaults.DibHandle = DibHandle;

			// ---------------
			// set the stamp tool text and font for this demo
			// ---------------
			layer.Toolbar.StampToolbarDefaults.Text = "NotateXpress 8";
			System.Drawing.Font fontCurrent = layer.Toolbar.StampToolbarDefaults.TextFont;
			System.Drawing.Font fontNew = new System.Drawing.Font (fontCurrent.Name, 16, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic);
			layer.Toolbar.StampToolbarDefaults.TextFont = fontNew;

			return layer;
		}

		private void ImageStatusEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs e) 
		{
			lstStatus.Items.Add(e.Status.ToString(cultNumber));
			
		}
		private void ProgressEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs e) 
		{
			lstStatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
			if (e.IsComplete) 
			{
				lstStatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
			}
			lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
		}

		private void FormMain_Load(object sender, System.EventArgs e)
		{
            

			this.Top = (Screen.PrimaryScreen.WorkingArea.Height - this.Height) / 2;
			this.Left = (Screen.PrimaryScreen.WorkingArea.Width - this.Width) / 2;
			//--------------
			// load file into ImagXpress
			//--------------
			try
			{

				//**The UnlockRuntime function must be called to distribute the runtime**
				//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);

				try 
				{
					//this is where events are assigned. This happens before the file gets loaded.
					PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEventHandler( this.ProgressEventHandler );
					PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEventHandler( this.ImageStatusEventHandler );
		
				}
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
				{
					PegasusError(ex, lblerror);
				}
				
				System.String strCurrentDir  = System.IO.Directory.GetCurrentDirectory();
                
				System.String strImagePath = System.IO.Path.Combine (strCurrentDir, "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\benefits.tif");
				imagXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImagePath);


				strImagePath = System.IO.Path.Combine (strCurrentDir,"..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\vermont.jpg");
				imageObject = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImagePath);


				// Use the SetClientMethod to connect the ImagXpress control and NotateXpress control
				notateXpress1.ClientWindow = imagXView1.Handle;

				// pass an image file relative path from the samples
				strImagePath = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\vermont.jpg");
				imageObject = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImagePath);
				imagXView1.AutoScroll = true;

				// set the "global" toolbar default values
				ToolbarDefaults toolbarDefaults = notateXpress1.ToolbarDefaults;
				toolbarDefaults.SetToolTip (AnnotationTool.EllipseTool, "The Ellipse-O-Matic!");
				toolbarDefaults.SetToolTip (AnnotationTool.FreehandTool, "Draw with this!");
				toolbarDefaults.SetToolTip (AnnotationTool.LineTool, "Liney, very, very Liney");
				toolbarDefaults.SetToolTip (AnnotationTool.PointerTool, "Point 'em out!");
				toolbarDefaults.SetToolTip (AnnotationTool.PolygonTool, "Polygons aren't gone");
				toolbarDefaults.SetToolTip (AnnotationTool.RectangleTool, "Rectangle, we got Rectangle");
				toolbarDefaults.SetToolTip (AnnotationTool.StampTool, "Stamp out dumb tool tips!");
				toolbarDefaults.SetToolTip (AnnotationTool.TextTool, "Write me a masterpiece!");
				toolbarDefaults.SetToolTip (AnnotationTool.PolyLineTool, "PolyLine");
				toolbarDefaults.SetToolTip (AnnotationTool.ButtonTool,"Button Tool");
				toolbarDefaults.SetToolTip (AnnotationTool.ImageTool, "Image Tool!");
				toolbarDefaults.SetToolTip (AnnotationTool.RulerTool,"Ruler Tool");

				// Create a new layer and its toolbar
				Layer layer = createNewLayer("Layer1", imageObject.ToHdib(false));

				m_loadOptions.AnnType = m_saveOptions.AnnType = AnnotationType.ImagingForWindows;

				mnu_file_saveann2fileANN.Enabled = false;
				mnu_file_loadannotationANN.Enabled = false;

				mnu_mode_interactive.Checked = false;
				mnu_mode_editmode.Checked = true;
				_bAutoSelect = true;
				_bUpdateMoved = true;
			}
			catch (Exception ex)
			{
				MessageBox.Show("FormMain_Load Error msg:   " + ex.Message + "\n" +
					"Error type:  " + ex.GetType().ToString());
			}
		}

		private void mnu_file_select(object sender, System.EventArgs e)
		{
			menuItemImagingForWindows.Checked = m_saveOptions.AnnType == AnnotationType.ImagingForWindows ? true : false;
			menuItemNotateAnnotations.Checked = m_saveOptions.AnnType == AnnotationType.NotateXpress ? true : false;
			menuItemViewDirectorAnnotations.Checked = m_saveOptions.AnnType == AnnotationType.TMSSequoia ? true : false;

			mnu_file_wangcompatible.Checked = m_saveOptions.PreserveWangLayers;
			menuItemUnicodeMode.Checked = m_loadOptions.UnicodeMode;
			menuItemToggleAllowPaint.Checked = notateXpress1.AllowPaint;

			menuItemLoadMemoryStream.Enabled = _SavedAnnotationMemoryStream == null ? false : true;
			menuItemLoadByteArray.Enabled = _SavedAnnotationByteArray == null ? false : true;
			menuItemLoadAnnotations.Checked = notateXpress1.ImagXpressLoad;
			menuItemSaveAnnotations.Checked = notateXpress1.ImagXpressSave;
		}

		private void menuItemImagingForWindows_click(object sender, System.EventArgs e)
		{
			m_loadOptions.AnnType = m_saveOptions.AnnType = AnnotationType.ImagingForWindows;

			mnu_file_saveann2fileANN.Enabled = false;
			mnu_file_loadannotationANN.Enabled = false;

			mnu_file_loadannotationNXP.Enabled = true;
			mnu_file_saveann2fileNXP.Enabled = true;

		}

		private void menuItemNotateAnnotations_click(object sender, System.EventArgs e)
		{
			m_loadOptions.AnnType = m_saveOptions.AnnType = AnnotationType.NotateXpress;

			mnu_file_saveann2fileANN.Enabled = false;
			mnu_file_loadannotationANN.Enabled = false;

			mnu_file_loadannotationNXP.Enabled = true;
			mnu_file_saveann2fileNXP.Enabled = true;
		}

		private void menuItemViewDirectorAnnotations_click(object sender, System.EventArgs e)
		{
			m_loadOptions.AnnType = m_saveOptions.AnnType = AnnotationType.TMSSequoia;

			mnu_file_saveann2fileANN.Enabled = true;
			mnu_file_loadannotationANN.Enabled = true;

			mnu_file_loadannotationNXP.Enabled = false;
			mnu_file_saveann2fileNXP.Enabled = false;
		}

		private void menuItemLoadAnnotations_Click(object sender, System.EventArgs e)
		{
			notateXpress1.ImagXpressLoad = !notateXpress1.ImagXpressLoad;
		}

		private void menuItemSaveAnnotations_Click(object sender, System.EventArgs e)
		{
			notateXpress1.ImagXpressSave = !notateXpress1.ImagXpressSave;
		}

		private void mnu_file_save_Click(object sender, System.EventArgs e)
		{
			saveFileDialog.Title = "Save Tiff File";
			saveFileDialog.Filter = "Tiff File (*.tif) | *.tif";
			saveFileDialog.DefaultExt = ".tif";
			saveFileDialog.InitialDirectory = strCurrentDir;
			saveFileDialog.ShowDialog (this);

			string strSaveFilePath = saveFileDialog.FileName;
			if (strSaveFilePath != "" && strSaveFilePath != null)
			{
				
				PegasusImaging.WinForms.ImagXpress8.SaveOptions saveOptions = new PegasusImaging.WinForms.ImagXpress8.SaveOptions();
				imagXView1.Image.Save(strSaveFilePath, saveOptions);
			}
		}

		private void mnu_file_wangcompatible_Click(object sender, System.EventArgs e)
		{

			if (m_saveOptions.PreserveWangLayers)
			{
				m_loadOptions.PreserveWangLayers = m_saveOptions.PreserveWangLayers = false;
			}
			else
			{
				m_loadOptions.PreserveWangLayers = m_saveOptions.PreserveWangLayers = true;
			}
		}

            

		private void menuItemSaveByteArray_Click(object sender, System.EventArgs e)
		{
			if(_SavedAnnotationByteArray != null)
				_SavedAnnotationByteArray = null;

			_SavedAnnotationByteArray = notateXpress1.Layers.SaveToByteArray(m_saveOptions);

		}

		private void menuItemUnicodeMode_Click(object sender, System.EventArgs e)
		{
			m_loadOptions.UnicodeMode = !m_loadOptions.UnicodeMode;
		}

		private void menuItemLoadByteArray_Click(object sender, System.EventArgs e)
		{
			if(_SavedAnnotationByteArray != null)
				notateXpress1.Layers.FromByteArray(_SavedAnnotationByteArray, m_loadOptions);
		}

		private void menuItemSaveMemoryStream_Click(object sender, System.EventArgs e)
		{
			if(_SavedAnnotationMemoryStream != null)
				_SavedAnnotationMemoryStream = null;

			_SavedAnnotationMemoryStream = notateXpress1.Layers.SaveToMemoryStream(m_saveOptions);
		}

		private void menuItemLoadMemoryStream_Click(object sender, System.EventArgs e)
		{
			if(_SavedAnnotationMemoryStream != null)
				notateXpress1.Layers.FromMemoryStream(_SavedAnnotationMemoryStream, m_loadOptions);
		}

		private void mnu_fileIResY_Click(object sender, System.EventArgs e)
		{
			if (notateXpress1.FontScaling == FontScaling.ResolutionY)
			{
				notateXpress1.FontScaling = FontScaling.Normal;
			}
			else
			{
				notateXpress1.FontScaling = FontScaling.ResolutionY;
			}
			mnu_fileIResY.Checked = (notateXpress1.FontScaling == FontScaling.ResolutionY);
		}

		private void menuItemToggleAllowPain_Click(object sender, System.EventArgs e)
		{
			notateXpress1.AllowPaint = !notateXpress1.AllowPaint;
			menuItemToggleAllowPaint.Checked = notateXpress1.AllowPaint;
		}

		private void mnu_file_exit_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.Application.Exit();
		}

		private void mnu_objects_autoselect_Click(object sender, System.EventArgs e)
		{
			_bAutoSelect = !_bAutoSelect;
			mnu_objects_autoselect.Checked = _bAutoSelect;
		}

		private void mnu_objects_reverse_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
				if(layer.Elements.Count > 0)
					layer.Elements.Reverse ();
		}

		private void mnu_objects_create10_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				int top = 15, left = 15, right = 115, bottom = 115;

				for (int i = 1; i <= 10; i++)
				{
					System.Random rand = new System.Random ();
					System.Drawing.Rectangle rect = new Rectangle(top, left, right, bottom);
					if (i % 2 > 0)
					{ // create a rectangle object
						RectangleTool element = new RectangleTool();
						element.PenWidth = (i % 3 + 1);
						element.PenColor = System.Drawing.Color.FromArgb ((int) (rand.NextDouble () * 255), (int) (rand.NextDouble () * 255), (int) (rand.NextDouble () * 255));
						element.FillColor = System.Drawing.Color.FromArgb ((int) (rand.NextDouble () * 255), (int) (rand.NextDouble () * 255), (int) (rand.NextDouble () * 255));
						element.BoundingRectangle = rect;
						layer.Elements.Add(element);
					}
					else
					{ // create an ellipse object
						EllipseTool element = new EllipseTool();
						element.PenWidth = (i % 3 + 1);
						element.PenColor = System.Drawing.Color.FromArgb ((int) (rand.NextDouble () * 255), (int) (rand.NextDouble () * 255), (int) (rand.NextDouble () * 255));
						element.FillColor = System.Drawing.Color.FromArgb ((int) (rand.NextDouble () * 255), (int) (rand.NextDouble () * 255), (int) (rand.NextDouble () * 255));
						element.BoundingRectangle = rect;
						layer.Elements.Add(element);
					}

					top = top + 2;
					left = left + 2;
					right = right + 2;
					bottom = bottom + 2;
				}
			}
		}

		private void mnu_objects_rects_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				int top = 20, left = 20, right = 120, bottom = 120;
				for (int i = 1; i <= 10; i++)
				{
					// create a rectangle object
					RectangleTool element = new RectangleTool();
					System.Drawing.Rectangle rect = new Rectangle(top, left, right, bottom);
					element.BoundingRectangle = rect;
					element.PenWidth = 1;
					System.Random rand = new System.Random ();
					element.PenColor = (System.Drawing.Color.FromArgb ((int) (rand.NextDouble () * 255), (int) (rand.NextDouble () * 255), (int) (rand.NextDouble () * 255)));
					element.FillColor = (System.Drawing.Color.FromArgb ((int) (rand.NextDouble () * 255), (int) (rand.NextDouble () * 255), (int) (rand.NextDouble () * 255)));

					int x = new System.Random().Next() * 400;
					int y = new System.Random().Next() * 400;

					layer.Elements.Add(element);
					top += 2;
					left += 2;
					right += 2;
					bottom += 2;
				}
			}
		}

		private void mnu_objects_buttons_Click(object sender, System.EventArgs e)
		{
			//---------------
			// initialize object location
			//---------------
			int top = 10;
			int left = 10;
			int right = 100;
			int bottom = 35;

			//--------------
			// now using this one programmable element,
			// create 10 buttons
			//--------------
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for (int i = 1; i <= 10; i++)
				{
					//---------------
					// create a programmable element
					//---------------
					ButtonTool element = new ButtonTool();

					//---------------
					// set some programmable element properties
					//---------------
					System.Drawing.Rectangle rect = new Rectangle(top, left, right, bottom);
					element.BoundingRectangle = rect;
					element.PenWidth = 4;
					element.Text = "Button " + i.ToString ();

					System.Random rand = new System.Random ();
					int x = new System.Random().Next() * 400;
					int y = new System.Random().Next() * 400;

					layer.Elements.Add(element);
					top += 2;
					left += 2;
					right += 2;
					bottom += 2;
				}
			}
		}

		private void mnu_objects_createtransrect_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				RectangleTool element = new RectangleTool();
				System.Drawing.Rectangle rect = new Rectangle(100, 100, 200, 200);
				element.BoundingRectangle = rect;
				element.FillColor = System.Drawing.Color.Yellow;
				element.PenWidth = 1;
				element.PenColor = System.Drawing.Color.Blue;
				element.HighlightFill = true; /* ****** Outline is "permanent" without this line */
				layer.Elements.Add(element);
				layer.Toolbar.Selected = AnnotationTool.PointerTool;
			}
		}

		private void mnu_objects_createImage_Click(object sender, System.EventArgs e)
		{
			try
			{
				Layer layer = notateXpress1.Layers.Selected();
				if(layer != null)
				{
					imageObject = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(System.IO.Path.Combine (System.IO.Directory.GetCurrentDirectory (), "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\vermont.jpg"));

					ImageTool element = new ImageTool();
					element.DibHandle = imageObject.ToHdib(false);
					System.Drawing.Rectangle rect = new Rectangle(100, 100, 100 + imageObject.ImageXData.Width, 100 + imageObject.ImageXData.Height);
					element.BoundingRectangle = rect;
					layer.Elements.Add(element);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error msg:   " + ex.Message + "\n" +
					"Error type:  " + ex.GetType().ToString());
			}
		}

		private void mnu_objects_nudgeup_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				for(int i = 0; i < layer.Elements.Count; i++)
				{
					if(layer.Elements[i].Selected)
						layer.Elements[i].NudgeUp(); //TODO - doesnt seem to do anything, DevTrack #161
				}
			}
		}

		private void mnu_objects_nudgedn_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					element.NudgeDown();  //TODO - doesnt seem to do anything, DevTrack #161
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_objects_inflate_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					//TODO - DevTrack 157
					System.Drawing.Rectangle rect = element.BoundingRectangle;
					for (int i = 1; i <= 100; i++)
					{
						element.BoundingRectangle = new Rectangle(Math.Max(rect.Left - i, 0), Math.Max(rect.Top - i, 0), rect.Width + i, rect.Height + i);
						Application.DoEvents ();
					}
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_objects_largetext_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				TextTool element = new TextTool();
				System.Drawing.Rectangle rect = new Rectangle(30, 30, 200, 300);
				element.BoundingRectangle = rect;

				MessageBox.Show(element.BoundingRectangle.Height.ToString());
MessageBox.Show(element.BoundingRectangle.Width.ToString());

				element.TextJustification = TextJustification.Center;
				element.BackColor = System.Drawing.Color.FromArgb (255, 255, 0);
				element.TextColor = System.Drawing.Color.FromArgb (0, 0, 0);
				element.BackStyle = BackStyle.Translucent;
				element.PenWidth = 2;
				element.PenColor = System.Drawing.Color.FromArgb (255, 0, 0);

				string strText = "NotateXpress Version 8\n\n";
				strText += "NotateXpress version 8's programmatic annotation features let you enter, edit, and query all types of annotation objects programmatically. ";
				strText += "Using NotateXpress's programmatic capabilities is fun, fascinating, and has a New Ruler Tool! Try it today.";

				element.Text = strText;
				System.Drawing.Font fontNew = new System.Drawing.Font ("Times New Roman", 12);
				element.TextFont = fontNew;
				layer.Elements.Add(element);
			}
		}

		private void mnu_objects_useMLE_Click(object sender, System.EventArgs e)
		{
			notateXpress1.MultiLineEdit = !notateXpress1.MultiLineEdit;
			mnu_objects_useMLE.Checked = notateXpress1.MultiLineEdit;
		}

		private void mnu_objects_brand_Click(object sender, System.EventArgs e)
		{
			notateXpress1.Layers.Brand (24);
		}

		private void menuItemCreateNewNX8_click(object sender, System.EventArgs e)
		{
			notateXpress1.Layers.Clear();
			// 'detach' existing NX8 from the IX window
			notateXpress1.ClientWindow = IntPtr.Zero;
			notateXpress1.Dispose();
			notateXpress1 = null;
			// create new NX8 component
			notateXpress1 = new PegasusImaging.WinForms.NotateXpress8.NotateXpress();
			// init default values for new NX8 component
			notateXpress1.ClientWindow = imagXView1.Handle;
			notateXpress1.AllowPaint = true;
			notateXpress1.Debug = false;
			notateXpress1.DebugLogFile = "c:\\NotateXpress8.log";
			notateXpress1.ErrorLevel = PegasusImaging.WinForms.NotateXpress8.ErrorLevelInfo.Production;
			notateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress8.FontScaling.Normal;
			notateXpress1.ImagXpressLoad = true;
			notateXpress1.ImagXpressSave = true;
			notateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress8.AnnotationMode.Edit;
			notateXpress1.MultiLineEdit = false;
			// create a layer for the new NX8 component
			createNewLayer("Layer1", imageObject.ToHdib(false));
		}

		private void mnu_orient_ltbr_Click(object sender, System.EventArgs e)
		{
			imagXProcessor.Image = imagXView1.Image;
			imagXProcessor.Rotate (270);
		}

		private void mnu_orient_brtl_Click(object sender, System.EventArgs e)
		{
			imagXProcessor.Image = imagXView1.Image;
			imagXProcessor.Flip();
		}

		private void mnu_orient_rblt_Click(object sender, System.EventArgs e)
		{
			imagXProcessor.Image = imagXView1.Image;
			imagXProcessor.Rotate (90);
		}

		private void mnu_orient_tlbr_Click(object sender, System.EventArgs e)
		{
			imagXProcessor.Image = imagXView1.Image;
			imagXProcessor.Mirror ();
		}

		private void mnu_orientation_rotate180_Click(object sender, System.EventArgs e)
		{
			imagXProcessor.Image = imagXView1.Image;
			imagXProcessor.Rotate (180);
		}

		private void mnu_stuff_settext_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				string newText = "This is a test and only a test. Had this been something other than a test, something significant would have happened!";
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					SetPropertyValue(element, "Text", newText);
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_stuff_backcolor_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					SetPropertyValue(element, "BackColor", System.Drawing.Color.FromArgb (0, 255, 0));
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_stuff_pencolor_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					SetPropertyValue(element, "PenColor", System.Drawing.Color.FromArgb (0, 255, 0));
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_stuff_penwidth_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				System.Int32 newWidth = 15;
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					SetPropertyValue(element, "PenWidth", newWidth);
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_stuff_bevelshadow_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					SetPropertyValue(element, "BevelShadowColor", System.Drawing.Color.FromArgb (0, 255, 0));
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_stuff_bevellight_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					SetPropertyValue(element, "BevelLightColor", System.Drawing.Color.FromArgb (0, 255, 255));
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_stuff_setfont_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				System.Drawing.Font font = null;
				// only perform on first selected items
				Element element = layer.Elements.FirstSelected();
				if(element != null)
				{
					font = (Font)GetPropertyValue(element, "TextFont");
					if(font != null)
						fontDialog1.Font = font;

					if( fontDialog1.ShowDialog(this) != DialogResult.Cancel)
						font = fontDialog1.Font;
					else
						return; // font dialog canceled
				}
				while(element != null)
				{
					SetPropertyValue(element, "TextFont", font);
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_stuff_getitemtext_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				// only perform on first selected element
				object element = layer.Elements.FirstSelected();
				if(element != null)
				{
					object obj = GetPropertyValue(element, "Text");
					if(obj != null)
						System.Windows.Forms.MessageBox.Show((String)obj);
				}
			}
		}

		private void mnu_stuff_hb_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					object obj = GetPropertyValue(this, "HighlightBack");
					if(obj != null)
					{
						Boolean b = (Boolean)obj;
						b = !b;
						SetPropertyValue(this, "HighlightBack", b);
					}
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_stuff_hf_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					object obj = GetPropertyValue(element, "HighlightFill");
					if(obj != null)
					{
						Boolean b = (Boolean)obj;
						b = !b;
						SetPropertyValue(element, "HighlightFill", b);
					}
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_stuff_moveable_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					object obj = GetPropertyValue(element, "Moveable");
					if(obj != null)
					{
						Boolean b = (Boolean)obj;
						b = !b;
						SetPropertyValue(element, "Moveable", b);
					}
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_stuff_fixed_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					object obj = GetPropertyValue(element, "Fixed");
					if(obj != null)
					{
						Boolean b = (Boolean)obj;
						b = !b;
						SetPropertyValue(element, "Fixed", b);
					}
					element = layer.Elements.NextSelected();
				}
			}
		}
		private void mnu_stuff_sizeable_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					object obj = GetPropertyValue(element, "Sizeable");
					if(obj != null)
					{
						Boolean b = (Boolean)obj;
						b = !b;
						SetPropertyValue(element, "Sizeable", b);
					}
					element = layer.Elements.NextSelected();
				}
			}
		}

		private void mnu_stuff_getitemfont_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				// only perform on first selected element
				object element = layer.Elements.FirstSelected();
				if(element != null)
				{
					object obj = GetPropertyValue(element, "TextFont");
					if(obj != null)
					{
						System.Drawing.Font font = (System.Drawing.Font)obj;
						MessageBox.Show (font.Name + " " + font.SizeInPoints.ToString ());
					}
				}
			}
		}

		private void mnu_stuff_setnewdib_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				try
				{
					string strCurrentDir = System.IO.Directory.GetCurrentDirectory ();
					string strPath = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\Water.jpg");
					imageObject = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strPath);
					if (imageObject != null)
					{
						if(layer.Toolbar.ImageToolbarDefaults.DibHandle != System.IntPtr.Zero)
						{
							GlobalFree((System.IntPtr)layer.Toolbar.ImageToolbarDefaults.DibHandle);
							layer.Toolbar.ImageToolbarDefaults.DibHandle = System.IntPtr.Zero;
						}
						layer.Toolbar.ImageToolbarDefaults.DibHandle = imageObject.ToHdib(false);
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show("Error msg:   " + ex.Message + "\n" +
						"Error type:  " + ex.GetType().ToString());
				}
			}
		}

		private void mnu_mode_editmode_Click(object sender, System.EventArgs e)
		{
			notateXpress1.InteractMode = AnnotationMode.Edit;
			mnu_mode_interactive.Checked = false;
			mnu_mode_editmode.Checked = true;
		}

		private void mnu_mode_interactive_Click(object sender, System.EventArgs e)
		{
			notateXpress1.InteractMode = AnnotationMode.Interactive;
			mnu_mode_interactive.Checked = true;
			mnu_mode_editmode.Checked = false;
		}

		private void mnu_cm_enable_Click(object sender, System.EventArgs e)
		{
			notateXpress1.MenuSetEnabled(MenuType.Context, AnnotationTool.NoTool, !notateXpress1.MenuGetEnabled(MenuType.Context, AnnotationTool.NoTool));
		}

		private void ZoomIn_Click(object sender, System.EventArgs e)
		{
			imagXView1.ZoomFactor = imagXView1.ZoomFactor / .9;
		}

		private void ZoomOut_Click(object sender, System.EventArgs e)
		{
			imagXView1.ZoomFactor = imagXView1.ZoomFactor * .9;
			//this.Text = "Image = " + imagXpressdotNet.IWidth + " x " + imagXpressdotNet.IHeight + " " + (imagXpressdotNet.IPZoomF * 100) + "%";
		}

		private object GetPropertyValue(object Object, string strPropName)
		{
			foreach (System.Reflection.PropertyInfo property in Object.GetType().GetProperties())
				if(property.Name == strPropName)
					return property.GetValue(Object, null);

			return null;
		}

		private bool SetPropertyValue(object Object, string strPropName, object objValue)
		{
			foreach (System.Reflection.PropertyInfo property in Object.GetType().GetProperties())
				if(property.Name == strPropName)
				{
					property.SetValue(Object, objValue, null);
					return true;
				}
			return false;
		}

		private bool HasProperty(object Object, string strPropName)
		{
			foreach (System.Reflection.PropertyInfo property in Object.GetType().GetProperties())
				if(property.Name == strPropName)
					return true;

			return false;
		}

		private void mnu_stuff_Select(object sender, System.EventArgs e)
		{

			//  ------------
			//   disable all menu selections on this menu
			//  ------------
			menuItemBoundingRectangle.Enabled = false;
			mnu_stuff_backcolor.Enabled = false;
			mnu_stuff_bevellight.Enabled = false;
			mnu_stuff_bevelshadow.Enabled = false;
			mnu_stuff_getitemfont.Enabled = false;
			mnu_stuff_getitemtext.Enabled = false;
			mnu_stuff_hb.Enabled = false;
			mnu_stuff_hf.Enabled = false;
			mnu_stuff_moveable.Enabled = false;
			mnu_stuff_fixed.Enabled = false;
			mnu_stuff_pencolor.Enabled = false;
			mnu_stuff_penwidth.Enabled = false;
			mnu_stuff_setfont.Enabled = false;
			mnu_stuff_setnewdib.Enabled = false;
			mnu_stuff_settext.Enabled = false;
			mnu_stuff_sizeable.Enabled = false;
			mnu_stuff_toggleuserdraw.Enabled = false;

			Layer layer = notateXpress1.Layers.Selected();
			if (layer != null)
			{
				//-----------------
				// Enable menu items as appropriate for this menu
				//-----------------
				Element element = layer.Elements.FirstSelected();
				// only one element must be selected to enable
				if(element != null && layer.Elements.NextSelected() == null)
				{
					menuItemBoundingRectangle.Enabled = true;
					mnu_stuff_settext.Enabled = HasProperty (element, "Text");
					mnu_stuff_backcolor.Enabled = HasProperty (element, "BackColor");
					mnu_stuff_pencolor.Enabled = HasProperty (element, "PenColor");
					mnu_stuff_penwidth.Enabled = HasProperty (element, "PenWidth");
					mnu_stuff_bevelshadow.Enabled = HasProperty (element, "BevelShadowColor");
					mnu_stuff_bevellight.Enabled = HasProperty (element, "BevelLightColor");
					mnu_stuff_setfont.Enabled = HasProperty (element, "TextFont");
					mnu_stuff_getitemtext.Enabled = HasProperty (element, "Text");
					mnu_stuff_hb.Enabled = HasProperty (element, "HighlightBack");
					mnu_stuff_hf.Enabled = HasProperty (element, "HighlightFill");
					mnu_stuff_moveable.Enabled = HasProperty (element, "Moveable");
					mnu_stuff_fixed.Enabled = HasProperty (element, "Fixed");
					mnu_stuff_sizeable.Enabled = HasProperty (element, "Sizeable");
					mnu_stuff_getitemfont.Enabled = HasProperty (element, "TextFont");
					mnu_stuff_setnewdib.Enabled = HasProperty (element, "DibHandle");
					mnu_stuff_toggleuserdraw.Enabled = HasProperty (element, "UserDraw");
					if(mnu_stuff_toggleuserdraw.Enabled)
						mnu_stuff_toggleuserdraw.Checked = (bool)GetPropertyValue(element, "UserDraw");
				}
			}
		}

		private void menuItemBoundingRectangle_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if (layer != null)
			{
				Element element = layer.Elements.FirstSelected();
//ImageTool it = new ImageTool();
//
//				ImageToolbarDefaults tb = new PegasusImaging.WinForms.NotateXpress8.ImageToolbarDefaults();
				
				
				
				// only one element must be selected
				if(element != null && layer.Elements.NextSelected() == null)
				{
					BoundingRectDlg dlg = new BoundingRectDlg(element.BoundingRectangle,new System.Drawing.Rectangle(0,0,imagXView1.Image.ImageXData.Width,imagXView1.Image.ImageXData.Height));
					if(System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
					{
						element.BoundingRectangle = dlg.Rect;
					}
				}
			}
		}

		private void mnu_stuff_toggleuserdraw_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if (layer != null)
			{
				Element element = layer.Elements.FirstSelected();
				// only one element must be selected
				if(element != null && element is RectangleTool && layer.Elements.NextSelected() == null)
				{
					RectangleTool rectTool = (RectangleTool)element;



					rectTool.UserDraw = !rectTool.UserDraw;
				}
			}
		}

		private void mnu_layers_Select(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			bool bEnabled = layer != null ? true : false;

			mnu_layers_deletecurrent.Enabled = bEnabled;
			mnu_layers_togglehide.Enabled = bEnabled;
			mnu_layers_setdescription.Enabled = bEnabled;
			mnu_layers_setname.Enabled = bEnabled;
			mnu_layers_setpassword.Enabled = bEnabled;
			menuItemDeactivateCurrentLayer.Enabled = bEnabled;
			menuItemSendToBack.Enabled = false;
			menuItemBringToFront.Enabled = false;
			menuItemDeleteSelected.Enabled = false;
			menuItemDisableToolbar.Enabled = bEnabled;

			if (bEnabled)
			{
				mnu_layers_toggletoolbar.Checked = layer.Toolbar.Visible;

				if (layer.Visible)
				{
					mnu_layers_togglehide.Text = "Hide current";
				}
				else
				{
					mnu_layers_togglehide.Text = "Show current";
				}

				if (layer.Active)
				{
					menuItemDeactivateCurrentLayer.Text = "Deactivate current";
				}
				else
				{
					menuItemDeactivateCurrentLayer.Text = "Activate current";
				}

				if(layer.Toolbar.Enabled)
					menuItemDisableToolbar.Text = "Disable toolbar";
				else
					menuItemDisableToolbar.Text = "Enable toolbar";

				if(layer.Elements.FirstSelected() != null)
				{
					menuItemSendToBack.Enabled = true;
					menuItemBringToFront.Enabled = true;
					menuItemDeleteSelected.Enabled = true;
				}
			}
			else
				mnu_layers_toggletoolbar.Enabled = false;

			if (notateXpress1.Layers.Count < 1)
			{
				mnu_layers_setcurrent.Enabled = false;
			}
			else
			{
				mnu_layers_setcurrent.Enabled = true;

				// ---------------
				//  clean set current layer sub-menu
				// ---------------
				mnu_layers_setcurrent.MenuItems.Clear ();

				System.EventHandler layers_setcurrent_eventhandler = new System.EventHandler(this.mnu_layers_setcurrent_Click);

				// ---------------
				//  add the description for each layer under this submenu
				// ---------------
				for(int i = 0; i < notateXpress1.Layers.Count; i++)
				{
					string str = notateXpress1.Layers[i].Name + "   [" + (notateXpress1.Layers[i].Visible ? "Visible " : "NOTVisible ")
						+ (notateXpress1.Layers[i].Active ? "Active " : "NOTActive ")
						+ notateXpress1.Layers[i].Elements.Count.ToString() + "-Elements]";
					mnu_layers_setcurrent.MenuItems.Add(str); // creates new MenuItem
					if(notateXpress1.Layers.Selected() == notateXpress1.Layers[i])
						mnu_layers_setcurrent.MenuItems[i].Checked = true; // check if selected layer
					else
						mnu_layers_setcurrent.MenuItems[i].Checked = false;
					mnu_layers_setcurrent.MenuItems[i].Click += layers_setcurrent_eventhandler; // set event handler for new MenuItem
				}
			}
		}

		private void mnu_layers_create_Click(object sender, System.EventArgs e)
		{
			InputDlg dlg = new InputDlg("Specify Name of Layer to Create");
			if(System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
			{
				// Create a new layer, note that it will also now be the selected layer
				createNewLayer(dlg.InputText, System.IntPtr.Zero);
			}
		}

		private void mnu_layers_setcurrent_Click(object sender, System.EventArgs e)
		{
			if(sender is System.Windows.Forms.MenuItem)
			{
				notateXpress1.Layers[((System.Windows.Forms.MenuItem)sender).Index].Select();
			}
		}

		private void mnu_layers_deletecurrent_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
				notateXpress1.Layers.Remove(layer);
		}

		private void mnu_layers_togglehide_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
				layer.Visible = !layer.Visible;
		}

		private void menuItemDeactivateCurrentLayer_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
				layer.Active = !layer.Active;

			MessageBox.Show(layer.Active.ToString());
		}

		private void menuItemSaveCurrentLayer_click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				saveFileDialog.Title = "Save Current Annotation Layer to File";
				saveFileDialog.Filter = "NotateXpress File (*.nxp) | *.nxp";
				saveFileDialog.DefaultExt = ".nxp";
				saveFileDialog.ShowDialog (this);

				string strFileName = saveFileDialog.FileName;
				if (strFileName != "" && strFileName != null)
				{
					m_saveOptions.AllLayers = false;
					m_saveOptions.SaveLayer = layer;
					notateXpress1.Layers.Comments = "This file contains 1 saved layer";
					notateXpress1.Layers.Subject = "Single layer annotation file";
					notateXpress1.Layers.Title = "One awesome single layer annotation file";
					notateXpress1.Layers.SaveToFile(strFileName, m_saveOptions);
					m_saveOptions.AllLayers = true;
				}
			}
		}

		private void mnu_layers_toggletoolbar_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				if (layer.Toolbar.Visible)
				{
					layer.Toolbar.Visible = false;
					menuItemToolBar.Enabled = false;
				}
				else
				{
					layer.Toolbar.Visible = true;
					menuItemToolBar.Enabled = true;
				}
			}
		}

		private void menuItemDisableToolbar_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				if (layer.Toolbar.Enabled)
				{
					layer.Toolbar.Enabled = false;
				}
				else
				{
					layer.Toolbar.Enabled = true;
				}
			}
		}

		private void mnu_layers_setdescription_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				InputDlg dlg = new InputDlg("Set Layer Description");
				dlg.InputText = layer.Description;
				if(System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
				{
					layer.Description = dlg.InputText;
				}
			}
		}

		private void mnu_layers_setname_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				InputDlg dlg = new InputDlg("Set Layer Name");
				dlg.InputText = layer.Name;
				if(System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
				{
					layer.Name = dlg.InputText;
				}
			}
		}

		private void mnu_layers_setpassword_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				InputDlg dlg = new InputDlg("Set Layer Password");
				dlg.InputText = layer.Password;
				if(System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
				{
					layer.Password = dlg.InputText;
				}
			}
		}

		private void menuItemSendToBack_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
				layer.Elements.SendToBack();
			imagXView1.Refresh();
		}

		private void menuItemBringToFront_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
				layer.Elements.BringToFront();
			imagXView1.Refresh();
		}

		private void menuItemDeleteSelected_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
				layer.Elements.DeleteSelected();
		}

		private void mnu_cm_Select(object sender, System.EventArgs e)
		{
			if (notateXpress1.MenuGetEnabled(MenuType.Context, AnnotationTool.NoTool))
			{
				mnu_cm_enable.Text = "Disable";
			}
			else
			{
				mnu_cm_enable.Text = "Enable";
			}
		}

		private void mnu_objects_Select(object sender, System.EventArgs e)
		{
			// ------------
			// get the selected/current layer
			// ------------
			Layer layer = notateXpress1.Layers.Selected();
			if (layer == null)
			{
				// ------------
				//  disable all menu selections on this menu
				// ------------
				foreach (MenuItem item in mnu_objects.MenuItems)
				{
					item.Enabled = false;
				}
			}
			else
			{
				int nCount = layer.Elements.Count;
				mnu_objects_reverse.Enabled = (nCount > 1) ? true : false;
				mnu_objects_brand.Enabled = (nCount > 0) ? true : false;
				mnu_objects_nudgeup.Enabled = layer.Elements.IsSelection ? true : false;
				mnu_objects_nudgedn.Enabled = layer.Elements.IsSelection ? true : false;
				mnu_objects_createtransrect.Enabled = true;

				// determine whether to enable inflate
				mnu_objects_inflate.Enabled = false;
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{ // enable inflate if ALL the seleted elements are either an EllipseTool, RectangleTool, TextTool, or ButtonTool
					if(element is EllipseTool || element is RectangleTool || element is TextTool || element is ButtonTool)
						mnu_objects_inflate.Enabled = true;
					else
					{
						mnu_objects_inflate.Enabled = false;
						break;
					}
					element = layer.Elements.NextSelected();

					
				}
			}
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				//set the annotation type to the Ruler annotation
				RulerTool element = new RulerTool();
				System.Drawing.Rectangle rect = new Rectangle(25, 50, 200, 100);
				element.BoundingRectangle = rect;
				element.PenWidth = 4;
				element.GaugeLength = 20;
				element.MeasurementUnit = MeasurementUnit.Pixels;
				element.Precision = 1;
				layer.Elements.Add(element);
				element.Selected = true;
			}
		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{ // Redaction
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				TextTool element = new TextTool();
				System.Drawing.Rectangle rect = new Rectangle(100, 100, 300, 200);
				element.BoundingRectangle = rect;
				element.TextJustification = TextJustification.Center;
				element.TextColor = System.Drawing.Color.Red;
				element.BackStyle = BackStyle.Opaque;
				element.Text = "Approved by Pegasus Imaging Corp." + System.DateTime.Now;
				element.PenWidth = 1;
				element.PenColor = System.Drawing.Color.White;
				element.BackColor = System.Drawing.Color.White;
				layer.Elements.Add(element);
				layer.Toolbar.Selected = AnnotationTool.PointerTool;
			}
		}

		private void menuItemGroups_Click(object sender, System.EventArgs e)
		{
			mnu_objects_allowgrouping.Enabled = false;
			menuItemCreateGroup.Enabled = false;
			menuItemDeleteGroup.Enabled = false;
			menuItemSetGroupEmpty.Enabled = false;
			menuItemSelectGroupItems.Enabled = false;
			menuItemGroupAddSelectedItems.Enabled = false;
			menuItemGroupRemoveSelectedItems.Enabled = false;
			menuItemYokeGroupItems.Enabled = false;
			menuItemSetGroupUserString.Enabled = false;
			menuItemMirrorYoked.Enabled = false;
			menuItemInvertYoked.Enabled = false;

			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				mnu_objects_allowgrouping.Enabled = true;
				mnu_objects_allowgrouping.Checked = layer.Groups.AllowUserGrouping;
				if(!mnu_objects_allowgrouping.Checked)
					return;

				menuItemCreateGroup.Enabled = true;

				if(layer.Groups.Count > 0)
				{
					// ---------------
					//  Initialize delete group sub-menu
					// ---------------
					menuItemDeleteGroup.Enabled = true;
					menuItemDeleteGroup.MenuItems.Clear (); //  clean groups sub-menu
					System.EventHandler menuItemDeleteGroup_eventhandler = new System.EventHandler(this.menuItemDeleteGroup_Click);
					for(int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
					{
						menuItemDeleteGroup.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
						menuItemDeleteGroup.MenuItems[i].Click += menuItemDeleteGroup_eventhandler; // set event handler for new MenuItem
					}
					// ---------------
					//  Initialize set group empty sub-menu
					// ---------------
					menuItemSetGroupEmpty.Enabled = true;
					menuItemSetGroupEmpty.MenuItems.Clear (); //  clean groups sub-menu
					System.EventHandler menuItemSetGroupEmpty_eventhandler = new System.EventHandler(this.menuItemSetGroupEmpty_Click);
					for(int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
					{
						string str = layer.Groups[i].Name + "  #Items: " + layer.Groups[i].GroupElements.Count.ToString();
						menuItemSetGroupEmpty.MenuItems.Add(str); // creates new MenuItem
						menuItemSetGroupEmpty.MenuItems[i].Enabled = layer.Groups[i].GroupElements.Count > 0 ? true : false;
						menuItemSetGroupEmpty.MenuItems[i].Click += menuItemSetGroupEmpty_eventhandler; // set event handler for new MenuItem
					}
					// ---------------
					//  Initialize select group items sub-menu
					// ---------------
					menuItemSelectGroupItems.Enabled = true;
					menuItemSelectGroupItems.MenuItems.Clear (); //  clean groups sub-menu
					System.EventHandler menuItemSelectGroupItems_eventhandler = new System.EventHandler(this.menuItemSelectGroupItems_Click);
					for(int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
					{
						menuItemSelectGroupItems.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
						menuItemSelectGroupItems.MenuItems[i].Enabled = layer.Groups[i].GroupElements.Count > 0 ? true : false;
						menuItemSelectGroupItems.MenuItems[i].Click += menuItemSelectGroupItems_eventhandler; // set event handler for new MenuItem
					}
					// ---------------
					//  Initialize add selected items to group
					// ---------------
					menuItemGroupAddSelectedItems.Enabled = layer.Elements.FirstSelected() != null ? true : false;
					menuItemGroupAddSelectedItems.MenuItems.Clear (); //  clean groups sub-menu
					System.EventHandler menuItemGroupAddSelectedItems_eventhandler = new System.EventHandler(this.menuItemGroupAddSelectedItems_Click);
					for(int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
					{
						menuItemGroupAddSelectedItems.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
						menuItemGroupAddSelectedItems.MenuItems[i].Click += menuItemGroupAddSelectedItems_eventhandler; // set event handler for new MenuItem
					}
					// ---------------
					//  Initialize remove selected items from group
					// ---------------
					menuItemGroupRemoveSelectedItems.Enabled = layer.Elements.FirstSelected() != null ? true : false;
					menuItemGroupRemoveSelectedItems.MenuItems.Clear (); //  clean groups sub-menu
					System.EventHandler menuItemGroupRemoveSelectedItems_eventhandler = new System.EventHandler(this.menuItemGroupRemoveSelectedItems_Click);
					for(int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
					{
						menuItemGroupRemoveSelectedItems.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
						menuItemGroupRemoveSelectedItems.MenuItems[i].Enabled = layer.Groups[i].GroupElements.Count > 0 ? true : false;
						menuItemGroupRemoveSelectedItems.MenuItems[i].Click += menuItemGroupRemoveSelectedItems_eventhandler; // set event handler for new MenuItem
					}
					// ---------------
					//  Initialize yoke group items sub-menu
					// ---------------
					menuItemYokeGroupItems.Enabled = true;
					menuItemYokeGroupItems.MenuItems.Clear (); //  clean groups sub-menu
					System.EventHandler menuItemYokeGroupItems_eventhandler = new System.EventHandler(this.menuItemYokeGroupItems_Click);
					for(int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
					{
						menuItemYokeGroupItems.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
						menuItemYokeGroupItems.MenuItems[i].Checked = layer.Groups[i].Yoked;
						menuItemYokeGroupItems.MenuItems[i].Click += menuItemYokeGroupItems_eventhandler; // set event handler for new MenuItem
					}
					// ---------------
					//  Initialize invert group items Yoked sub-menu
					// ---------------
					menuItemInvertYoked.Enabled = true;
					menuItemInvertYoked.MenuItems.Clear (); //  clean groups sub-menu
					System.EventHandler menuItemInvertYoked_eventhandler = new System.EventHandler(this.menuItemInvertYoked_click);
					for(int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
					{
						menuItemInvertYoked.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
						menuItemInvertYoked.MenuItems[i].Enabled = layer.Groups[i].Yoked;
						menuItemInvertYoked.MenuItems[i].Click += menuItemInvertYoked_eventhandler; // set event handler for new MenuItem
					}
					// ---------------
					//  Initialize Mirror group items Yoked sub-menu
					// ---------------
					menuItemMirrorYoked.Enabled = true;
					menuItemMirrorYoked.MenuItems.Clear (); //  clean groups sub-menu
					System.EventHandler menuItemMirrorYoked_eventhandler = new System.EventHandler(this.menuItemMirrorYoked_click);
					for(int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
					{
						menuItemMirrorYoked.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
						menuItemMirrorYoked.MenuItems[i].Enabled = layer.Groups[i].Yoked;
						menuItemMirrorYoked.MenuItems[i].Click += menuItemMirrorYoked_eventhandler; // set event handler for new MenuItem
					}
					// ---------------
					//  Initialize group User String sub-menu
					// ---------------
					menuItemSetGroupUserString.Enabled = true;
					menuItemSetGroupUserString.MenuItems.Clear (); //  clean groups sub-menu
					System.EventHandler SetGroupUserString_eventhandler = new System.EventHandler(this.SetGroupUserString_Select);
					for(int i = 0; i < layer.Groups.Count; i++) //  add the name for each group under this submenu
					{
						menuItemSetGroupUserString.MenuItems.Add(layer.Groups[i].Name); // creates new MenuItem
						menuItemSetGroupUserString.MenuItems[i].Click += SetGroupUserString_eventhandler; // set event handler for new MenuItem
					}
				}
			}
		}

		private void mnu_objects_allowgrouping_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer == null)
			{
				MessageBox.Show("A Layer must be selected before a Group operation can be performed");
				return;
			}
			bool bUserGrouping = !layer.Groups.AllowUserGrouping;
			layer.Groups.AllowUserGrouping = bUserGrouping;
			mnu_objects_allowgrouping.Checked = bUserGrouping;
		}

		private void menuItemCreateGroup_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer == null)
			{
				MessageBox.Show("A Layer must be selected before a Group operation can be performed");
				return;
			}
			InputDlg dlg = new InputDlg("Specify Name of Group to create");
			if(System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
			{
				PegasusImaging.WinForms.NotateXpress8.Group group = new PegasusImaging.WinForms.NotateXpress8.Group();
				group.Name = dlg.InputText;
				layer.Groups.Add(group);
			}
		}

		private void menuItemDeleteGroup_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				if(sender is System.Windows.Forms.MenuItem)
				{
					layer.Groups.RemoveAt(((System.Windows.Forms.MenuItem)sender).Index);
				}
			}
		}

		private void menuItemSetGroupEmpty_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				if(sender is System.Windows.Forms.MenuItem)
				{
					Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
					group.GroupElements.Clear();
				}
			}
		}

		private void menuItemSelectGroupItems_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				if(sender is System.Windows.Forms.MenuItem)
				{
					Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
					// first clear any currently selected items in the layer
					Element element = layer.Elements.FirstSelected();
					while(element != null)
					{
						element.Selected = false;
						element = layer.Elements.NextSelected();
					}
					// now select all items that are in the group
					for(int i = 0; i < group.GroupElements.Count; i++)
						group.GroupElements[i].Selected = true;
				}
			}
		}

		private void menuItemGroupAddSelectedItems_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null && sender is System.Windows.Forms.MenuItem)
			{
				Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					if(element.Selected)
						group.GroupElements.Add(element);

					element = layer.Elements.NextSelected();
				}
			}
		}

		private void menuItemGroupRemoveSelectedItems_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null && sender is System.Windows.Forms.MenuItem)
			{
				Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
				Element element = layer.Elements.FirstSelected();
				while(element != null)
				{
					if(element.Selected)
						group.GroupElements.Remove(element);

					element = layer.Elements.NextSelected();
				}
			}
		}

		private void menuItemYokeGroupItems_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null && sender is System.Windows.Forms.MenuItem)
			{
				Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
				group.Yoked = !group.Yoked;
			}
		}

		private void SetGroupUserString_Select(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null && sender is System.Windows.Forms.MenuItem)
			{
				Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
				InputDlg dlg = new InputDlg("User String for Group: " + group.Name);
				dlg.InputText = group.UserString;
				if(System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
				{
					group.UserString = dlg.InputText;
				}
			}
		}

		private void menuItemMirrorYoked_click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null && sender is System.Windows.Forms.MenuItem)
			{
				Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
				group.GroupElements.GroupMirror();
			}
		}

		private void menuItemInvertYoked_click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null && sender is System.Windows.Forms.MenuItem)
			{
				Group group = layer.Groups[(((System.Windows.Forms.MenuItem)sender).Index)];
				group.GroupElements.GroupInvert();
			}
		}

		private void menuItemToolBar_Select(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{                
				bool bEnabled = layer.Toolbar.Visible;
				menuItemToolbarRulerTool.Enabled = bEnabled;
				menuItemToolbarFreehandTool.Enabled = bEnabled;
				menuItemToolbarEllipseTool.Enabled = bEnabled;
				menuItemToolbarTextTool.Enabled = bEnabled;
				menuItemToolbarRectangleTool.Enabled = bEnabled;
				menuItemToolbarStampTool.Enabled = bEnabled;
				menuItemToolbarPolylineTool.Enabled = bEnabled;
				menuItemToolbarLineTool.Enabled = bEnabled;
				menuItemToolbarImageTool.Enabled = bEnabled;
				menuItemToolbarButtonTool.Enabled = bEnabled;
				menuItemToolbarPolygonTool.Enabled = bEnabled;

				menuItemRulerToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.RulerTool);
				menuItemRulerToolEnabeled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.RulerTool);
				menuItemFreehandToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.FreehandTool);
				menuItemFreehandToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.FreehandTool);
				menuItemEllipseToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.EllipseTool);
				menuItemEllipseToolEnabeld.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.EllipseTool);
				menuItemTextToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.TextTool);
				menuItemTextToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.TextTool);
				menuItemRectangleToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.RectangleTool);
				menuItemRectangleToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.RectangleTool);
				menuItemStampToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.StampTool);
				menuItemStampToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.StampTool);
				menuItemPolylineToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.PolyLineTool);
				menuItemPolylineToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.PolyLineTool);
				menuItemLineToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.LineTool);
				menuItemLineToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.LineTool);
				menuItemImageToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.ImageTool);
				menuItemImageToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.ImageTool);
				menuItemButtonToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.ButtonTool);
				menuItemButtonToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.ButtonTool);
				menuItemPolygonToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.PolygonTool);
				menuItemPolygonToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.PolygonTool);
			}
		}

		private void menuItemRulerToolVisible_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolVisible(AnnotationTool.RulerTool, !layer.Toolbar.GetToolVisible(AnnotationTool.RulerTool));
			}
		}

		private void menuItemRulerToolEnabeled_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolEnabled(AnnotationTool.RulerTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.RulerTool));
			}
		}

		private void menuItemFreehandToolEnabled_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolEnabled(AnnotationTool.FreehandTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.FreehandTool));
			}
		}

		private void menuItemFreehandToolVisible_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolVisible(AnnotationTool.FreehandTool, !layer.Toolbar.GetToolVisible(AnnotationTool.FreehandTool));
			}
		}

		private void menuItemEllipseToolEnabeld_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolEnabled(AnnotationTool.EllipseTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.EllipseTool));
			}
		}

		private void menuItemEllipseToolVisible_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolVisible(AnnotationTool.EllipseTool, !layer.Toolbar.GetToolVisible(AnnotationTool.EllipseTool));
			}
		}

		private void menuItemTextToolEnabled_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolEnabled(AnnotationTool.TextTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.TextTool));
			}
		}

		private void menuItemTextToolVisible_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolVisible(AnnotationTool.TextTool, !layer.Toolbar.GetToolVisible(AnnotationTool.TextTool));
			}
		}

		private void menuItemRectangleToolEnabled_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolEnabled(AnnotationTool.RectangleTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.RectangleTool));
			}
		}

		private void menuItemRectangleToolVisible_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolVisible(AnnotationTool.RectangleTool, !layer.Toolbar.GetToolVisible(AnnotationTool.RectangleTool));
			}
		}

		private void menuItemStampToolEnabled_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolEnabled(AnnotationTool.StampTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.StampTool));
			}
		}

		private void menuItemStampToolVisible_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolVisible(AnnotationTool.StampTool, !layer.Toolbar.GetToolVisible(AnnotationTool.StampTool));
			}
		}

		private void menuItemPolylineToolVisible_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolVisible(AnnotationTool.PolyLineTool, !layer.Toolbar.GetToolVisible(AnnotationTool.PolyLineTool));
			}
		}

		private void menuItemPolylineToolEnabled_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolEnabled(AnnotationTool.PolyLineTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.PolyLineTool));
			}
		}

		private void menuItemLineToolEnabled_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolEnabled(AnnotationTool.LineTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.LineTool));
			}
		}

		private void menuItemLineToolVisible_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolVisible(AnnotationTool.LineTool, !layer.Toolbar.GetToolVisible(AnnotationTool.LineTool));
			}
		}

		private void menuItemImageToolEnabled_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolEnabled(AnnotationTool.ImageTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.ImageTool));
			}
		}

		private void menuItemImageToolVisible_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolVisible(AnnotationTool.ImageTool, !layer.Toolbar.GetToolVisible(AnnotationTool.ImageTool));
			}
		}

		private void menuItemButtonToolEnabled_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolEnabled(AnnotationTool.ButtonTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.ButtonTool));
			}
		}

		private void menuItemButtonToolVisible_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolVisible(AnnotationTool.ButtonTool, !layer.Toolbar.GetToolVisible(AnnotationTool.ButtonTool));
			}
		}

		private void menuItemPolygonToolVisible_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolVisible(AnnotationTool.PolygonTool, !layer.Toolbar.GetToolVisible(AnnotationTool.PolygonTool));
			}
		}

		private void menuItemPolygonToolEnabled_Click(object sender, System.EventArgs e)
		{
			Layer layer = notateXpress1.Layers.Selected();
			if(layer != null)
			{
				layer.Toolbar.SetToolEnabled(AnnotationTool.PolygonTool, !layer.Toolbar.GetToolEnabled(AnnotationTool.PolygonTool));
			}
		}
        
		private void notateXpress1_Click(object sender, PegasusImaging.WinForms.NotateXpress8.ClickEventArgs e)
		{
			// interactive mode

			Element element = e.Element;
			Layer layer = e.Layer;
			string strAnnotationType = " ";
			if(layer != null && element != null)
			{
				int i = element.GetType().ToString().LastIndexOf('.');
				if(i == -1)
					strAnnotationType += element.GetType().ToString();
				else
					strAnnotationType += element.GetType().ToString().Substring(i + 1);
			}
			OutputList.Items.Insert(0, strAnnotationType + " annotation clicked on, layer name = " + layer.Name + ", layer description = " + layer.Description);
		}

		private void notateXpress1_DoubleClick(object sender, PegasusImaging.WinForms.NotateXpress8.DoubleClickEventArgs e)
		{
			// interactive mode
			Layer layer = e.Layer;
			Element element = e.Element;
			string strAnnotationType = " ";
			if(layer != null && element != null)
			{
				int i = element.GetType().ToString().LastIndexOf('.');
				if(i == -1)
					strAnnotationType += element.GetType().ToString();
				else
					strAnnotationType += element.GetType().ToString().Substring(i + 1);
				OutputList.Items.Insert(0, "Mouse double clidk event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
			}
		}

		private void notateXpress1_MouseDown(object sender, PegasusImaging.WinForms.NotateXpress8.MouseDownEventArgs e)
		{
			// interactive mode
			Layer layer = e.Layer;
			Element element = e.Element;
			string strAnnotationType = " ";
			if(layer != null && element != null)
			{
				int i = element.GetType().ToString().LastIndexOf('.');
				if(i == -1)
					strAnnotationType += element.GetType().ToString();
				else
					strAnnotationType += element.GetType().ToString().Substring(i + 1);
				OutputList.Items.Insert(0, "       X coorinate = " + e.X.ToString() + ", Y coordinate = " + e.Y.ToString());
				OutputList.Items.Insert(0, "Mouse down event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
			}
		}

		private void notateXpress1_MouseMove(object sender, PegasusImaging.WinForms.NotateXpress8.MouseMoveEventArgs e)
		{
			// interactive mode
			Layer layer = e.Layer;
			Element element = e.Element;
			string strAnnotationType = " ";
			if(layer != null && element != null)
			{
				int i = element.GetType().ToString().LastIndexOf('.');
				if(i == -1)
					strAnnotationType += element.GetType().ToString();
				else
					strAnnotationType += element.GetType().ToString().Substring(i + 1);
				OutputList.Items.Insert(0, "       X coorinate = " + e.X.ToString() + ", Y coordinate = " + e.Y.ToString());
				OutputList.Items.Insert(0, "Mouse move event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
			}
		}

		private void notateXpress1_MouseUp(object sender, PegasusImaging.WinForms.NotateXpress8.MouseUpEventArgs e)
		{
			// interactive mode
			Layer layer = e.Layer;
			Element element = e.Element;
			string strAnnotationType = " ";
			if(layer != null && element != null)
			{
				int i = element.GetType().ToString().LastIndexOf('.');
				if(i == -1)
					strAnnotationType += element.GetType().ToString();
				else
					strAnnotationType += element.GetType().ToString().Substring(i + 1);
				OutputList.Items.Insert(0, "       X coorinate = " + e.X.ToString() + ", Y coordinate = " + e.Y.ToString());
				OutputList.Items.Insert(0, "Mouse up event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
			}
		}

		private void notateXpress1_ItemChanged(object sender, PegasusImaging.WinForms.NotateXpress8.ItemChangedEventArgs e)
		{
			Layer layer = e.Layer;
			Element element = e.Element;
			string strAnnotationType = " ";
			if(layer != null && element != null)
			{
				int i = element.GetType().ToString().LastIndexOf('.');
				if(i == -1)
					strAnnotationType += element.GetType().ToString();
				else
					strAnnotationType += element.GetType().ToString().Substring(i + 1);
				OutputList.Items.Insert(0, "Item changed event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
			}
		}

		private void notateXpress1_CurrentLayerChange(object sender, PegasusImaging.WinForms.NotateXpress8.CurrentLayerChangeEventArgs e)
		{
			Layer layer = e.Layer;
			
			if(layer != null)
			{		
				OutputList.Items.Insert( 0, "Layer change event, current layer name = " + e.Layer.Name + ", description = " + e.Layer.Description);
			}
		}

		private void notateXpress1_LayerRestored(object sender, PegasusImaging.WinForms.NotateXpress8.LayerRestoredEventArgs e)
		{
			Layer layer = e.Layer;
			if(layer != null)
			{
				OutputList.Items.Insert(0, "Layer restored event on layer name = " + e.Layer.Name + ", description = " + layer.Description);

			}
		}

		private void notateXpress1_MenuSelect(object sender, PegasusImaging.WinForms.NotateXpress8.MenuEventArgs e)
		{
			MenuType menuType = e.Menu;
			string strMenuType = "Unknown";
			if(menuType == MenuType.Context)
				strMenuType = "Context";
			else if(menuType == MenuType.Toolbar)
				strMenuType = "Toolbar";
			AnnotationTool annotationTool = e.Tool;
			Layer layer = e.Layer;
			Element element = e.Element;
			string strAnnotationType = " ";
			if(layer != null && element != null)
			{
				int i = element.GetType().ToString().LastIndexOf('.');
				if(i == -1)
					strAnnotationType += element.GetType().ToString();
				else
					strAnnotationType += element.GetType().ToString().Substring(i + 1);
				OutputList.Items.Insert(0, "         Top Menu ID = " + e.TopMenuId.ToString() + ", Sub Menu ID = " + e.SubMenuId.ToString() + ", User 1 = " + e.User1.ToString() + ", User 2 = " + e.User2.ToString());
				OutputList.Items.Insert(0, strMenuType + " menu select event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
			}
		}

		private void notateXpress1_RequestLayerPassword(object sender, PegasusImaging.WinForms.NotateXpress8.GetLayerPasswordEventArgs e)
		{
			OutputList.Items.Insert(0, "Request Layer Password event for layer name = " + e.LayerName + ", layer password = " + e.LayerPassword);
			InputDlg dlg = new InputDlg("Provide Layer '" + e.LayerName + "' Password");
			if(System.Windows.Forms.DialogResult.OK == dlg.ShowDialog(this))
			{
				e.LayerPassword = dlg.InputText;
			}
		}
        
		private void notateXpress1_ToolbarSelect(object sender, PegasusImaging.WinForms.NotateXpress8.ToolbarEventArgs e)
		{
			Layer layer = e.Layer;
			if(layer != null)
			{
				OutputList.Items.Insert(0, "Toolbar " + GetAnnotationToolString(e.Tool) + " selected, layer name = " + layer.Name + ", description = " + layer.Description);
			}
		}

		private string GetAnnotationToolString (AnnotationTool annotationTool)
		{
			switch(annotationTool)
			{
				case AnnotationTool.NoTool: // = 0x0,
					return "NoTool";
				case AnnotationTool.PointerTool: // = 0x1000,
					return "PointerTool";
				case AnnotationTool.TextTool: // = 0x1001,
					return "TextTool";
				case AnnotationTool.RectangleTool: // = 0x1002,
					return "RectangleTool";
				case AnnotationTool.EllipseTool: // = 0x1003,
					return "EllipseTool";
				case AnnotationTool.PolygonTool: // = 0x1004,
					return "PolygonTool";
				case AnnotationTool.PolyLineTool: // = 0x1005,
					return "PolyLineTool";
				case AnnotationTool.LineTool: // = 0x1006,
					return "LineTool";
				case AnnotationTool.FreehandTool: // = 0x1007,
					return "FreehandTool";
				case AnnotationTool.StampTool: // = 0x1008,
					return "StampTool";
				case AnnotationTool.ImageTool: // = 0x1009,
					return "ImageTool";
				case AnnotationTool.ButtonTool: // = 0x100a,
					return "ButtonTool";
				case AnnotationTool.RulerTool: // = 0x100b
					return "RulerTool";
				default:
					return "Unknown";
			}
		}

		private void notateXpress1_UserDraw(object sender, PegasusImaging.WinForms.NotateXpress8.UserDrawEventArgs e)
		{
			Layer layer = e.Layer;
			Element element = e.Element;
			string strAnnotationType = " ";
			if(layer != null && element != null)
			{
				int i = element.GetType().ToString().LastIndexOf('.');
				if(i == -1)
					strAnnotationType += element.GetType().ToString();
				else
					strAnnotationType += element.GetType().ToString().Substring(i + 1);
				OutputList.Items.Insert(0, "         Device context = " + e.HandleDeviceContext.ToString() + ", X cooordinate = " + e.X.ToString() + ", Y coordinate = " + e.Y.ToString() + ", Width = " + e.Width.ToString() + ", Height = " + e.Height.ToString());
				OutputList.Items.Insert(0, "User draw event on " + strAnnotationType + ", current layer name = " + layer.Name + ", description = " + layer.Description);
			}
		}

		private void notateXpress1_UserGroupCreated(object sender, PegasusImaging.WinForms.NotateXpress8.UserGroupCreatedEventArgs e)
		{            
			Layer layer = e.Layer;
			Group group = e.Group;
			OutputList.Items.Insert(0, "User group '" + group.Name + "' created event, layer name = " + layer.Name + ", description = " + layer.Description);
		}

		private void notateXpress1_UserGroupDestroyed(object sender, PegasusImaging.WinForms.NotateXpress8.UserGroupDestroyedEventArgs e)
		{
			Layer layer = e.Layer;
			Group group = e.Group;
			OutputList.Items.Insert(0, "User group '" + group.Name + "' deleted event, layer name = " + layer.Name + ", description = " + layer.Description);
		}

		private void menuItemAboutNX_Click(object sender, System.EventArgs e)
		{
			notateXpress1.AboutBox();
		}

		private void menuItemAboutIX_Click(object sender, System.EventArgs e)
		{
			imagXpress1.AboutBox();
		}

		// New Code

		private void notateXpress1_AnnotationAdded(object sender, PegasusImaging.WinForms.NotateXpress8.AnnotationAddedEventArgs e)

		{
			Element element = e.Element;
			string strAnnotationType = " ";
			Layer layer = notateXpress1.Layers.Selected();
			if (layer != null)
			{
				int i = element.GetType().ToString().LastIndexOf('.');
				if (i == -1)
					strAnnotationType += element.GetType().ToString();
				else
					strAnnotationType += element.GetType().ToString().Substring(i + 1);

				   

			}

			if (!(sender is PegasusImaging.WinForms.NotateXpress8.NotateXpress))
				MessageBox.Show("sender is " + sender.GetType().ToString());

			// entry has been Added
			OutputList.Items.Insert(0, "Add event for" + strAnnotationType + " annotation");
		}


		private void notateXpress1_AnnotationDeleted(object sender, PegasusImaging.WinForms.NotateXpress8.AnnotationDeletedEventArgs e)

		{
			try
			{
				Element element = e.Element;
				string strAnnotationType = " ";
				Layer layer = notateXpress1.Layers.Selected();
				if (layer != null)
				{
					int i = element.GetType().ToString().LastIndexOf('.');
					if (i == -1)
						strAnnotationType += element.GetType().ToString();
					else
						strAnnotationType += element.GetType().ToString().Substring(i + 1);
				}

				if (!(sender is PegasusImaging.WinForms.NotateXpress8.NotateXpress))
					MessageBox.Show("sender is " + sender.GetType().ToString());

				// entry has been Deleted
				OutputList.Items.Insert(0, "Delete event for" + strAnnotationType + " annotation");
			}
			catch (PegasusImaging.WinForms.NotateXpress8.NotateXpressException ex)
			{
				PegasusError(ex, lblerror);
			}
			catch (System.Exception ex)
			{
				PegasusError(ex, lblerror);
			}
		}


		private void notateXpress1_AnnotationMoved(object sender, PegasusImaging.WinForms.NotateXpress8.AnnotationMovedEventArgs e)

		{
			Element element = e.Element;
			string strAnnotationType = " ";
			Layer layer = notateXpress1.Layers.Selected();
			if (layer != null)
			{
				int i = element.GetType().ToString().LastIndexOf('.');
				if (i == -1)
					strAnnotationType += element.GetType().ToString();
				else
					strAnnotationType += element.GetType().ToString().Substring(i + 1);
			}

			if (!(sender is PegasusImaging.WinForms.NotateXpress8.NotateXpress))
				MessageBox.Show("sender is " + sender.GetType().ToString());

			// entry has been moved
			OutputList.Items.Insert(0, "Move event for" + strAnnotationType + " annotation");
			if (_bAutoSelect && element != null)
				// select newly moved layer element
				element.Selected = true;

		}

		private void notateXpress1_AnnotationSelected(object sender, PegasusImaging.WinForms.NotateXpress8.AnnotationSelectedEventArgs e)
		{
			Element element = e.Element;
			string strAnnotationType = " ";
			Layer layer = notateXpress1.Layers.Selected();
			if (layer != null)
			{
				int i = element.GetType().ToString().LastIndexOf('.');
				if (i == -1)
					strAnnotationType += element.GetType().ToString();
				else
					strAnnotationType += element.GetType().ToString().Substring(i + 1);
			}

			if (!(sender is PegasusImaging.WinForms.NotateXpress8.NotateXpress))
				MessageBox.Show("sender is " + sender.GetType().ToString());

			// entry has been selected
			OutputList.Items.Insert(0, "Selected event for" + strAnnotationType + " annotation");
		}

		private void notateXpress1_AnnotationEndSelected(object sender, System.EventArgs e)
		{
			/* 		
			 Element element = e.Element;
			 string strAnnotationType = " ";
			 Layer layer = notateXpress1.Layers.Selected();
			 if (layer != null)
			 {
				 int i = element.GetType().ToString().LastIndexOf('.');
				 if (i == -1)
					 strAnnotationType += element.GetType().ToString();
				 else
					 strAnnotationType += element.GetType().ToString().Substring(i + 1);
			 }

			 if (!(sender is PegasusImaging.WinForms.NotateXpress8.NotateXpress))
				 MessageBox.Show("sender is " + sender.GetType().ToString());

			 // entry has been EndSelected
			 OutputList.Items.Insert(0, "Select event for" + strAnnotationType + " annotation");
			 if (_bAutoSelect && element != null)
				 // select newly selected layer element
				 element.Selected = true;
		
		 */
		}

		private void mnu_file_saveann2fileANN_Click(object sender, System.EventArgs e)
		{
			saveFileDialog.Title = "Save Annotation File";
			saveFileDialog.Filter = "TMS Annotation File (*.ann) | *.ann";
			saveFileDialog.DefaultExt = ".ann";
			saveFileDialog.InitialDirectory = strCurrentDir;
			saveFileDialog.ShowDialog (this);

			string strFileName = saveFileDialog.FileName;
			if (strFileName != "" && strFileName != null)
			{

				//clear out the error in case there was an error from a previous operation
				lblerror.Text = "";

				notateXpress1.Layers.Comments = "This file was saved today";
				notateXpress1.Layers.Subject = "testing 123";
				notateXpress1.Layers.Title = "One awesome annotation file";
				notateXpress1.Layers.SaveToFile(strFileName, m_saveOptions);
			}
		}

		

			
		private void mnu_file_saveann2fileNXP_Click(object sender, System.EventArgs e)
		{
			saveFileDialog.Title = "Save Annotation File";
			saveFileDialog.Filter = "NotateXpress File (*.nxp) | *.nxp";
			saveFileDialog.DefaultExt = ".nxp";
			saveFileDialog.InitialDirectory = strCurrentDir;
			saveFileDialog.ShowDialog (this);

			string strFileName = saveFileDialog.FileName;
			if (strFileName != "" && strFileName != null)
			{
				
				notateXpress1.Layers.Comments = "This file was saved today";
				notateXpress1.Layers.Subject = "testing 123";
				notateXpress1.Layers.Title = "One awesome annotation file";
				notateXpress1.Layers.SaveToFile(strFileName, m_saveOptions);
			}
		}
	
		private void mnu_file_loadannotationNXP_Click(object sender, System.EventArgs e)
		{
			PegasusImaging.WinForms.NotateXpress8.Layer layer;

			openFileDialog.Title = "Open NotateXpress File";
			openFileDialog.Filter = "NotateXpress File (*.nxp) | *.nxp";
			openFileDialog.DefaultExt = ".nxp";
			openFileDialog.Multiselect = false;
			openFileDialog.InitialDirectory = strCurrentDir;
			openFileDialog.ShowDialog (this);

			string strFileName = openFileDialog.FileName;
			if (System.IO.File.Exists (strFileName))
			{
				
				notateXpress1.Layers.FromFile(strFileName, m_loadOptions);
				layer = notateXpress1.Layers.Selected();
				if (layer != null)
				{
					layer.Toolbar.Visible = true;
				}

			}
		}

		private void mnu_file_loadannotationANN_Click(object sender, System.EventArgs e)
		{
			PegasusImaging.WinForms.NotateXpress8.Layer layer;

			openFileDialog.Title = "Open NotateXpress File";
			openFileDialog.Filter = "TMS Annotation File (*.ann) | *.ann";
			openFileDialog.DefaultExt = ".ann";
			openFileDialog.Multiselect = false;
			openFileDialog.InitialDirectory = strCurrentDir;
			openFileDialog.ShowDialog (this);

			string strFileName = openFileDialog.FileName;
			if (System.IO.File.Exists (strFileName))
			{
				try
				{
					notateXpress1.Layers.FromFile(strFileName, m_loadOptions);
					layer = notateXpress1.Layers.Selected();
					if (layer != null)
					{
						layer.Toolbar.Visible = true;
					}
				}
				catch (PegasusImaging.WinForms.NotateXpress8.NotateXpressException ex)
				{
					PegasusError(ex, lblerror);
				}
			}
		}

		private void mnu_layers_setcurrent_Click_1(object sender, System.EventArgs e)
		{
		
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			
			PegasusImaging.WinForms.NotateXpress8.Layer layer;

			
			iPageNum = Convert.ToInt32(txtPageNum.Text);
			string strFilePath = Application.StartupPath + "\\file1.tif";

			string strNXPFileName = strFilePath + txtPageNum.Text + ".NXP";


			
			if (strFilePath != "" && strFilePath != null)
			{
				
				//clear out the error in case there was an error from a previous operation
				lblerror.Text = "";

				// ---------------
				// clear existing annotations and layers
				// ---------------
				notateXpress1.Layers.Clear ();

				PegasusImaging.WinForms.NotateXpress8.Layer theNewLayer = createNewLayer("Layer1", System.IntPtr.Zero);
				
				if (imagXView1.Image != null)
				{
					imagXView1.Image.Dispose();
					imagXView1.Image = null;
				}
				try
				{
					imagXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strFilePath,iPageNum);
					this.Text = "Image = " + imagXView1.Image.ImageXData.Width.ToString () + " x " + imagXView1.Image.ImageXData.Height.ToString ();
					theNewLayer.Toolbar.Visible = menuItemToolBar.Enabled;
					theNewLayer.Toolbar.Enabled = menuItemToolBar.Enabled;


					if (System.IO.File.Exists (strNXPFileName))
					{
				
						notateXpress1.Layers.FromFile(strNXPFileName, m_loadOptions);
						layer = notateXpress1.Layers.Selected();
						if (layer != null)
						{
							layer.Toolbar.Visible = true;
						}

					}
			
				}
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
				{
					PegasusError(ex, lblerror);
				}
			}


		}

		private void button2_Click(object sender, System.EventArgs e)
		{

			iPageNum = Convert.ToInt32(txtPageNum.Text);
			string strFilePath = Application.StartupPath + "\\file1.tif";

			strNXPFileName = strFilePath + txtPageNum.Text + ".NXP";


			if (strNXPFileName != "" && strNXPFileName != null)
			{
				
				notateXpress1.Layers.Comments = "This file was saved today";
				notateXpress1.Layers.Subject = "testing 123";
				notateXpress1.Layers.Title = "One awesome annotation file";
				notateXpress1.Layers.SaveToFile(strNXPFileName, m_saveOptions);
			}
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			//PegasusImaging.WinForms.ImagXpress8.ImageX imgTmp;
			string sTempFile;

			
			sTempFile = Application.StartupPath + "\\file1.tif." + txtPageNum.Text + ".tif";

			m_loadOptions.PreserveWangLayers = m_saveOptions.PreserveWangLayers = true;
			mnu_file_wangcompatible.Checked = true;

			try 
			{
				//imgTmp= PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFile1);
				PegasusImaging.WinForms.ImagXpress8.SaveOptions soOpts = new PegasusImaging.WinForms.ImagXpress8.SaveOptions();
				try 
				{
					soOpts.Format = PegasusImaging.WinForms.ImagXpress8.ImageXFormat.Tiff;
					soOpts.Tiff.Compression = PegasusImaging.WinForms.ImagXpress8.Compression.Group4;
					//soOpts.Tiff.MultiPage = true;

					
				}
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
				{
					MessageBox.Show(eX.Message);
				}
				imagXView1.Image.Save(sTempFile,soOpts);

				if (System.IO.File.Exists (strNXPFileName))
				{
					System.IO.File.Delete(strNXPFileName);
				}			
				
			} 
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				MessageBox.Show(ex.Message);;
				return;
			}		
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			PegasusImaging.WinForms.ImagXpress8.ImageX imgTmp;

            
			string sTempFile;
			string sNewFile = Application.StartupPath + "\\file1NEW.tif.";

			for(int a = 1;a < 3; a++)
			{
		
				sTempFile = Application.StartupPath + "\\file1.tif." + a.ToString() + ".tif";


				PegasusImaging.WinForms.ImagXpress8.SaveOptions soOpts = new PegasusImaging.WinForms.ImagXpress8.SaveOptions();
				
				soOpts.Format = PegasusImaging.WinForms.ImagXpress8.ImageXFormat.Tiff;
				soOpts.Tiff.Compression = PegasusImaging.WinForms.ImagXpress8.Compression.Group4;
				soOpts.Tiff.MultiPage = true;

					
				
			
				if(System.IO.File.Exists (sTempFile))
				{
					imgTmp = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(sTempFile);
					imgTmp.Save(sNewFile,soOpts);

				}
				else
				{
					//new page was not created, so we will add the original page
					imgTmp = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(Application.StartupPath + "\\file1.tif.",a);
					imgTmp.Save(sNewFile,soOpts);
						

				{
				
				}
				}

			}
		}
	}
}
	
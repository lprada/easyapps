/*****************************************************************'
* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/

using PegasusImaging.WinForms.ImagXpress8;
using PegasusImaging.WinForms.NotateXpress8;
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

namespace SimpleAnnotations
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		string strImageFileName;
		string strNXPFileName;
		private System.String strCurrentDir;
		private System.String strCurrentImage;
		private System.String strSaveFileName;
		PegasusImaging.WinForms.ImagXpress8.ImageX imagX1;
		PegasusImaging.WinForms.NotateXpress8.Layer layer;
		PegasusImaging.WinForms.ImagXpress8.LoadOptions loIXLoadOptions;
		PegasusImaging.WinForms.ImagXpress8.SaveOptions soIXSaveOptions;
		PegasusImaging.WinForms.NotateXpress8.SaveOptions soNXSaveOptions;
		PegasusImaging.WinForms.NotateXpress8.LoadOptions soNXLoadOptions;



		private PegasusImaging.WinForms.ImagXpress8.ImageXView imageXView1;
		private PegasusImaging.WinForms.ImagXpress8.ImagXpress imagXpress1;
		private PegasusImaging.WinForms.NotateXpress8.NotateXpress notateXpress1;
		private System.Windows.Forms.MainMenu mainMenu1;
		internal System.Windows.Forms.ListBox lstStatus;
		internal System.Windows.Forms.Label lblLoadStatus;
		internal System.Windows.Forms.Label lblLastError;
		internal System.Windows.Forms.ListBox lstInfo;
		internal System.Windows.Forms.Label lblerror;
		private System.Windows.Forms.MenuItem mnuFile;
		private System.Windows.Forms.MenuItem mnuOpen;
		private System.Windows.Forms.MenuItem mnuSave;
		private System.Windows.Forms.MenuItem mnuWang;
		private System.Windows.Forms.MenuItem mnuSaveA;
		private System.Windows.Forms.MenuItem mnuLoadAnno;
		private System.Windows.Forms.MenuItem mnuExit;
		private System.Windows.Forms.MenuItem mnuCreate;
		private System.Windows.Forms.MenuItem mnuTxt;
		private System.Windows.Forms.MenuItem mnuRedac;
		private System.Windows.Forms.MenuItem mnuRuler;
		private System.Windows.Forms.MenuItem mnuRects;
		private System.Windows.Forms.MenuItem mnuNotate;
		private System.Windows.Forms.MenuItem mnuAbout;
		private System.Windows.Forms.MenuItem mnuToolbar;
		private System.Windows.Forms.MenuItem mnuNotate1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//**Must call the Unlock function here
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12,12,12,12);
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
			
				//Don't forget to dispose IX and NX
                if (imagXpress1 != null)
                {
                    imagXpress1.Dispose();
                    imagXpress1 = null;
				}

				if (imageXView1.Image != null)
				{
					imageXView1.Image.Dispose();
					imageXView1.Image = null;
				}

				if (imagX1 != null)
				{
					imagX1.Dispose();
					imagX1 = null;
				}

				if (notateXpress1 != null)
				{
					notateXpress1.Dispose();
					notateXpress1 = null;	
				}
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.imageXView1 = new PegasusImaging.WinForms.ImagXpress8.ImageXView();
			this.imagXpress1 = new PegasusImaging.WinForms.ImagXpress8.ImagXpress();
			this.notateXpress1 = new PegasusImaging.WinForms.NotateXpress8.NotateXpress();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.mnuFile = new System.Windows.Forms.MenuItem();
			this.mnuOpen = new System.Windows.Forms.MenuItem();
			this.mnuSave = new System.Windows.Forms.MenuItem();
			this.mnuWang = new System.Windows.Forms.MenuItem();
			this.mnuSaveA = new System.Windows.Forms.MenuItem();
			this.mnuLoadAnno = new System.Windows.Forms.MenuItem();
			this.mnuExit = new System.Windows.Forms.MenuItem();
			this.mnuCreate = new System.Windows.Forms.MenuItem();
			this.mnuTxt = new System.Windows.Forms.MenuItem();
			this.mnuRedac = new System.Windows.Forms.MenuItem();
			this.mnuRuler = new System.Windows.Forms.MenuItem();
			this.mnuRects = new System.Windows.Forms.MenuItem();
			this.mnuToolbar = new System.Windows.Forms.MenuItem();
			this.mnuNotate1 = new System.Windows.Forms.MenuItem();
			this.mnuAbout = new System.Windows.Forms.MenuItem();
			this.mnuNotate = new System.Windows.Forms.MenuItem();
			this.lstStatus = new System.Windows.Forms.ListBox();
			this.lblLoadStatus = new System.Windows.Forms.Label();
			this.lblLastError = new System.Windows.Forms.Label();
			this.lstInfo = new System.Windows.Forms.ListBox();
			this.lblerror = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// imageXView1
			// 
			this.imageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.imageXView1.Location = new System.Drawing.Point(16, 88);
			this.imageXView1.Name = "imageXView1";
			this.imageXView1.Size = new System.Drawing.Size(560, 408);
			this.imageXView1.TabIndex = 0;
			// 
			// notateXpress1
			// 
			this.notateXpress1.AllowPaint = true;
			this.notateXpress1.Debug = false;
			this.notateXpress1.DebugLogFile = "c:\\NotateXpress8.log";
			this.notateXpress1.ErrorLevel = PegasusImaging.WinForms.NotateXpress8.ErrorLevelInfo.Production;
			this.notateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress8.FontScaling.Normal;
			this.notateXpress1.ImagXpressLoad = true;
			this.notateXpress1.ImagXpressSave = true;
			this.notateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress8.AnnotationMode.Edit;
			this.notateXpress1.MultiLineEdit = false;
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnuFile,
																					  this.mnuCreate,
																					  this.mnuToolbar,
																					  this.mnuAbout});
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					this.mnuOpen,
																					this.mnuSave,
																					this.mnuWang,
																					this.mnuSaveA,
																					this.mnuLoadAnno,
																					this.mnuExit});
			this.mnuFile.Text = "&File";
			// 
			// mnuOpen
			// 
			this.mnuOpen.Index = 0;
			this.mnuOpen.Text = "&Open Image";
			this.mnuOpen.Click += new System.EventHandler(this.mnuOpen_Click);
			// 
			// mnuSave
			// 
			this.mnuSave.Index = 1;
			this.mnuSave.Text = "&Save Image";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// mnuWang
			// 
			this.mnuWang.Checked = true;
			this.mnuWang.Index = 2;
			this.mnuWang.Text = "&Wang Compatible";
			this.mnuWang.Click += new System.EventHandler(this.mnuWang_Click);
			// 
			// mnuSaveA
			// 
			this.mnuSaveA.Index = 3;
			this.mnuSaveA.Text = "&Save Annotation";
			this.mnuSaveA.Click += new System.EventHandler(this.mnuSaveA_Click);
			// 
			// mnuLoadAnno
			// 
			this.mnuLoadAnno.Index = 4;
			this.mnuLoadAnno.Text = "&Load Annotation";
			this.mnuLoadAnno.Click += new System.EventHandler(this.mnuLoadAnno_Click);
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 5;
			this.mnuExit.Text = "&Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// mnuCreate
			// 
			this.mnuCreate.Index = 1;
			this.mnuCreate.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.mnuTxt,
																					  this.mnuRedac,
																					  this.mnuRuler,
																					  this.mnuRects});
			this.mnuCreate.Text = "&Create Annotations";
			// 
			// mnuTxt
			// 
			this.mnuTxt.Index = 0;
			this.mnuTxt.Text = "Add Text";
			this.mnuTxt.Click += new System.EventHandler(this.mnuTxt_Click);
			// 
			// mnuRedac
			// 
			this.mnuRedac.Index = 1;
			this.mnuRedac.Text = "Add Redaction";
			this.mnuRedac.Click += new System.EventHandler(this.mnuRedac_Click);
			// 
			// mnuRuler
			// 
			this.mnuRuler.Index = 2;
			this.mnuRuler.Text = "Add Ruler";
			this.mnuRuler.Click += new System.EventHandler(this.mnuRuler_Click);
			// 
			// mnuRects
			// 
			this.mnuRects.Index = 3;
			this.mnuRects.Text = "Add Rectangles";
			this.mnuRects.Click += new System.EventHandler(this.mnuRects_Click);
			// 
			// mnuToolbar
			// 
			this.mnuToolbar.Index = 2;
			this.mnuToolbar.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					   this.mnuNotate1});
			this.mnuToolbar.Text = "Toolbar";
			
			// 
			// mnuNotate1
			// 
			this.mnuNotate1.Index = 0;
			this.mnuNotate1.Text = "Show";
			this.mnuNotate1.Click += new System.EventHandler(this.mnuNotate1_Click);
			// 
			// mnuAbout
			// 
			this.mnuAbout.Index = 3;
			this.mnuAbout.Text = "&About";
			this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
			// 
			// mnuNotate
			// 
			this.mnuNotate.Index = -1;
			this.mnuNotate.Text = "";
			// 
			// lstStatus
			// 
			this.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstStatus.Location = new System.Drawing.Point(592, 155);
			this.lstStatus.Name = "lstStatus";
			this.lstStatus.Size = new System.Drawing.Size(208, 121);
			this.lstStatus.TabIndex = 10;
			// 
			// lblLoadStatus
			// 
			this.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right);
			this.lblLoadStatus.Location = new System.Drawing.Point(592, 96);
			this.lblLoadStatus.Name = "lblLoadStatus";
			this.lblLoadStatus.Size = new System.Drawing.Size(160, 40);
			this.lblLoadStatus.TabIndex = 9;
			this.lblLoadStatus.Text = "Load Status:";
			// 
			// lblLastError
			// 
			this.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.lblLastError.Location = new System.Drawing.Point(592, 296);
			this.lblLastError.Name = "lblLastError";
			this.lblLastError.Size = new System.Drawing.Size(184, 32);
			this.lblLastError.TabIndex = 8;
			this.lblLastError.Text = "Last Error:";
			// 
			// lstInfo
			// 
			this.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right);
			this.lstInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lstInfo.Items.AddRange(new object[] {
														 "This sample demonstrates the following functionality:",
														 "1)Creating various annotations programatically."});
			this.lstInfo.Location = new System.Drawing.Point(16, 8);
			this.lstInfo.Name = "lstInfo";
			this.lstInfo.Size = new System.Drawing.Size(776, 69);
			this.lstInfo.TabIndex = 7;
			// 
			// lblerror
			// 
			this.lblerror.Anchor = (System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right);
			this.lblerror.Location = new System.Drawing.Point(592, 352);
			this.lblerror.Name = "lblerror";
			this.lblerror.Size = new System.Drawing.Size(200, 128);
			this.lblerror.TabIndex = 6;
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(808, 521);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.lstStatus,
																		  this.lblLoadStatus,
																		  this.lblLastError,
																		  this.lstInfo,
																		  this.lblerror,
																		  this.imageXView1});
			this.Menu = this.mainMenu1;
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "NotateXpress 8 Simple Annotations C#";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion


		#region Pegasus Imaging Sample Application Standard Functions

		System.Globalization.NumberFormatInfo cultNumber = System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
		System.Globalization.TextInfo cultText = System.Globalization.CultureInfo.CurrentCulture.TextInfo;
		System.Globalization.CompareInfo cultCompare = System.Globalization.CultureInfo.CurrentCulture.CompareInfo;
		private const System.String strCommonImagesDirectory = "..\\..\\..\\..\\..\\..\\..\\..\\Common\\Images\\";
		string strDefaultImageFilter = ("All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" +
			"" + (";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" + ("2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" + ("t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" + ("e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" + (")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" + ("cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" + ("s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" + "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"))))))));

    
		private string GetFileName(string FullName) 
		{
			return FullName.Substring((FullName.LastIndexOf("\\") + 1), (FullName.Length 
				- (FullName.LastIndexOf("\\") + 1)));
		}
    
		private static void PegasusError(System.Exception ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = (ErrorException.Message + ("" + ("\n" 
				+ ((ErrorException.Source + "") 
				+ "\n"))));
		}
    
		private static void PegasusError(PegasusImaging.WinForms.ImagXpress8.ImagXpressException ErrorException, System.Windows.Forms.Label ErrorLabel) 
		{
			ErrorLabel.Text = (ErrorException.Message + ("" + ("\n" 
				+ (ErrorException.Source + ("" + ("\n" + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))));
		}
    
		private string PegasusOpenFile(string sTitle, string sFileFilter) 
		{
			OpenFileDialog dlg = new OpenFileDialog();
			dlg.Title = sTitle;
			dlg.Filter = sFileFilter;
			dlg.InitialDirectory = strCurrentDir;
			if ((dlg.ShowDialog() == DialogResult.OK)) 
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
				return dlg.FileName;
			}
			else 
			{
				return "";
			}
		}
    
		private string PegasusSaveFile(string sTitle, string sFilter) 
		{
			SaveFileDialog dlg = new SaveFileDialog();
			dlg.Title = sTitle;
			dlg.Filter = sFilter;
			dlg.InitialDirectory = strCurrentDir;
			if ((dlg.ShowDialog() == DialogResult.OK)) 
			{
				strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\\")));
				return dlg.FileName;
			}
			else 
			{
				return "";
			}
		}

		#endregion


		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void LoadFile() 
		{
			try 
			{
				PegasusImaging.WinForms.ImagXpress8.ImageX oldImage = imageXView1.Image;
				imagX1 = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFileName,loIXLoadOptions);
				imageXView1.Image = imagX1;
				lblerror.Text = "";
				if (oldImage != null)
				{
					oldImage.Dispose();
					oldImage = null;
				}
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
		}


		
		private void Form1_Load(object sender, System.EventArgs e)
		{
			try 
			{
				//**The UnlockRuntime function must be called to distribute the runtime**
				//imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);


				// Create a new load options object so we can recieve events from the images we load
				loIXLoadOptions = new PegasusImaging.WinForms.ImagXpress8.LoadOptions();
				soIXSaveOptions = new PegasusImaging.WinForms.ImagXpress8.SaveOptions();
				soIXSaveOptions.Tiff.Compression = PegasusImaging.WinForms.ImagXpress8.Compression.Group4;
				soNXSaveOptions = new PegasusImaging.WinForms.NotateXpress8.SaveOptions();
				soNXLoadOptions = new PegasusImaging.WinForms.NotateXpress8.LoadOptions();
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
			try 
			{
				//this is where events are assigned. This happens before the file gets loaded.
				PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEventHandler( this.ProgressEventHandler );
				PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent += new PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEventHandler( this.ImageStatusEventHandler );
		
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
			//here we set the current directory and image so that the file open dialog box works well
			strCurrentDir = System.IO.Directory.GetCurrentDirectory ().ToString (cultNumber);
			strCurrentImage = System.IO.Path.Combine (strCurrentDir, @"..\..\..\..\..\..\..\..\Common\Images\benefits.tif");
			strCurrentDir = strCommonImagesDirectory;
			
			if (System.IO.File.Exists(strCurrentImage)) 
			{
				try 
				{
					imageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strCurrentImage,loIXLoadOptions);
					imageXView1.AutoScroll = true;
					notateXpress1.ClientWindow = imageXView1.Handle;
					layer = new PegasusImaging.WinForms.NotateXpress8.Layer();
					// ****This is a workaround issue ***
					layer.Active = true;
					notateXpress1.Layers.Add(layer);
					// disable the toolbar 
					notateXpress1.ToolbarDefaults.ToolbarActivated = false;
					notateXpress1.MenuSetEnabled(PegasusImaging.WinForms.NotateXpress8.MenuType.Context, PegasusImaging.WinForms.NotateXpress8.AnnotationTool.NoTool, true);
				}
				catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException eX) 
				{
					PegasusError(eX,lblerror);
				}
			}
		}

		private void mnuOpen_Click(object sender, System.EventArgs e)
		{
			strImageFileName = PegasusOpenFile("Select an Image File", strDefaultImageFilter);
			if (strImageFileName.Length != 0) 
			{
				//If it is valid, we set our internal image filename equal to it
					LoadFile();
			}
			
			notateXpress1.Layers.Clear();

			layer = new PegasusImaging.WinForms.NotateXpress8.Layer();
			layer.Active = true;
			notateXpress1.Layers.Add(layer);
			mnuNotate1_Click(sender, e);

		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			try 
			{
				strSaveFileName = PegasusSaveFile("Save TIFF File", "Tiff File (*.tif) | *.tif");
				imagX1.Save(strSaveFileName, soIXSaveOptions);
			}
			catch (PegasusImaging.WinForms.ImagXpress8.ImagXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
			catch (System.Exception ex) 
			{
				PegasusError(ex, lblerror);
			}


		}

		private void mnuWang_Click(object sender, System.EventArgs e)
		{
			if (soNXSaveOptions.PreserveWangLayers) 
			{
				soNXSaveOptions.PreserveWangLayers = false;
				mnuWang.Checked = false;
			}
			else 
			{
				soNXSaveOptions.PreserveWangLayers = true;
				mnuWang.Checked = true;
			}


		}

		private void mnuSaveA_Click(object sender, System.EventArgs e)
		{
			strSaveFileName = PegasusSaveFile("Save Annotation File", "NotateXpress File (*.nxp) | *.nxp");
			try 
			{
				notateXpress1.Layers.Comments = "This file was saved today";
				notateXpress1.Layers.Subject = "testing 123";
				notateXpress1.Layers.Title = "One awesome annotation file";
				notateXpress1.Layers.SaveToFile(strSaveFileName, soNXSaveOptions);
			}
			catch (PegasusImaging.WinForms.NotateXpress8.NotateXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
			catch (System.Exception ex) 
			{
				PegasusError(ex, lblerror);
			}

		}

		private void mnuLoadAnno_Click(object sender, System.EventArgs e)
		{
			strNXPFileName = PegasusOpenFile("Open NotateXpress File", "NotateXpress File (*.nxp) | *.nxp");
			try 
			{
				notateXpress1.Layers.FromFile(strNXPFileName, soNXLoadOptions);
				mnuNotate1_Click(sender, e);
			}
			catch (PegasusImaging.WinForms.NotateXpress8.NotateXpressException ex) 
			{
				PegasusError(ex, lblerror);
			}
			catch (System.Exception ex) 
			{
				PegasusError(ex, lblerror);
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			System.Windows.Forms.Application.Exit();
		}

		private void mnuTxt_Click(object sender, System.EventArgs e)
		{
			System.Drawing.Rectangle rect = new System.Drawing.Rectangle();
			rect.X = 30;
			rect.Y = 30;
			rect.Width = 280;
			rect.Height = 300;
			PegasusImaging.WinForms.NotateXpress8.TextTool text = new PegasusImaging.WinForms.NotateXpress8.TextTool();
			text.TextJustification = PegasusImaging.WinForms.NotateXpress8.TextJustification.Center;
			text.Text = "NotateXpress";
			text.BackColor = System.Drawing.Color.Red;
			text.BoundingRectangle = rect;
			text.PenWidth = 2;
			text.BackStyle = PegasusImaging.WinForms.NotateXpress8.BackStyle.Translucent;
			text.PenWidth = 2;
			// Add the annotation to the image
			layer.Elements.Add(text);
		}

		private void mnuRedac_Click(object sender, System.EventArgs e)
		{
			DateTime saveNow = DateTime.Now;

			
			System.Drawing.Rectangle rect = new System.Drawing.Rectangle(100, 100, 150, 150);
			PegasusImaging.WinForms.NotateXpress8.TextTool text = new PegasusImaging.WinForms.NotateXpress8.TextTool();
			text.Text = ("Approved by Pegasus Imaging Corp." 
				+ "  " + DateTime.Now.Date.ToString() );
			text.TextJustification = PegasusImaging.WinForms.NotateXpress8.TextJustification.Center;
			text.TextColor = System.Drawing.Color.Red;
			text.PenWidth = 1;
			text.PenColor = System.Drawing.Color.White;
			text.BackStyle = PegasusImaging.WinForms.NotateXpress8.BackStyle.Opaque;
			text.BackColor = System.Drawing.Color.White;
			text.BoundingRectangle = rect;
			layer.Elements.Add(text);

		}

		private void mnuRuler_Click(object sender, System.EventArgs e)
		{
			System.Drawing.Rectangle rect = new System.Drawing.Rectangle(25, 50, 200, 100);
			PegasusImaging.WinForms.NotateXpress8.RulerTool ruler = new PegasusImaging.WinForms.NotateXpress8.RulerTool();
			ruler.BoundingRectangle = rect;
			ruler.GaugeLength = 20;
			ruler.MeasurementUnit = PegasusImaging.WinForms.NotateXpress8.MeasurementUnit.Pixels;
			ruler.Precision = 1;
			ruler.BackColor = System.Drawing.Color.Blue;
			layer.Elements.Add(ruler);

		}

		private void mnuRects_Click(object sender, System.EventArgs e)
		{
			int counter;
			System.Drawing.Rectangle rect = new System.Drawing.Rectangle();
			rect.X = 30;
			rect.Y = 30;
			rect.Width = 60;
			rect.Height = 60;
			System.Random rand = new System.Random();
			for (counter = 1; (counter <= 10); counter++) 
			{
				PegasusImaging.WinForms.NotateXpress8.RectangleTool rectangle = new PegasusImaging.WinForms.NotateXpress8.RectangleTool();
				rectangle.BoundingRectangle = rect;
				rectangle.PenColor = System.Drawing.Color.FromArgb((int)((rand.NextDouble() * 255)), (int)((rand.NextDouble() * 255)), (int)((rand.NextDouble() * 255)));
				rectangle.FillColor = System.Drawing.Color.FromArgb((int)((rand.NextDouble() * 255)), (int)((rand.NextDouble() * 255)), (int)((rand.NextDouble() * 255)));
				layer.Elements.Add(rectangle);
				rect.X = (rect.X + 5);
				rect.Y = (rect.Y + 5);
			}

		}

		private void mnuNotate1_Click(object sender, System.EventArgs e)
		{
			if ((notateXpress1.ToolbarDefaults.ToolbarActivated == true)) 
			{
				mnuNotate1.Text = "Show";
				notateXpress1.ToolbarDefaults.ToolbarActivated = false;
			}
			else 
			{
				mnuNotate1.Text = "Hide";
				notateXpress1.ToolbarDefaults.ToolbarActivated = true;
			}

		}

		private void mnuAbout_Click(object sender, System.EventArgs e)
		{
			notateXpress1.AboutBox();
		}
		private void ImageStatusEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs e) 
		{
			lstStatus.Items.Add(e.Status.ToString(cultNumber));
			
		}
		private void ProgressEventHandler(object sender, PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs e) 
		{
				lstStatus.Items.Add(e.PercentDone.ToString(cultNumber) + "% Loading Complete.");
			if (e.IsComplete) 
			{
					lstStatus.Items.Add(e.TotalBytes + " Bytes Completed Loading.");
			}
				lstStatus.SelectedIndex = lstStatus.Items.Count - 1;
		}

		
			
	}
}

/*****************************************************************'
* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/


using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace NXDemoCSharp
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class InputDlg : System.Windows.Forms.Form
	{
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Button IDOK;
		private System.Windows.Forms.Button InputCancel;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public InputDlg( string strTitle)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			this.Text = strTitle;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.IDOK = new System.Windows.Forms.Button();
			this.InputCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(16, 8);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(272, 20);
			this.textBox1.TabIndex = 0;
			this.textBox1.Text = "";
			this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_OnKeyDown);
			// 
			// IDOK
			// 
			this.IDOK.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.IDOK.Location = new System.Drawing.Point(16, 40);
			this.IDOK.Name = "IDOK";
			this.IDOK.Size = new System.Drawing.Size(120, 24);
			this.IDOK.TabIndex = 1;
			this.IDOK.Text = "OK";
			this.IDOK.Click += new System.EventHandler(this.IDOK_Click);
			// 
			// InputCancel
			// 
			this.InputCancel.Location = new System.Drawing.Point(152, 40);
			this.InputCancel.Name = "InputCancel";
			this.InputCancel.Size = new System.Drawing.Size(136, 24);
			this.InputCancel.TabIndex = 2;
			this.InputCancel.Text = "Cancel";
			this.InputCancel.Click += new System.EventHandler(this.InputCancel_Click);
			// 
			// InputDlg
			// 
			//this.AutoScale = false;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(304, 78);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.InputCancel,
																		  this.IDOK,
																		  this.textBox1});
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "InputDlg";
			this.ShowInTaskbar = false;
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.InputDlg_OnKeyDown);
			this.Load += new System.EventHandler(this.InputDlg_Load);
			this.Activated += new System.EventHandler(this.InputDlg_Activated);
			this.ResumeLayout(false);

		}
		#endregion

		private void InputDlg_Activated(object sender, System.EventArgs e)
		{
			this.CenterToParent();
		}

		
		private void IDOK_Click(object sender, System.EventArgs e)
		{
			DialogResult = DialogResult.OK;
		}

		public string InputText
		{
			get
			{
				return textBox1.Text;
			}

			set
			{
				textBox1.Text = value;
			}
		}

		private void InputCancel_Click(object sender, System.EventArgs e)
		{
			DialogResult = DialogResult.Cancel;		
		}

		private void InputDlg_OnKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Return)
				DialogResult = DialogResult.OK;
		}

		private void textBox_OnKeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Return)
				DialogResult = DialogResult.OK;
		
		}

		private void InputDlg_Load(object sender, System.EventArgs e)
		{
		
		}
	}
}

'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.ImagXpress8
Imports PegasusImaging.WinForms.NotateXpress8
Imports System.ComponentModel
Imports System.Windows.Forms
Imports System
Imports System.Data
Imports System.Drawing
Imports System.Runtime.InteropServices

Public Class Form1
    Inherits System.Windows.Forms.Form


    Private strImageFileName As String

    Private imagX1 As PegasusImaging.WinForms.ImagXpress8.ImageX
    Private ImageXView2 As PegasusImaging.WinForms.ImagXpress8.ImageXView

    Private loIX_loadOptions As PegasusImaging.WinForms.ImagXpress8.LoadOptions
    Private m_saveOptions As PegasusImaging.WinForms.NotateXpress8.SaveOptions
    Private m_loadOptions As PegasusImaging.WinForms.NotateXpress8.LoadOptions

    Private _SavedAnnotationMemoryStream As System.IO.MemoryStream = Nothing
    Private _SavedAnnotationByteArray() As Byte = Nothing

    Private _bAutoSelect As Boolean
    Private _bUpdateMoved As Boolean

  

    Private Declare Function GlobalFree Lib "kernel32.dll" (ByVal hMem As IntPtr) As IntPtr

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        '**The UnlockControl function must be called to distribute the runtime**
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(12345,12345,12345,12345)

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If (disposing) Then

            ' Don't forget to dispose IX
            '
            If Not (imagX1 Is Nothing) Then

                imagX1.Dispose()
                imagX1 = Nothing
            End If

            If Not (ImageXView1 Is Nothing) Then

                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If
            If Not (ImagXpress1 Is Nothing) Then

                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If
            If Not (ImageXView2 Is Nothing) Then

                ImageXView2.Dispose()
                ImageXView2 = Nothing
            End If

            If Not (Processor1 Is Nothing) Then

                Processor1.Dispose()
                Processor1 = Nothing
            End If
            ' Don't forget to dispose NXP
            '                
            If Not (NotateXpress1 Is Nothing) Then

                NotateXpress1.Dispose()
                NotateXpress1 = Nothing
            End If
            If Not (components Is Nothing) Then

                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)

    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents NotateXpress1 As PegasusImaging.WinForms.NotateXpress8.NotateXpress
    Friend WithEvents lstStatus As System.Windows.Forms.ListBox
    Friend WithEvents lblLoadStatus As System.Windows.Forms.Label
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuLoadSaveAnnType As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemImagingForWindows As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemNotateAnnotations As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemViewDirectorAnnotations As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_file_wangcompatible As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemLoadAnnotations As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_load_tiff As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemSaveAnnotations As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_file_save As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemSaveByteArray As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemSaveMemoryStream As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem17 As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemUnicodeMode As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemLoadByteArray As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemLoadMemoryStream As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem22 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_fileIResY As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem24 As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToggleAllowPaint As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem26 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_file_exit As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_create As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem32 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_setcurrent As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_deletecurrent As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_togglehide As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemDeactivateCurrentLayer As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_setname As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_setdescription As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_reverse As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemSendToBack As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemBringToFront As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemDeleteSelected As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemSaveCurrentLayer As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_layers_toggletoolbar As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemDisableToolbar As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_autoselect As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem49 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_create10 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_rects As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_buttons As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_createtransrect As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_createImage As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_nudgeup As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_nudgedn As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_inflate As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem58 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_redaction As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_AddRulerAnn As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_largetext As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_useMLE As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem63 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_brand As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem65 As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemCreateNewNX8 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_orient As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_orient_ltbr As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_orient_brtl As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_orient_rblt As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_orient_tlbr As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_orientation_rotate180 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemBoundingRectangle As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem75 As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_settext As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_backcolor As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_pencolor As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_penwidth As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_bevelshadow As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_bevellight As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_setfont As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_getitemtext As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_hb As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_hf As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_moveable As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_sizeable As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_getitemfont As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_setnewdib As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_mode As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_mode_editmode As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_mode_interactive As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_cm As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_cm_enable As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemGroups As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_objects_allowgrouping As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem97 As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemCreateGroup As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemDeleteGroup As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemSetGroupEmpty As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemSelectGroupItems As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemGroupAddSelectedItems As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemGroupRemoveSelectedItems As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemMirrorYoked As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemInvertYoked As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemSetGroupUserString As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToolBar As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToolbarRulerTool As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemRulerToolEnabeled As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemRulerToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToolbarFreehandTool As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemFreehandToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemFreehandToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToolbarEllipseTool As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemEllipseToolEnabeld As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemEllipseToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToolbarTextTool As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemTextToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemTextToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToolbarRectangleTool As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemRectangleToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemRectangleToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToolbarStampTool As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemStampToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemStampToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToolbarPolylineTool As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemPolylineToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemPolylineToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToolbarLineTool As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemLineToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemLineToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToolbarImageTool As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemImageToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemImageToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToolbarButtonTool As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemButtonToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemButtonToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemToolbarPolygonTool As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemPolygonToolEnabled As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemPolygonToolVisible As System.Windows.Forms.MenuItem
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents Processor1 As PegasusImaging.WinForms.ImagXpress8.Processor
    Friend WithEvents mnu_stuff_toggleuserdraw As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemHelp As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemAboutIX As System.Windows.Forms.MenuItem
    Friend WithEvents menuItemAboutNX As System.Windows.Forms.MenuItem
    Friend WithEvents OutputList As System.Windows.Forms.ListBox
    Friend WithEvents lblEvents As System.Windows.Forms.Label
    Public WithEvents ZoomOut As System.Windows.Forms.Button
    Public WithEvents ZoomIn As System.Windows.Forms.Button
    Private WithEvents menuItemYokeGroupItems As System.Windows.Forms.MenuItem
    Friend WithEvents FontDialog1 As System.Windows.Forms.FontDialog
    Friend WithEvents mnu_file_saveann2fileNXP As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_file_saveann2fileANN As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_file_loadannotationNXP As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_file_loadannotationANN As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_stuff_fixed As System.Windows.Forms.MenuItem
    Friend WithEvents mnu_SelectAll As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.NotateXpress1 = New PegasusImaging.WinForms.NotateXpress8.NotateXpress()
        Me.lstStatus = New System.Windows.Forms.ListBox()
        Me.lblLoadStatus = New System.Windows.Forms.Label()
        Me.lblLastError = New System.Windows.Forms.Label()
        Me.lblerror = New System.Windows.Forms.Label()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.mnuLoadSaveAnnType = New System.Windows.Forms.MenuItem()
        Me.menuItemImagingForWindows = New System.Windows.Forms.MenuItem()
        Me.menuItemNotateAnnotations = New System.Windows.Forms.MenuItem()
        Me.menuItemViewDirectorAnnotations = New System.Windows.Forms.MenuItem()
        Me.mnu_file_wangcompatible = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.menuItemLoadAnnotations = New System.Windows.Forms.MenuItem()
        Me.mnu_load_tiff = New System.Windows.Forms.MenuItem()
        Me.MenuItem10 = New System.Windows.Forms.MenuItem()
        Me.menuItemSaveAnnotations = New System.Windows.Forms.MenuItem()
        Me.mnu_file_save = New System.Windows.Forms.MenuItem()
        Me.MenuItem13 = New System.Windows.Forms.MenuItem()
        Me.mnu_file_saveann2fileNXP = New System.Windows.Forms.MenuItem()
        Me.mnu_file_saveann2fileANN = New System.Windows.Forms.MenuItem()
        Me.menuItemSaveByteArray = New System.Windows.Forms.MenuItem()
        Me.menuItemSaveMemoryStream = New System.Windows.Forms.MenuItem()
        Me.MenuItem17 = New System.Windows.Forms.MenuItem()
        Me.menuItemUnicodeMode = New System.Windows.Forms.MenuItem()
        Me.mnu_file_loadannotationNXP = New System.Windows.Forms.MenuItem()
        Me.mnu_file_loadannotationANN = New System.Windows.Forms.MenuItem()
        Me.menuItemLoadByteArray = New System.Windows.Forms.MenuItem()
        Me.menuItemLoadMemoryStream = New System.Windows.Forms.MenuItem()
        Me.MenuItem22 = New System.Windows.Forms.MenuItem()
        Me.mnu_fileIResY = New System.Windows.Forms.MenuItem()
        Me.MenuItem24 = New System.Windows.Forms.MenuItem()
        Me.menuItemToggleAllowPaint = New System.Windows.Forms.MenuItem()
        Me.MenuItem26 = New System.Windows.Forms.MenuItem()
        Me.mnu_file_exit = New System.Windows.Forms.MenuItem()
        Me.mnu_layers = New System.Windows.Forms.MenuItem()
        Me.mnu_layers_create = New System.Windows.Forms.MenuItem()
        Me.MenuItem32 = New System.Windows.Forms.MenuItem()
        Me.mnu_layers_setcurrent = New System.Windows.Forms.MenuItem()
        Me.mnu_layers_deletecurrent = New System.Windows.Forms.MenuItem()
        Me.mnu_layers_togglehide = New System.Windows.Forms.MenuItem()
        Me.menuItemDeactivateCurrentLayer = New System.Windows.Forms.MenuItem()
        Me.mnu_layers_setname = New System.Windows.Forms.MenuItem()
        Me.mnu_layers_setdescription = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_reverse = New System.Windows.Forms.MenuItem()
        Me.menuItemSendToBack = New System.Windows.Forms.MenuItem()
        Me.menuItemBringToFront = New System.Windows.Forms.MenuItem()
        Me.menuItemDeleteSelected = New System.Windows.Forms.MenuItem()
        Me.menuItemSaveCurrentLayer = New System.Windows.Forms.MenuItem()
        Me.mnu_SelectAll = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.mnu_layers_toggletoolbar = New System.Windows.Forms.MenuItem()
        Me.menuItemDisableToolbar = New System.Windows.Forms.MenuItem()
        Me.mnu_objects = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_autoselect = New System.Windows.Forms.MenuItem()
        Me.MenuItem49 = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_create10 = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_rects = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_buttons = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_createtransrect = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_createImage = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_nudgeup = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_nudgedn = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_inflate = New System.Windows.Forms.MenuItem()
        Me.MenuItem58 = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_redaction = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_AddRulerAnn = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_largetext = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_useMLE = New System.Windows.Forms.MenuItem()
        Me.MenuItem63 = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_brand = New System.Windows.Forms.MenuItem()
        Me.MenuItem65 = New System.Windows.Forms.MenuItem()
        Me.menuItemCreateNewNX8 = New System.Windows.Forms.MenuItem()
        Me.mnu_orient = New System.Windows.Forms.MenuItem()
        Me.mnu_orient_ltbr = New System.Windows.Forms.MenuItem()
        Me.mnu_orient_brtl = New System.Windows.Forms.MenuItem()
        Me.mnu_orient_rblt = New System.Windows.Forms.MenuItem()
        Me.mnu_orient_tlbr = New System.Windows.Forms.MenuItem()
        Me.mnu_orientation_rotate180 = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff = New System.Windows.Forms.MenuItem()
        Me.menuItemBoundingRectangle = New System.Windows.Forms.MenuItem()
        Me.MenuItem75 = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_settext = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_backcolor = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_pencolor = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_penwidth = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_bevelshadow = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_bevellight = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_setfont = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_getitemtext = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_hb = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_hf = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_moveable = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_fixed = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_sizeable = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_getitemfont = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_setnewdib = New System.Windows.Forms.MenuItem()
        Me.mnu_stuff_toggleuserdraw = New System.Windows.Forms.MenuItem()
        Me.mnu_mode = New System.Windows.Forms.MenuItem()
        Me.mnu_mode_editmode = New System.Windows.Forms.MenuItem()
        Me.mnu_mode_interactive = New System.Windows.Forms.MenuItem()
        Me.mnu_cm = New System.Windows.Forms.MenuItem()
        Me.mnu_cm_enable = New System.Windows.Forms.MenuItem()
        Me.menuItemGroups = New System.Windows.Forms.MenuItem()
        Me.mnu_objects_allowgrouping = New System.Windows.Forms.MenuItem()
        Me.MenuItem97 = New System.Windows.Forms.MenuItem()
        Me.menuItemCreateGroup = New System.Windows.Forms.MenuItem()
        Me.menuItemDeleteGroup = New System.Windows.Forms.MenuItem()
        Me.menuItemSetGroupEmpty = New System.Windows.Forms.MenuItem()
        Me.menuItemSelectGroupItems = New System.Windows.Forms.MenuItem()
        Me.menuItemGroupAddSelectedItems = New System.Windows.Forms.MenuItem()
        Me.menuItemGroupRemoveSelectedItems = New System.Windows.Forms.MenuItem()
        Me.menuItemYokeGroupItems = New System.Windows.Forms.MenuItem()
        Me.menuItemMirrorYoked = New System.Windows.Forms.MenuItem()
        Me.menuItemInvertYoked = New System.Windows.Forms.MenuItem()
        Me.menuItemSetGroupUserString = New System.Windows.Forms.MenuItem()
        Me.menuItemToolBar = New System.Windows.Forms.MenuItem()
        Me.menuItemToolbarRulerTool = New System.Windows.Forms.MenuItem()
        Me.menuItemRulerToolEnabeled = New System.Windows.Forms.MenuItem()
        Me.menuItemRulerToolVisible = New System.Windows.Forms.MenuItem()
        Me.menuItemToolbarFreehandTool = New System.Windows.Forms.MenuItem()
        Me.menuItemFreehandToolEnabled = New System.Windows.Forms.MenuItem()
        Me.menuItemFreehandToolVisible = New System.Windows.Forms.MenuItem()
        Me.menuItemToolbarEllipseTool = New System.Windows.Forms.MenuItem()
        Me.menuItemEllipseToolEnabeld = New System.Windows.Forms.MenuItem()
        Me.menuItemEllipseToolVisible = New System.Windows.Forms.MenuItem()
        Me.menuItemToolbarTextTool = New System.Windows.Forms.MenuItem()
        Me.menuItemTextToolEnabled = New System.Windows.Forms.MenuItem()
        Me.menuItemTextToolVisible = New System.Windows.Forms.MenuItem()
        Me.menuItemToolbarRectangleTool = New System.Windows.Forms.MenuItem()
        Me.menuItemRectangleToolEnabled = New System.Windows.Forms.MenuItem()
        Me.menuItemRectangleToolVisible = New System.Windows.Forms.MenuItem()
        Me.menuItemToolbarStampTool = New System.Windows.Forms.MenuItem()
        Me.menuItemStampToolEnabled = New System.Windows.Forms.MenuItem()
        Me.menuItemStampToolVisible = New System.Windows.Forms.MenuItem()
        Me.menuItemToolbarPolylineTool = New System.Windows.Forms.MenuItem()
        Me.menuItemPolylineToolEnabled = New System.Windows.Forms.MenuItem()
        Me.menuItemPolylineToolVisible = New System.Windows.Forms.MenuItem()
        Me.menuItemToolbarLineTool = New System.Windows.Forms.MenuItem()
        Me.menuItemLineToolEnabled = New System.Windows.Forms.MenuItem()
        Me.menuItemLineToolVisible = New System.Windows.Forms.MenuItem()
        Me.menuItemToolbarImageTool = New System.Windows.Forms.MenuItem()
        Me.menuItemImageToolEnabled = New System.Windows.Forms.MenuItem()
        Me.menuItemImageToolVisible = New System.Windows.Forms.MenuItem()
        Me.menuItemToolbarButtonTool = New System.Windows.Forms.MenuItem()
        Me.menuItemButtonToolEnabled = New System.Windows.Forms.MenuItem()
        Me.menuItemButtonToolVisible = New System.Windows.Forms.MenuItem()
        Me.menuItemToolbarPolygonTool = New System.Windows.Forms.MenuItem()
        Me.menuItemPolygonToolEnabled = New System.Windows.Forms.MenuItem()
        Me.menuItemPolygonToolVisible = New System.Windows.Forms.MenuItem()
        Me.menuItemHelp = New System.Windows.Forms.MenuItem()
        Me.menuItemAboutNX = New System.Windows.Forms.MenuItem()
        Me.menuItemAboutIX = New System.Windows.Forms.MenuItem()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.Processor1 = New PegasusImaging.WinForms.ImagXpress8.Processor()
        Me.OutputList = New System.Windows.Forms.ListBox()
        Me.lblEvents = New System.Windows.Forms.Label()
        Me.ZoomOut = New System.Windows.Forms.Button()
        Me.ZoomIn = New System.Windows.Forms.Button()
        Me.FontDialog1 = New System.Windows.Forms.FontDialog()
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.ImageXView1.Location = New System.Drawing.Point(24, 96)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(520, 392)
        Me.ImageXView1.TabIndex = 0
        '
        'NotateXpress1
        '
        Me.NotateXpress1.AllowPaint = True
        Me.NotateXpress1.Debug = False
        Me.NotateXpress1.DebugLogFile = "c:\NotateXpress8.log"
        Me.NotateXpress1.ErrorLevel = PegasusImaging.WinForms.NotateXpress8.ErrorLevelInfo.Production
        Me.NotateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress8.FontScaling.Normal
        Me.NotateXpress1.ImagXpressLoad = True
        Me.NotateXpress1.ImagXpressSave = True
        Me.NotateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress8.AnnotationMode.Edit
        Me.NotateXpress1.MultiLineEdit = False
        '
        'lstStatus
        '
        Me.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstStatus.Location = New System.Drawing.Point(552, 144)
        Me.lstStatus.Name = "lstStatus"
        Me.lstStatus.Size = New System.Drawing.Size(232, 82)
        Me.lstStatus.TabIndex = 9
        '
        'lblLoadStatus
        '
        Me.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLoadStatus.Location = New System.Drawing.Point(552, 96)
        Me.lblLoadStatus.Name = "lblLoadStatus"
        Me.lblLoadStatus.Size = New System.Drawing.Size(192, 40)
        Me.lblLoadStatus.TabIndex = 8
        Me.lblLoadStatus.Text = "Load Status:"
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLastError.Location = New System.Drawing.Point(560, 256)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(216, 32)
        Me.lblLastError.TabIndex = 7
        Me.lblLastError.Text = "Last Error:"
        '
        'lblerror
        '
        Me.lblerror.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblerror.Location = New System.Drawing.Point(560, 296)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(224, 128)
        Me.lblerror.TabIndex = 6
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnu_layers, Me.mnu_objects, Me.mnu_orient, Me.mnu_stuff, Me.mnu_mode, Me.mnu_cm, Me.menuItemGroups, Me.menuItemToolBar, Me.menuItemHelp})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuLoadSaveAnnType, Me.mnu_file_wangcompatible, Me.MenuItem7, Me.menuItemLoadAnnotations, Me.mnu_load_tiff, Me.MenuItem10, Me.menuItemSaveAnnotations, Me.mnu_file_save, Me.MenuItem13, Me.mnu_file_saveann2fileNXP, Me.mnu_file_saveann2fileANN, Me.menuItemSaveByteArray, Me.menuItemSaveMemoryStream, Me.MenuItem17, Me.menuItemUnicodeMode, Me.mnu_file_loadannotationNXP, Me.mnu_file_loadannotationANN, Me.menuItemLoadByteArray, Me.menuItemLoadMemoryStream, Me.MenuItem22, Me.mnu_fileIResY, Me.MenuItem24, Me.menuItemToggleAllowPaint, Me.MenuItem26, Me.mnu_file_exit})
        Me.mnuFile.Text = "File"
        '
        'mnuLoadSaveAnnType
        '
        Me.mnuLoadSaveAnnType.Index = 0
        Me.mnuLoadSaveAnnType.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemImagingForWindows, Me.menuItemNotateAnnotations, Me.menuItemViewDirectorAnnotations})
        Me.mnuLoadSaveAnnType.Text = "Load/Save Annotation Type"
        '
        'menuItemImagingForWindows
        '
        Me.menuItemImagingForWindows.Index = 0
        Me.menuItemImagingForWindows.Text = "Imaging For Windows (Wang)"
        '
        'menuItemNotateAnnotations
        '
        Me.menuItemNotateAnnotations.Index = 1
        Me.menuItemNotateAnnotations.Text = "NotateXpress (NXP)"
        '
        'menuItemViewDirectorAnnotations
        '
        Me.menuItemViewDirectorAnnotations.Index = 2
        Me.menuItemViewDirectorAnnotations.Text = "ViewDirector - TMSSequoia (ANN)"
        '
        'mnu_file_wangcompatible
        '
        Me.mnu_file_wangcompatible.Checked = True
        Me.mnu_file_wangcompatible.Index = 1
        Me.mnu_file_wangcompatible.Text = "Toggle Load/Save Preserve Wang"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 2
        Me.MenuItem7.Text = "-"
        '
        'menuItemLoadAnnotations
        '
        Me.menuItemLoadAnnotations.Index = 3
        Me.menuItemLoadAnnotations.Text = "Toggle Load Annotations thru IX"
        '
        'mnu_load_tiff
        '
        Me.mnu_load_tiff.Index = 4
        Me.mnu_load_tiff.Text = "Load Image..."
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 5
        Me.MenuItem10.Text = "-"
        '
        'menuItemSaveAnnotations
        '
        Me.menuItemSaveAnnotations.Index = 6
        Me.menuItemSaveAnnotations.Text = "Toggle Save Annotations thru IX"
        '
        'mnu_file_save
        '
        Me.mnu_file_save.Index = 7
        Me.mnu_file_save.Text = "Save Image..."
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 8
        Me.MenuItem13.Text = "-"
        '
        'mnu_file_saveann2fileNXP
        '
        Me.mnu_file_saveann2fileNXP.Index = 9
        Me.mnu_file_saveann2fileNXP.Text = "Save annotation to file(NXP)"
        '
        'mnu_file_saveann2fileANN
        '
        Me.mnu_file_saveann2fileANN.Index = 10
        Me.mnu_file_saveann2fileANN.Text = "Save annotation to file(ANN)"
        '
        'menuItemSaveByteArray
        '
        Me.menuItemSaveByteArray.Index = 11
        Me.menuItemSaveByteArray.Text = "Save annotation to ByteArray"
        '
        'menuItemSaveMemoryStream
        '
        Me.menuItemSaveMemoryStream.Index = 12
        Me.menuItemSaveMemoryStream.Text = "Save annotation to MemoryStream"
        '
        'MenuItem17
        '
        Me.MenuItem17.Index = 13
        Me.MenuItem17.Text = "-"
        '
        'menuItemUnicodeMode
        '
        Me.menuItemUnicodeMode.Checked = True
        Me.menuItemUnicodeMode.Index = 14
        Me.menuItemUnicodeMode.Text = "Toggle Load in Unicode Mode"
        '
        'mnu_file_loadannotationNXP
        '
        Me.mnu_file_loadannotationNXP.Index = 15
        Me.mnu_file_loadannotationNXP.Text = "Load annotation from file(NXP)"
        '
        'mnu_file_loadannotationANN
        '
        Me.mnu_file_loadannotationANN.Index = 16
        Me.mnu_file_loadannotationANN.Text = "Load Annotation from file(ANN)"
        '
        'menuItemLoadByteArray
        '
        Me.menuItemLoadByteArray.Index = 17
        Me.menuItemLoadByteArray.Text = "Load annotation from ByteArray"
        '
        'menuItemLoadMemoryStream
        '
        Me.menuItemLoadMemoryStream.Index = 18
        Me.menuItemLoadMemoryStream.Text = "Load annotation from MemoryStream"
        '
        'MenuItem22
        '
        Me.MenuItem22.Index = 19
        Me.MenuItem22.Text = "-"
        '
        'mnu_fileIResY
        '
        Me.mnu_fileIResY.Index = 20
        Me.mnu_fileIResY.Text = "IResY FontScaling"
        '
        'MenuItem24
        '
        Me.MenuItem24.Index = 21
        Me.MenuItem24.Text = "-"
        '
        'menuItemToggleAllowPaint
        '
        Me.menuItemToggleAllowPaint.Index = 22
        Me.menuItemToggleAllowPaint.Text = "Toggle Allow Paint"
        '
        'MenuItem26
        '
        Me.MenuItem26.Index = 23
        Me.MenuItem26.Text = "-"
        '
        'mnu_file_exit
        '
        Me.mnu_file_exit.Index = 24
        Me.mnu_file_exit.Text = "E&xit"
        '
        'mnu_layers
        '
        Me.mnu_layers.Index = 1
        Me.mnu_layers.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_layers_create, Me.MenuItem32, Me.mnu_layers_setcurrent, Me.mnu_layers_deletecurrent, Me.mnu_layers_togglehide, Me.menuItemDeactivateCurrentLayer, Me.mnu_layers_setname, Me.mnu_layers_setdescription, Me.mnu_objects_reverse, Me.menuItemSendToBack, Me.menuItemBringToFront, Me.menuItemDeleteSelected, Me.menuItemSaveCurrentLayer, Me.mnu_SelectAll, Me.MenuItem1, Me.mnu_layers_toggletoolbar, Me.menuItemDisableToolbar})
        Me.mnu_layers.Text = "Layers"
        '
        'mnu_layers_create
        '
        Me.mnu_layers_create.Index = 0
        Me.mnu_layers_create.Text = "Create..."
        '
        'MenuItem32
        '
        Me.MenuItem32.Index = 1
        Me.MenuItem32.Text = "-"
        '
        'mnu_layers_setcurrent
        '
        Me.mnu_layers_setcurrent.Index = 2
        Me.mnu_layers_setcurrent.Text = "Set current"
        '
        'mnu_layers_deletecurrent
        '
        Me.mnu_layers_deletecurrent.Index = 3
        Me.mnu_layers_deletecurrent.Text = "Delete current"
        '
        'mnu_layers_togglehide
        '
        Me.mnu_layers_togglehide.Index = 4
        Me.mnu_layers_togglehide.Text = "Hide current"
        '
        'menuItemDeactivateCurrentLayer
        '
        Me.menuItemDeactivateCurrentLayer.Index = 5
        Me.menuItemDeactivateCurrentLayer.Text = "Deactivate current"
        '
        'mnu_layers_setname
        '
        Me.mnu_layers_setname.Index = 6
        Me.mnu_layers_setname.Text = "Current name..."
        '
        'mnu_layers_setdescription
        '
        Me.mnu_layers_setdescription.Index = 7
        Me.mnu_layers_setdescription.Text = "Current description..."
        '
        'mnu_objects_reverse
        '
        Me.mnu_objects_reverse.Index = 8
        Me.mnu_objects_reverse.Text = "Reverse current ZOrder"
        '
        'menuItemSendToBack
        '
        Me.menuItemSendToBack.Index = 9
        Me.menuItemSendToBack.Text = "Current selected To Back of ZOrder"
        '
        'menuItemBringToFront
        '
        Me.menuItemBringToFront.Index = 10
        Me.menuItemBringToFront.Text = "Current selected To Front of ZOrder"
        '
        'menuItemDeleteSelected
        '
        Me.menuItemDeleteSelected.Index = 11
        Me.menuItemDeleteSelected.Text = "Current delete Selected"
        '
        'menuItemSaveCurrentLayer
        '
        Me.menuItemSaveCurrentLayer.Index = 12
        Me.menuItemSaveCurrentLayer.Text = "Save Current..."
        '
        'mnu_SelectAll
        '
        Me.mnu_SelectAll.Index = 13
        Me.mnu_SelectAll.Text = "Toggle Current SelectAll"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 14
        Me.MenuItem1.Text = "-"
        '
        'mnu_layers_toggletoolbar
        '
        Me.mnu_layers_toggletoolbar.Index = 15
        Me.mnu_layers_toggletoolbar.Text = "Toggle toolbar visibility"
        '
        'menuItemDisableToolbar
        '
        Me.menuItemDisableToolbar.Index = 16
        Me.menuItemDisableToolbar.Text = "Disable toolbar"
        '
        'mnu_objects
        '
        Me.mnu_objects.Index = 2
        Me.mnu_objects.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_objects_autoselect, Me.MenuItem49, Me.mnu_objects_create10, Me.mnu_objects_rects, Me.mnu_objects_buttons, Me.mnu_objects_createtransrect, Me.mnu_objects_createImage, Me.mnu_objects_nudgeup, Me.mnu_objects_nudgedn, Me.mnu_objects_inflate, Me.MenuItem58, Me.mnu_objects_redaction, Me.mnu_objects_AddRulerAnn, Me.mnu_objects_largetext, Me.mnu_objects_useMLE, Me.MenuItem63, Me.mnu_objects_brand, Me.MenuItem65, Me.menuItemCreateNewNX8})
        Me.mnu_objects.Text = "Objects"
        '
        'mnu_objects_autoselect
        '
        Me.mnu_objects_autoselect.Checked = True
        Me.mnu_objects_autoselect.Index = 0
        Me.mnu_objects_autoselect.Text = "Auto-select when created"
        '
        'MenuItem49
        '
        Me.MenuItem49.Index = 1
        Me.MenuItem49.Text = "-"
        '
        'mnu_objects_create10
        '
        Me.mnu_objects_create10.Index = 2
        Me.mnu_objects_create10.Text = "Create 10 objects"
        '
        'mnu_objects_rects
        '
        Me.mnu_objects_rects.Index = 3
        Me.mnu_objects_rects.Text = "Create 10 rects"
        '
        'mnu_objects_buttons
        '
        Me.mnu_objects_buttons.Index = 4
        Me.mnu_objects_buttons.Text = "Create 10 buttons"
        '
        'mnu_objects_createtransrect
        '
        Me.mnu_objects_createtransrect.Index = 5
        Me.mnu_objects_createtransrect.Text = "Create translucent rectangle"
        '
        'mnu_objects_createImage
        '
        Me.mnu_objects_createImage.Index = 6
        Me.mnu_objects_createImage.Text = "Create an Image object"
        '
        'mnu_objects_nudgeup
        '
        Me.mnu_objects_nudgeup.Index = 7
        Me.mnu_objects_nudgeup.Text = "Nudge up"
        '
        'mnu_objects_nudgedn
        '
        Me.mnu_objects_nudgedn.Index = 8
        Me.mnu_objects_nudgedn.Text = "Nudge dn"
        '
        'mnu_objects_inflate
        '
        Me.mnu_objects_inflate.Index = 9
        Me.mnu_objects_inflate.Text = "Inflate me"
        '
        'MenuItem58
        '
        Me.MenuItem58.Index = 10
        Me.MenuItem58.Text = "-"
        '
        'mnu_objects_redaction
        '
        Me.mnu_objects_redaction.Index = 11
        Me.mnu_objects_redaction.Text = "Redaction"
        '
        'mnu_objects_AddRulerAnn
        '
        Me.mnu_objects_AddRulerAnn.Index = 12
        Me.mnu_objects_AddRulerAnn.Text = "Add Ruler Annotation"
        '
        'mnu_objects_largetext
        '
        Me.mnu_objects_largetext.Index = 13
        Me.mnu_objects_largetext.Text = "Create large text object"
        '
        'mnu_objects_useMLE
        '
        Me.mnu_objects_useMLE.Index = 14
        Me.mnu_objects_useMLE.Text = "Use MLE to edit existing text"
        '
        'MenuItem63
        '
        Me.MenuItem63.Index = 15
        Me.MenuItem63.Text = "-"
        '
        'mnu_objects_brand
        '
        Me.mnu_objects_brand.Index = 16
        Me.mnu_objects_brand.Text = "Brand"
        '
        'MenuItem65
        '
        Me.MenuItem65.Index = 17
        Me.MenuItem65.Text = "-"
        '
        'menuItemCreateNewNX8
        '
        Me.menuItemCreateNewNX8.Index = 18
        Me.menuItemCreateNewNX8.Text = "Create new NX8 component"
        '
        'mnu_orient
        '
        Me.mnu_orient.Index = 3
        Me.mnu_orient.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_orient_ltbr, Me.mnu_orient_brtl, Me.mnu_orient_rblt, Me.mnu_orient_tlbr, Me.mnu_orientation_rotate180})
        Me.mnu_orient.Text = "Orientation"
        '
        'mnu_orient_ltbr
        '
        Me.mnu_orient_ltbr.Index = 0
        Me.mnu_orient_ltbr.Text = "Rotate 90 clockwise"
        '
        'mnu_orient_brtl
        '
        Me.mnu_orient_brtl.Index = 1
        Me.mnu_orient_brtl.Text = "Invert"
        '
        'mnu_orient_rblt
        '
        Me.mnu_orient_rblt.Index = 2
        Me.mnu_orient_rblt.Text = "Rotate 90 counterclockwise"
        '
        'mnu_orient_tlbr
        '
        Me.mnu_orient_tlbr.Index = 3
        Me.mnu_orient_tlbr.Text = "Mirror"
        '
        'mnu_orientation_rotate180
        '
        Me.mnu_orientation_rotate180.Index = 4
        Me.mnu_orientation_rotate180.Text = "Rotate 180"
        '
        'mnu_stuff
        '
        Me.mnu_stuff.Index = 4
        Me.mnu_stuff.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemBoundingRectangle, Me.MenuItem75, Me.mnu_stuff_settext, Me.mnu_stuff_backcolor, Me.mnu_stuff_pencolor, Me.mnu_stuff_penwidth, Me.mnu_stuff_bevelshadow, Me.mnu_stuff_bevellight, Me.mnu_stuff_setfont, Me.mnu_stuff_getitemtext, Me.mnu_stuff_hb, Me.mnu_stuff_hf, Me.mnu_stuff_moveable, Me.mnu_stuff_fixed, Me.mnu_stuff_sizeable, Me.mnu_stuff_getitemfont, Me.mnu_stuff_setnewdib, Me.mnu_stuff_toggleuserdraw})
        Me.mnu_stuff.Text = "Stuff"
        '
        'menuItemBoundingRectangle
        '
        Me.menuItemBoundingRectangle.Index = 0
        Me.menuItemBoundingRectangle.Text = "Bounding Rectangle..."
        '
        'MenuItem75
        '
        Me.MenuItem75.Index = 1
        Me.MenuItem75.Text = "-"
        '
        'mnu_stuff_settext
        '
        Me.mnu_stuff_settext.Index = 2
        Me.mnu_stuff_settext.Text = "Set item text"
        '
        'mnu_stuff_backcolor
        '
        Me.mnu_stuff_backcolor.Index = 3
        Me.mnu_stuff_backcolor.Text = "Set item backcolor"
        '
        'mnu_stuff_pencolor
        '
        Me.mnu_stuff_pencolor.Index = 4
        Me.mnu_stuff_pencolor.Text = "Set item pen color"
        '
        'mnu_stuff_penwidth
        '
        Me.mnu_stuff_penwidth.Index = 5
        Me.mnu_stuff_penwidth.Text = "Set item pen width"
        '
        'mnu_stuff_bevelshadow
        '
        Me.mnu_stuff_bevelshadow.Index = 6
        Me.mnu_stuff_bevelshadow.Text = "Set item bevelshadowcolor"
        '
        'mnu_stuff_bevellight
        '
        Me.mnu_stuff_bevellight.Index = 7
        Me.mnu_stuff_bevellight.Text = "Set item bevellightcolor"
        '
        'mnu_stuff_setfont
        '
        Me.mnu_stuff_setfont.Index = 8
        Me.mnu_stuff_setfont.Text = "Set item font..."
        '
        'mnu_stuff_getitemtext
        '
        Me.mnu_stuff_getitemtext.Index = 9
        Me.mnu_stuff_getitemtext.Text = "Get item text"
        '
        'mnu_stuff_hb
        '
        Me.mnu_stuff_hb.Index = 10
        Me.mnu_stuff_hb.Text = "Toggle HighlightBack"
        '
        'mnu_stuff_hf
        '
        Me.mnu_stuff_hf.Index = 11
        Me.mnu_stuff_hf.Text = "Toggle HighlightFill"
        '
        'mnu_stuff_moveable
        '
        Me.mnu_stuff_moveable.Index = 12
        Me.mnu_stuff_moveable.Text = "Toggle item moveable"
        '
        'mnu_stuff_fixed
        '
        Me.mnu_stuff_fixed.Index = 13
        Me.mnu_stuff_fixed.Text = "Toggle item fixed"
        '
        'mnu_stuff_sizeable
        '
        Me.mnu_stuff_sizeable.Index = 14
        Me.mnu_stuff_sizeable.Text = "Toggle item sizeable"
        '
        'mnu_stuff_getitemfont
        '
        Me.mnu_stuff_getitemfont.Index = 15
        Me.mnu_stuff_getitemfont.Text = "Get item font"
        '
        'mnu_stuff_setnewdib
        '
        Me.mnu_stuff_setnewdib.Index = 16
        Me.mnu_stuff_setnewdib.Text = "Set new dib into Image object"
        '
        'mnu_stuff_toggleuserdraw
        '
        Me.mnu_stuff_toggleuserdraw.Index = 17
        Me.mnu_stuff_toggleuserdraw.Text = "Toggle user Draw"
        '
        'mnu_mode
        '
        Me.mnu_mode.Index = 5
        Me.mnu_mode.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_mode_editmode, Me.mnu_mode_interactive})
        Me.mnu_mode.Text = "Mode"
        '
        'mnu_mode_editmode
        '
        Me.mnu_mode_editmode.Index = 0
        Me.mnu_mode_editmode.Text = "Set Edit mode"
        '
        'mnu_mode_interactive
        '
        Me.mnu_mode_interactive.Index = 1
        Me.mnu_mode_interactive.Text = "Set Interactive mode"
        '
        'mnu_cm
        '
        Me.mnu_cm.Index = 6
        Me.mnu_cm.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_cm_enable})
        Me.mnu_cm.Text = "Context Menu"
        '
        'mnu_cm_enable
        '
        Me.mnu_cm_enable.Index = 0
        Me.mnu_cm_enable.Text = "Enable"
        '
        'menuItemGroups
        '
        Me.menuItemGroups.Index = 7
        Me.menuItemGroups.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnu_objects_allowgrouping, Me.MenuItem97, Me.menuItemCreateGroup, Me.menuItemDeleteGroup, Me.menuItemSetGroupEmpty, Me.menuItemSelectGroupItems, Me.menuItemGroupAddSelectedItems, Me.menuItemGroupRemoveSelectedItems, Me.menuItemYokeGroupItems, Me.menuItemMirrorYoked, Me.menuItemInvertYoked, Me.menuItemSetGroupUserString})
        Me.menuItemGroups.Text = "Groups"
        '
        'mnu_objects_allowgrouping
        '
        Me.mnu_objects_allowgrouping.Index = 0
        Me.mnu_objects_allowgrouping.Text = "Toggle Allow Grouping"
        '
        'MenuItem97
        '
        Me.MenuItem97.Index = 1
        Me.MenuItem97.Text = "-"
        '
        'menuItemCreateGroup
        '
        Me.menuItemCreateGroup.Index = 2
        Me.menuItemCreateGroup.Text = "Create..."
        '
        'menuItemDeleteGroup
        '
        Me.menuItemDeleteGroup.Index = 3
        Me.menuItemDeleteGroup.Text = "Delete"
        '
        'menuItemSetGroupEmpty
        '
        Me.menuItemSetGroupEmpty.Index = 4
        Me.menuItemSetGroupEmpty.Text = "Set Empty"
        '
        'menuItemSelectGroupItems
        '
        Me.menuItemSelectGroupItems.Index = 5
        Me.menuItemSelectGroupItems.Text = "Show Items Selected"
        '
        'menuItemGroupAddSelectedItems
        '
        Me.menuItemGroupAddSelectedItems.Index = 6
        Me.menuItemGroupAddSelectedItems.Text = "Add Selected Items"
        '
        'menuItemGroupRemoveSelectedItems
        '
        Me.menuItemGroupRemoveSelectedItems.Index = 7
        Me.menuItemGroupRemoveSelectedItems.Text = "Remove Selected Items"
        '
        'menuItemYokeGroupItems
        '
        Me.menuItemYokeGroupItems.Index = 8
        Me.menuItemYokeGroupItems.Text = "Toggle Yoked"
        '
        'menuItemMirrorYoked
        '
        Me.menuItemMirrorYoked.Index = 9
        Me.menuItemMirrorYoked.Text = "Mirror Yoked Items"
        '
        'menuItemInvertYoked
        '
        Me.menuItemInvertYoked.Index = 10
        Me.menuItemInvertYoked.Text = "Invert Yoked Items"
        '
        'menuItemSetGroupUserString
        '
        Me.menuItemSetGroupUserString.Index = 11
        Me.menuItemSetGroupUserString.Text = " User String..."
        '
        'menuItemToolBar
        '
        Me.menuItemToolBar.Index = 8
        Me.menuItemToolBar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemToolbarRulerTool, Me.menuItemToolbarFreehandTool, Me.menuItemToolbarEllipseTool, Me.menuItemToolbarTextTool, Me.menuItemToolbarRectangleTool, Me.menuItemToolbarStampTool, Me.menuItemToolbarPolylineTool, Me.menuItemToolbarLineTool, Me.menuItemToolbarImageTool, Me.menuItemToolbarButtonTool, Me.menuItemToolbarPolygonTool})
        Me.menuItemToolBar.Text = "ToolBar"
        '
        'menuItemToolbarRulerTool
        '
        Me.menuItemToolbarRulerTool.Index = 0
        Me.menuItemToolbarRulerTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemRulerToolEnabeled, Me.menuItemRulerToolVisible})
        Me.menuItemToolbarRulerTool.Text = "RulerTool"
        '
        'menuItemRulerToolEnabeled
        '
        Me.menuItemRulerToolEnabeled.Index = 0
        Me.menuItemRulerToolEnabeled.Text = "Enabled"
        '
        'menuItemRulerToolVisible
        '
        Me.menuItemRulerToolVisible.Index = 1
        Me.menuItemRulerToolVisible.Text = "Visible"
        '
        'menuItemToolbarFreehandTool
        '
        Me.menuItemToolbarFreehandTool.Index = 1
        Me.menuItemToolbarFreehandTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemFreehandToolEnabled, Me.menuItemFreehandToolVisible})
        Me.menuItemToolbarFreehandTool.Text = "Freehand Tool"
        '
        'menuItemFreehandToolEnabled
        '
        Me.menuItemFreehandToolEnabled.Index = 0
        Me.menuItemFreehandToolEnabled.Text = "Enabled"
        '
        'menuItemFreehandToolVisible
        '
        Me.menuItemFreehandToolVisible.Index = 1
        Me.menuItemFreehandToolVisible.Text = "Visible"
        '
        'menuItemToolbarEllipseTool
        '
        Me.menuItemToolbarEllipseTool.Index = 2
        Me.menuItemToolbarEllipseTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemEllipseToolEnabeld, Me.menuItemEllipseToolVisible})
        Me.menuItemToolbarEllipseTool.Text = "Elliplse Tool"
        '
        'menuItemEllipseToolEnabeld
        '
        Me.menuItemEllipseToolEnabeld.Index = 0
        Me.menuItemEllipseToolEnabeld.Text = "Enabled"
        '
        'menuItemEllipseToolVisible
        '
        Me.menuItemEllipseToolVisible.Index = 1
        Me.menuItemEllipseToolVisible.Text = "Visible"
        '
        'menuItemToolbarTextTool
        '
        Me.menuItemToolbarTextTool.Index = 3
        Me.menuItemToolbarTextTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemTextToolEnabled, Me.menuItemTextToolVisible})
        Me.menuItemToolbarTextTool.Text = "Text Tool"
        '
        'menuItemTextToolEnabled
        '
        Me.menuItemTextToolEnabled.Index = 0
        Me.menuItemTextToolEnabled.Text = "Enabled"
        '
        'menuItemTextToolVisible
        '
        Me.menuItemTextToolVisible.Index = 1
        Me.menuItemTextToolVisible.Text = "Visible"
        '
        'menuItemToolbarRectangleTool
        '
        Me.menuItemToolbarRectangleTool.Index = 4
        Me.menuItemToolbarRectangleTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemRectangleToolEnabled, Me.menuItemRectangleToolVisible})
        Me.menuItemToolbarRectangleTool.Text = "Rectangle Tool"
        '
        'menuItemRectangleToolEnabled
        '
        Me.menuItemRectangleToolEnabled.Index = 0
        Me.menuItemRectangleToolEnabled.Text = "Enabled"
        '
        'menuItemRectangleToolVisible
        '
        Me.menuItemRectangleToolVisible.Index = 1
        Me.menuItemRectangleToolVisible.Text = "Visible"
        '
        'menuItemToolbarStampTool
        '
        Me.menuItemToolbarStampTool.Index = 5
        Me.menuItemToolbarStampTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemStampToolEnabled, Me.menuItemStampToolVisible})
        Me.menuItemToolbarStampTool.Text = "Stamp Tool"
        '
        'menuItemStampToolEnabled
        '
        Me.menuItemStampToolEnabled.Index = 0
        Me.menuItemStampToolEnabled.Text = "Enabled"
        '
        'menuItemStampToolVisible
        '
        Me.menuItemStampToolVisible.Index = 1
        Me.menuItemStampToolVisible.Text = "Visible"
        '
        'menuItemToolbarPolylineTool
        '
        Me.menuItemToolbarPolylineTool.Index = 6
        Me.menuItemToolbarPolylineTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemPolylineToolEnabled, Me.menuItemPolylineToolVisible})
        Me.menuItemToolbarPolylineTool.Text = "Polyline Tool"
        '
        'menuItemPolylineToolEnabled
        '
        Me.menuItemPolylineToolEnabled.Index = 0
        Me.menuItemPolylineToolEnabled.Text = "Enabled"
        '
        'menuItemPolylineToolVisible
        '
        Me.menuItemPolylineToolVisible.Index = 1
        Me.menuItemPolylineToolVisible.Text = "Visible"
        '
        'menuItemToolbarLineTool
        '
        Me.menuItemToolbarLineTool.Index = 7
        Me.menuItemToolbarLineTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemLineToolEnabled, Me.menuItemLineToolVisible})
        Me.menuItemToolbarLineTool.Text = "Line Tool"
        '
        'menuItemLineToolEnabled
        '
        Me.menuItemLineToolEnabled.Index = 0
        Me.menuItemLineToolEnabled.Text = "Enabled"
        '
        'menuItemLineToolVisible
        '
        Me.menuItemLineToolVisible.Index = 1
        Me.menuItemLineToolVisible.Text = "Visible"
        '
        'menuItemToolbarImageTool
        '
        Me.menuItemToolbarImageTool.Index = 8
        Me.menuItemToolbarImageTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemImageToolEnabled, Me.menuItemImageToolVisible})
        Me.menuItemToolbarImageTool.Text = "Image Tool"
        '
        'menuItemImageToolEnabled
        '
        Me.menuItemImageToolEnabled.Index = 0
        Me.menuItemImageToolEnabled.Text = "Enabled"
        '
        'menuItemImageToolVisible
        '
        Me.menuItemImageToolVisible.Index = 1
        Me.menuItemImageToolVisible.Text = "Visible"
        '
        'menuItemToolbarButtonTool
        '
        Me.menuItemToolbarButtonTool.Index = 9
        Me.menuItemToolbarButtonTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemButtonToolEnabled, Me.menuItemButtonToolVisible})
        Me.menuItemToolbarButtonTool.Text = "Button Tool"
        '
        'menuItemButtonToolEnabled
        '
        Me.menuItemButtonToolEnabled.Index = 0
        Me.menuItemButtonToolEnabled.Text = "Enabled"
        '
        'menuItemButtonToolVisible
        '
        Me.menuItemButtonToolVisible.Index = 1
        Me.menuItemButtonToolVisible.Text = "Visible"
        '
        'menuItemToolbarPolygonTool
        '
        Me.menuItemToolbarPolygonTool.Index = 10
        Me.menuItemToolbarPolygonTool.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemPolygonToolEnabled, Me.menuItemPolygonToolVisible})
        Me.menuItemToolbarPolygonTool.Text = "Polygon Tool"
        '
        'menuItemPolygonToolEnabled
        '
        Me.menuItemPolygonToolEnabled.Index = 0
        Me.menuItemPolygonToolEnabled.Text = "Enabled"
        '
        'menuItemPolygonToolVisible
        '
        Me.menuItemPolygonToolVisible.Index = 1
        Me.menuItemPolygonToolVisible.Text = "Visible"
        '
        'menuItemHelp
        '
        Me.menuItemHelp.Index = 9
        Me.menuItemHelp.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.menuItemAboutNX, Me.menuItemAboutIX})
        Me.menuItemHelp.Text = "&Help"
        '
        'menuItemAboutNX
        '
        Me.menuItemAboutNX.Index = 0
        Me.menuItemAboutNX.Text = "About &NotateXpress..."
        '
        'menuItemAboutIX
        '
        Me.menuItemAboutIX.Index = 1
        Me.menuItemAboutIX.Text = "About &ImagXpress..."
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following:", "1)Adding various annotation types including redaction and ruler types.", "2)Creating,deleting and hiding layers.", "3)Saving annotations in the Pegasus NXP format and the Kodak Wang Imaging format." & _
        ""})
        Me.lstInfo.Location = New System.Drawing.Point(24, 8)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(752, 69)
        Me.lstInfo.TabIndex = 10
        '
        'Processor1
        '
        Me.Processor1.BackgroundColor = System.Drawing.Color.Black
        Me.Processor1.ContrastMethod = PegasusImaging.WinForms.ImagXpress8.ContrastMethod.DefaultContrast
        Me.Processor1.ProgressPercent = 10
        Me.Processor1.Redeyes = Nothing
        '
        'OutputList
        '
        Me.OutputList.Anchor = ((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.OutputList.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OutputList.Location = New System.Drawing.Point(24, 528)
        Me.OutputList.Name = "OutputList"
        Me.OutputList.Size = New System.Drawing.Size(736, 134)
        Me.OutputList.TabIndex = 11
        '
        'lblEvents
        '
        Me.lblEvents.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left)
        Me.lblEvents.Location = New System.Drawing.Point(24, 496)
        Me.lblEvents.Name = "lblEvents"
        Me.lblEvents.Size = New System.Drawing.Size(104, 24)
        Me.lblEvents.TabIndex = 12
        Me.lblEvents.Text = "Events:"
        '
        'ZoomOut
        '
        Me.ZoomOut.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.ZoomOut.BackColor = System.Drawing.SystemColors.Control
        Me.ZoomOut.Cursor = System.Windows.Forms.Cursors.Default
        Me.ZoomOut.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZoomOut.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ZoomOut.Location = New System.Drawing.Point(568, 488)
        Me.ZoomOut.Name = "ZoomOut"
        Me.ZoomOut.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ZoomOut.Size = New System.Drawing.Size(96, 33)
        Me.ZoomOut.TabIndex = 14
        Me.ZoomOut.Text = "Zoom Out"
        '
        'ZoomIn
        '
        Me.ZoomIn.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.ZoomIn.BackColor = System.Drawing.SystemColors.Control
        Me.ZoomIn.Cursor = System.Windows.Forms.Cursors.Default
        Me.ZoomIn.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ZoomIn.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ZoomIn.Location = New System.Drawing.Point(568, 448)
        Me.ZoomIn.Name = "ZoomIn"
        Me.ZoomIn.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ZoomIn.Size = New System.Drawing.Size(96, 33)
        Me.ZoomIn.TabIndex = 13
        Me.ZoomIn.Text = "Zoom In"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(792, 681)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.ZoomOut, Me.ZoomIn, Me.lblEvents, Me.OutputList, Me.lstInfo, Me.lstStatus, Me.lblLoadStatus, Me.lblLastError, Me.lblerror, Me.ImageXView1})
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.Text = "NotateXpress 8 VB.NET Demo"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Dim printDocument1 As System.Drawing.Printing.PrintDocument

    Dim strCurrentDir As System.String = "..\..\..\..\..\..\..\Common\Images\"
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub



    Private Overloads Function PegasusOpenFile(ByVal sTitle As String, ByVal sFileFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = sTitle
        dlg.Filter = sFileFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusSaveFile(ByVal sTitle As String, ByVal sFilter As String) As String
        Dim dlg As SaveFileDialog = New SaveFileDialog()
        dlg.Title = sTitle
        dlg.Filter = sFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function


#End Region

    Private Function createNewLayer(ByVal strLayerName As String, ByVal DibHandle As System.IntPtr) As PegasusImaging.WinForms.NotateXpress8.Layer
        ' ---------------
        ' create new layer
        ' ---------------
        Dim layer As PegasusImaging.WinForms.NotateXpress8.Layer = New Layer()

        ' ---------------
        ' Add the new layer to the collection and make it the selected layer
        ' ---------------
        ' TODO - Remove the following line before source is distro'd as sample code, Devtrack #158
        layer.Active = True ' short term bug work-around

        NotateXpress1.Layers.Add(layer)
        layer.Select()

        layer.Name = strLayerName
        layer.Description = "Layer created " + System.DateTime.Now.ToLongTimeString()
        layer.Active = True ' probably default value
        layer.Visible = True ' probably default value
        If NotateXpress1.InteractMode = AnnotationMode.Edit Then
            layer.Toolbar.Visible = True
        Else
            layer.Toolbar.Visible = False
            menuItemToolBar.Enabled = False
        End If

        ' ---------------
        ' set the button tool text and pen width for this demo
        ' ---------------
        layer.Toolbar.ButtonToolbarDefaults.PenWidth = 4
        layer.Toolbar.ButtonToolbarDefaults.Text = "Click me"

        ' ---------------
        ' set the image tool defaults for this demo
        ' ---------------
        layer.Toolbar.ImageToolbarDefaults.BackStyle = BackStyle.Transparent
        If DibHandle.ToInt32 <> IntPtr.Zero.ToInt32 Then
            layer.Toolbar.ImageToolbarDefaults.DibHandle = DibHandle
        End If

        ' ---------------
        ' set the stamp tool text and font for this demo
        ' ---------------
        layer.Toolbar.StampToolbarDefaults.Text = "NotateXpress 8"
        Dim fontCurrent As System.Drawing.Font = layer.Toolbar.StampToolbarDefaults.TextFont
        Dim fontNew As System.Drawing.Font = New System.Drawing.Font(fontCurrent.Name, 16, System.Drawing.FontStyle.Bold)
        layer.Toolbar.StampToolbarDefaults.TextFont = fontNew

        Return layer
    End Function

    Private Sub StatusChanged(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)
        lstStatus.Items.Add(e.Status.ToString(cultNumber))
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
    End Sub

    Private Sub StatusChanged(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)
        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If e.IsComplete Then
            lstStatus.Items.Add((e.TotalBytes + " Bytes Completed Loading."))
        End If
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
    End Sub



    Private Sub LoadFile(ByVal ImageXView As PegasusImaging.WinForms.ImagXpress8.ImageXView)
        Try
            imagX1 = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFileName)
            ImageXView.Image = imagX1
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub

    Public Sub ProgressEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)

        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If (e.IsComplete) Then
            lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))

            lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
        End If
    End Sub

    Public Sub ImageStatusEvent(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)

        '
        If (e.Status.Decoded) Then
            lstStatus.Items.Add(e.Status.ToString)
        End If
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Top = (Screen.PrimaryScreen.WorkingArea.Height - Me.Height) / 2
        Me.Left = (Screen.PrimaryScreen.WorkingArea.Width - Me.Width) / 2
        '--------------
        ' load file into ImagXpress
        '--------------
        Try
            Dim strCurrentDir As System.String = System.IO.Directory.GetCurrentDirectory()

            '**The UnlockRuntime function must be called to distribute the runtime**
            'imagXpress1.License.UnlockRuntime(12345, 12345, 12345, 12345)

            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEvent
            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEvent

            m_saveOptions = New PegasusImaging.WinForms.NotateXpress8.SaveOptions()
            m_loadOptions = New PegasusImaging.WinForms.NotateXpress8.LoadOptions()

            'Create a new load options object so we can recieve events from the images we load
            loIX_loadOptions = New PegasusImaging.WinForms.ImagXpress8.LoadOptions()



            strImageFileName = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\benefits.tif")
            LoadFile(ImageXView1)

            ' pass an image file relative path from the samples
            strImageFileName = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\vermont.jpg")

            ImageXView2 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
            LoadFile(ImageXView2)

            ' Use the SetClientMethod to connect the ImagXpress control and NotateXpress control
            NotateXpress1.ClientWindow = ImageXView1.Handle

            ImageXView1.AutoScroll = True

            ' set the "global" toolbar default values
            Dim toolbarDefaults As ToolbarDefaults = NotateXpress1.ToolbarDefaults
            toolbarDefaults.SetToolTip(AnnotationTool.EllipseTool, "The Ellipse-O-Matic!")
            toolbarDefaults.SetToolTip(AnnotationTool.FreehandTool, "Draw with this!")
            toolbarDefaults.SetToolTip(AnnotationTool.LineTool, "Liney, very, very Liney")
            toolbarDefaults.SetToolTip(AnnotationTool.PointerTool, "Point 'em out!")
            toolbarDefaults.SetToolTip(AnnotationTool.PolygonTool, "Polygons aren't gone")
            toolbarDefaults.SetToolTip(AnnotationTool.RectangleTool, "Rectangle, we got Rectangle")
            toolbarDefaults.SetToolTip(AnnotationTool.StampTool, "Stamp out dumb tool tips!")
            toolbarDefaults.SetToolTip(AnnotationTool.TextTool, "Write me a masterpiece!")
            toolbarDefaults.SetToolTip(AnnotationTool.PolyLineTool, "PolyLine")
            toolbarDefaults.SetToolTip(AnnotationTool.ButtonTool, "Button Tool")
            toolbarDefaults.SetToolTip(AnnotationTool.ImageTool, "Image Tool!")
            toolbarDefaults.SetToolTip(AnnotationTool.RulerTool, "Ruler Tool")

            ' Create a new layer and its toolbar
            Dim layer As Layer = createNewLayer("Layer1", ImageXView2.Image.ToHdib(False))

            m_loadOptions.AnnType = AnnotationType.ImagingForWindows
            m_saveOptions.AnnType = AnnotationType.ImagingForWindows

            mnu_file_saveann2fileANN.Enabled = False
            mnu_file_loadannotationANN.Enabled = False

            mnu_mode_interactive.Checked = False
            mnu_mode_editmode.Checked = True
            _bAutoSelect = True
            _bUpdateMoved = True
        Catch ex As Exception
            MessageBox.Show("FormMain_Load Error msg:   " + ex.Message + _
                            "Error type:  " + ex.GetType().ToString())
        End Try


    End Sub

    Private Sub mnuFile_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuFile.Select
        menuItemImagingForWindows.Checked = (m_saveOptions.AnnType = AnnotationType.ImagingForWindows)
        menuItemNotateAnnotations.Checked = (m_saveOptions.AnnType = AnnotationType.NotateXpress)
        menuItemViewDirectorAnnotations.Checked = (m_saveOptions.AnnType = AnnotationType.TMSSequoia)
        mnu_file_wangcompatible.Checked = m_saveOptions.PreserveWangLayers
        menuItemUnicodeMode.Checked = m_loadOptions.UnicodeMode
        menuItemToggleAllowPaint.Checked = NotateXpress1.AllowPaint

        menuItemLoadMemoryStream.Enabled = Not (_SavedAnnotationMemoryStream) Is Nothing
        menuItemLoadByteArray.Enabled = Not (_SavedAnnotationByteArray) Is Nothing

        menuItemLoadAnnotations.Checked = NotateXpress1.ImagXpressLoad
        menuItemSaveAnnotations.Checked = NotateXpress1.ImagXpressSave


    End Sub
    Private Sub menuItemImagingForWindows_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemImagingForWindows.Click
        m_saveOptions.AnnType = AnnotationType.ImagingForWindows
        m_loadOptions.AnnType = AnnotationType.ImagingForWindows

        mnu_file_saveann2fileANN.Enabled = False
        mnu_file_loadannotationANN.Enabled = False

        mnu_file_loadannotationNXP.Enabled = True
        mnu_file_saveann2fileNXP.Enabled = True


    End Sub

    Private Sub menuItemNotateAnnotations_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemNotateAnnotations.Click
        m_saveOptions.AnnType = AnnotationType.NotateXpress
        m_loadOptions.AnnType = AnnotationType.NotateXpress

        mnu_file_saveann2fileANN.Enabled = False
        mnu_file_loadannotationANN.Enabled = False

        mnu_file_loadannotationNXP.Enabled = True
        mnu_file_saveann2fileNXP.Enabled = True
    End Sub

    Private Sub menuItemViewDirectorAnnotations_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemViewDirectorAnnotations.Click
        m_saveOptions.AnnType = AnnotationType.TMSSequoia
        m_loadOptions.AnnType = AnnotationType.TMSSequoia

        mnu_file_saveann2fileANN.Enabled = True
        mnu_file_loadannotationANN.Enabled = True

        mnu_file_loadannotationNXP.Enabled = False
        mnu_file_saveann2fileNXP.Enabled = False
    End Sub

    Private Sub mnu_file_wangcompatible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_wangcompatible.Click

        'If (m_saveOptions.PreserveWangLayers) Then
        '    m_loadOptions.PreserveWangLayers = m_saveOptions.PreserveWangLayers = False
        'Else
        '    m_loadOptions.PreserveWangLayers = m_saveOptions.PreserveWangLayers = True
        'End If

        m_saveOptions.PreserveWangLayers = Not mnu_file_wangcompatible.Checked
        m_loadOptions.PreserveWangLayers = Not mnu_file_wangcompatible.Checked

        mnu_file_wangcompatible.Checked = Not mnu_file_wangcompatible.Checked

    End Sub

    Private Sub menuItemLoadAnnotations_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemLoadAnnotations.Click
        NotateXpress1.ImagXpressLoad = Not NotateXpress1.ImagXpressLoad
        menuItemLoadAnnotations.Checked = Not menuItemLoadAnnotations.Checked
    End Sub

    Private Sub mnu_load_tiff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_load_tiff.Click
        strImageFileName = PegasusOpenFile("Select an Image File", strDefaultImageFilter)
        If strImageFileName.Length <> 0 Then

            ' ---------------
            ' clear existing annotations and layers
            ' ---------------
            NotateXpress1.Layers.Clear()

            LoadFile(ImageXView1)
        End If
        Try

            createNewLayer("Layer1", System.IntPtr.Zero)
            Me.Text = "Image = " + ImageXView1.Image.ImageXData.Width.ToString() + " x " + ImageXView1.Image.ImageXData.Height.ToString()

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub menuItemSaveAnnotations_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemSaveAnnotations.Click
        NotateXpress1.ImagXpressSave = Not NotateXpress1.ImagXpressSave
        menuItemSaveAnnotations.Checked = Not menuItemSaveAnnotations.Checked

    End Sub

    Private Sub mnu_file_save_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_save.Click
        Dim strSaveFilePath As String = PegasusSaveFile("Save TIFF File", "Tiff File (*.tif) | *.tif")

        If strSaveFilePath <> "" Then

            Try
                Dim saveOptions As PegasusImaging.WinForms.ImagXpress8.SaveOptions = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()
                ImageXView1.Image.Save(strSaveFilePath, saveOptions)

                'clear out the error in case there was an error from a previous operation
                lblerror.Text = ""
            Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
                PegasusError(ex, lblerror)

            Catch ex As System.Exception
                PegasusError(ex, lblerror)

            End Try
        End If
    End Sub

    Private Sub mnu_file_saveann2fileNXP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_saveann2fileNXP.Click
        Dim strFileName As String = PegasusSaveFile("Save Annotation File", "NotateXpress File (*.nxp) | *.nxp")
        Try
            NotateXpress1.Layers.Comments = "This file was saved today"
            NotateXpress1.Layers.Subject = "testing 123"
            NotateXpress1.Layers.Title = "One awesome annotation file"
            NotateXpress1.Layers.SaveToFile(strFileName, m_saveOptions)

            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.NotateXpress8.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub mnu_file_saveann2fileANN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_saveann2fileANN.Click
        Dim strFileName As String = PegasusSaveFile("Save Annotation File", "TMS Annotation File (*.ann) | *.ann")
        Try
            NotateXpress1.Layers.Comments = "This file was saved today"
            NotateXpress1.Layers.Subject = "testing 123"
            NotateXpress1.Layers.Title = "One awesome annotation file"
            NotateXpress1.Layers.SaveToFile(strFileName, m_saveOptions)

            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.NotateXpress8.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub menuItemSaveByteArray_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemSaveByteArray.Click
        If (Not (_SavedAnnotationByteArray) Is Nothing) Then
            _SavedAnnotationByteArray = Nothing
        End If
        _SavedAnnotationByteArray = NotateXpress1.Layers.SaveToByteArray(m_saveOptions)
    End Sub

    Private Sub menuItemSaveMemoryStream_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemSaveMemoryStream.Click
        If (Not (_SavedAnnotationMemoryStream) Is Nothing) Then
            _SavedAnnotationMemoryStream = Nothing
        End If
        _SavedAnnotationMemoryStream = NotateXpress1.Layers.SaveToMemoryStream(m_saveOptions)
    End Sub

    Private Sub menuItemUnicodeMode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemUnicodeMode.Click
        m_loadOptions.UnicodeMode = Not m_loadOptions.UnicodeMode
    End Sub

    Private Sub mnu_file_loadannotationNXP_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_loadannotationNXP.Click
        Dim strFileName As String = PegasusOpenFile("Open NotateXpress File", "NotateXpress File (*.nxp) | *.nxp")
        Dim layer As Layer
        Try
            NotateXpress1.Layers.FromFile(strFileName, m_loadOptions)

            layer = NotateXpress1.Layers.Selected()
            If Not Layer Is Nothing Then
                Layer.Toolbar.Visible = True
            End If
            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.NotateXpress8.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub mnu_file_loadannotationANN_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_loadannotationANN.Click
        Dim strFileName As String = PegasusOpenFile("Open TMS Annotation File", "TMS Annotation File (*.ann) | *.ann")
        Dim layer As Layer
        Try
            NotateXpress1.Layers.FromFile(strFileName, m_loadOptions)

            layer = NotateXpress1.Layers.Selected()
            If Not layer Is Nothing Then
                layer.Toolbar.Visible = True
            End If

            'clear out the error in case there was an error from a previous operation
            lblerror.Text = ""
        Catch ex As PegasusImaging.WinForms.NotateXpress8.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try
    End Sub

    Private Sub menuItemLoadByteArray_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemLoadByteArray.Click
        If (Not (_SavedAnnotationByteArray) Is Nothing) Then
            NotateXpress1.Layers.FromByteArray(_SavedAnnotationByteArray, m_loadOptions)
        End If
    End Sub

    Private Sub menuItemLoadMemoryStream_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemLoadMemoryStream.Click
        If (Not (_SavedAnnotationMemoryStream) Is Nothing) Then
            NotateXpress1.Layers.FromMemoryStream(_SavedAnnotationMemoryStream, m_loadOptions)
        End If
    End Sub

    Private Sub mnu_fileIResY_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_fileIResY.Click
        If (NotateXpress1.FontScaling = FontScaling.ResolutionY) Then
            NotateXpress1.FontScaling = FontScaling.Normal
        Else
            NotateXpress1.FontScaling = FontScaling.ResolutionY
        End If
        mnu_fileIResY.Checked = (NotateXpress1.FontScaling = FontScaling.ResolutionY)
    End Sub

    Private Sub menuItemToggleAllowPaint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemToggleAllowPaint.Click
        NotateXpress1.AllowPaint = Not NotateXpress1.AllowPaint
        menuItemToggleAllowPaint.Checked = NotateXpress1.AllowPaint
    End Sub

    Private Sub menuItemAboutBox_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        NotateXpress1.AboutBox()
    End Sub

    Private Sub mnu_file_exit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_file_exit.Click
        System.Windows.Forms.Application.Exit()
    End Sub

    Private Sub mnu_layers_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnu_layers.Select

        Dim Layer As PegasusImaging.WinForms.NotateXpress8.Layer

        Layer = NotateXpress1.Layers.Selected
        Dim bEnabled As Boolean = (Not (Layer) Is Nothing)
        mnu_layers_deletecurrent.Enabled = bEnabled
        mnu_layers_togglehide.Enabled = bEnabled
        mnu_layers_setdescription.Enabled = bEnabled
        mnu_layers_setname.Enabled = bEnabled
        menuItemDeactivateCurrentLayer.Enabled = bEnabled
        menuItemSendToBack.Enabled = False
        menuItemBringToFront.Enabled = False
        menuItemDeleteSelected.Enabled = False
        menuItemDisableToolbar.Enabled = bEnabled
        If bEnabled Then
            mnu_layers_toggletoolbar.Checked = Layer.Toolbar.Visible
            'TODO: Warning!!!, inline IF is not supported ?
            If Layer.Visible Then
                mnu_layers_togglehide.Text = "Hide current"
            Else
                mnu_layers_togglehide.Text = "Show current"
            End If
            If Layer.Active Then
                menuItemDeactivateCurrentLayer.Text = "Deactivate current"
            Else
                menuItemDeactivateCurrentLayer.Text = "Activate current"
            End If
            If Layer.Toolbar.Enabled Then
                menuItemDisableToolbar.Text = "Disable toolbar"
            Else
                menuItemDisableToolbar.Text = "Enable toolbar"
            End If
            If (Not (Layer.Elements.FirstSelected) Is Nothing) Then
                menuItemSendToBack.Enabled = True
                menuItemBringToFront.Enabled = True
                menuItemDeleteSelected.Enabled = True
            End If
        Else
            mnu_layers_toggletoolbar.Enabled = False
        End If
        If (NotateXpress1.Layers.Count < 1) Then
            mnu_layers_setcurrent.Enabled = False
        Else
            mnu_layers_setcurrent.Enabled = True
            ' ---------------
            '  clean set current layer sub-menu
            ' ---------------
            mnu_layers_setcurrent.MenuItems.Clear()
            Dim layers_setcurrent_eventhandler As System.EventHandler = AddressOf Me.mnu_layers_setcurrent_Click
            ' ---------------
            '  add the description for each layer under this submenu
            ' ---------------
            Dim i As Integer = 0
            Do While (i < NotateXpress1.Layers.Count)
                Dim str As String
                str = NotateXpress1.Layers(i).Name _
                    + "   [" _
                    + NotateXpress1.Layers(i).Visible.ToString _
                    + IIf(NotateXpress1.Layers(i).Active, "Active ", "NOTActive ") _
                    + NotateXpress1.Layers(i).Elements.Count.ToString _
                    + "-Elements]"

                ' creates new MenuItem
                mnu_layers_setcurrent.MenuItems.Add(str)

                ' check if selected layer
                If (NotateXpress1.Layers.Selected Is NotateXpress1.Layers(i)) Then
                    mnu_layers_setcurrent.MenuItems(i).Checked = True
                Else

                    mnu_layers_setcurrent.MenuItems(i).Checked = False
                End If
                ' set event handler for new MenuItem
                AddHandler mnu_layers_setcurrent.MenuItems(i).Click, layers_setcurrent_eventhandler

                i = (i + 1)
            Loop
        End If
    End Sub

    Private Sub mnu_layers_create_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_create.Click
        Dim dlg As InputDlg = New InputDlg("Specify Name of Layer to Create")
        If System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me) Then
            ' Create a new layer, note that it will also now be the selected layer
            createNewLayer(dlg.InputText, System.IntPtr.Zero)
        End If
    End Sub

    Private Sub mnu_layers_setcurrent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_setcurrent.Click

        If (TypeOf sender Is System.Windows.Forms.MenuItem) Then
            NotateXpress1.Layers(CType(sender, System.Windows.Forms.MenuItem).Index).Select()
        End If


    End Sub

    Private Sub mnu_layers_deletecurrent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_deletecurrent.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            NotateXpress1.Layers.Remove(layer)
        End If
    End Sub

    Private Sub mnu_layers_togglehide_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_togglehide.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            layer.Visible = Not layer.Visible
        End If
    End Sub

    Private Sub menuItemDeactivateCurrentLayer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemDeactivateCurrentLayer.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            layer.Active = Not layer.Active
        End If
    End Sub

    Private Sub mnu_layers_setname_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_setname.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim dlg As InputDlg = New InputDlg("Set Layer Name")
            dlg.InputText = layer.Name
            If System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me) Then
                layer.Name = dlg.InputText
            End If
        End If
    End Sub

    Private Sub mnu_layers_setdescription_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_setdescription.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim dlg As InputDlg = New InputDlg("Set Layer Description")
            dlg.InputText = layer.Description
            If System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me) Then
                layer.Description = dlg.InputText
            End If
        End If
    End Sub

    Private Sub mnu_objects_reverse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_reverse.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            If layer.Elements.Count > 0 Then
                layer.Elements.Reverse()
            End If
        End If
    End Sub

    Private Sub menuItemSendToBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemSendToBack.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            layer.Elements.SendToBack()
            ImageXView1.Refresh()
        End If
    End Sub

    Private Sub menuItemBringToFront_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemBringToFront.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            layer.Elements.BringToFront()
            ImageXView1.Refresh()
        End If
    End Sub

    Private Sub menuItemDeleteSelected_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemDeleteSelected.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            layer.Elements.DeleteSelected()
        End If
    End Sub

    Private Sub menuItemSaveCurrentLayer_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemSaveCurrentLayer.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        Dim strFileName As String
        If Not layer Is Nothing Then


            strFileName = PegasusSaveFile("Save Current Annotation Layer to File", "NotateXpress File (*.nxp) | *.nxp")

            Try
                NotateXpress1.Layers.Comments = "This file contains 1 saved layer"
                NotateXpress1.Layers.Subject = "Single layer annotation file"
                NotateXpress1.Layers.Title = "One awesome single layer annotation file"
                NotateXpress1.Layers.SaveToFile(strFileName, m_saveOptions)
                m_saveOptions.AllLayers = True
            Catch ex As PegasusImaging.WinForms.NotateXpress8.NotateXpressException
                PegasusError(ex, lblerror)

            Catch ex As System.Exception
                PegasusError(ex, lblerror)

            End Try
        End If
    End Sub

    Private Sub mnu_layers_toggletoolbar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_layers_toggletoolbar.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            If layer.Toolbar.Visible Then
                layer.Toolbar.Visible = False
                menuItemToolBar.Enabled = False
            Else
                layer.Toolbar.Visible = True
                menuItemToolBar.Enabled = True
            End If
        End If
    End Sub

    Private Sub menuItemDisableToolbar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemDisableToolbar.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            If layer.Toolbar.Enabled Then
                layer.Toolbar.Enabled = False
            Else
                layer.Toolbar.Enabled = True
            End If
        End If
    End Sub

    Private Sub mnu_objects_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnu_objects.Select

        ' ------------
        ' get the selected/current layer
        ' ------------
        Dim layer As Layer = NotateXpress1.Layers.Selected()


        If layer Is Nothing Then
            ' ------------
            '  disable all menu selections on this menu
            ' ------------
            Dim item As MenuItem
            For Each item In mnu_objects.MenuItems
                item.Enabled = False
            Next
        Else
            Dim nCount As Integer = layer.Elements.Count
            mnu_objects_reverse.Enabled = nCount > 1
            mnu_objects_brand.Enabled = nCount > 0
            mnu_objects_nudgeup.Enabled = layer.Elements.IsSelection
            mnu_objects_nudgedn.Enabled = layer.Elements.IsSelection
            mnu_objects_createtransrect.Enabled = True

            ' determine whether to enable inflate
            mnu_objects_inflate.Enabled = False

            Dim element As Element = layer.Elements.FirstSelected()



            While Not element Is Nothing
                ' enable inflate if ALL the seleted elements are either an EllipseTool, RectangleTool, TextTool, or ButtonTool
                If TypeOf element Is EllipseTool Or TypeOf element Is RectangleTool Or TypeOf element Is TextTool Or TypeOf element Is ButtonTool Then
                    mnu_objects_inflate.Enabled = True
                Else
                    mnu_objects_inflate.Enabled = False
                    Exit While
                End If
                element = layer.Elements.NextSelected()
            End While
        End If

    End Sub

    Private Sub mnu_objects_autoselect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_autoselect.Click
        _bAutoSelect = Not _bAutoSelect
        mnu_objects_autoselect.Checked = _bAutoSelect
    End Sub

    Private Sub mnu_objects_create10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_create10.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim top As Integer = 15, left As Integer = 15, right As Integer = 115, bottom As Integer = 115

            Dim i As Integer
            For i = 1 To 10 Step i + 1
                Dim rand As System.Random = New System.Random()
                Dim rect As System.Drawing.Rectangle = New Rectangle(top, left, right, bottom)
                If Decimal.Remainder(i, 2) > 0 Then
                    ' create a rectangle object
                    Dim element As RectangleTool = New RectangleTool()
                    element.PenWidth = Decimal.Remainder(i, 3) + 1
                    element.PenColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                    element.FillColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                    element.BoundingRectangle = rect
                    layer.Elements.Add(element)
                Else
                    ' create an ellipse object
                    Dim element As EllipseTool = New EllipseTool()
                    element.PenWidth = Decimal.Remainder(i, 3) + 1
                    element.PenColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                    element.FillColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                    element.BoundingRectangle = rect
                    layer.Elements.Add(element)
                End If

                top = top + 2
                left = left + 2
                right = right + 2
                bottom = bottom + 2
            Next
        End If
    End Sub

    Private Sub mnu_objects_rects_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_rects.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim top As Integer = 20, left As Integer = 20, right As Integer = 120, bottom As Integer = 120
            Dim i As Integer
            For i = 1 To 10 Step i + 1
                ' create a rectangle object
                Dim element As RectangleTool = New RectangleTool()
                Dim rect As System.Drawing.Rectangle = New Rectangle(top, left, right, bottom)
                element.BoundingRectangle = rect
                element.PenWidth = 1
                Dim rand As System.Random = New System.Random()
                element.PenColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
                element.FillColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))

                layer.Elements.Add(element)
                top += 2
                left += 2
                right += 2
                bottom += 2
            Next
        End If
    End Sub

    Private Sub mnu_objects_buttons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_buttons.Click
        '---------------
        ' initialize object location
        '---------------
        Dim top As Integer = 10
        Dim left As Integer = 10
        Dim right As Integer = 100
        Dim bottom As Integer = 35

        '--------------
        ' now using this one programmable element,
        ' create 10 buttons
        '--------------
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim i As Integer
            For i = 1 To 10 Step i + 1
                '---------------
                ' create a programmable element
                '---------------
                Dim element As ButtonTool = New ButtonTool()

                '---------------
                ' set some programmable element properties
                '---------------
                Dim rect As System.Drawing.Rectangle = New Rectangle(top, left, right, bottom)
                element.BoundingRectangle = rect
                element.PenWidth = 4
                element.Text = "Button " + i.ToString()

                element.TextColor = SystemColors.ControlText
                element.BackColor = SystemColors.Control
                element.BevelLightColor = SystemColors.ControlLight
                element.BevelShadowColor = SystemColors.ControlDark

                Dim rand As System.Random = New System.Random()
                Dim x As Integer = New System.Random().NextDouble() * 400
                Dim y As Integer = New System.Random().NextDouble() * 400

                layer.Elements.Add(element)
                top += 2
                left += 2
                right += 2
                bottom += 2
            Next
        End If
    End Sub

    Private Sub mnu_objects_createtransrect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_createtransrect.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As RectangleTool = New RectangleTool()
            Dim rect As System.Drawing.Rectangle = New Rectangle(100, 100, 200, 200)
            element.BoundingRectangle = rect
            element.FillColor = System.Drawing.Color.Yellow
            element.PenWidth = 1
            element.PenColor = System.Drawing.Color.Blue ' 				element.HighlightFill = True  ****** Outline is "permanent" without Me line */
            layer.Elements.Add(element)
            layer.Toolbar.Selected = AnnotationTool.PointerTool
        End If
    End Sub

    Private Sub mnu_objects_createImage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_createImage.Click
        Try
            Dim layer As Layer = NotateXpress1.Layers.Selected()
            If Not layer Is Nothing Then
                ImageXView2.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "..\..\..\..\..\..\..\Common\Images\vermont.jpg"))

                Dim element As ImageTool = New ImageTool()
                'element.DibHandle = CType(ImageXView2.Image.ToHdib(False), Integer) 'TODO remove cast, DibHandle should take an IntPtr, DevTrack #159

                element.DibHandle = ImageXView2.Image.ToHdib(False)

                Dim rect As System.Drawing.Rectangle = New Rectangle(100, 100, 100 + ImageXView2.Image.ImageXData.Width, 100 + ImageXView2.Image.ImageXData.Height)
                element.BoundingRectangle = rect
                layer.Elements.Add(element)
            End If
        Catch ex As Exception
            MessageBox.Show("Error msg:   " + ex.Message + _
             "Error type:  " + ex.GetType().ToString())
        End Try
    End Sub

    Private Sub mnu_objects_nudgeup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_nudgeup.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim i As Integer
            For i = 0 To layer.Elements.Count - 1 Step i + 1
                If layer.Elements(i).Selected Then
                    'TODO - doesnt seem to do anything, DevTrack #161
                    layer.Elements(i).NudgeUp()

                End If
            Next
        End If
    End Sub

    Private Sub mnu_objects_nudgedn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_nudgedn.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                'TODO - doesnt seem to do anything, DevTrack #161
                element.NudgeDown()
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_objects_inflate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_inflate.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                'TODO - DevTrack 157
                Dim rect As System.Drawing.Rectangle = element.BoundingRectangle
                Dim i As Integer
                For i = 1 To 100 Step i + 1
                    element.BoundingRectangle = New Rectangle(Math.Max(rect.Left - i, 0), Math.Max(rect.Top - i, 0), rect.Width + i, rect.Height + i)
                    Application.DoEvents()
                Next
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_objects_redaction_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_redaction.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As TextTool = New TextTool()
            Dim rect As System.Drawing.Rectangle = New Rectangle(100, 100, 300, 200)
            element.BoundingRectangle = rect
            element.TextJustification = TextJustification.Center
            element.TextColor = System.Drawing.Color.Red
            element.BackStyle = BackStyle.Opaque
            element.Text = "Approved by Pegasus Imaging Corp." + System.DateTime.Now
            element.PenWidth = 1
            element.PenColor = System.Drawing.Color.White
            element.BackColor = System.Drawing.Color.White
            layer.Elements.Add(element)
            layer.Toolbar.Selected = AnnotationTool.PointerTool
        End If
    End Sub

    Private Sub mnu_objects_AddRulerAnn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_AddRulerAnn.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            'set the annotation type to the Ruler annotation
            Dim element As RulerTool = New RulerTool()
            Dim rect As System.Drawing.Rectangle = New Rectangle(25, 50, 200, 100)
            element.BoundingRectangle = rect
            element.PenWidth = 4
            element.GaugeLength = 20
            element.MeasurementUnit = MeasurementUnit.Inches
            element.Precision = 1

            layer.Elements.Add(element)
            element.Selected = True
        End If
    End Sub

    Private Sub mnu_objects_largetext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_largetext.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As TextTool = New TextTool()
            Dim rect As System.Drawing.Rectangle = New Rectangle(30, 30, 280, 300)
            element.BoundingRectangle = rect
            element.TextJustification = TextJustification.Center
            element.BackColor = System.Drawing.Color.FromArgb(255, 255, 0)
            element.TextColor = System.Drawing.Color.FromArgb(0, 0, 0)
            element.BackStyle = BackStyle.Translucent
            element.PenWidth = 2
            element.PenColor = System.Drawing.Color.FromArgb(255, 0, 0)

            Dim strText As String = "NotateXpress Version 8\n\n"
            strText += "NotateXpress version 8's programmatic annotation features let you enter, edit, and query all types of annotation objects programmatically. "
            strText += "Using NotateXpress's programmatic capabilities is fun, fascinating, and has a New Ruler Tool! Try it today."

            element.Text = strText
            Dim fontNew As System.Drawing.Font = New System.Drawing.Font("Times New Roman", 12)
            element.TextFont = fontNew
            layer.Elements.Add(element)
        End If
    End Sub

    Private Sub mnu_objects_useMLE_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_useMLE.Click
        NotateXpress1.MultiLineEdit = Not NotateXpress1.MultiLineEdit
        mnu_objects_useMLE.Checked = NotateXpress1.MultiLineEdit
    End Sub

    Private Sub mnu_objects_brand_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_brand.Click
        NotateXpress1.Layers.Brand(24)
    End Sub

    Private Sub menuItemCreateNewNX8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemCreateNewNX8.Click
        NotateXpress1.Layers.Clear()
        ' 'detach' existing NX8 from the IX window
        NotateXpress1.ClientWindow = IntPtr.Zero
        ' create new NX8 component
        NotateXpress1 = New PegasusImaging.WinForms.NotateXpress8.NotateXpress()
        ' init default values for new NX8 component
        NotateXpress1.ClientWindow = ImageXView1.Handle
        NotateXpress1.AllowPaint = True
        NotateXpress1.Debug = False
        NotateXpress1.DebugLogFile = "c:\\NotateXpress8.log"
        NotateXpress1.ErrorLevel = PegasusImaging.WinForms.NotateXpress8.ErrorLevelInfo.Production
        NotateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress8.FontScaling.Normal
        NotateXpress1.ImagXpressLoad = True
        NotateXpress1.ImagXpressSave = True
        NotateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress8.AnnotationMode.Edit
        NotateXpress1.MultiLineEdit = False
        ' create a layer for the new NX8 component
        createNewLayer("Layer1", ImageXView2.Image.ToHdib(False))
    End Sub

    Private Sub mnu_orient_ltbr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_orient_ltbr.Click
        Processor1.Image = ImageXView1.Image
        Processor1.Rotate(270)
    End Sub

    Private Sub mnu_orient_brtl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_orient_brtl.Click
        Processor1.Image = ImageXView1.Image
        Processor1.Flip()
    End Sub

    Private Sub mnu_orient_rblt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_orient_rblt.Click
        Processor1.Image = ImageXView1.Image
        Processor1.Rotate(90)
    End Sub

    Private Sub mnu_orient_tlbr_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_orient_tlbr.Click
        Processor1.Image = ImageXView1.Image
        Processor1.Mirror()
    End Sub

    Private Sub mnu_orientation_rotate180_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_orientation_rotate180.Click
        Processor1.Image = ImageXView1.Image
        Processor1.Rotate(180)
    End Sub

    Private Sub mnu_stuff_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnu_stuff.Select

        '  ------------
        '   disable all menu selections on this menu
        '  ------------
        menuItemBoundingRectangle.Enabled = False
        mnu_stuff_backcolor.Enabled = False
        mnu_stuff_bevellight.Enabled = False
        mnu_stuff_bevelshadow.Enabled = False
        mnu_stuff_getitemfont.Enabled = False
        mnu_stuff_getitemtext.Enabled = False
        mnu_stuff_hb.Enabled = False
        mnu_stuff_hf.Enabled = False
        mnu_stuff_moveable.Enabled = False
        mnu_stuff_fixed.Enabled = False
        mnu_stuff_pencolor.Enabled = False
        mnu_stuff_penwidth.Enabled = False
        mnu_stuff_setfont.Enabled = False
        mnu_stuff_setnewdib.Enabled = False
        mnu_stuff_settext.Enabled = False
        mnu_stuff_sizeable.Enabled = False
        mnu_stuff_toggleuserdraw.Enabled = False
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            '-----------------
            ' Enable menu items as appropriate for this menu
            '-----------------
            Dim element As Element = layer.Elements.FirstSelected
            ' only one element must be selected to enable
            If ((Not (element) Is Nothing) _
                        AndAlso (layer.Elements.NextSelected Is Nothing)) Then
                menuItemBoundingRectangle.Enabled = True
                mnu_stuff_settext.Enabled = HasProperty(element, "Text")
                mnu_stuff_backcolor.Enabled = HasProperty(element, "BackColor")
                mnu_stuff_pencolor.Enabled = HasProperty(element, "PenColor")
                mnu_stuff_penwidth.Enabled = HasProperty(element, "PenWidth")
                mnu_stuff_bevelshadow.Enabled = HasProperty(element, "BevelShadowColor")
                mnu_stuff_bevellight.Enabled = HasProperty(element, "BevelLightColor")
                mnu_stuff_setfont.Enabled = HasProperty(element, "TextFont")
                mnu_stuff_getitemtext.Enabled = HasProperty(element, "Text")
                mnu_stuff_hb.Enabled = HasProperty(element, "HighlightBack")
                mnu_stuff_hf.Enabled = HasProperty(element, "HighlightFill")
                mnu_stuff_moveable.Enabled = HasProperty(element, "Moveable")
                mnu_stuff_fixed.Enabled = HasProperty(element, "Fixed")
                mnu_stuff_sizeable.Enabled = HasProperty(element, "Sizeable")
                mnu_stuff_getitemfont.Enabled = HasProperty(element, "TextFont")
                mnu_stuff_setnewdib.Enabled = HasProperty(element, "DibHandle")
                mnu_stuff_toggleuserdraw.Enabled = HasProperty(element, "UserDraw")
                If mnu_stuff_toggleuserdraw.Enabled Then
                    mnu_stuff_toggleuserdraw.Checked = CType(GetPropertyValue(element, "UserDraw"), Boolean)
                End If
            End If
        End If

    End Sub

    Private Sub menuItemBoundingRectangle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemBoundingRectangle.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            '-----------------
            ' Enable menu items as appropriate for this menu
            '-----------------
            Dim element As Element = layer.Elements.FirstSelected()
            ' only one element must be selected to enable
            If Not element Is Nothing And layer.Elements.NextSelected() Is Nothing Then
                Dim dlg As BoundingRectDlg = New BoundingRectDlg(element.BoundingRectangle)
                If System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me) Then
                    element.BoundingRectangle = dlg.Rect
                End If
            End If
        End If
    End Sub

    Private Sub mnu_stuff_settext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_settext.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim NewText As String = "This is a test and only a test. Had this been something other than a test, something significant would have happened!"
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                SetPropertyValue(element, "Text", NewText)
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_backcolor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_backcolor.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                SetPropertyValue(element, "BackColor", System.Drawing.Color.FromArgb(0, 255, 0))
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_pencolor_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_pencolor.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                SetPropertyValue(element, "PenColor", System.Drawing.Color.FromArgb(0, 255, 0))
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_penwidth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_penwidth.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim NewWidth As System.Int32 = 15
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                SetPropertyValue(element, "PenWidth", NewWidth)
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_bevelshadow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_bevelshadow.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                SetPropertyValue(element, "BevelShadowColor", System.Drawing.Color.FromArgb(0, 255, 0))
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_bevellight_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_bevellight.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                SetPropertyValue(element, "BevelLightColor", System.Drawing.Color.FromArgb(0, 255, 255))
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_setfont_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_setfont.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim font As System.Drawing.Font = Nothing
            ' only perform on first selected items
            Dim element As Element = layer.Elements.FirstSelected()
            If Not element Is Nothing Then
                font = CType(GetPropertyValue(element, "TextFont"), Font)
                If Not font Is Nothing Then
                    FontDialog1.Font = font
                End If

                If FontDialog1.ShowDialog(Me) <> DialogResult.Cancel Then
                    font = FontDialog1.Font
                Else
                    Return ' font dialog canceled
                End If
            End If
            While Not element Is Nothing
                SetPropertyValue(element, "TextFont", font)
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_getitemtext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_getitemtext.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            ' only perform on first selected element
            Dim element As Object = layer.Elements.FirstSelected()
            If Not element Is Nothing Then
                Dim obj As Object = GetPropertyValue(element, "Text")
                If Not obj Is Nothing Then
                    System.Windows.Forms.MessageBox.Show(CType(obj, String))
                End If
            End If
        End If
    End Sub

    Private Sub mnu_stuff_hb_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_hb.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                Dim obj As Object = GetPropertyValue(Me, "HighlightBack")
                If Not obj Is Nothing Then
                    Dim b As Boolean = CType(obj, Boolean)
                    b = Not b
                    SetPropertyValue(Me, "HighlightBack", b)
                End If
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_hf_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_hf.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                Dim obj As Object = GetPropertyValue(element, "HighlightFill")
                If Not obj Is Nothing Then
                    Dim b As Boolean = CType(obj, Boolean)
                    b = Not b
                    SetPropertyValue(element, "HighlightFill", b)
                End If
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_moveable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_moveable.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                Dim obj As Object = GetPropertyValue(element, "Moveable")
                If Not obj Is Nothing Then
                    Dim b As Boolean = CType(obj, Boolean)
                    b = Not b
                    SetPropertyValue(element, "Moveable", b)
                End If
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_fixed_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_fixed.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                Dim obj As Object = GetPropertyValue(element, "Fixed")
                If Not obj Is Nothing Then
                    Dim b As Boolean = CType(obj, Boolean)
                    b = Not b
                    SetPropertyValue(element, "Fixed", b)
                End If
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_sizeable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_sizeable.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Dim element As Element = layer.Elements.FirstSelected()
            While Not element Is Nothing
                Dim obj As Object = GetPropertyValue(element, "Sizeable")
                If Not obj Is Nothing Then
                    Dim b As Boolean = CType(obj, Boolean)
                    b = Not b
                    SetPropertyValue(element, "Sizeable", b)
                End If
                element = layer.Elements.NextSelected()
            End While
        End If
    End Sub

    Private Sub mnu_stuff_getitemfont_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_getitemfont.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            ' only perform on first selected element
            Dim element As Object = layer.Elements.FirstSelected()
            If Not element Is Nothing Then
                Dim obj As Object = GetPropertyValue(element, "TextFont")
                If Not obj Is Nothing Then
                    Dim font As System.Drawing.Font = CType(obj, System.Drawing.Font)
                    MessageBox.Show(font.Name + " " + font.SizeInPoints.ToString())
                End If
            End If
        End If
    End Sub

    Private Sub mnu_stuff_setnewdib_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_setnewdib.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            Try
                Dim strCurrentDir As String = System.IO.Directory.GetCurrentDirectory()
                strImageFileName = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\..\Common\Images\Water.jpg")
                LoadFile(ImageXView2)
                If Not ImageXView2.Image Is Nothing Then
                    If layer.Toolbar.ImageToolbarDefaults.DibHandle.ToInt32 = 0 Then


                        GlobalFree(CType(layer.Toolbar.ImageToolbarDefaults.DibHandle, System.IntPtr))
                        layer.Toolbar.ImageToolbarDefaults.DibHandle = System.IntPtr.Zero
                    End If

                    layer.Toolbar.ImageToolbarDefaults.DibHandle = ImageXView2.Image.ToHdib(False)
                End If
            Catch ex As Exception
                PegasusError(ex, lblerror)
            End Try
        End If
    End Sub

    Private Sub mnu_stuff_toggleuserdraw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff_toggleuserdraw.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            Dim element As Element = layer.Elements.FirstSelected
            ' only one element must be selected
            If (((Not (element) Is Nothing) And TypeOf element Is RectangleTool _
                        And (layer.Elements.NextSelected Is Nothing))) Then
                Dim rectTool As RectangleTool = CType(element, RectangleTool)
                rectTool.UserDraw = Not rectTool.UserDraw
            End If
        End If


    End Sub

    Private Function HasProperty(ByVal obj As Object, ByVal strPropName As String) As Boolean
        Dim piProperty As System.Reflection.PropertyInfo
        For Each piProperty In obj.GetType().GetProperties()
            If piProperty.Name = strPropName Then
                Return True
            End If
        Next

        Return False
    End Function


    Private Function GetPropertyValue(ByVal obj As Object, ByVal strPropName As String) As Object
        Dim piProperty As System.Reflection.PropertyInfo
        For Each piProperty In obj.GetType().GetProperties()
            If piProperty.Name = strPropName Then
                Return piProperty.GetValue(obj, Nothing)
            End If
        Next

        Return Nothing
    End Function


    Private Function SetPropertyValue(ByVal obj As Object, ByVal strPropName As String, ByVal objValue As Object) As Boolean
        Dim piProperty As System.Reflection.PropertyInfo


        For Each piProperty In obj.GetType().GetProperties()
            If (piProperty.Name = strPropName) Then
                piProperty.SetValue(obj, objValue, Nothing)
                Return True
            End If
        Next
        Return False
    End Function

    Private Sub mnu_stuff_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_stuff.Click
        '  ------------
        '   disable all menu selections on this menu
        '  ------------
        menuItemBoundingRectangle.Enabled = False
        mnu_stuff_backcolor.Enabled = False
        mnu_stuff_bevellight.Enabled = False
        mnu_stuff_bevelshadow.Enabled = False
        mnu_stuff_getitemfont.Enabled = False
        mnu_stuff_getitemtext.Enabled = False
        mnu_stuff_hb.Enabled = False
        mnu_stuff_hf.Enabled = False
        mnu_stuff_moveable.Enabled = False
        mnu_stuff_fixed.Enabled = False
        mnu_stuff_pencolor.Enabled = False
        mnu_stuff_penwidth.Enabled = False
        mnu_stuff_setfont.Enabled = False
        mnu_stuff_setnewdib.Enabled = False
        mnu_stuff_settext.Enabled = False
        mnu_stuff_sizeable.Enabled = False

        Dim layer As Layer = NotateXpress1.Layers.Selected()
        If Not layer Is Nothing Then
            '-----------------
            ' Enable menu items as appropriate for this menu
            '-----------------
            Dim element As Element = layer.Elements.FirstSelected()
            ' only one element must be selected to enable
            If Not element Is Nothing And layer.Elements.NextSelected() Is Nothing Then
                menuItemBoundingRectangle.Enabled = True
                mnu_stuff_settext.Enabled = HasProperty(element, "Text")
                mnu_stuff_backcolor.Enabled = HasProperty(element, "BackColor")
                mnu_stuff_pencolor.Enabled = HasProperty(element, "PenColor")
                mnu_stuff_penwidth.Enabled = HasProperty(element, "PenWidth")
                mnu_stuff_bevelshadow.Enabled = HasProperty(element, "BevelShadowColor")
                mnu_stuff_bevellight.Enabled = HasProperty(element, "BevelLightColor")
                mnu_stuff_setfont.Enabled = HasProperty(element, "TextFont")
                mnu_stuff_getitemtext.Enabled = HasProperty(element, "Text")
                mnu_stuff_hb.Enabled = HasProperty(element, "HighlightBack")
                mnu_stuff_hf.Enabled = HasProperty(element, "HighlightFill")
                mnu_stuff_moveable.Enabled = HasProperty(element, "Moveable")
                mnu_stuff_fixed.Enabled = HasProperty(element, "Fixed")
                mnu_stuff_sizeable.Enabled = HasProperty(element, "Sizeable")
                mnu_stuff_getitemfont.Enabled = HasProperty(element, "TextFont")
                mnu_stuff_setnewdib.Enabled = HasProperty(element, "DibHandle")
            End If
        End If

    End Sub


    Private Sub mnu_mode_editmode_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_mode_editmode.Click
        NotateXpress1.InteractMode = AnnotationMode.Edit
        mnu_mode_interactive.Checked = False
        mnu_mode_editmode.Checked = True

    End Sub

    Private Sub mnu_mode_interactive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_mode_interactive.Click
        NotateXpress1.InteractMode = AnnotationMode.Interactive
        mnu_mode_interactive.Checked = True
        mnu_mode_editmode.Checked = False

    End Sub

    Private Sub mnu_cm_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnu_cm.Select
        If NotateXpress1.MenuGetEnabled(MenuType.Context, AnnotationTool.NoTool) Then
            mnu_cm_enable.Text = "Disable"
        Else
            mnu_cm_enable.Text = "Enable"
        End If


    End Sub

    Private Sub menuItemGroups_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles menuItemGroups.Select
        mnu_objects_allowgrouping.Enabled = False
        menuItemCreateGroup.Enabled = False
        menuItemDeleteGroup.Enabled = False
        menuItemSetGroupEmpty.Enabled = False
        menuItemSelectGroupItems.Enabled = False
        menuItemGroupAddSelectedItems.Enabled = False
        menuItemGroupRemoveSelectedItems.Enabled = False
        menuItemYokeGroupItems.Enabled = False
        menuItemSetGroupUserString.Enabled = False
        menuItemMirrorYoked.Enabled = False
        menuItemInvertYoked.Enabled = False
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            mnu_objects_allowgrouping.Enabled = True
            mnu_objects_allowgrouping.Checked = layer.Groups.AllowUserGrouping
            If Not mnu_objects_allowgrouping.Checked Then
                Return
            End If
            menuItemCreateGroup.Enabled = True
            If (layer.Groups.Count > 0) Then
                ' ---------------
                '  Initialize delete group sub-menu
                ' ---------------
                menuItemDeleteGroup.Enabled = True
                '  clean groups sub-menu      
                menuItemDeleteGroup.MenuItems.Clear()

                Dim menuItemDeleteGroup_eventhandler As System.EventHandler = AddressOf Me.menuItemDeleteGroup_Click
                Dim i As Integer = 0
                Do While (i < layer.Groups.Count)
                    ' creates new MenuItem
                    menuItemDeleteGroup.MenuItems.Add(layer.Groups(i).Name)

                    ' set event handler for new MenuItem                
                    AddHandler menuItemDeleteGroup.MenuItems(i).Click, menuItemDeleteGroup_eventhandler ' (menuItemDeleteGroup.MenuItems(i).Click + menuItemDeleteGroup_eventhandler)

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize set group empty sub-menu
                ' ---------------
                menuItemSetGroupEmpty.Enabled = True
                '  clean groups sub-menu      
                menuItemSetGroupEmpty.MenuItems.Clear()

                Dim menuItemSetGroupEmpty_eventhandler As System.EventHandler = AddressOf Me.menuItemSetGroupEmpty_Click
                i = 0
                Do While (i < layer.Groups.Count)
                    Dim str As String = (layer.Groups(i).Name + ("  #Items: " + layer.Groups(i).GroupElements.Count.ToString))
                    menuItemSetGroupEmpty.MenuItems.Add(str)
                    ' creates new MenuItem
                    menuItemSetGroupEmpty.MenuItems(i).Enabled = (layer.Groups(i).GroupElements.Count > 0)

                    ' set event handler for new MenuItem
                    AddHandler menuItemSetGroupEmpty.MenuItems(i).Click, menuItemSetGroupEmpty_eventhandler
                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize select group items sub-menu
                ' ---------------
                menuItemSelectGroupItems.Enabled = True
                '  clean groups sub-menu   
                menuItemSelectGroupItems.MenuItems.Clear()

                Dim menuItemSelectGroupItems_eventhandler As System.EventHandler = AddressOf Me.menuItemSelectGroupItems_Click
                i = 0
                Do While (i < layer.Groups.Count)

                    ' creates new MenuItem
                    menuItemSelectGroupItems.MenuItems.Add(layer.Groups(i).Name)

                    menuItemSelectGroupItems.MenuItems(i).Enabled = (layer.Groups(i).GroupElements.Count > 0)

                    ' set event handler for new MenuItem     
                    AddHandler menuItemSelectGroupItems.MenuItems(i).Click, menuItemSelectGroupItems_eventhandler

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize add selected items to group
                ' ---------------
                menuItemGroupAddSelectedItems.Enabled = (Not (layer.Elements.FirstSelected) Is Nothing)
                '  clean groups sub-menu      
                menuItemGroupAddSelectedItems.MenuItems.Clear()

                Dim menuItemGroupAddSelectedItems_eventhandler As System.EventHandler = AddressOf Me.menuItemGroupAddSelectedItems_Click
                i = 0
                Do While (i < layer.Groups.Count)

                    ' creates new MenuItem
                    menuItemGroupAddSelectedItems.MenuItems.Add(layer.Groups(i).Name)

                    ' set event handler for new MenuItem                
                    AddHandler menuItemGroupAddSelectedItems.MenuItems(i).Click, menuItemGroupAddSelectedItems_eventhandler

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize remove selected items from group
                ' ---------------
                menuItemGroupRemoveSelectedItems.Enabled = (Not (layer.Elements.FirstSelected) Is Nothing)
                '  clean groups sub-menu      
                menuItemGroupRemoveSelectedItems.MenuItems.Clear()

                Dim menuItemGroupRemoveSelectedItems_eventhandler As System.EventHandler = AddressOf Me.menuItemGroupRemoveSelectedItems_Click
                i = 0
                Do While (i < layer.Groups.Count)
                    menuItemGroupRemoveSelectedItems.MenuItems.Add(layer.Groups(i).Name)
                    ' creates new MenuItem
                    menuItemGroupRemoveSelectedItems.MenuItems(i).Enabled = (layer.Groups(i).GroupElements.Count > 0)

                    ' set event handler for new MenuItem                
                    AddHandler menuItemGroupRemoveSelectedItems.MenuItems(i).Click, menuItemGroupRemoveSelectedItems_eventhandler

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize yoke group items sub-menu
                ' ---------------
                menuItemYokeGroupItems.Enabled = True
                menuItemYokeGroupItems.MenuItems.Clear()
                '  clean groups sub-menu      
                Dim menuItemYokeGroupItems_eventhandler As System.EventHandler = AddressOf Me.menuItemYokeGroupItems_Click
                i = 0
                Do While (i < layer.Groups.Count)

                    ' creates new MenuItem
                    menuItemYokeGroupItems.MenuItems.Add(layer.Groups(i).Name)

                    menuItemYokeGroupItems.MenuItems(i).Checked = layer.Groups(i).Yoked

                    ' set event handler for new MenuItem                
                    AddHandler menuItemYokeGroupItems.MenuItems(i).Click, menuItemYokeGroupItems_eventhandler

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize invert group items Yoked sub-menu
                ' ---------------
                menuItemInvertYoked.Enabled = True
                '  clean groups sub-menu   
                menuItemInvertYoked.MenuItems.Clear()

                Dim menuItemInvertYoked_eventhandler As System.EventHandler = AddressOf Me.menuItemInvertYoked_Click
                i = 0
                Do While (i < layer.Groups.Count)

                    ' creates new MenuItem
                    menuItemInvertYoked.MenuItems.Add(layer.Groups(i).Name)

                    menuItemInvertYoked.MenuItems(i).Enabled = layer.Groups(i).Yoked

                    ' set event handler for new MenuItem                
                    AddHandler menuItemInvertYoked.MenuItems(i).Click, menuItemInvertYoked_eventhandler

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize Mirror group items Yoked sub-menu
                ' ---------------
                menuItemMirrorYoked.Enabled = True

                '  clean groups sub-menu  
                menuItemMirrorYoked.MenuItems.Clear()

                Dim menuItemMirrorYoked_eventhandler As System.EventHandler = AddressOf Me.menuItemMirrorYoked_Click
                i = 0
                Do While (i < layer.Groups.Count)
                    menuItemMirrorYoked.MenuItems.Add(layer.Groups(i).Name)
                    ' creates new MenuItem
                    menuItemMirrorYoked.MenuItems(i).Enabled = layer.Groups(i).Yoked

                    ' set event handler for new MenuItem                
                    AddHandler menuItemMirrorYoked.MenuItems(i).Click, menuItemMirrorYoked_eventhandler

                    i = (i + 1)
                Loop
                ' ---------------
                '  Initialize group User String sub-menu
                ' ---------------
                menuItemSetGroupUserString.Enabled = True

                ' clean groups sub-menu
                menuItemSetGroupUserString.MenuItems.Clear()

                Dim SetGroupUserString_eventhandler As System.EventHandler = AddressOf Me.menuItemSetGroupUserString_select
                i = 0
                Do While (i < layer.Groups.Count)

                    ' creates new MenuItem
                    menuItemSetGroupUserString.MenuItems.Add(layer.Groups(i).Name)

                    ' set event handler for new MenuItem                
                    AddHandler menuItemSetGroupUserString.MenuItems(i).Click, SetGroupUserString_eventhandler

                    i = (i + 1)
                Loop
            End If
        End If
    End Sub

    Private Sub mnu_objects_allowgrouping_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_objects_allowgrouping.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (layer Is Nothing) Then
            MessageBox.Show("A Layer must be selected before a Group operation can be performed")
            Return
        End If
        Dim bUserGrouping As Boolean = Not layer.Groups.AllowUserGrouping
        layer.Groups.AllowUserGrouping = bUserGrouping
        mnu_objects_allowgrouping.Checked = bUserGrouping
    End Sub

    Private Sub menuItemGroupAddSelectedItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemGroupAddSelectedItems.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (((Not (layer) Is Nothing) _
                    AndAlso TypeOf sender _
                    Is System.Windows.Forms.MenuItem)) Then
            Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
            Dim element As Element = layer.Elements.FirstSelected

            While (Not (element) Is Nothing)
                If element.Selected Then
                    group.GroupElements.Add(element)
                End If
                element = layer.Elements.NextSelected

            End While
        End If

    End Sub


    Private Sub menuItemCreateGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemCreateGroup.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (layer Is Nothing) Then
            MessageBox.Show("A Layer must be selected before a Group operation can be performed")
            Return
        End If
        Dim dlg As InputDlg = New InputDlg("Specify Name of Group to create")
        If (System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me)) Then
            Dim group As PegasusImaging.WinForms.NotateXpress8.Group = New PegasusImaging.WinForms.NotateXpress8.Group()
            group.Name = dlg.InputText
            layer.Groups.Add(group)
        End If
    End Sub

    Private Sub menuItemDeleteGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemDeleteGroup.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            If (TypeOf sender Is System.Windows.Forms.MenuItem) Then
                layer.Groups.RemoveAt(CType(sender, System.Windows.Forms.MenuItem).Index)
            End If
        End If

    End Sub


    Private Sub menuItemSetGroupEmpty_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemSetGroupEmpty.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            If (TypeOf sender Is System.Windows.Forms.MenuItem) Then
                Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
                group.GroupElements.Clear()
            End If
        End If

    End Sub

    Private Sub menuItemSelectGroupItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemSelectGroupItems.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            If (TypeOf sender Is System.Windows.Forms.MenuItem) Then
                Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
                ' first clear any currently selected items in the layer
                Dim element As Element = layer.Elements.FirstSelected

                While (Not (element) Is Nothing)
                    element.Selected = False
                    element = layer.Elements.NextSelected

                End While
                ' now select all items that are in the group
                Dim i As Integer = 0
                Do While (i < group.GroupElements.Count)
                    group.GroupElements(i).Selected = True
                    i = (i + 1)
                Loop
            End If
        End If

    End Sub


    Private Sub menuItemGroupRemoveSelectedItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemGroupRemoveSelectedItems.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (((Not (layer) Is Nothing) _
                    AndAlso TypeOf sender _
                    Is System.Windows.Forms.MenuItem)) Then
            Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
            Dim element As Element = layer.Elements.FirstSelected

            While (Not (element) Is Nothing)
                If element.Selected Then
                    group.GroupElements.Remove(element)
                End If
                element = layer.Elements.NextSelected

            End While
        End If

    End Sub

    Private Sub menuItemYokeGroupItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemYokeGroupItems.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (((Not (layer) Is Nothing) _
                    AndAlso TypeOf sender _
                    Is System.Windows.Forms.MenuItem)) Then
            Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
            group.Yoked = Not group.Yoked
        End If

    End Sub

    Private Sub menuItemMirrorYoked_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemMirrorYoked.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected

        If (((Not (layer) Is Nothing) _
                    AndAlso TypeOf sender _
                    Is System.Windows.Forms.MenuItem)) Then
            Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
            group.GroupElements.GroupMirror()
        End If

    End Sub

    Private Sub menuItemInvertYoked_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemInvertYoked.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (((Not (layer) Is Nothing) _
                     AndAlso TypeOf sender _
                    Is System.Windows.Forms.MenuItem)) Then
            Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
            group.GroupElements.GroupInvert()


        End If

    End Sub

    Private Sub menuItemSetGroupUserString_select(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemSetGroupUserString.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (((Not (layer) Is Nothing) _
                    AndAlso TypeOf sender _
                    Is System.Windows.Forms.MenuItem)) Then
            Dim group As Group = layer.Groups(CType(sender, System.Windows.Forms.MenuItem).Index)
            Dim dlg As InputDlg = New InputDlg(("User String for Group: " + group.Name))
            dlg.InputText = group.UserString
            If (System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me)) Then
                group.UserString = dlg.InputText
            End If
        End If

    End Sub

    Private Sub mnu_cm_enable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_cm_enable.Click
        NotateXpress1.MenuSetEnabled(MenuType.Context, AnnotationTool.NoTool, Not NotateXpress1.MenuGetEnabled(MenuType.Context, AnnotationTool.NoTool))
    End Sub

    Private Sub menuItemToolBar_Select(ByVal sender As Object, ByVal e As System.EventArgs) Handles menuItemToolBar.Select
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            'TODO - The toolbar doesn't seem to have an interface to allow
            ' determining whether a particular tool is present
            ' DevTrack #170
            ' For now just always enable if toolbar is visible
            Dim bEnabled As Boolean = layer.Toolbar.Visible
            menuItemToolbarRulerTool.Enabled = bEnabled
            menuItemToolbarFreehandTool.Enabled = bEnabled
            menuItemToolbarEllipseTool.Enabled = bEnabled
            menuItemToolbarTextTool.Enabled = bEnabled
            menuItemToolbarRectangleTool.Enabled = bEnabled
            menuItemToolbarStampTool.Enabled = bEnabled
            menuItemToolbarPolylineTool.Enabled = bEnabled
            menuItemToolbarLineTool.Enabled = bEnabled
            menuItemToolbarImageTool.Enabled = bEnabled
            menuItemToolbarButtonTool.Enabled = bEnabled
            menuItemToolbarPolygonTool.Enabled = bEnabled
            menuItemRulerToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.RulerTool)
            menuItemRulerToolEnabeled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.RulerTool)
            menuItemFreehandToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.FreehandTool)
            menuItemFreehandToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.FreehandTool)
            menuItemEllipseToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.EllipseTool)
            menuItemEllipseToolEnabeld.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.EllipseTool)
            menuItemTextToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.TextTool)
            menuItemTextToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.TextTool)
            menuItemRectangleToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.RectangleTool)
            menuItemRectangleToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.RectangleTool)
            menuItemStampToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.StampTool)
            menuItemStampToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.StampTool)
            menuItemPolylineToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.PolyLineTool)
            menuItemPolylineToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.PolyLineTool)
            menuItemLineToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.LineTool)
            menuItemLineToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.LineTool)
            menuItemImageToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.ImageTool)
            menuItemImageToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.ImageTool)
            menuItemButtonToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.ButtonTool)
            menuItemButtonToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.ButtonTool)
            menuItemPolygonToolVisible.Checked = layer.Toolbar.GetToolVisible(AnnotationTool.PolygonTool)
            menuItemPolygonToolEnabled.Checked = layer.Toolbar.GetToolEnabled(AnnotationTool.PolygonTool)
        End If
    End Sub

    Private Sub menuItemRulerToolEnabeled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemRulerToolEnabeled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.RulerTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.RulerTool))
        End If
    End Sub

    Private Sub menuItemRulerToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemRulerToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.RulerTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.RulerTool))
        End If
    End Sub


    Private Sub menuItemFreehandToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemFreehandToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.FreehandTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.FreehandTool))
        End If
    End Sub

    Private Sub menuItemFreehandToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemFreehandToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.FreehandTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.FreehandTool))
        End If
    End Sub

    Private Sub menuItemEllipseToolEnabeld_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemEllipseToolEnabeld.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.EllipseTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.EllipseTool))
        End If
    End Sub

    Private Sub menuItemEllipseToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemEllipseToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.EllipseTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.EllipseTool))
        End If
    End Sub

    Private Sub menuItemTextToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemTextToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.TextTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.TextTool))
        End If
    End Sub

    Private Sub menuItemTextToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemTextToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.TextTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.TextTool))
        End If
    End Sub

    Private Sub menuItemRectangleToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemRectangleToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.RectangleTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.RectangleTool))
        End If
    End Sub

    Private Sub menuItemRectangleToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemRectangleToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.RectangleTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.RectangleTool))
        End If
    End Sub

    Private Sub menuItemStampToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemStampToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.StampTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.StampTool))
        End If
    End Sub

    Private Sub menuItemStampToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemStampToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.StampTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.StampTool))
        End If
    End Sub

    Private Sub menuItemPolylineToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemPolylineToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.PolyLineTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.PolyLineTool))
        End If
    End Sub

    Private Sub menuItemPolylineToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemPolylineToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.PolyLineTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.PolyLineTool))
        End If
    End Sub

    Private Sub menuItemLineToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemLineToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.LineTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.LineTool))
        End If
    End Sub

    Private Sub menuItemLineToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemLineToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.LineTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.LineTool))
        End If
    End Sub

    Private Sub menuItemImageToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemImageToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.ImageTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.ImageTool))
        End If
    End Sub

    Private Sub menuItemImageToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemImageToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.ImageTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.ImageTool))
        End If
    End Sub

    Private Sub menuItemButtonToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemButtonToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.ButtonTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.ButtonTool))
        End If
    End Sub

    Private Sub menuItemButtonToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemButtonToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.ButtonTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.ButtonTool))
        End If
    End Sub

    Private Sub menuItemPolygonToolEnabled_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemPolygonToolEnabled.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolEnabled(AnnotationTool.PolygonTool, Not layer.Toolbar.GetToolEnabled(AnnotationTool.PolygonTool))
        End If
    End Sub

    Private Sub menuItemPolygonToolVisible_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemPolygonToolVisible.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            layer.Toolbar.SetToolVisible(AnnotationTool.PolygonTool, Not layer.Toolbar.GetToolVisible(AnnotationTool.PolygonTool))
        End If
    End Sub


    Private Sub menuItemAboutNX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemAboutNX.Click
        NotateXpress1.AboutBox()
    End Sub

    Private Sub menuItemAboutIX_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles menuItemAboutIX.Click
        ImagXpress1.AboutBox()
    End Sub

    Private Sub NotateXpress1_AnnotationAdded(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.AnnotationAddedEventArgs) Handles NotateXpress1.AnnotationAdded
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
        End If
        If Not (TypeOf sender Is PegasusImaging.WinForms.NotateXpress8.NotateXpress) Then
            MessageBox.Show(("sender is " + sender.GetType.ToString))
        End If
        ' entry has been added
        OutputList.Items.Insert(0, ("Add event for" _
                        + (strAnnotationType + " annotation")))
        If (_bAutoSelect AndAlso (Not (element) Is Nothing)) Then
            element.Selected = True
        End If
    End Sub

    Private Sub NotateXpress1_AnnotationDeleted(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.AnnotationDeletedEventArgs) Handles NotateXpress1.AnnotationDeleted
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        Dim layer As Layer = NotateXpress1.Layers.Selected

        Try
            If (Not (layer) Is Nothing) Then
                Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
                If (i = -1) Then
                    strAnnotationType = (strAnnotationType + element.GetType.ToString)
                Else
                    strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
                End If
            End If
            If Not (TypeOf sender Is PegasusImaging.WinForms.NotateXpress8.NotateXpress) Then
                MessageBox.Show(("sender is " + sender.GetType.ToString))
            End If
            ' entry has been deleted
            OutputList.Items.Insert(0, ("Delete event for" _
                            + (strAnnotationType + " annotation")))

        Catch ex As PegasusImaging.WinForms.NotateXpress8.NotateXpressException
            PegasusError(ex, lblerror)

        Catch ex As System.Exception
            PegasusError(ex, lblerror)

        End Try

    End Sub

    Private Sub NotateXpress1_AnnotationMoved(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.AnnotationMovedEventArgs) Handles NotateXpress1.AnnotationMoved
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
        End If
        If Not (TypeOf sender Is PegasusImaging.WinForms.NotateXpress8.NotateXpress) Then
            MessageBox.Show(("sender is " + sender.GetType.ToString))
        End If
        ' entry has been moved
        OutputList.Items.Insert(0, ("Move event for" _
                        + (strAnnotationType + " annotation")))
        If (_bAutoSelect _
                    AndAlso (Not (element) Is Nothing)) Then
            element.Selected = True
        End If
    End Sub

    Private Sub NotateXpress1_AnnotationSelected(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.AnnotationSelectedEventArgs) Handles NotateXpress1.AnnotationSelected
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
        End If

        If Not (TypeOf sender Is PegasusImaging.WinForms.NotateXpress8.NotateXpress) Then
            MessageBox.Show(("sender is " + sender.GetType.ToString))
        End If
        ' entry has been selected
        OutputList.Items.Insert(0, ("Selected event for" _
                        + (strAnnotationType + " annotation")))
        '   if (_bAutoSelect && element != null)
        'select newly selected layer element
        'element.Selected = true;
    End Sub


    Private Sub NotateXpress1_Click(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.ClickEventArgs) Handles NotateXpress1.Click
        Dim element As Element = e.Element
        Dim layer As Layer = e.Layer
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
                    AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
        End If
        OutputList.Items.Insert(0, (strAnnotationType + (" annotation clicked on, layer name = " _
                        + (layer.Name + (", layer description = " + layer.Description)))))
    End Sub

    Private Sub NotateXpress1_MouseDown(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.MouseDownEventArgs) Handles NotateXpress1.MouseDown
        Dim layer As Layer = e.Layer
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
                    AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
            OutputList.Items.Insert(0, ("       X coorinate = " _
                            + (e.X.ToString + (", Y coordinate = " + e.Y.ToString))))
            OutputList.Items.Insert(0, ("Mouse down event on " _
                            + (strAnnotationType + (", current layer name = " _
                            + (layer.Name + (", description = " + layer.Description))))))
        End If
    End Sub

    Private Sub NotateXpress1_MouseMove(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.MouseMoveEventArgs) Handles NotateXpress1.MouseMove
        Dim layer As Layer = e.Layer
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
                    AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
            OutputList.Items.Insert(0, ("       X coorinate = " _
                            + (e.X.ToString + (", Y coordinate = " + e.Y.ToString))))
            OutputList.Items.Insert(0, ("Mouse move event on " _
                            + (strAnnotationType + (", current layer name = " _
                            + (layer.Name + (", description = " + layer.Description))))))
        End If


    End Sub

    Private Sub NotateXpress1_MouseUp(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.MouseUpEventArgs) Handles NotateXpress1.MouseUp
        Dim layer As Layer = e.Layer
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
                    AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
            OutputList.Items.Insert(0, ("       X coorinate = " _
                            + (e.X.ToString + (", Y coordinate = " + e.Y.ToString))))
            OutputList.Items.Insert(0, ("Mouse up event on " _
                            + (strAnnotationType + (", current layer name = " _
                            + (layer.Name + (", description = " + layer.Description))))))
        End If


    End Sub

    Private Sub NotateXpress1_ToolbarSelect(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.ToolbarEventArgs) Handles NotateXpress1.ToolbarSelect
        Dim layer As Layer = e.Layer
        If (Not (layer) Is Nothing) Then
            OutputList.Items.Insert(0, ("Toolbar " _
                            + (GetAnnotationToolString(e.Tool) + (" selected, layer name = " _
                            + (layer.Name + (", description = " + layer.Description))))))
        End If
    End Sub

    Private Sub NotateXpress1_UserDraw(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.UserDrawEventArgs) Handles NotateXpress1.UserDraw
        Dim layer As Layer = e.Layer
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
                    AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
            OutputList.Items.Insert(0, ("         Device context = " _
                            + (e.HandleDeviceContext.ToString + (", X cooordinate = " _
                            + (e.X.ToString + (", Y coordinate = " _
                            + (e.Y.ToString + (", Width = " _
                            + (e.Width.ToString + (", Height = " + e.Height.ToString))))))))))
            OutputList.Items.Insert(0, ("User draw event on " _
                            + (strAnnotationType + (", current layer name = " _
                            + (layer.Name + (", description = " + layer.Description))))))
        End If


    End Sub


    Private Sub NotateXpress1_CurrentLayerChange(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.CurrentLayerChangeEventArgs) Handles NotateXpress1.CurrentLayerChange

        'TODO DevTrack #189

        Dim layer As Layer = e.Layer
        If (Not (layer) Is Nothing) Then
            OutputList.Items.Insert(0, ("Layer change event, current layer name = " _
                            + (layer.Name + (", description = " + layer.Description))))
        End If

    End Sub

    Private Sub NotateXpress1_DoubleClick(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.DoubleClickEventArgs) Handles NotateXpress1.DoubleClick
        Dim layer As Layer = e.Layer
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
                    AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
            OutputList.Items.Insert(0, ("Mouse double clidk event on " _
                            + (strAnnotationType + (", current layer name = " _
                            + (layer.Name + (", description = " + layer.Description))))))
        End If

    End Sub

    Private Sub NotateXpress1_ItemChanged(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.ItemChangedEventArgs) Handles NotateXpress1.ItemChanged

        'TODO DevTrack #191

        Dim layer As Layer = e.Layer
        Dim element As Element = e.Element
        Dim strAnnotationType As String = " "
        If ((Not (layer) Is Nothing) _
                    AndAlso (Not (element) Is Nothing)) Then
            Dim i As Integer = element.GetType.ToString.LastIndexOf(Microsoft.VisualBasic.ChrW(46))
            If (i = -1) Then
                strAnnotationType = (strAnnotationType + element.GetType.ToString)
            Else
                strAnnotationType = (strAnnotationType + element.GetType.ToString.Substring((i + 1)))
            End If
            OutputList.Items.Insert(0, ("Item changed event on " _
                            + (strAnnotationType + (", current layer name = " _
                            + (layer.Name + (", description = " + layer.Description))))))
        End If


    End Sub

    Private Sub NotateXpress1_LayerRestored(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.LayerRestoredEventArgs) Handles NotateXpress1.LayerRestored
        'TODO DevTrack #192
        Dim layer As Layer = e.Layer
        If (Not (layer) Is Nothing) Then
            OutputList.Items.Insert(0, ("Layer restored event on layer name = " _
                            + (layer.Name + (", description = " + layer.Description))))
        End If
    End Sub

    Private Sub NotateXpress1_RequestLayerPassword(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.GetLayerPasswordEventArgs) Handles NotateXpress1.RequestLayerPassword
        OutputList.Items.Insert(0, ("Request Layer Password event for layer name = " _
                + (e.LayerName + (", layer password = " + e.LayerPassword))))
        Dim dlg As InputDlg = New InputDlg(("Provide Layer '" _
                        + (e.LayerName + "' Password")))
        If (System.Windows.Forms.DialogResult.OK = dlg.ShowDialog(Me)) Then
            e.LayerPassword = dlg.InputText
        End If


    End Sub

    Private Sub NotateXpress1_UserGroupCreated(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.UserGroupCreatedEventArgs) Handles NotateXpress1.UserGroupCreated
        'TODO DevTrack #194
        Dim layer As Layer = e.Layer
        Dim group As Group = e.Group
        OutputList.Items.Insert(0, ("User group '" _
                        + (group.Name + ("' created event, layer name = " _
                        + (layer.Name + (", description = " + layer.Description))))))
    End Sub

    Private Sub NotateXpress1_UserGroupDestroyed(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.NotateXpress8.UserGroupDestroyedEventArgs) Handles NotateXpress1.UserGroupDestroyed
        'TODO DevTrack #194
        Dim layer As Layer = e.Layer
        Dim group As Group = e.Group
        OutputList.Items.Insert(0, ("User group '" _
                        + (group.Name + ("' created event, layer name = " _
                        + (layer.Name + (", description = " + layer.Description))))))
    End Sub

    Private Function GetAnnotationToolString(ByVal annotationTool As AnnotationTool) As String
        Select Case (annotationTool)
            Case annotationTool.NoTool
                ' = 0x0,
                Return "NoTool"
            Case annotationTool.PointerTool
                ' = 0x1000,
                Return "PointerTool"
            Case annotationTool.TextTool
                ' = 0x1001,
                Return "TextTool"
            Case annotationTool.RectangleTool
                ' = 0x1002,
                Return "RectangleTool"
            Case annotationTool.EllipseTool
                ' = 0x1003,
                Return "EllipseTool"
            Case annotationTool.PolygonTool
                ' = 0x1004,
                Return "PolygonTool"
            Case annotationTool.PolyLineTool
                ' = 0x1005,
                Return "PolyLineTool"
            Case annotationTool.LineTool
                ' = 0x1006,
                Return "LineTool"
            Case annotationTool.FreehandTool
                ' = 0x1007,
                Return "FreehandTool"
            Case annotationTool.StampTool
                ' = 0x1008,
                Return "StampTool"
            Case annotationTool.ImageTool
                ' = 0x1009,
                Return "ImageTool"
            Case annotationTool.ButtonTool
                ' = 0x100a,
                Return "ButtonTool"
            Case annotationTool.RulerTool
                ' = 0x100b
                Return "RulerTool"
            Case Else
                Return "Unknown"
        End Select
    End Function


    Private Sub NotateXpress1_Stamping(ByVal sender As Object, ByVal e As System.EventArgs) Handles NotateXpress1.Stamping
        '//TODO DevTrack #180
    End Sub

    Private Sub ZoomIn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZoomIn.Click
        ImageXView1.ZoomFactor = ImageXView1.ZoomFactor / 0.9
    End Sub

    Private Sub ZoomOut_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ZoomOut.Click
        ImageXView1.ZoomFactor = ImageXView1.ZoomFactor * 0.9
    End Sub

    Private Sub mnu_SelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnu_SelectAll.Click

        Dim layer As PegasusImaging.WinForms.NotateXpress8.Layer

        Try
            layer = NotateXpress1.Layers.Selected()

            If (Not (layer) Is Nothing) And layer.Elements.Count > 0 Then
                mnu_SelectAll.Checked = Not mnu_SelectAll.Checked
                layer.Elements.SelectAll(mnu_SelectAll.Checked)

            End If
        Catch ex As PegasusImaging.WinForms.NotateXpress8.NotateXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub

End Class

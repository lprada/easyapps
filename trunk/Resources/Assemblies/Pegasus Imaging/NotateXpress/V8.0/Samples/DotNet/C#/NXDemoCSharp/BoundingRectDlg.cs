/*****************************************************************'
* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
* This sample code is provided to Pegasus licensees "as is"    *'
* with no restrictions on use or modification. No warranty for *'
* use of this sample code is provided by Pegasus.              *'
*****************************************************************/


using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace NXDemoCSharp
{
	/// <summary>
	/// Summary description for BoundingRectDlg.
	/// </summary>
	public class BoundingRectDlg : System.Windows.Forms.Form
	{
		private System.Windows.Forms.NumericUpDown numericUpDownBoundingRectTop;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.NumericUpDown numericUpDownBoundingRectLeft;
		private System.Windows.Forms.Label LabelRight;
		private System.Windows.Forms.NumericUpDown numericUpDownBoundingRectRight;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button buttonBoundingRectOK;
		private System.Windows.Forms.Button buttonBoundingRectCancel;
		private System.Windows.Forms.NumericUpDown numericUpDownBoundingRectBottom;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public BoundingRectDlg(Rectangle rect, Rectangle imageRect)
		{


			//****Must call the UnlockControl function *****
			//PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234,1234,1234,1234);
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			numericUpDownBoundingRectRight.Maximum = imageRect.Right;
			numericUpDownBoundingRectBottom.Maximum = imageRect.Bottom;
			numericUpDownBoundingRectLeft.Maximum = imageRect.Right;
			numericUpDownBoundingRectTop.Maximum = imageRect.Bottom;

			Rect = rect;
		}

		public Rectangle Rect
		{
			get
			{
				Rectangle rect = new Rectangle((Int32)numericUpDownBoundingRectTop.Value,
												(Int32)numericUpDownBoundingRectLeft.Value,
					(Int32)numericUpDownBoundingRectRight.Value - (Int32)numericUpDownBoundingRectLeft.Value,
					(Int32)numericUpDownBoundingRectBottom.Value - (Int32)numericUpDownBoundingRectTop.Value);
				return rect;
			}

			set
			{
				try
				{
					numericUpDownBoundingRectTop.Value = (decimal)value.Top;
					numericUpDownBoundingRectLeft.Value = (decimal)value.Left;
					numericUpDownBoundingRectRight.Value = (decimal)value.Right;
					numericUpDownBoundingRectBottom.Value = (decimal)value.Bottom;
				}
				catch (System.Exception e)
				{
					MessageBox.Show(e.Message +  "   BoundingRect bug in NotateXpress");
				}
			}
		}

		public Int32 RectTop
		{
			get
			{
				return (Int32)numericUpDownBoundingRectTop.Value;
			}

			set
			{
				numericUpDownBoundingRectTop.Value = (Int32)value;
			}
		}

		public Int32 RectLeft
		{
			get
			{
				return (Int32)numericUpDownBoundingRectLeft.Value;
			}

			set
			{
				numericUpDownBoundingRectLeft.Value = (Int32)value;
			}
		}

		public Int32 RectRight
		{
			get
			{
				return (Int32)numericUpDownBoundingRectRight.Value;
			}

			set
			{
				numericUpDownBoundingRectRight.Value = (Int32)value;
			}
		}

		public Int32 RectBottom
		{
			get
			{
				return (Int32)numericUpDownBoundingRectBottom.Value;
			}

			set
			{
				numericUpDownBoundingRectBottom.Value = (Int32)value;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{

				
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.numericUpDownBoundingRectTop = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.numericUpDownBoundingRectLeft = new System.Windows.Forms.NumericUpDown();
			this.LabelRight = new System.Windows.Forms.Label();
			this.numericUpDownBoundingRectRight = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.numericUpDownBoundingRectBottom = new System.Windows.Forms.NumericUpDown();
			this.buttonBoundingRectOK = new System.Windows.Forms.Button();
			this.buttonBoundingRectCancel = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownBoundingRectTop)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownBoundingRectLeft)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownBoundingRectRight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownBoundingRectBottom)).BeginInit();
			this.SuspendLayout();
			// 
			// numericUpDownBoundingRectTop
			// 
			this.numericUpDownBoundingRectTop.Location = new System.Drawing.Point(136, 24);
			this.numericUpDownBoundingRectTop.Name = "numericUpDownBoundingRectTop";
			this.numericUpDownBoundingRectTop.Size = new System.Drawing.Size(56, 20);
			this.numericUpDownBoundingRectTop.TabIndex = 0;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(96, 24);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(32, 16);
			this.label1.TabIndex = 1;
			this.label1.Text = "Top";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 80);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(32, 16);
			this.label2.TabIndex = 3;
			this.label2.Text = "Left";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numericUpDownBoundingRectLeft
			// 
			this.numericUpDownBoundingRectLeft.Location = new System.Drawing.Point(56, 80);
			this.numericUpDownBoundingRectLeft.Name = "numericUpDownBoundingRectLeft";
			this.numericUpDownBoundingRectLeft.Size = new System.Drawing.Size(56, 20);
			this.numericUpDownBoundingRectLeft.TabIndex = 2;
			// 
			// LabelRight
			// 
			this.LabelRight.Location = new System.Drawing.Point(184, 80);
			this.LabelRight.Name = "LabelRight";
			this.LabelRight.Size = new System.Drawing.Size(32, 16);
			this.LabelRight.TabIndex = 5;
			this.LabelRight.Text = "Right";
			this.LabelRight.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numericUpDownBoundingRectRight
			// 
			this.numericUpDownBoundingRectRight.Location = new System.Drawing.Point(224, 80);
			this.numericUpDownBoundingRectRight.Name = "numericUpDownBoundingRectRight";
			this.numericUpDownBoundingRectRight.Size = new System.Drawing.Size(56, 20);
			this.numericUpDownBoundingRectRight.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(88, 144);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(40, 16);
			this.label3.TabIndex = 7;
			this.label3.Text = "Bottom";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// numericUpDownBoundingRectBottom
			// 
			this.numericUpDownBoundingRectBottom.Location = new System.Drawing.Point(136, 144);
			this.numericUpDownBoundingRectBottom.Name = "numericUpDownBoundingRectBottom";
			this.numericUpDownBoundingRectBottom.Size = new System.Drawing.Size(56, 20);
			this.numericUpDownBoundingRectBottom.TabIndex = 6;
			// 
			// buttonBoundingRectOK
			// 
			this.buttonBoundingRectOK.Location = new System.Drawing.Point(32, 224);
			this.buttonBoundingRectOK.Name = "buttonBoundingRectOK";
			this.buttonBoundingRectOK.Size = new System.Drawing.Size(112, 32);
			this.buttonBoundingRectOK.TabIndex = 8;
			this.buttonBoundingRectOK.Text = "OK";
			this.buttonBoundingRectOK.Click += new System.EventHandler(this.buttonBoundingRectOK_Click);
			// 
			// buttonBoundingRectCancel
			// 
			this.buttonBoundingRectCancel.Location = new System.Drawing.Point(184, 224);
			this.buttonBoundingRectCancel.Name = "buttonBoundingRectCancel";
			this.buttonBoundingRectCancel.Size = new System.Drawing.Size(112, 32);
			this.buttonBoundingRectCancel.TabIndex = 9;
			this.buttonBoundingRectCancel.Text = "Cancel";
			this.buttonBoundingRectCancel.Click += new System.EventHandler(this.buttonBoundingRectCancel_Click);
			// 
			// BoundingRectDlg
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(320, 278);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.buttonBoundingRectCancel,
																		  this.buttonBoundingRectOK,
																		  this.label3,
																		  this.numericUpDownBoundingRectBottom,
																		  this.LabelRight,
																		  this.numericUpDownBoundingRectRight,
																		  this.label2,
																		  this.numericUpDownBoundingRectLeft,
																		  this.label1,
																		  this.numericUpDownBoundingRectTop});
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "BoundingRectDlg";
			this.ShowInTaskbar = false;
			this.Text = "Element Bounding Rectangle coordinates";
			this.Load += new System.EventHandler(this.BoundingRectDlg_Load);
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownBoundingRectTop)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownBoundingRectLeft)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownBoundingRectRight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numericUpDownBoundingRectBottom)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void buttonBoundingRectOK_Click(object sender, System.EventArgs e)
		{
			DialogResult = DialogResult.OK;		
		}

		private void buttonBoundingRectCancel_Click(object sender, System.EventArgs e)
		{
			DialogResult = DialogResult.Cancel;				
		}

		private void BoundingRectDlg_Load(object sender, System.EventArgs e)
		{
		
		}
	}
}

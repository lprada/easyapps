'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports System
Imports System.Drawing
Imports System.Collections
Imports System.ComponentModel
Imports System.Windows.Forms

Public Class BoundingRectDlg
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New(ByVal rect As Rectangle)
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents buttonBoundingRectCancel As System.Windows.Forms.Button
    Friend WithEvents buttonBoundingRectOK As System.Windows.Forms.Button
    Friend WithEvents label3 As System.Windows.Forms.Label
    Friend WithEvents numericUpDownBoundingRectBottom As System.Windows.Forms.NumericUpDown
    Friend WithEvents LabelRight As System.Windows.Forms.Label
    Friend WithEvents numericUpDownBoundingRectRight As System.Windows.Forms.NumericUpDown
    Friend WithEvents label2 As System.Windows.Forms.Label
    Friend WithEvents numericUpDownBoundingRectLeft As System.Windows.Forms.NumericUpDown
    Friend WithEvents label1 As System.Windows.Forms.Label
    Friend WithEvents numericUpDownBoundingRectTop As System.Windows.Forms.NumericUpDown
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.buttonBoundingRectCancel = New System.Windows.Forms.Button()
        Me.buttonBoundingRectOK = New System.Windows.Forms.Button()
        Me.label3 = New System.Windows.Forms.Label()
        Me.numericUpDownBoundingRectBottom = New System.Windows.Forms.NumericUpDown()
        Me.LabelRight = New System.Windows.Forms.Label()
        Me.numericUpDownBoundingRectRight = New System.Windows.Forms.NumericUpDown()
        Me.label2 = New System.Windows.Forms.Label()
        Me.numericUpDownBoundingRectLeft = New System.Windows.Forms.NumericUpDown()
        Me.label1 = New System.Windows.Forms.Label()
        Me.numericUpDownBoundingRectTop = New System.Windows.Forms.NumericUpDown()
        CType(Me.numericUpDownBoundingRectBottom, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numericUpDownBoundingRectRight, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numericUpDownBoundingRectLeft, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.numericUpDownBoundingRectTop, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'buttonBoundingRectCancel
        '
        Me.buttonBoundingRectCancel.Location = New System.Drawing.Point(216, 217)
        Me.buttonBoundingRectCancel.Name = "buttonBoundingRectCancel"
        Me.buttonBoundingRectCancel.Size = New System.Drawing.Size(112, 32)
        Me.buttonBoundingRectCancel.TabIndex = 19
        Me.buttonBoundingRectCancel.Text = "Cancel"
        '
        'buttonBoundingRectOK
        '
        Me.buttonBoundingRectOK.Location = New System.Drawing.Point(64, 217)
        Me.buttonBoundingRectOK.Name = "buttonBoundingRectOK"
        Me.buttonBoundingRectOK.Size = New System.Drawing.Size(112, 32)
        Me.buttonBoundingRectOK.TabIndex = 18
        Me.buttonBoundingRectOK.Text = "OK"
        '
        'label3
        '
        Me.label3.Location = New System.Drawing.Point(120, 137)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(40, 16)
        Me.label3.TabIndex = 17
        Me.label3.Text = "Bottom"
        Me.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'numericUpDownBoundingRectBottom
        '
        Me.numericUpDownBoundingRectBottom.Location = New System.Drawing.Point(168, 137)
        Me.numericUpDownBoundingRectBottom.Name = "numericUpDownBoundingRectBottom"
        Me.numericUpDownBoundingRectBottom.Size = New System.Drawing.Size(56, 20)
        Me.numericUpDownBoundingRectBottom.TabIndex = 16
        '
        'LabelRight
        '
        Me.LabelRight.Location = New System.Drawing.Point(216, 73)
        Me.LabelRight.Name = "LabelRight"
        Me.LabelRight.Size = New System.Drawing.Size(32, 16)
        Me.LabelRight.TabIndex = 15
        Me.LabelRight.Text = "Right"
        Me.LabelRight.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'numericUpDownBoundingRectRight
        '
        Me.numericUpDownBoundingRectRight.Location = New System.Drawing.Point(256, 73)
        Me.numericUpDownBoundingRectRight.Name = "numericUpDownBoundingRectRight"
        Me.numericUpDownBoundingRectRight.Size = New System.Drawing.Size(56, 20)
        Me.numericUpDownBoundingRectRight.TabIndex = 14
        '
        'label2
        '
        Me.label2.Location = New System.Drawing.Point(48, 73)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(32, 16)
        Me.label2.TabIndex = 13
        Me.label2.Text = "Left"
        Me.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'numericUpDownBoundingRectLeft
        '
        Me.numericUpDownBoundingRectLeft.Location = New System.Drawing.Point(88, 73)
        Me.numericUpDownBoundingRectLeft.Name = "numericUpDownBoundingRectLeft"
        Me.numericUpDownBoundingRectLeft.Size = New System.Drawing.Size(56, 20)
        Me.numericUpDownBoundingRectLeft.TabIndex = 12
        '
        'label1
        '
        Me.label1.Location = New System.Drawing.Point(128, 17)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(32, 16)
        Me.label1.TabIndex = 11
        Me.label1.Text = "Top"
        Me.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'numericUpDownBoundingRectTop
        '
        Me.numericUpDownBoundingRectTop.Location = New System.Drawing.Point(168, 17)
        Me.numericUpDownBoundingRectTop.Name = "numericUpDownBoundingRectTop"
        Me.numericUpDownBoundingRectTop.Size = New System.Drawing.Size(56, 20)
        Me.numericUpDownBoundingRectTop.TabIndex = 10
        '
        'BoundingRectDlg
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(376, 266)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.buttonBoundingRectCancel, Me.buttonBoundingRectOK, Me.label3, Me.numericUpDownBoundingRectBottom, Me.LabelRight, Me.numericUpDownBoundingRectRight, Me.label2, Me.numericUpDownBoundingRectLeft, Me.label1, Me.numericUpDownBoundingRectTop})
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Name = "BoundingRectDlg"
        Me.Text = "Element Bounding Rectangle coordinates"
        CType(Me.numericUpDownBoundingRectBottom, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numericUpDownBoundingRectRight, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numericUpDownBoundingRectLeft, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.numericUpDownBoundingRectTop, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public Property Rect() As Rectangle
        Get
            Rect = New Rectangle(numericUpDownBoundingRectTop.Value, _
                                numericUpDownBoundingRectLeft.Value, _
                                numericUpDownBoundingRectRight.Value - numericUpDownBoundingRectLeft.Value, _
                                numericUpDownBoundingRectBottom.Value - numericUpDownBoundingRectTop.Value)
            Return rect
        End Get
        Set(ByVal Value As Rectangle)
            Try
                numericUpDownBoundingRectTop.Value = CType(value.Top, Decimal)
                numericUpDownBoundingRectLeft.Value = CType(value.Left, Decimal)
                numericUpDownBoundingRectRight.Value = CType(value.Right, Decimal)
                numericUpDownBoundingRectBottom.Value = CType(value.Bottom, Decimal)
            Catch
                MessageBox.Show("BoundingRect bug in NotateXpress")
            End Try
        End Set
    End Property

    Public Property RectTop() As Int32
        Get
            Return CType(numericUpDownBoundingRectTop.Value, Int32)
        End Get
        Set(ByVal Value As Int32)
            numericUpDownBoundingRectTop.Value = CType(value, Int32)
        End Set
    End Property

    Public Property RectLeft() As Int32
        Get
            Return CType(numericUpDownBoundingRectLeft.Value, Int32)
        End Get
        Set(ByVal Value As Int32)
            numericUpDownBoundingRectLeft.Value = CType(value, Int32)
        End Set
    End Property

    Public Property RectRight() As Int32
        Get
            Return CType(numericUpDownBoundingRectRight.Value, Int32)
        End Get
        Set(ByVal Value As Int32)
            numericUpDownBoundingRectRight.Value = CType(value, Int32)
        End Set
    End Property

    Public Property RectBottom() As Int32
        Get
            Return CType(numericUpDownBoundingRectBottom.Value, Int32)
        End Get
        Set(ByVal Value As Int32)
            numericUpDownBoundingRectBottom.Value = CType(value, Int32)
        End Set
    End Property


    Private Sub buttonBoundingRectOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonBoundingRectOK.Click
        DialogResult = DialogResult.OK
    End Sub

    Private Sub buttonBoundingRectCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles buttonBoundingRectCancel.Click
        DialogResult = DialogResult.Cancel
    End Sub

    Private Sub BoundingRectDlg_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

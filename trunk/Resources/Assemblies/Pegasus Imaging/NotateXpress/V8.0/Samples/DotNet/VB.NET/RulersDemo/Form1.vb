'****************************************************************'
'* Copyright 2006 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.ImagXpress8
Imports PegasusImaging.WinForms.NotateXpress8
Imports System.Windows.Forms
Imports System
Imports System.Drawing

Public Class Form1
    Inherits System.Windows.Forms.Form

    Dim imagX1 As PegasusImaging.WinForms.ImagXpress8.ImageX
    Dim layer As PegasusImaging.WinForms.NotateXpress8.Layer
    Dim loIXLoadOptions As PegasusImaging.WinForms.ImagXpress8.LoadOptions
    Dim soIXSaveOptions As PegasusImaging.WinForms.ImagXpress8.SaveOptions
    Dim soNXSaveOptions As PegasusImaging.WinForms.NotateXpress8.SaveOptions
    Dim soNXLoadOptions As PegasusImaging.WinForms.NotateXpress8.LoadOptions
    Dim strImageFileName As String
    Dim strNXPFileName As String
    Dim strSaveFileName As String

    Dim strCurrentDir As String



#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()


        '****Must call the UnlockControl function *****
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234,1234,1234,1234);


        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            ' Don't forget to dispose IX
            '
            If (Not (ImageXView1) Is Nothing) Then
                ' following line is causing exception on shutdown
                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If

            If (Not (imagX1) Is Nothing) Then
                ' following line is causing exception on shutdown
                imagX1.Dispose()
                imagX1 = Nothing
            End If
            If (Not (imagXpress1) Is Nothing) Then
                ' following line is causing exception on shutdown
                imagXpress1.Dispose()
                imagXpress1 = Nothing
            End If
            ' Don't forget to dispose NXP
            '                
            If (Not (NotateXpress1) Is Nothing) Then
                NotateXpress1.Dispose()
                NotateXpress1 = Nothing
            End If
            If (Not (components) Is Nothing) Then
                components.Dispose()
            End If



            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Miles As System.Windows.Forms.RadioButton
    Friend WithEvents Twips As System.Windows.Forms.RadioButton
    Friend WithEvents Pixels As System.Windows.Forms.RadioButton
    Friend WithEvents Millimeters As System.Windows.Forms.RadioButton
    Friend WithEvents Meters As System.Windows.Forms.RadioButton
    Friend WithEvents Kilometers As System.Windows.Forms.RadioButton
    Friend WithEvents MicroMeters As System.Windows.Forms.RadioButton
    Friend WithEvents Inches As System.Windows.Forms.RadioButton
    Friend WithEvents Feet As System.Windows.Forms.RadioButton
    Friend WithEvents Centimeters As System.Windows.Forms.RadioButton
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents RulerPrecision As System.Windows.Forms.TrackBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents ClearList As System.Windows.Forms.Button
    Friend WithEvents RulerLengths As System.Windows.Forms.Button
    Friend WithEvents ShowAbbreviations As System.Windows.Forms.CheckBox
    Friend WithEvents ShowGauge As System.Windows.Forms.CheckBox
    Friend WithEvents ShowMeasurement As System.Windows.Forms.CheckBox
    Friend WithEvents lstStatus As System.Windows.Forms.ListBox
    Friend WithEvents lblLoadStatus As System.Windows.Forms.Label
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents LoadImageFile As System.Windows.Forms.MenuItem
    Friend WithEvents SaveImageFile As System.Windows.Forms.MenuItem
    Friend WithEvents SaveAnnotationFile As System.Windows.Forms.MenuItem
    Friend WithEvents LoadAnnotationFile As System.Windows.Forms.MenuItem
    Friend WithEvents FileExit As System.Windows.Forms.MenuItem
    Friend WithEvents mnuRulers As System.Windows.Forms.MenuItem
    Friend WithEvents CreateRuler As System.Windows.Forms.MenuItem
    Friend WithEvents DeleteRulers As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbar As System.Windows.Forms.MenuItem
    Friend WithEvents mnuToolbarShow As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents SaveFileDialog As System.Windows.Forms.SaveFileDialog
    Friend WithEvents OpenFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents imagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents NotateXpress1 As PegasusImaging.WinForms.NotateXpress8.NotateXpress
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Miles = New System.Windows.Forms.RadioButton()
        Me.Twips = New System.Windows.Forms.RadioButton()
        Me.Pixels = New System.Windows.Forms.RadioButton()
        Me.Millimeters = New System.Windows.Forms.RadioButton()
        Me.Meters = New System.Windows.Forms.RadioButton()
        Me.Kilometers = New System.Windows.Forms.RadioButton()
        Me.MicroMeters = New System.Windows.Forms.RadioButton()
        Me.Inches = New System.Windows.Forms.RadioButton()
        Me.Feet = New System.Windows.Forms.RadioButton()
        Me.Centimeters = New System.Windows.Forms.RadioButton()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.RulerPrecision = New System.Windows.Forms.TrackBar()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ClearList = New System.Windows.Forms.Button()
        Me.RulerLengths = New System.Windows.Forms.Button()
        Me.ShowAbbreviations = New System.Windows.Forms.CheckBox()
        Me.ShowGauge = New System.Windows.Forms.CheckBox()
        Me.ShowMeasurement = New System.Windows.Forms.CheckBox()
        Me.lstStatus = New System.Windows.Forms.ListBox()
        Me.lblLoadStatus = New System.Windows.Forms.Label()
        Me.lblLastError = New System.Windows.Forms.Label()
        Me.lblerror = New System.Windows.Forms.Label()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.LoadImageFile = New System.Windows.Forms.MenuItem()
        Me.SaveImageFile = New System.Windows.Forms.MenuItem()
        Me.SaveAnnotationFile = New System.Windows.Forms.MenuItem()
        Me.LoadAnnotationFile = New System.Windows.Forms.MenuItem()
        Me.FileExit = New System.Windows.Forms.MenuItem()
        Me.mnuRulers = New System.Windows.Forms.MenuItem()
        Me.CreateRuler = New System.Windows.Forms.MenuItem()
        Me.DeleteRulers = New System.Windows.Forms.MenuItem()
        Me.mnuToolbar = New System.Windows.Forms.MenuItem()
        Me.mnuToolbarShow = New System.Windows.Forms.MenuItem()
        Me.mnuAbout = New System.Windows.Forms.MenuItem()
        Me.SaveFileDialog = New System.Windows.Forms.SaveFileDialog()
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.imagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.NotateXpress1 = New PegasusImaging.WinForms.NotateXpress8.NotateXpress()
        Me.GroupBox1.SuspendLayout()
        CType(Me.RulerPrecision, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Adding Ruler annotations and setting the various attributes."})
        Me.lstInfo.Location = New System.Drawing.Point(8, 16)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(952, 56)
        Me.lstInfo.TabIndex = 28
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.GroupBox1.Controls.AddRange(New System.Windows.Forms.Control() {Me.Miles, Me.Twips, Me.Pixels, Me.Millimeters, Me.Meters, Me.Kilometers, Me.MicroMeters, Me.Inches, Me.Feet, Me.Centimeters})
        Me.GroupBox1.Location = New System.Drawing.Point(600, 88)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(136, 280)
        Me.GroupBox1.TabIndex = 27
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Unit of Measurement"
        '
        'Miles
        '
        Me.Miles.Location = New System.Drawing.Point(16, 240)
        Me.Miles.Name = "Miles"
        Me.Miles.Size = New System.Drawing.Size(112, 16)
        Me.Miles.TabIndex = 9
        Me.Miles.Text = "Miles"
        '
        'Twips
        '
        Me.Twips.Location = New System.Drawing.Point(16, 216)
        Me.Twips.Name = "Twips"
        Me.Twips.Size = New System.Drawing.Size(112, 16)
        Me.Twips.TabIndex = 8
        Me.Twips.Text = "Twips"
        '
        'Pixels
        '
        Me.Pixels.Location = New System.Drawing.Point(16, 192)
        Me.Pixels.Name = "Pixels"
        Me.Pixels.Size = New System.Drawing.Size(112, 16)
        Me.Pixels.TabIndex = 7
        Me.Pixels.Text = "Pixels"
        '
        'Millimeters
        '
        Me.Millimeters.Location = New System.Drawing.Point(16, 168)
        Me.Millimeters.Name = "Millimeters"
        Me.Millimeters.Size = New System.Drawing.Size(112, 16)
        Me.Millimeters.TabIndex = 6
        Me.Millimeters.Text = "Millimeters"
        '
        'Meters
        '
        Me.Meters.Location = New System.Drawing.Point(16, 144)
        Me.Meters.Name = "Meters"
        Me.Meters.Size = New System.Drawing.Size(112, 16)
        Me.Meters.TabIndex = 5
        Me.Meters.Text = "Meters"
        '
        'Kilometers
        '
        Me.Kilometers.Location = New System.Drawing.Point(16, 120)
        Me.Kilometers.Name = "Kilometers"
        Me.Kilometers.Size = New System.Drawing.Size(112, 16)
        Me.Kilometers.TabIndex = 4
        Me.Kilometers.Text = "Kilometers"
        '
        'MicroMeters
        '
        Me.MicroMeters.Location = New System.Drawing.Point(16, 96)
        Me.MicroMeters.Name = "MicroMeters"
        Me.MicroMeters.Size = New System.Drawing.Size(112, 16)
        Me.MicroMeters.TabIndex = 3
        Me.MicroMeters.Text = "MicroMeters"
        '
        'Inches
        '
        Me.Inches.Location = New System.Drawing.Point(16, 72)
        Me.Inches.Name = "Inches"
        Me.Inches.Size = New System.Drawing.Size(112, 16)
        Me.Inches.TabIndex = 2
        Me.Inches.Text = "Inches"
        '
        'Feet
        '
        Me.Feet.Location = New System.Drawing.Point(16, 48)
        Me.Feet.Name = "Feet"
        Me.Feet.Size = New System.Drawing.Size(112, 16)
        Me.Feet.TabIndex = 1
        Me.Feet.Text = "Feet"
        '
        'Centimeters
        '
        Me.Centimeters.Location = New System.Drawing.Point(16, 24)
        Me.Centimeters.Name = "Centimeters"
        Me.Centimeters.Size = New System.Drawing.Size(112, 16)
        Me.Centimeters.TabIndex = 0
        Me.Centimeters.Text = "Centimeters"
        '
        'ImageXView1
        '
        Me.ImageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.ImageXView1.Location = New System.Drawing.Point(16, 88)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(568, 544)
        Me.ImageXView1.TabIndex = 29
        '
        'RulerPrecision
        '
        Me.RulerPrecision.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.RulerPrecision.Location = New System.Drawing.Point(600, 568)
        Me.RulerPrecision.Name = "RulerPrecision"
        Me.RulerPrecision.Size = New System.Drawing.Size(152, 45)
        Me.RulerPrecision.TabIndex = 37
        '
        'Label1
        '
        Me.Label1.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.Label1.Location = New System.Drawing.Point(600, 536)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(160, 24)
        Me.Label1.TabIndex = 36
        Me.Label1.Text = "Set Ruler's Unit of Precision"
        '
        'ListBox1
        '
        Me.ListBox1.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.ListBox1.Location = New System.Drawing.Point(768, 512)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(192, 82)
        Me.ListBox1.TabIndex = 35
        '
        'ClearList
        '
        Me.ClearList.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.ClearList.Location = New System.Drawing.Point(880, 456)
        Me.ClearList.Name = "ClearList"
        Me.ClearList.Size = New System.Drawing.Size(72, 32)
        Me.ClearList.TabIndex = 34
        Me.ClearList.Text = "Clear List"
        '
        'RulerLengths
        '
        Me.RulerLengths.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.RulerLengths.Location = New System.Drawing.Point(776, 456)
        Me.RulerLengths.Name = "RulerLengths"
        Me.RulerLengths.Size = New System.Drawing.Size(72, 32)
        Me.RulerLengths.TabIndex = 33
        Me.RulerLengths.Text = "Get Ruler Lengths"
        '
        'ShowAbbreviations
        '
        Me.ShowAbbreviations.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.ShowAbbreviations.Location = New System.Drawing.Point(600, 432)
        Me.ShowAbbreviations.Name = "ShowAbbreviations"
        Me.ShowAbbreviations.Size = New System.Drawing.Size(152, 32)
        Me.ShowAbbreviations.TabIndex = 31
        Me.ShowAbbreviations.Text = "Show Abbreviations"
        '
        'ShowGauge
        '
        Me.ShowGauge.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.ShowGauge.Checked = True
        Me.ShowGauge.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ShowGauge.Location = New System.Drawing.Point(600, 480)
        Me.ShowGauge.Name = "ShowGauge"
        Me.ShowGauge.Size = New System.Drawing.Size(152, 32)
        Me.ShowGauge.TabIndex = 32
        Me.ShowGauge.Text = "Show/Hide Ruler's Gauge"
        '
        'ShowMeasurement
        '
        Me.ShowMeasurement.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.ShowMeasurement.Checked = True
        Me.ShowMeasurement.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ShowMeasurement.Location = New System.Drawing.Point(600, 384)
        Me.ShowMeasurement.Name = "ShowMeasurement"
        Me.ShowMeasurement.Size = New System.Drawing.Size(152, 32)
        Me.ShowMeasurement.TabIndex = 30
        Me.ShowMeasurement.Text = "Show/Hide Unit of Measurement"
        '
        'lstStatus
        '
        Me.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstStatus.Location = New System.Drawing.Point(760, 136)
        Me.lstStatus.Name = "lstStatus"
        Me.lstStatus.Size = New System.Drawing.Size(192, 69)
        Me.lstStatus.TabIndex = 41
        '
        'lblLoadStatus
        '
        Me.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLoadStatus.Location = New System.Drawing.Point(760, 88)
        Me.lblLoadStatus.Name = "lblLoadStatus"
        Me.lblLoadStatus.Size = New System.Drawing.Size(160, 40)
        Me.lblLoadStatus.TabIndex = 40
        Me.lblLoadStatus.Text = "Load Status:"
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLastError.Location = New System.Drawing.Point(760, 240)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(184, 32)
        Me.lblLastError.TabIndex = 39
        Me.lblLastError.Text = "Last Error:"
        '
        'lblerror
        '
        Me.lblerror.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblerror.Location = New System.Drawing.Point(768, 288)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(192, 128)
        Me.lblerror.TabIndex = 38
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuRulers, Me.mnuToolbar, Me.mnuAbout})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.LoadImageFile, Me.SaveImageFile, Me.SaveAnnotationFile, Me.LoadAnnotationFile, Me.FileExit})
        Me.mnuFile.Text = "File"
        '
        'LoadImageFile
        '
        Me.LoadImageFile.Index = 0
        Me.LoadImageFile.Text = "Load Image File"
        '
        'SaveImageFile
        '
        Me.SaveImageFile.Index = 1
        Me.SaveImageFile.Text = "Save Image File"
        '
        'SaveAnnotationFile
        '
        Me.SaveAnnotationFile.Index = 2
        Me.SaveAnnotationFile.Text = "Save Annotation File"
        '
        'LoadAnnotationFile
        '
        Me.LoadAnnotationFile.Index = 3
        Me.LoadAnnotationFile.Text = "Load Annotation File"
        '
        'FileExit
        '
        Me.FileExit.Index = 4
        Me.FileExit.Text = "Exit"
        '
        'mnuRulers
        '
        Me.mnuRulers.Index = 1
        Me.mnuRulers.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.CreateRuler, Me.DeleteRulers})
        Me.mnuRulers.Text = "Rulers"
        '
        'CreateRuler
        '
        Me.CreateRuler.Index = 0
        Me.CreateRuler.Text = "Create Ruler"
        '
        'DeleteRulers
        '
        Me.DeleteRulers.Index = 1
        Me.DeleteRulers.Text = "Delete All Rulers"
        '
        'mnuToolbar
        '
        Me.mnuToolbar.Index = 2
        Me.mnuToolbar.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolbarShow})
        Me.mnuToolbar.Text = "Toolbar"
        '
        'mnuToolbarShow
        '
        Me.mnuToolbarShow.Index = 0
        Me.mnuToolbarShow.Text = "Hide"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 3
        Me.mnuAbout.Text = "&About"
        '
        'NotateXpress1
        '
        Me.NotateXpress1.AllowPaint = True
        Me.NotateXpress1.Debug = False
        Me.NotateXpress1.DebugLogFile = "c:\NotateXpress8.log"
        Me.NotateXpress1.ErrorLevel = PegasusImaging.WinForms.NotateXpress8.ErrorLevelInfo.Production
        Me.NotateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress8.FontScaling.Normal
        Me.NotateXpress1.ImagXpressLoad = True
        Me.NotateXpress1.ImagXpressSave = True
        Me.NotateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress8.AnnotationMode.Edit
        Me.NotateXpress1.MultiLineEdit = False
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(968, 670)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lstStatus, Me.lblLoadStatus, Me.lblLastError, Me.lblerror, Me.RulerPrecision, Me.Label1, Me.ListBox1, Me.ClearList, Me.RulerLengths, Me.ShowAbbreviations, Me.ShowGauge, Me.ShowMeasurement, Me.ImageXView1, Me.lstInfo, Me.GroupBox1})
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.Text = "VB.NET Rulers Demo"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.RulerPrecision, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim strCommonImagesDirectory As String = "..\..\..\..\..\..\..\Common\Images\"


    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"


    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub



    Private Overloads Function PegasusOpenFile(ByVal sTitle As String, ByVal sFileFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = sTitle
        dlg.Filter = sFileFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusSaveFile(ByVal sTitle As String, ByVal sFilter As String) As String
        Dim dlg As SaveFileDialog = New SaveFileDialog()
        dlg.Title = sTitle
        dlg.Filter = sFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function


#End Region

    Private Sub LoadFile()
        Try
            Dim oldImage As PegasusImaging.WinForms.ImagXpress8.ImageX = ImageXView1.Image
            imagX1 = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFileName)
            ImageXView1.Image = imagX1
            lblerror.Text = ""
            If Not oldImage Is Nothing Then
                oldImage.Dispose()
                oldImage = Nothing
            End If
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

    End Sub


    Private Sub LoadImageFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadImageFile.Click
        strImageFileName = PegasusOpenFile("Select an Image File", strDefaultImageFilter)
        LoadFile()

        NotateXpress1.Layers.Clear()

        layer = New PegasusImaging.WinForms.NotateXpress8.Layer()

        layer.Active = True
        NotateXpress1.Layers.Add(layer)

        mnuToolbarShow.Text = "Hide"
        NotateXpress1.ToolbarDefaults.ToolbarActivated = True
    End Sub

    Private Sub ImageStatusEventHandler(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)
        lstStatus.Items.Add(e.Status.ToString(cultNumber))
    End Sub

    Private Sub ProgressEventHandler(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)
        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If e.IsComplete Then
            lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))
        End If
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '**The UnlockRuntime function must be called to distribute the runtime**
            'imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);

            ' Create a new load options object so we can recieve events from the images we load
            loIXLoadOptions = New PegasusImaging.WinForms.ImagXpress8.LoadOptions()
            soIXSaveOptions = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()
            soIXSaveOptions.Tiff.Compression = PegasusImaging.WinForms.ImagXpress8.Compression.Group4
            soNXSaveOptions = New PegasusImaging.WinForms.NotateXpress8.SaveOptions()
            soNXLoadOptions = New PegasusImaging.WinForms.NotateXpress8.LoadOptions()
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try
        Try
            'this is where events are assigned. This happens before the file gets loaded.
            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEventHandler
            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEventHandler
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try
        strCurrentDir = System.IO.Directory.GetCurrentDirectory.ToString(cultNumber)
        Dim strImagePath As String = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\benefits.tif")
        strCurrentDir = strCommonImagesDirectory
        Try
            ImageXView1.Image = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImagePath)
            ImageXView1.AutoScroll = True
            NotateXpress1.ClientWindow = ImageXView1.Handle
            ' Create a new layer and its toolbar
            createNewLayer(System.IntPtr.Zero)

        Catch ex As Exception
            MessageBox.Show(("FormMain_Load Error msg:   " _
                            + (ex.Message + ("" & vbLf + ("Error type:  " + ex.GetType.ToString)))))
        End Try
    End Sub

    Private Function createNewLayer(ByVal DibHandle As System.IntPtr) As PegasusImaging.WinForms.NotateXpress8.Layer
        ' create new layer

        Dim layer As PegasusImaging.WinForms.NotateXpress8.Layer = New Layer()
        ' Add the new layer to the collection and make it the selected layer
        NotateXpress1.Layers.Add(layer)
        layer.Select()
        layer.Description = ("Layer created " + System.DateTime.Now.ToShortTimeString)
        layer.Active = True
        layer.Visible = True
        If (NotateXpress1.InteractMode = AnnotationMode.Edit) Then
            layer.Toolbar.Visible = True
            mnuToolbarShow.Text = "Hide"
        Else
            layer.Toolbar.Visible = False
            mnuToolbarShow.Text = "Show"

        End If
        ' set the button tool text and pen width for this demo
        layer.Toolbar.ButtonToolbarDefaults.PenWidth = 4
        layer.Toolbar.ButtonToolbarDefaults.Text = "Click me"
        ' set the image tool defaults for this demo
        layer.Toolbar.ImageToolbarDefaults.BackStyle = BackStyle.Transparent
        If (DibHandle.ToInt32 <> 0) Then
            layer.Toolbar.ImageToolbarDefaults.DibHandle = DibHandle
        End If
        ' set the stamp tool text and font for this demo
        layer.Toolbar.StampToolbarDefaults.Text = "NotateXpress 8"
        Dim fontCurrent As System.Drawing.Font = layer.Toolbar.StampToolbarDefaults.TextFont
        Dim fontNew As System.Drawing.Font = New System.Drawing.Font(fontCurrent.Name, 16, (System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic))
        layer.Toolbar.StampToolbarDefaults.TextFont = fontNew
        Return layer
    End Function


    Private Sub SaveImageFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveImageFile.Click
        Try
            strSaveFileName = PegasusSaveFile("Save TIFF File", "Tiff File (*.tif) | *.tif")
            imagX1.Save(strSaveFileName, soIXSaveOptions)
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        Catch ex As System.Exception
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub SaveAnnotationFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveAnnotationFile.Click
        strSaveFileName = PegasusSaveFile("Save Annotation File", "NotateXpress File (*.nxp) | *.nxp")
        Try
            NotateXpress1.Layers.Comments = "This file was saved today"
            NotateXpress1.Layers.Subject = "testing 123"
            NotateXpress1.Layers.Title = "One awesome annotation file"
            NotateXpress1.Layers.SaveToFile(strSaveFileName, soNXSaveOptions)
        Catch ex As PegasusImaging.WinForms.NotateXpress8.NotateXpressException
            PegasusError(ex, lblerror)
        Catch ex As System.Exception
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub LoadAnnotationFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LoadAnnotationFile.Click
        strNXPFileName = PegasusOpenFile("Open NotateXpress File", "NotateXpress File (*.nxp) | *.nxp")
        Try
            NotateXpress1.Layers.FromFile(strNXPFileName, soNXLoadOptions)
            mnuToolbarShow.Text = "Hide"
            NotateXpress1.ToolbarDefaults.ToolbarActivated = True
        Catch ex As PegasusImaging.WinForms.NotateXpress8.NotateXpressException
            PegasusError(ex, lblerror)
        Catch ex As System.Exception
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub FileExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FileExit.Click
        System.Windows.Forms.Application.Exit()
    End Sub

    Private Sub CreateRuler_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CreateRuler.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            Dim rulerTool As RulerTool = New RulerTool()
            rulerTool.PenWidth = 4
            Dim rect As Rectangle = New Rectangle(25, 50, 100, 100)
            rulerTool.BoundingRectangle = rect
            'set the size of the gauge
            rulerTool.GaugeLength = 20
            'set the unit of measurement being used for the ruler
            rulerTool.MeasurementUnit = MeasurementUnit.Inches
            Inches.Checked = True
            'set the precision being used for the ruler
            rulerTool.Precision = 1
            'set the color for the units of measurement
            rulerTool.TextColor = System.Drawing.Color.Blue
            'append the ruler to image
            layer.Elements.Add(rulerTool)
            'set the annotation to selected
            rulerTool.Selected = True
        End If
    End Sub

    Private Sub DeleteRulers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DeleteRulers.Click
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            NotateXpress1.Layers.Remove(layer)
            createNewLayer(System.IntPtr.Zero)
        End If
    End Sub

    Private Sub mnuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolbarShow.Click
        If (NotateXpress1.ToolbarDefaults.ToolbarActivated = True) Then
            mnuToolbarShow.Text = "Show"
            NotateXpress1.ToolbarDefaults.ToolbarActivated = False
        Else
            mnuToolbarShow.Text = "Hide"
            NotateXpress1.ToolbarDefaults.ToolbarActivated = True
        End If
    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        NotateXpress1.AboutBox()
    End Sub

    Private Sub Centimeters_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Centimeters.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool AndAlso Centimeters.Checked) Then
                    CType(layer.Elements(iCounter), RulerTool).MeasurementUnit = MeasurementUnit.Centimeters
                End If
                iCounter = (iCounter + 1)
            Loop
        End If

    End Sub

    Private Sub Feet_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Feet.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool AndAlso Feet.Checked) Then
                    CType(layer.Elements(iCounter), RulerTool).MeasurementUnit = MeasurementUnit.Feet
                End If
                iCounter = (iCounter + 1)
            Loop
        End If
    End Sub

    Private Sub Inches_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Inches.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool AndAlso Inches.Checked) Then
                    CType(layer.Elements(iCounter), RulerTool).MeasurementUnit = MeasurementUnit.Inches
                End If
                iCounter = (iCounter + 1)
            Loop
        End If

    End Sub

    Private Sub MicroMeters_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MicroMeters.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool AndAlso MicroMeters.Checked) Then
                    CType(layer.Elements(iCounter), RulerTool).MeasurementUnit = MeasurementUnit.MicroMeters
                End If
                iCounter = (iCounter + 1)
            Loop
        End If

    End Sub

    Private Sub Kilometers_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Kilometers.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool AndAlso Kilometers.Checked) Then
                    CType(layer.Elements(iCounter), RulerTool).MeasurementUnit = MeasurementUnit.Kilometers
                End If
                iCounter = (iCounter + 1)
            Loop
        End If
    End Sub

    Private Sub Meters_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Meters.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool AndAlso Meters.Checked) Then
                    CType(layer.Elements(iCounter), RulerTool).MeasurementUnit = MeasurementUnit.Meters
                End If
                iCounter = (iCounter + 1)
            Loop
        End If

    End Sub

    Private Sub Millimeters_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Millimeters.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool AndAlso Millimeters.Checked) Then
                    CType(layer.Elements(iCounter), RulerTool).MeasurementUnit = MeasurementUnit.Millimeters
                End If
                iCounter = (iCounter + 1)
            Loop
        End If
    End Sub

    Private Sub Pixels_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Pixels.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool AndAlso Pixels.Checked) Then
                    CType(layer.Elements(iCounter), RulerTool).MeasurementUnit = MeasurementUnit.Pixels
                End If
                iCounter = (iCounter + 1)
            Loop
        End If
    End Sub

    Private Sub Twips_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Twips.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool AndAlso Twips.Checked) Then
                    CType(layer.Elements(iCounter), RulerTool).MeasurementUnit = MeasurementUnit.Twips
                End If
                iCounter = (iCounter + 1)
            Loop
        End If
    End Sub

    Private Sub Miles_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Miles.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool AndAlso Miles.Checked) Then
                    CType(layer.Elements(iCounter), RulerTool).MeasurementUnit = MeasurementUnit.Miles
                End If
                iCounter = (iCounter + 1)
            Loop
        End If
    End Sub

    Private Sub ShowMeasurement_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowMeasurement.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool) Then
                    CType(layer.Elements(iCounter), RulerTool).ShowLength = ShowMeasurement.Checked
                End If
                iCounter = (iCounter + 1)
            Loop
        End If

    End Sub

    Private Sub ShowAbbreviations_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowAbbreviations.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool) Then
                    CType(layer.Elements(iCounter), RulerTool).ShowAbbreviation = ShowAbbreviations.Checked
                End If
                iCounter = (iCounter + 1)
            Loop
        End If
    End Sub


    Private Sub ShowGauge_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ShowGauge.CheckedChanged
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool) Then
                    CType(layer.Elements(iCounter), RulerTool).ShowGauge = ShowGauge.Checked
                End If
                iCounter = (iCounter + 1)
            Loop
        End If
    End Sub

    Private Sub RulerPrecision_Scroll(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RulerPrecision.Scroll
        Dim iCounter As Integer
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool) Then
                    CType(layer.Elements(iCounter), RulerTool).Precision = RulerPrecision.Value
                End If
                iCounter = (iCounter + 1)
            Loop
        End If
    End Sub

    Private Sub RulerLengths_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RulerLengths.Click
        Dim iCounter As Integer
        Dim iNumRulers As Integer
        iNumRulers = 0
        ListBox1.Items.Clear()
        Dim layer As Layer = NotateXpress1.Layers.Selected
        If (Not (layer) Is Nothing) Then
            iCounter = 0
            Do While (iCounter < layer.Elements.Count)
                If (TypeOf layer.Elements(iCounter) Is RulerTool) Then
                    iNumRulers = (iNumRulers + 1)
                    ListBox1.Items.Add(("Ruler#" _
                                    + (iNumRulers.ToString() + ("     " + CType(layer.Elements(iCounter), RulerTool).RulerLength.ToString()))))
                End If
                iCounter = (iCounter + 1)
            Loop
        End If

    End Sub

    Private Sub ClearList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ClearList.Click
        ListBox1.Items.Clear()
    End Sub

End Class

'****************************************************************'
'* Copyright 2005 - Pegasus Imaging Corporation, Tampa Florida. *'
'* This sample code is provided to Pegasus licensees "as is"    *'
'* with no restrictions on use or modification. No warranty for *'
'* use of this sample code is provided by Pegasus.              *'
'****************************************************************'

Imports PegasusImaging.WinForms.ImagXpress8
Imports PegasusImaging.WinForms.NotateXpress8
Imports System.Windows.Forms
Imports System
Imports System.Drawing


Public Class simpleannotations
    Inherits System.Windows.Forms.Form

    'global variables declared here
    Private strImageFileName As String
    Private strNXPFileName As String
    Private strCurrentDir As String
    Private strSaveFileName As String
    Private strCommonImagesDirectory As String

    Private imagX1 As PegasusImaging.WinForms.ImagXpress8.ImageX
    Private layer As PegasusImaging.WinForms.NotateXpress8.Layer
    Private loIXLoadOptions As PegasusImaging.WinForms.ImagXpress8.LoadOptions
    Private soIXSaveOptions As PegasusImaging.WinForms.ImagXpress8.SaveOptions
    Private soNXSaveOptions As PegasusImaging.WinForms.NotateXpress8.SaveOptions
    Private soNXLoadOptions As PegasusImaging.WinForms.NotateXpress8.LoadOptions

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        '****Must call the UnlockControl function *****
        'PegasusImaging.WinForms.ImagXpress8.Licensing.UnlockControl(1234,1234,1234,1234);

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then

            ' Don't forget to dispose IX
            '
            If (Not (ImageXView1) Is Nothing) Then

                ImageXView1.Dispose()
                ImageXView1 = Nothing
            End If
            If (Not (ImagXpress1) Is Nothing) Then

                ImagXpress1.Dispose()
                ImagXpress1 = Nothing
            End If
            If (Not (imagX1) Is Nothing) Then

                imagX1.Dispose()
                imagX1 = Nothing
            End If

            ' Don't forget to dispose NXP
            '                
            If (Not (NotateXpress1) Is Nothing) Then
                NotateXpress1.Dispose()
                NotateXpress1 = Nothing
            End If

            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents ImageXView1 As PegasusImaging.WinForms.ImagXpress8.ImageXView
    Friend WithEvents ImagXpress1 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Friend WithEvents NotateXpress1 As PegasusImaging.WinForms.NotateXpress8.NotateXpress
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents mnuFile As System.Windows.Forms.MenuItem
    Friend WithEvents mnuOpen As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSave As System.Windows.Forms.MenuItem
    Friend WithEvents mnuWang As System.Windows.Forms.MenuItem
    Friend WithEvents mnuSaveA As System.Windows.Forms.MenuItem
    Friend WithEvents mnuLoadAnno As System.Windows.Forms.MenuItem
    Friend WithEvents mnuExit As System.Windows.Forms.MenuItem
    Friend WithEvents lblerror As System.Windows.Forms.Label
    Friend WithEvents mnuCreate As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents mnuAbout As System.Windows.Forms.MenuItem
    Friend WithEvents mnuTxt As System.Windows.Forms.MenuItem
    Friend WithEvents mnuRedac As System.Windows.Forms.MenuItem
    Friend WithEvents mnuRuler As System.Windows.Forms.MenuItem
    Friend WithEvents mnuRects As System.Windows.Forms.MenuItem
    Friend WithEvents lstInfo As System.Windows.Forms.ListBox
    Friend WithEvents lblLastError As System.Windows.Forms.Label
    Friend WithEvents lblLoadStatus As System.Windows.Forms.Label
    Friend WithEvents lstStatus As System.Windows.Forms.ListBox
    Friend WithEvents mnuToolbarShow As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.ImageXView1 = New PegasusImaging.WinForms.ImagXpress8.ImageXView()
        Me.ImagXpress1 = New PegasusImaging.WinForms.ImagXpress8.ImagXpress()
        Me.NotateXpress1 = New PegasusImaging.WinForms.NotateXpress8.NotateXpress()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu()
        Me.mnuFile = New System.Windows.Forms.MenuItem()
        Me.mnuOpen = New System.Windows.Forms.MenuItem()
        Me.mnuSave = New System.Windows.Forms.MenuItem()
        Me.mnuWang = New System.Windows.Forms.MenuItem()
        Me.mnuSaveA = New System.Windows.Forms.MenuItem()
        Me.mnuLoadAnno = New System.Windows.Forms.MenuItem()
        Me.mnuExit = New System.Windows.Forms.MenuItem()
        Me.mnuCreate = New System.Windows.Forms.MenuItem()
        Me.mnuTxt = New System.Windows.Forms.MenuItem()
        Me.mnuRedac = New System.Windows.Forms.MenuItem()
        Me.mnuRuler = New System.Windows.Forms.MenuItem()
        Me.mnuRects = New System.Windows.Forms.MenuItem()
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.mnuToolbarShow = New System.Windows.Forms.MenuItem()
        Me.mnuAbout = New System.Windows.Forms.MenuItem()
        Me.lblerror = New System.Windows.Forms.Label()
        Me.lstInfo = New System.Windows.Forms.ListBox()
        Me.lblLastError = New System.Windows.Forms.Label()
        Me.lblLoadStatus = New System.Windows.Forms.Label()
        Me.lstStatus = New System.Windows.Forms.ListBox()
        Me.SuspendLayout()
        '
        'ImageXView1
        '
        Me.ImageXView1.Anchor = (((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.ImageXView1.AutoScroll = True
        Me.ImageXView1.Location = New System.Drawing.Point(24, 112)
        Me.ImageXView1.Name = "ImageXView1"
        Me.ImageXView1.Size = New System.Drawing.Size(552, 334)
        Me.ImageXView1.TabIndex = 0
        '
        'NotateXpress1
        '
        Me.NotateXpress1.AllowPaint = True
        Me.NotateXpress1.Debug = False
        Me.NotateXpress1.DebugLogFile = "c:\NotateXpress8.log"
        Me.NotateXpress1.ErrorLevel = PegasusImaging.WinForms.NotateXpress8.ErrorLevelInfo.Production
        Me.NotateXpress1.FontScaling = PegasusImaging.WinForms.NotateXpress8.FontScaling.Normal
        Me.NotateXpress1.ImagXpressLoad = True
        Me.NotateXpress1.ImagXpressSave = True
        Me.NotateXpress1.InteractMode = PegasusImaging.WinForms.NotateXpress8.AnnotationMode.Edit
        Me.NotateXpress1.MultiLineEdit = False
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuFile, Me.mnuCreate, Me.MenuItem1, Me.mnuAbout})
        '
        'mnuFile
        '
        Me.mnuFile.Index = 0
        Me.mnuFile.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuOpen, Me.mnuSave, Me.mnuWang, Me.mnuSaveA, Me.mnuLoadAnno, Me.mnuExit})
        Me.mnuFile.Text = "&File"
        '
        'mnuOpen
        '
        Me.mnuOpen.Index = 0
        Me.mnuOpen.Text = "&Open Image"
        '
        'mnuSave
        '
        Me.mnuSave.Index = 1
        Me.mnuSave.Text = "&Save Image"
        '
        'mnuWang
        '
        Me.mnuWang.Checked = True
        Me.mnuWang.Index = 2
        Me.mnuWang.Text = "&Wang Compatible"
        '
        'mnuSaveA
        '
        Me.mnuSaveA.Index = 3
        Me.mnuSaveA.Text = "&Save Annotation"
        '
        'mnuLoadAnno
        '
        Me.mnuLoadAnno.Index = 4
        Me.mnuLoadAnno.Text = "&Load Annotation"
        '
        'mnuExit
        '
        Me.mnuExit.Index = 5
        Me.mnuExit.Text = "&Exit"
        '
        'mnuCreate
        '
        Me.mnuCreate.Index = 1
        Me.mnuCreate.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuTxt, Me.mnuRedac, Me.mnuRuler, Me.mnuRects})
        Me.mnuCreate.Text = "&Create Annotations"
        '
        'mnuTxt
        '
        Me.mnuTxt.Index = 0
        Me.mnuTxt.Text = "Add Text"
        '
        'mnuRedac
        '
        Me.mnuRedac.Index = 1
        Me.mnuRedac.Text = "Add Redaction"
        '
        'mnuRuler
        '
        Me.mnuRuler.Index = 2
        Me.mnuRuler.Text = "Add Ruler"
        '
        'mnuRects
        '
        Me.mnuRects.Index = 3
        Me.mnuRects.Text = "Add Rectangles"
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 2
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.mnuToolbarShow})
        Me.MenuItem1.Text = "&Toolbar"
        '
        'mnuToolbarShow
        '
        Me.mnuToolbarShow.Index = 0
        Me.mnuToolbarShow.Text = "&Show"
        '
        'mnuAbout
        '
        Me.mnuAbout.Index = 3
        Me.mnuAbout.Text = "&About"
        '
        'lblerror
        '
        Me.lblerror.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblerror.BackColor = System.Drawing.SystemColors.Control
        Me.lblerror.Location = New System.Drawing.Point(584, 339)
        Me.lblerror.Name = "lblerror"
        Me.lblerror.Size = New System.Drawing.Size(192, 109)
        Me.lblerror.TabIndex = 1
        '
        'lstInfo
        '
        Me.lstInfo.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstInfo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstInfo.Items.AddRange(New Object() {"This sample demonstrates the following functionality:", "1)Creating various annotations programatically."})
        Me.lstInfo.Location = New System.Drawing.Point(24, 24)
        Me.lstInfo.Name = "lstInfo"
        Me.lstInfo.Size = New System.Drawing.Size(768, 69)
        Me.lstInfo.TabIndex = 2
        '
        'lblLastError
        '
        Me.lblLastError.Anchor = (System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLastError.Location = New System.Drawing.Point(584, 296)
        Me.lblLastError.Name = "lblLastError"
        Me.lblLastError.Size = New System.Drawing.Size(184, 32)
        Me.lblLastError.TabIndex = 3
        Me.lblLastError.Text = "Last Error:"
        '
        'lblLoadStatus
        '
        Me.lblLoadStatus.Anchor = (System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right)
        Me.lblLoadStatus.Location = New System.Drawing.Point(584, 120)
        Me.lblLoadStatus.Name = "lblLoadStatus"
        Me.lblLoadStatus.Size = New System.Drawing.Size(160, 40)
        Me.lblLoadStatus.TabIndex = 4
        Me.lblLoadStatus.Text = "Load Status:"
        '
        'lstStatus
        '
        Me.lstStatus.Anchor = ((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right)
        Me.lstStatus.Location = New System.Drawing.Point(584, 168)
        Me.lstStatus.Name = "lstStatus"
        Me.lstStatus.Size = New System.Drawing.Size(208, 108)
        Me.lstStatus.TabIndex = 5
        '
        'simpleannotations
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(808, 479)
        Me.Controls.AddRange(New System.Windows.Forms.Control() {Me.lstStatus, Me.lblLoadStatus, Me.lblLastError, Me.lstInfo, Me.lblerror, Me.ImageXView1})
        Me.Menu = Me.MainMenu1
        Me.Name = "simpleannotations"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NotateXpress 8 Simple Annotations VB.NET"
        Me.ResumeLayout(False)

    End Sub

#End Region

#Region "Pegasus Imaging Sample Application Standard Functions"
    '/*********************************************************************
    '*     Pegasus Imaging Corporation Standard Function Definitions     *
    ' *********************************************************************/
    Dim cultNumber As System.Globalization.NumberFormatInfo = System.Globalization.CultureInfo.CurrentCulture.NumberFormat
    Dim cultText As System.Globalization.TextInfo = System.Globalization.CultureInfo.CurrentCulture.TextInfo
    Dim cultCompare As System.Globalization.CompareInfo = System.Globalization.CultureInfo.CurrentCulture.CompareInfo
    Dim imagXpress2 As PegasusImaging.WinForms.ImagXpress8.ImagXpress
    Dim printDocument1 As System.Drawing.Printing.PrintDocument
    Dim strDefaultImageFilter As String = "All ImagXpress Supported File Types|*.bmp;*.cal;*.dib;*.dca;*.mod;*.dcx;*.gif;*.jp2;*.jls;*.jpg;*.jif" & _
    ";*.ljp;*.pbm;*.pcx;*.pgm;*.pic;*.png;*.ppm;*.tiff;*.tif;*.tga;*.wsq;*.jb2;*.gif;*.jpeg;*.cals;*.jbig" & _
    "2;*.ico;*.rle;*.lzw;*.wbmp|Windows Bitmap (*.BMP)|*.bmp|CALS (*.CAL)|*.cal|Windows Device Independen" & _
    "t Bitmap(*.DIB)|*.dib|MO:DCA (*.DCA & *.MOD)|*.dca;*.mod|Zsoft Multiple Page (*.DCX)|*.dcx|CompuServ" & _
    "e GIF (*.GIF)|*.gif|JPEG 2000 (*.JP2)|*.jp2|JPEG LS (*.JLS)|*.jls|JFIF Compliant JPEG (*.JPG & *.JIF" & _
    ")|*.jpg;*.jif|Lossless JPEG (*.LJP)|*.ljp|Portable Bitmap (*.PBM)|*.pbm|Zsoft PaintBrush (*.PCX)|*.p" & _
    "cx|Portable Graymap (*.PGM)|*.pgm|Pegasus PIC or Enhanced PIC (*.PIC)|*.pic|Portable Network Graphic" & _
    "s (*.PNG)|*.png|Portable Pixmap (*.PPM)|*.ppm|Tagged Image Format (*.TIFF)|*.tif;*.tiff|Truevision T" & _
    "ARGA (*.TGA)|*.tga|WSQ Fingerprint File (*.WSQ)|*.wsq|JBIG2 File (*.JB2)|*.jb2|All Files (*.*)|*.*"

    Private Function GetFileName(ByVal FullName As String) As String

        Return FullName.Substring((FullName.LastIndexOf("\") + 1), (FullName.Length - (FullName.LastIndexOf("\") + 1)))
    End Function

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As System.Exception, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + "" & vbLf)))
    End Sub

    Private Overloads Shared Sub PegasusError(ByVal ErrorException As PegasusImaging.WinForms.ImagXpress8.ImagXpressException, ByVal ErrorLabel As System.Windows.Forms.Label)
        ErrorLabel.Text = (ErrorException.Message + ("" & vbLf _
                    + (ErrorException.Source + ("" & vbLf + ("Error Number: " + ErrorException.Number.ToString(System.Globalization.CultureInfo.CurrentCulture.NumberFormat))))))
    End Sub

    Private Overloads Function PegasusOpenFile(ByVal sTitle As String, ByVal sFileFilter As String) As String
        Dim dlg As OpenFileDialog = New OpenFileDialog()
        dlg.Title = sTitle
        dlg.Filter = sFileFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

    Private Overloads Function PegasusSaveFile(ByVal sTitle As String, ByVal sFilter As String) As String
        Dim dlg As SaveFileDialog = New SaveFileDialog()
        dlg.Title = sTitle
        dlg.Filter = sFilter
        dlg.InitialDirectory = strCurrentDir
        If (dlg.ShowDialog = DialogResult.OK) Then
            strCurrentDir = dlg.FileName.Remove(dlg.FileName.LastIndexOf("\"), (dlg.FileName.Length - dlg.FileName.LastIndexOf("\")))
            Return dlg.FileName
        Else
            Return ""
        End If
    End Function

#End Region

    Private Sub mnuExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        System.Windows.Forms.Application.Exit()
    End Sub

    Private Sub mnuOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuOpen.Click
        strImageFileName = PegasusOpenFile("Select an Image File", strDefaultImageFilter)
        LoadFile()

        NotateXpress1.Layers.Clear()

        layer = New PegasusImaging.WinForms.NotateXpress8.Layer()
        layer.Active = True
        NotateXpress1.Layers.Add(layer)

        mnuToolbarShow.Text = "Hide"
        NotateXpress1.ToolbarDefaults.ToolbarActivated = True
    End Sub

    Private Sub LoadFile()
        Try
            Dim oldImage As PegasusImaging.WinForms.ImagXpress8.ImageX = ImageXView1.Image
            imagX1 = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFileName)
            ImageXView1.Image = imagX1
            lblerror.Text = ""
            If Not oldImage Is Nothing Then
                oldImage.Dispose()
            End If
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub mnuAbout_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAbout.Click
        NotateXpress1.AboutBox()
    End Sub

    Private Sub ImageStatusEventHandler(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ImageStatusEventArgs)
        lstStatus.Items.Add(e.Status.ToString(cultNumber))
    End Sub

    Private Sub ProgressEventHandler(ByVal sender As Object, ByVal e As PegasusImaging.WinForms.ImagXpress8.ProgressEventArgs)
        lstStatus.Items.Add((e.PercentDone.ToString(cultNumber) + "% Loading Complete."))
        If e.IsComplete Then
            lstStatus.Items.Add((e.TotalBytes.ToString() + " Bytes Completed Loading."))
        End If
        lstStatus.SelectedIndex = (lstStatus.Items.Count - 1)
    End Sub

    Private Sub simpleannotations_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            '**The UnlockRuntime function must be called to distribute the runtime**
            'imagXpress1.License.UnlockRuntime(12345,12345,12345,12345);

            'Create a new load options object so we can recieve events from the images we load
            loIXLoadOptions = New PegasusImaging.WinForms.ImagXpress8.LoadOptions()

            soIXSaveOptions = New PegasusImaging.WinForms.ImagXpress8.SaveOptions()
            soIXSaveOptions.Tiff.Compression = PegasusImaging.WinForms.ImagXpress8.Compression.Group4

            soNXSaveOptions = New PegasusImaging.WinForms.NotateXpress8.SaveOptions()
            soNXLoadOptions = New PegasusImaging.WinForms.NotateXpress8.LoadOptions()
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try
        Try
            'this is where events are assigned. This happens before the file gets loaded.
            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ProgressEvent, AddressOf Me.ProgressEventHandler
            AddHandler PegasusImaging.WinForms.ImagXpress8.ImageX.ImageStatusEvent, AddressOf Me.ImageStatusEventHandler
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        End Try

        strCurrentDir = System.IO.Directory.GetCurrentDirectory()
        strImageFileName = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\benefits.tif")
        strCommonImagesDirectory = System.IO.Path.Combine(strCurrentDir, "..\..\..\..\..\..\..\Common\Images\")
        strCurrentDir = strCommonImagesDirectory

        imagX1 = PegasusImaging.WinForms.ImagXpress8.ImageX.FromFile(strImageFileName)
        ImageXView1.Image = imagX1
        NotateXpress1.ClientWindow = ImageXView1.Handle
        layer = New PegasusImaging.WinForms.NotateXpress8.Layer()
        layer.Active = True
        NotateXpress1.Layers.Add(layer)
        'disable the toolbar 
        NotateXpress1.ToolbarDefaults.ToolbarActivated = False
        NotateXpress1.MenuSetEnabled(PegasusImaging.WinForms.NotateXpress8.MenuType.Context, PegasusImaging.WinForms.NotateXpress8.AnnotationTool.NoTool, True)
    End Sub

    Private Sub mnuToolbarShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuToolbarShow.Click
        If NotateXpress1.ToolbarDefaults.ToolbarActivated = True Then
            mnuToolbarShow.Text = "Show"
            NotateXpress1.ToolbarDefaults.ToolbarActivated = False
        Else
            mnuToolbarShow.Text = "Hide"
            NotateXpress1.ToolbarDefaults.ToolbarActivated = True
        End If
    End Sub

    Private Sub mnuTxt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuTxt.Click
        '**** Text Annotation
        Dim rect As System.Drawing.Rectangle
        rect.X = 30
        rect.Y = 30
        rect.Width = 280
        rect.Height = 300
        Dim text As New PegasusImaging.WinForms.NotateXpress8.TextTool()

        text.TextJustification = PegasusImaging.WinForms.NotateXpress8.TextJustification.Center
        text.Text = "Rob Young"
        text.BackColor = System.Drawing.Color.Red
        text.BoundingRectangle = rect
        text.PenWidth = 2
        text.BackStyle = PegasusImaging.WinForms.NotateXpress8.BackStyle.Translucent
        text.PenWidth = 2
        'Add the annotation to the image
        layer.Elements.Add(text)
    End Sub

    Private Sub mnuRects_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRects.Click
        'Rectangle Annotation
        Dim counter As Integer
        Dim rect As System.Drawing.Rectangle
        rect.X = 30
        rect.Y = 30
        rect.Width = 60
        rect.Height = 60
        Dim rand As New System.Random()
        For counter = 1 To 10
            Dim rectangle As New PegasusImaging.WinForms.NotateXpress8.RectangleTool()
            rectangle.BoundingRectangle = rect
            rectangle.PenColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
            rectangle.FillColor = System.Drawing.Color.FromArgb(Int(rand.NextDouble * 255), Int(rand.NextDouble * 255), Int(rand.NextDouble * 255))
            layer.Elements.Add(rectangle)
            rect.X = rect.X + 5
            rect.Y = rect.Y + 5
        Next
    End Sub

    Private Sub mnuRedac_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRedac.Click
        Dim rect As New System.Drawing.Rectangle(100, 100, 150, 150)
        Dim text As New PegasusImaging.WinForms.NotateXpress8.TextTool()
        text.Text = "Approved by Pegasus Imaging Corp." & Space(2) & Now
        text.TextJustification = PegasusImaging.WinForms.NotateXpress8.TextJustification.Center
        text.TextColor = System.Drawing.Color.Red
        text.PenWidth = 1
        text.PenColor = System.Drawing.Color.White
        text.BackStyle = PegasusImaging.WinForms.NotateXpress8.BackStyle.Opaque
        text.BackColor = System.Drawing.Color.White
        text.BoundingRectangle = rect
        layer.Elements.Add(text)
    End Sub

    Private Sub mnuRuler_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRuler.Click
        Dim rect As New System.Drawing.Rectangle(25, 50, 200, 100)
        Dim ruler As New PegasusImaging.WinForms.NotateXpress8.RulerTool()
        ruler.BoundingRectangle = rect
        ruler.GaugeLength = 20
        ruler.MeasurementUnit = PegasusImaging.WinForms.NotateXpress8.MeasurementUnit.Pixels
        ruler.Precision = 1
        ruler.BackColor = System.Drawing.Color.Blue
        layer.Elements.Add(ruler)
    End Sub

    Private Sub mnuSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSave.Click
        Try
            strSaveFileName = PegasusSaveFile("Save TIFF File", "Tiff File (*.tif) | *.tif")
            imagX1.Save(strSaveFileName, soIXSaveOptions)
        Catch ex As PegasusImaging.WinForms.ImagXpress8.ImagXpressException
            PegasusError(ex, lblerror)
        Catch ex As System.Exception
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub mnuSaveA_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSaveA.Click
        strSaveFileName = PegasusSaveFile("Save Annotation File", "NotateXpress File (*.nxp) | *.nxp")
        Try
            NotateXpress1.Layers.Comments = "This file was saved today"
            NotateXpress1.Layers.Subject = "testing 123"
            NotateXpress1.Layers.Title = "One awesome annotation file"
            NotateXpress1.Layers.SaveToFile(strSaveFileName, soNXSaveOptions)
        Catch ex As PegasusImaging.WinForms.NotateXpress8.NotateXpressException
            PegasusError(ex, lblerror)
        Catch ex As System.Exception
            PegasusError(ex, lblerror)
        End Try
    End Sub

    Private Sub mnuWang_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuWang.Click
        If soNXSaveOptions.PreserveWangLayers Then
            soNXSaveOptions.PreserveWangLayers = False
            mnuWang.Checked = False
        Else
            soNXSaveOptions.PreserveWangLayers = True
            mnuWang.Checked = True
        End If
    End Sub

    Private Sub mnuLoadAnno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuLoadAnno.Click
        strNXPFileName = PegasusOpenFile("Open NotateXpress File", "NotateXpress File (*.nxp) | *.nxp")
        Try
            NotateXpress1.Layers.FromFile(strNXPFileName, soNXLoadOptions)
            mnuToolbarShow.Text = "Hide"
            NotateXpress1.ToolbarDefaults.ToolbarActivated = True
        Catch ex As PegasusImaging.WinForms.NotateXpress8.NotateXpressException
            PegasusError(ex, lblerror)
        Catch ex As System.Exception
            PegasusError(ex, lblerror)
        End Try
    End Sub
End Class
